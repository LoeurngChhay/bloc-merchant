package com.bongtk.blocmerchant

import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.bluetooth.BluetoothAdapter


class BluetoothManager(private val context: Context) {

    fun openBluetoothSettings() {
        val intent = Intent(Settings.ACTION_BLUETOOTH_SETTINGS)
        context.startActivity(intent)
    }

    fun isBluetoothEnabled(): Boolean {
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled
    }
}
