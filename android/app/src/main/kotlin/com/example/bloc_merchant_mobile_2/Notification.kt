package com.bongtk.blocmerchant

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import android.media.AudioAttributes
import android.net.Uri

object Notifications {
    const val NOTIFICATION_ID_BACKGROUND_SERVICE = 1

    private const val CHANNEL_ID_HIGH_IMPORTANCE_1 = "BP-Notification"
    private const val CHANNEL_ID_HIGH_IMPORTANCE_2 = "BC-Notification"

    fun createNotificationChannels(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // High Importance Channel 1
            val soundUri1: Uri = Uri.parse(
                "android.resource://" +
                        context.packageName +
                        "/raw/neworder"
            )

            val audioAttributes1 = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .build()

            val channelOrder = NotificationChannel(
                CHANNEL_ID_HIGH_IMPORTANCE_1,
                "BLOC Order Notification",
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                setSound(soundUri1, audioAttributes1)
            }

            // High Importance Channel 2
            val soundUri2: Uri = Uri.parse(
                "android.resource://" +
                        context.packageName +
                        "/raw/tickal"
            )

            val audioAttributes2 = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .build()

            val channelCancel = NotificationChannel(
                CHANNEL_ID_HIGH_IMPORTANCE_2,
                "BLOC Cancel Notification",
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                setSound(soundUri2, audioAttributes2)
            }

            // Register the channels with the system
            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channelOrder)
            manager.createNotificationChannel(channelCancel)
        }
    }

    fun buildNotification(context: Context, channelId: String, title: String, text: String): Notification {
        return NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(text)
            .build()
    }
}
