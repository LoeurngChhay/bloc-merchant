package com.bongtk.blocmerchant

import android.os.Bundle
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity(){
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Notifications.createNotificationChannels(this)
    }

    private val CHANNEL = "bluetooth"
    private lateinit var bluetoothManager: BluetoothManager

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        bluetoothManager = BluetoothManager(this)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)
            .setMethodCallHandler { call, result ->
                when (call.method) {
                    "openBluetoothSettings" -> {
                        bluetoothManager.openBluetoothSettings()
                        result.success(null)
                    }
                    "isBluetoothEnabled" -> {
                        val isEnabled = bluetoothManager.isBluetoothEnabled()
                        result.success(isEnabled)
                    }
                    else -> result.notImplemented()
                }
            }
    }
}

