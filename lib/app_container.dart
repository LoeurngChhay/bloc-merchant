import 'package:bloc_merchant_mobile_2/main.dart';
import 'package:bloc_merchant_mobile_2/providers/theme_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads/ads_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/ads_detail_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/category/category_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/image/create_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/create_video_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/views/video_post_preview.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/home_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/language/language_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/login/login_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/order_detail_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/order_history_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer/printer_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_bluetooth_view/printer_bluetooth_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_network_view.dart/printer_network_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/setting/setting_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/splash/splash_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/light_theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';

class AppContainer extends StatefulWidget {
  const AppContainer({super.key});

  @override
  State<AppContainer> createState() => _AppContainerState();
}

class _AppContainerState extends State<AppContainer> {
  // final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  // final RouteObserverProvider routeObserver = RouteObserverProvider();

  @override
  void initState() {
    FirebaseMessaging.instance.getInitialMessage().then(
          (value) => setState(
            () {
              // _resolved = t
              // rue;
              // initialMessage = value?.data.toString();

              // print('getInitialMessage : $value');
            },
          ),
        );

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      // _handleNotification();
    });

    super.initState();
  }

  void _handleNotification() {
    String targetRoute = OrderView.routeName;
    final currentRoute = routeObserver.currentRoute;

    print("currentRoute  : $currentRoute");

    if (currentRoute != targetRoute) {
      navigatorKey.currentState?.pushNamed(targetRoute);
    }
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      navigatorObservers: [routeObserver],
      builder: (context, child) =>
          ResponsiveBreakpoints.builder(breakpoints: const [
        Breakpoint(start: 0, end: 480, name: MOBILE),
        Breakpoint(start: 481, end: double.infinity, name: TABLET)
      ], child: child!),
      initialRoute: SplashView.routeName,
      debugShowCheckedModeBanner: false,
      theme: lightThemeData,
      darkTheme: lightThemeData,
      themeMode: context.watch<ThemeProvider>().themeMode,
      onGenerateRoute: (settings) {
        Route screen;

        final routes = {
          SplashView.routeName: const SplashView(),
          LoginView.routeName: const LoginView(),
          HomeView.routeName: const HomeView(),
          CategoryView.routeName: const CategoryView(),
          AdsView.routeName: const AdsView(),
          OrderView.routeName: const OrderView(),
          // PrinterView.routeName: const PrinterView(),
          OrderHistoryView.routeName: const OrderHistoryView(),
          // CreatePostView.routeName: const CreatePostView(),
          ProductTestView.routeName: const ProductTestView(),
          AdsDetailView.routeName: const AdsDetailView(),
          VideoPostPreview.routeName: const VideoPostPreview(),
          LanguageView.routeName: const LanguageView(),
          PrinterView.routeName: const PrinterView(),
          PrinterBluetoothView.routeName: const PrinterBluetoothView(),
          PrinterNetworkView.routeName: const PrinterNetworkView(),
        };

        switch (settings.name) {
          case HomeView.routeName:
            screen = MaterialPageRoute(
                settings: settings, builder: (context) => const HomeView());
            break;
          case ProductView.routeName:
            final args = settings.arguments != null
                ? settings.arguments as ProductViewArgument
                : null;
            screen = MaterialPageRoute(
                settings: settings,
                builder: (context) => ProductView(args: args));
            break;
          case OrderView.routeName:
            screen = MaterialPageRoute(
                builder: (context) => const OrderView(), settings: settings);
            break;
          case CategoryView.routeName:
            screen = MaterialPageRoute(
                settings: settings, builder: (context) => const CategoryView());
            break;
          case OrderDetailView.routeName:
            final args = settings.arguments as OrderArguments;

            screen = MaterialPageRoute(
                settings: settings,
                builder: (context) => OrderDetailView(args: args));
            break;
          case CreateProductView.routeName:
            final args = settings.arguments as CreateProductViewArguments?;
            screen = MaterialPageRoute(
                builder: (context) => CreateProductView(args: args));
            break;
          case SettingView.routeName:
            screen =
                MaterialPageRoute(builder: (context) => const SettingView());
            break;
          case AdsView.routeName:
            screen = MaterialPageRoute(
                settings: settings, builder: (context) => const AdsView());
            break;
          case CreatePostView.routeName:
            final args = settings.arguments as CreatePostArgument;
            screen = MaterialPageRoute(
                builder: (context) => CreatePostView(args: args));
            break;
          case CreateVideoPostView.routeName:
            final args = settings.arguments as CreatePostArgument;

            screen = MaterialPageRoute(
                builder: (context) => CreateVideoPostView(args: args));
            break;
          case VideoPostPreview.routeName:
            final args = settings.arguments as CreatePostArgument;

            screen = MaterialPageRoute(
                settings: settings,
                builder: (context) => VideoPostPreview(args: args));
            break;
          case PrinterView.routeName:
            screen = MaterialPageRoute(
              settings: settings,
              builder: (context) => const PrinterView(),
            );
            break;
          case PrinterBluetoothView.routeName:
            screen = MaterialPageRoute(
              settings: settings,
              builder: (context) => const PrinterBluetoothView(),
            );
            break;
          case PrinterNetworkView.routeName:
            screen = MaterialPageRoute(
              settings: settings,
              builder: (context) => const PrinterNetworkView(),
            );
            break;
          case OrderHistoryView.routeName:
            screen = MaterialPageRoute(
                builder: (context) => const OrderHistoryView());
            break;

          case ProductTestView.routeName:
            screen = MaterialPageRoute(
                builder: (context) => const ProductTestView());
            break;

          case AdsDetailView.routeName:
            final arg = settings.arguments as AdsDetailViewArgument;
            screen = MaterialPageRoute(
                builder: (context) => AdsDetailView(arg: arg));
            break;
          case LanguageView.routeName:
            screen = MaterialPageRoute(
                settings: settings, builder: (context) => const LanguageView());
            break;

          default:
            screen = MaterialPageRoute(
                settings: settings,
                builder: (_) {
                  return routes[settings.name] ??
                      Scaffold(
                        appBar: AppBar(),
                        body: const Center(
                          child: Text("Page Not Found"),
                        ),
                      );
                });
        }

        return screen;
      },
    );
  }
}
