import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_photo.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_video.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/social_info_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:dio/dio.dart';

class AdsRepo {
  final Dio request;

  AdsRepo({required this.request});

  Future<BaseResponse<SocialInfo>> getSocailInfo() async {
    final mappedRes =
        (await request.post(HttpApi.socialHome)).data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, SocialInfo.fromMap(mappedRes["data"]));
    print("Get Socical ads info : $mappedRes");

    return response;
  }

  Future<BaseResponse<SocialAds>> getAdsList(
      {required String socialId, String page = "1"}) async {
    final formData = FormData.fromMap({
      "social_uid": socialId,
      "page": page,
    });

    final mappedRes = (await request.post(HttpApi.postList, data: formData))
        .data as Map<String, dynamic>;

    // print("Get Post ads list : ${response.data}");

    final response =
        BaseResponse.fromMap(mappedRes, SocialAds.fromMap(mappedRes["data"]));

    return response;
  }

  Future<BaseResponse<String?>> createPostPhoto(
      CreatePostPhotoRequest req) async {
    List<MultipartFile> multipartFile = [];

    for (var e in req.images) {
      multipartFile
          .add(await MultipartFile.fromFile(e, filename: e.split('/').last));
    }

    FormData formData = FormData.fromMap({
      "title": req.title,
      "product_id": req.productId,
      "images[]": multipartFile,
    });

    final mappedRes = (await request.post(HttpApi.postPhoto, data: formData))
        .data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, mappedRes["data"].toString());
    print('resposen create Success : $response ');

    return response;
  }

  Future<BaseResponse<String>> createPostVideo(
      CreatePostVideoRequest req) async {
    Map<String, dynamic> mapRes = {
      'title': req.title,
      'product_id': req.productId,
    };
    // List<MultipartFile> multipartFile = [];
    if (req.video != null) {
      final videoFile = MultipartFile.fromFileSync(req.video!.path,
          filename: req.video?.path.split('/').last);
      mapRes.addAll({"video": videoFile});
    }

    if (req.cover != null) {
      final coverFile = await MultipartFile.fromFile(req.cover!.path,
          filename: req.cover!.path.split("/").last);

      mapRes.addAll({"cover": coverFile});
    }

    FormData formData = FormData.fromMap(mapRes);

    final mappedRep = (await request.post(HttpApi.postVideo, data: formData))
        .data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRep, mappedRep['data'].toString());

    return response;
  }
}
