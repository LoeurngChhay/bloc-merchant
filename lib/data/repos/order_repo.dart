import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_notify_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:dio/dio.dart';

abstract class OrderRepo {
  Future<BaseResponse<OrderList?>> orderList(
      {int? status, int? page, CancelToken? cancelToken});

  Future<BaseResponse<OrderDetail?>> orderDetail(String orderId);

  Future<BaseResponse<String?>> orderAccept(String orderId);

  Future<BaseResponse<String?>> orderCancel(String orderId, String reason);

  Future<BaseResponse<OrderNotify?>> orderNotify();
}

class OrderRepoImpl extends OrderRepo {
  final Dio request;
  OrderRepoImpl({required this.request});

  @override
  Future<BaseResponse<OrderList?>> orderList(
      {int? status = OrderState.newOrder,
      int? page,
      CancelToken? cancelToken}) async {
    // final formData = FormData.fromMap({'status': status, "page": page});

    // final mappedRes = (await request.post(HttpApi.getListOrder,
    //         data: formData, cancelToken: cancelToken))
    //     .data as Map<String, dynamic>;

    final mappedRes = fakeOrder as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, OrderList.fromMap(mappedRes["data"]));

    return response;
  }

  @override
  Future<BaseResponse<OrderDetail?>> orderDetail(String orderId) async {
    // final formData = FormData.fromMap({"order_id": orderId});
    // final mappedRes = (await request.post(HttpApi.orderDetail, data: formData))
    //     .data as Map<String, dynamic>;
    final mappedRes = fakeOrderDetail as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, OrderDetail.fromMap(mappedRes["data"]));

    return response;
  }

  @override
  Future<BaseResponse<String?>> orderAccept(String orderId) async {
    FormData formData = FormData.fromMap({"order_id": orderId});

    final mappedRep =
        (await request.post(HttpApi.acceptTheOrder, data: formData)).data
            as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRep, mappedRep['data'].toString());
    return response;
  }

  @override
  Future<BaseResponse<String?>> orderCancel(
      String orderId, String reason) async {
    FormData formData =
        FormData.fromMap({"order_id": orderId, "reason": reason});
    print('reason: $reason ');

    final mappedRep = (await request.post(HttpApi.cancelTheOrder,
        data: formData)) as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRep, mappedRep['data'].toString());

    return response;
  }

  @override
  Future<BaseResponse<OrderNotify?>> orderNotify() async {
    final mappedRes =
        (await request.post(HttpApi.orderNotify)).data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, OrderNotify.fromMap(mappedRes["data"]));

    return response;
  }
}

final fakeOrder = {
  "data": {
    "items": [
      {
        "order_id": "784527",
        "order_time": "24-04-2024 11:28",
        "customer": {
          "uid": "72338",
          "mobile": "+855969400333",
          "nickname": "Ma",
          "face":
              "https://s3.ap-southeast-1.amazonaws.com/bloc01/default/member_face.png",
          "order_count": "2189"
        },
        "driver_id": "456",
        "driver": {
          "staff_id": "456",
          "name": "2.Bloc Driver Province",
          "mobile": "+85516421333",
          "face":
              "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202009/20200928_CFEA3544D14E9085FC5AD7E1B273543F.jpg",
          "lat": "11562790",
          "lng": "104906442"
        },
        "product_price": "123.49",
        "package_price": "0.00",
        "pei_time_label": "Delivered ASAP",
        "queue_number": "1",
        "receiver_contact": "Ma",
        "receiver_mobile": "0969400333",
        "reveiver_adrress": "WELL B Pharmacy, 2, Phnom Penh, Cambodia",
        "payment": "Cash On Delivery",
        "payment_status": "",
        "remark": "TESTING ",
        "order_status": "-1",
        "order_type": "1",
        "order_type_label": "BLOC Delivery",
        "order_status_label": "canceled",
        "pin_code": "4527",
        "total_price": "123.49",
        "pei_amount": "0.00",
        "bag_price": 0,
        "discount_amount": 0,
        "vat_amount": 0,
        "comission_fee": 12.35,
        "shop_receive": 111.14
      },
      {
        "order_id": "798620",
        "order_time": "28-06-2024 13:32",
        "customer": {
          "uid": "167125",
          "mobile": "+85595288757",
          "nickname": "Chhaylc",
          "face":
              "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202401/20240104_6195CB2AF81483C3A0E65D043D22EF7B.jpg",
          "order_count": "1"
        },
        "driver_id": "0",
        "driver": {
          "staff_id": 0,
          "name": "",
          "mobile": "",
          "face": "",
          "lat": 0,
          "lng": 0
        },
        "product_price": "10.00",
        "package_price": "0.00",
        "pei_time_label": "Delivered ASAP",
        "queue_number": "1",
        "receiver_contact": "Chhay",
        "receiver_mobile": "095288757",
        "reveiver_adrress": "St 219, Phnom Penh, Cambodia",
        "payment": "Cash On Delivery",
        "payment_status": "",
        "remark": "",
        "order_status": "-1",
        "order_type": "1",
        "order_type_label": "BLOC Delivery",
        "order_status_label": "canceled",
        "pin_code": "8620",
        "total_price": "10.00",
        "pei_amount": "0.00",
        "bag_price": 0,
        "discount_amount": 0,
        "vat_amount": 0,
        "comission_fee": 1.3,
        "shop_receive": 8.7
      },
      {
        "order_id": "793414",
        "order_time": "06-06-2024 14:17",
        "customer": {
          "uid": "72338",
          "mobile": "+855969400333",
          "nickname": "Ma",
          "face":
              "https://s3.ap-southeast-1.amazonaws.com/bloc01/default/member_face.png",
          "order_count": "2189"
        },
        "driver_id": "0",
        "driver": {
          "staff_id": 0,
          "name": "",
          "mobile": "",
          "face": "",
          "lat": 0,
          "lng": 0
        },
        "product_price": "10.00",
        "package_price": "0.00",
        "pei_time_label": "Delivered ASAP",
        "queue_number": "2",
        "receiver_contact": "Ma",
        "receiver_mobile": "0719301330",
        "reveiver_adrress":
            "157 Oknha Tep Phan St. (182), Phnom Penh, Cambodia",
        "payment": "Cash On Delivery",
        "payment_status": "",
        "remark": "testing",
        "order_status": "-1",
        "order_type": "1",
        "order_type_label": "BLOC Delivery",
        "order_status_label": "canceled",
        "pin_code": "0000",
        "total_price": "10.00",
        "pei_amount": "0.00",
        "bag_price": 0,
        "discount_amount": 1,
        "vat_amount": 0,
        "comission_fee": 0.9,
        "shop_receive": 9.1
      },
    ],
    "total": 8537,
    "limit": 100,
  },
  "error": 0,
  "message": "Successful"
};

final fakeOrderDetail = {
  "data": {
    "driver_id": "456",
    "driver": {
      "staff_id": "456",
      "name": "2.Bloc Driver Province",
      "mobile": "+85516421333",
      "face":
          "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202009/20200928_CFEA3544D14E9085FC5AD7E1B273543F.jpg",
      "lat": "11562807",
      "lng": "104906497"
    },
    "customer": {
      "uid": "72338",
      "mobile": "+855969400333",
      "nickname": "Ma",
      "face":
          "https://s3.ap-southeast-1.amazonaws.com/bloc01/default/member_face.png",
      "order_count": "2198"
    },
    "products": [
      {
        "order_id": "784527",
        "product_id": "772525",
        "product_title": "BIO-OIL-SKINCARE OIL 60ml \n[Size M]",
        "product_name": "BIO-OIL-SKINCARE OIL 60ml \n[Size M] [Less] [100%]",
        "bar_code": "",
        "product_image":
            "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202404/20240418_B533E0A2B1ECEBD4DB8B8B1E19A57EA8.png",
        "product_thumb":
            "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202404/20240418_B533E0A2B1ECEBD4DB8B8B1E19A57EA8_thumb.png",
        "product_price": "50.00",
        "package_price": "0.00",
        "product_number": "1",
        "amount": "50.00",
        "spec_id": "322364",
        "unit": "unit",
        "specification": [
          {"key": "Ice", "val": "Less"},
          {"key": "Sugar", "val": "100%"}
        ],
        "product_prices": "108.08",
        "product_oldprice": "108.08",
        "product_oldprices": "108.08",
        "huodong_title": "",
        "cate_id": "50401",
        "waimai_bl": "10",
        "discount_bl": "0.00",
        "product_addon": [
          {"qty": 1, "product_name": "VISTRA-VALENGINY", "price": "29.04"},
          {
            "qty": 1,
            "product_name": "VISTRA-MARINE COLLAGEN Triptide 10000mg",
            "price": "29.04"
          },
        ],
        "product_addon_price": "58.08",
        "is_hide": "0",
        "spec_name": "Size M",
        "discount_type": "0",
        "margin_bl": "0.00",
        "note": "",
        "product_spec": [
          {"spec_title": "Ice", "spec_value": "Less"},
          {"spec_title": "Sugar", "spec_value": "100%"}
        ]
      },
      {
        "order_id": "784527",
        "product_id": "772526",
        "product_title": "BIO-OIL-SKINCARE OIL 125ml",
        "product_name": "BIO-OIL-SKINCARE OIL 125ml",
        "bar_code": "",
        "product_image":
            "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202404/20240423_02F7B47F8F7114E7F1C42D10DB7E5E43.jpg",
        "product_thumb":
            "https://s3.ap-southeast-1.amazonaws.com/bloc01/photo/202404/20240423_02F7B47F8F7114E7F1C42D10DB7E5E43_thumb.jpg",
        "product_price": "15.41",
        "package_price": "0.00",
        "product_number": "1",
        "amount": "15.41",
        "spec_id": "0",
        "unit": "unit",
        "specification": [],
        "product_prices": "15.41",
        "product_oldprice": "15.41",
        "product_oldprices": "15.41",
        "huodong_title": "",
        "cate_id": "50401",
        "waimai_bl": "10",
        "discount_bl": "0.00",
        "product_addon": [],
        "product_addon_price": "0.00",
        "is_hide": "0",
        "spec_name": "",
        "discount_type": "0",
        "margin_bl": "0.00",
        "note": ""
      }
    ],
    "bag_price": 0,
    "discount_amount": 0,
    "vat_amount": 0,
    "comission_fee": 12.35,
    "shop_receive": 111.14,
    "pei_time_label": "Delivered ASAP",
    "order_type": "1",
    "order_type_label": "BLOC Delivery",
    "queue_number": "1",
    "payment": "Cash On Delivery",
    "payment_status": "",
    "receiver_contact": "Ma",
    "receiver_mobile": "0969400333",
    "reveiver_adrress": "WELL B Pharmacy, 2, Phnom Penh, Cambodia",
    "product_price": "123.49",
    "package_price": "0.00",
    "note": "TESTING ",
    "pin_code": "4527",
    "total_price": "123.49",
    "pei_amount": "0.00",
    "order_status": "-1",
    "order_status_label": "canceled"
  },
  "error": 0,
  "message": "Successful"
};
