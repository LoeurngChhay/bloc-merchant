import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/login_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AuthRepo {
  Future<BaseResponse<LoginResponse?>> login({String? mobile, String? passwd});
}

class AuthRepoImpl extends AuthRepo {
  final _preferences = SharedPreferences.getInstance();
  final request = locator<Dio>();

  @override
  Future<BaseResponse<LoginResponse?>> login(
      {String? mobile, String? passwd}) async {
    // print('mobile : $mobile ,passwd : $passwd ');

    final formData = FormData.fromMap({
      "mobile": mobile,
      "passwd": passwd,
    });
    final prefs = await _preferences;
    final mappedRes = (await request.post(HttpApi.login, data: formData)).data
        as Map<String, dynamic>;
    // print("mappedRes : $mappedRes");

    final res = BaseResponse.fromMap(
        mappedRes, LoginResponse.fromMap(mappedRes["data"]));

    print('res.data!.shop : ${res.data!.shop}');

    if (res.data != null) {
      prefs.setString(StorageKeys.TOKEN_KEY, res.data!.token.toString());
      prefs.setString(StorageKeys.USER_KEY, res.data!.shop.toString());
      final fcm = await FirebaseMessaging.instance.getToken();
      prefs.setString(StorageKeys.FCM_KEY, fcm.toString());
    }
    return res;
  }
}
