import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/home_info_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class HomeRepo {
  Future<BaseResponse<String>> openOperation();

  Future<BaseResponse<String>> closeOperation({String? status, String? date});

  Future<BaseResponse<HomeInfo?>> getHomeInfo();
}

class HomeRepoImpl extends HomeRepo {
  final request = locator<Dio>();
  final _preferences = SharedPreferences.getInstance();
  @override
  Future<BaseResponse<String>> openOperation() async {
    final res = (await request.post(HttpApi.setOpenStatus)).data
        as Map<String, dynamic>;

    final response = BaseResponse.fromMap(res, res["data"].toString());

    return response;
  }

  @override
  Future<BaseResponse<String>> closeOperation(
      {String? status, String? date}) async {
    final formData = FormData.fromMap({
      "data_status": status,
      "data_date": date,
    });
    final res = (await request.post(HttpApi.setCloseStatus, data: formData))
        .data as Map<String, dynamic>;

    final response = BaseResponse.fromMap(res, res["data"].toString());

    return response;
  }

  @override
  Future<BaseResponse<HomeInfo?>> getHomeInfo() async {
    final mappedRes =
        (await request.post(HttpApi.info)).data as Map<String, dynamic>;

    final prefs = await _preferences;

    final response = mappedRes["error"] == 0
        ? BaseResponse.fromMap(mappedRes, HomeInfo.fromMap(mappedRes["data"]))
        : BaseResponse.fromMap(mappedRes, null);

    if (response.data != null) {
      prefs.setString(StorageKeys.SHOP_KEY, response.data.toString());
    }

    return response;
  }
}
