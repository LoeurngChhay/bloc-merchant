import 'package:bloc_merchant_mobile_2/data/models/requests/create_category_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/category_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/create_category_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:dio/dio.dart';

abstract class CategoryRepo {
  Future<BaseResponse<CategoryList?>> getCategory({String? search});

  Future<BaseResponse<CategoryDetail?>> getCategoryDetail(String id);

  Future<BaseResponse<CategoryDetail?>> createCategory(
      CreateCategoryRequest req);

  Future<BaseResponse<CategoryDetail?>> updateCategory(
      {required String cateId, required CreateCategoryRequest req});

  Future<BaseResponse<String?>> deleteCategory(String cateId);
}

class CategoryRepoImpl extends CategoryRepo {
  final request = locator<Dio>();

  @override
  Future<BaseResponse<CategoryList?>> getCategory({String? search}) async {
    final formData = FormData.fromMap({"search": search});
    final mappedRes =
        (await request.post(HttpApi.getListCategory, data: formData)).data
            as Map<String, dynamic>;

    final response = mappedRes["error"] > 0
        ? BaseResponse.fromMap(mappedRes, null)
        : BaseResponse.fromMap(
            mappedRes, CategoryList.fromMap(mappedRes["data"]));

    return response;
  }

  @override
  Future<BaseResponse<CategoryDetail?>> createCategory(
      CreateCategoryRequest req) async {
    print('req.showType : ${req.showType}');
    final formData = FormData.fromMap({
      "title": req.title,
      "orderby": req.orderby,
      "show_type": req.showType,
      "stime": req.stime,
      "ltime": req.ltime,
      "icon": req.icon,
      "is_release": req.releaseDate,
      "release_date": req.releaseDate,
      "is_expired": req.isExpired,
      "expired_date": req.expiredDate
    });

    final mappedRes =
        (await request.post(HttpApi.createCategory, data: formData)).data
            as Map<String, dynamic>;

    final response = mappedRes["error"] > 0
        ? BaseResponse.fromMap(mappedRes, null)
        : BaseResponse.fromMap(
            mappedRes, CategoryDetail.fromMap(mappedRes["data"]));

    // print('resposne cate : ${response.data}');

    return response;
  }

  @override
  Future<BaseResponse<CategoryDetail?>> getCategoryDetail(String id) async {
    final formData = FormData.fromMap({"cate_id": id});
    final mappedRes = (await request.post(HttpApi.getCategory, data: formData))
        .data as Map<String, dynamic>;

    final response = mappedRes["error"] > 0
        ? BaseResponse.fromMap(mappedRes, null)
        : BaseResponse.fromMap(
            mappedRes, CategoryDetail.fromMap(mappedRes["data"]));

    return response;
  }

  @override
  Future<BaseResponse<CategoryDetail?>> updateCategory(
      {required String cateId, required CreateCategoryRequest req}) async {
    final formData = FormData.fromMap({
      "cate_id": cateId,
      "title": req.title,
      "orderby": req.orderby,
      "show_type": req.showType,
      "stime": req.stime,
      "ltime": req.ltime,
      "icon": req.icon,
      "is_release": req.releaseDate,
      "release_date": req.releaseDate,
      "is_expired": req.isExpired,
      "expired_date": req.expiredDate
    });

    final mappedRes =
        (await request.post(HttpApi.updateCategory, data: formData)).data
            as Map<String, dynamic>;

    final response = mappedRes["error"] > 0
        ? BaseResponse.fromMap(mappedRes, null)
        : BaseResponse.fromMap(
            mappedRes, CategoryDetail.fromMap(mappedRes['data']));

    return response;
  }

  @override
  Future<BaseResponse<String?>> deleteCategory(String cateId) async {
    final formData = FormData.fromMap({"cate_id": cateId});

    final mappedRes =
        (await request.post(HttpApi.deleteCategory, data: formData)).data
            as Map<String, dynamic>;

    final response = mappedRes["error"] > 0
        ? BaseResponse.fromMap(mappedRes, null)
        : BaseResponse.fromMap(mappedRes, mappedRes['data'].toString());

    return response;
  }
}
