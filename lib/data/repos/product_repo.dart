import 'dart:async';
import 'dart:io';

import 'package:bloc_merchant_mobile_2/constants/product_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_product_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/base_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/get_product_data.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_cate_list_res.dart';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:dio/dio.dart';

class ProductRepo {
  final request = locator<Dio>();

  Future<BaseResponse<ProductList?>> getProductList(
      {String? page,
      String? search,
      String? cateId,
      CancelToken? cancelToken}) async {
    final formData =
        FormData.fromMap({"page": page, "search": search, "cate_id": cateId});

    // print('cate_id  : $cateId');

    final mappedRes = (await request.post(HttpApi.getProduct,
            data: formData, cancelToken: cancelToken))
        .data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, ProductList.fromMap(mappedRes["data"]));

    return response;
  }

  Future<BaseResponse<ProductCategoryList?>> getProductCategoryList() async {
    final mappedRes =
        (await request.post(HttpApi.getProdCat)).data as Map<String, dynamic>;

    final response = BaseResponse.fromMap(
        mappedRes, ProductCategoryList.fromMap(mappedRes["data"]));

    return response;
  }

  Future<BaseResponse<GetProductData>> getNewProduct() async {
    final mappedRes = (await request.post(HttpApi.getProductNew)).data
        as Map<String, dynamic>;

    final response = BaseResponse.fromMap(
        mappedRes, GetProductData.fromMap(mappedRes["data"]));

    return response;
  }

  Future<BaseResponse<GetProductData>> getProductDetail(
      String productId) async {
    final formData = FormData.fromMap({"product_id": productId});

    // print('product Detail product_id : $productId');

    final mappedRes =
        (await request.post(HttpApi.getProductDetail, data: formData)).data
            as Map<String, dynamic>;

    final response = BaseResponse.fromMap(
        mappedRes, GetProductData.fromMap(mappedRes["data"]));

    return response;
  }

  Future<BaseResponse<String?>> actionProduct(CreateProductRequest req) async {
    final videoFile = req.video != null
        ? MultipartFile.fromFileSync(req.video!.path,
            filename: req.video?.path.split('/').last)
        : null;

    print('req.videoUrl : ${req.videoUrl == null}, videoId : ${req.videoId}');

    Map<String, dynamic> map = {
      "product_id": req.productId,
      "title": req.title,
      "brand": req.brand,
      "orderby": req.orderBy,
      "unit": req.unit,
      "cate_id": req.cateId,
      "video_id": req.videoId,
      "deleted_video": req.videoUrl == null ? '1' : '0',
      "is_onsale": req.isOnSale,
      "is_addon": req.isAddon,
      "description": req.description,
      "secondary_title": req.secondaryTitle,
      "expired_date": req.expiredDate,
      "addon_sale": req.addonSale,
    };

    if (req.images != null && req.images!.isNotEmpty) {
      for (int i = 0; i < req.images!.length; i++) {
        final image = req.images![i];

        MultipartFile imageFile = await MultipartFile.fromFile(image.path,
            filename: image.path.split("/").last);

        map.addAll({
          "images[$i]": [imageFile]
        });
      }
    }

    if (videoFile != null || req.videoUrl != null) {
      map.addAll({"video": videoFile ?? req.videoUrl});
    }

    if (req.photos != null && req.photos!.isNotEmpty) {
      for (int i = 0; i < req.photos!.length; i++) {
        final photo = req.photos![i];

        map.addAll({"photos[$i][image_id]": photo.image_id ?? "0"});
        map.addAll({"photos[$i][name]": photo.photo?.split("/").last});
        map.addAll({
          "photos[$i][photo]":
              photo.type == ProductStatus.file ? '' : photo.photo
        });
        map.addAll({"photos[$i][type]": photo.type}); //1 = file , 0 = url
        map.addAll({"photos[$i][sort]": i});
      }
    }

    // print('imageTest photo : $imageTest');

    // Map<String, dynamic> testFormat = {};
    if (req.formats.isNotEmpty) {
      for (int i = 0; i < req.formats.length; i++) {
        req.formats[i].toMap().forEach((key, value) {
          return map.addAll({'formats[$i][$key]': value});
        });
      }
    }

    // print('testFormat : ${testFormat}');

    if (req.addons != null && req.addons!.isNotEmpty) {
      for (int i = 0; i < req.addons!.length; i++) {
        req.addons?[i].toMap().forEach((key, value) {
          // print("addons[$i][$key]: $value");

          return map.addAll({"addons[$i][$key]": value});
        });
      }
    }

    if (req.attributes != null) {
      for (int i = 0; i < req.attributes!.length; i++) {
        // print('attributes[$i][attr_name]": ${req.attributes![i].name}');

        map.addAll({"attributes[$i][attr_name]": req.attributes![i].name});
        req.attributes![i].values!.asMap().forEach((key, value) {
          return map.addAll({"attributes[$i][attr_val][$key]": value});
        });
      }
    }

    FormData formData = FormData.fromMap(map);

    final mappedRes = (await request.post(
            req.productId != null
                ? HttpApi.updateProduct
                : HttpApi.createProduct,
            data: formData))
        .data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, mappedRes["data"].toString());

    return response;
  }

  Future<BaseResponse<String?>> changeProductStatus(
      {String? productId, String? showType = ProductStatus.hide}) async {
    final formData =
        FormData.fromMap({"product_id": productId, "show_type": showType});
    final mappedRes =
        (await request.post(HttpApi.changeProductStatus, data: formData)).data
            as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, mappedRes['data'].toString());

    return response;
  }

  Future<BaseResponse<String?>> deleteProduct(String id) async {
    final formData = FormData.fromMap({"product_id": id});

    final mappedRes =
        (await request.post(HttpApi.deleteProduct, data: formData)).data
            as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, mappedRes["data"].toString());

    return response;
  }

  Future<BaseResponse<String?>> uploadImage(File photo) async {
    MultipartFile imageFile = await MultipartFile.fromFile(photo.path,
        filename: photo.path.split("/").last);

    final formData = FormData.fromMap({"photo": imageFile});

    final mappedRes = (await request.post(HttpApi.uploadImage, data: formData))
        .data as Map<String, dynamic>;

    final response =
        BaseResponse.fromMap(mappedRes, mappedRes["data"].toString());

    return response;
  }
}
