// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';
import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:flutter/foundation.dart';

class CreateProductRequest {
  String? productId;
  File? video;
  String? videoUrl;
  String? videoId;
  String title;
  String secondaryTitle;
  String brand;
  String cateId;
  String unit;
  String isOnSale;
  String isAddon;
  String addonSale;
  String orderBy;
  String? description;
  List<IFormatRequest> formats;
  List<Attribute>? attributes;
  List<IAddonRequest>? addons;
  String expiredDate;
  String? deletedVideo;
  List<File>? images;
  List<ProductImages>? photos;

  CreateProductRequest({
    this.productId,
    this.video,
    this.videoUrl,
    required this.title,
    required this.secondaryTitle,
    required this.brand,
    required this.cateId,
    required this.unit,
    required this.isOnSale,
    required this.isAddon,
    required this.addonSale,
    required this.orderBy,
    this.description,
    required this.formats,
    this.attributes,
    this.addons,
    required this.expiredDate,
    this.deletedVideo,
    this.videoId,
    this.images,
    this.photos,
  });

  CreateProductRequest copyWith({
    String? productId,
    List<File>? images,
    File? video,
    String? videoUrl,
    List<Photo>? photos,
    String? title,
    String? secondaryTitle,
    String? brand,
    String? cateId,
    String? unit,
    String? isOnSale,
    String? isAddon,
    String? addonSale,
    String? orderBy,
    String? description,
    List<IFormatRequest>? formats,
    List<Attribute>? attributes,
    List<IAddonRequest>? addons,
    String? expiredDate,
    String? deletedVideo,
    String? videoId,
  }) {
    return CreateProductRequest(
      productId: productId ?? this.productId,
      images: images ?? this.images,
      video: video ?? this.video,
      videoUrl: videoUrl ?? this.videoUrl,
      title: title ?? this.title,
      secondaryTitle: secondaryTitle ?? this.secondaryTitle,
      brand: brand ?? this.brand,
      cateId: cateId ?? this.cateId,
      unit: unit ?? this.unit,
      isOnSale: isOnSale ?? this.isOnSale,
      isAddon: isAddon ?? this.isAddon,
      addonSale: addonSale ?? this.addonSale,
      orderBy: orderBy ?? this.orderBy,
      description: description ?? this.description,
      formats: formats ?? this.formats,
      attributes: attributes ?? this.attributes,
      addons: addons ?? this.addons,
      expiredDate: expiredDate ?? this.expiredDate,
      deletedVideo: deletedVideo ?? this.deletedVideo,
      videoId: videoId ?? this.videoId,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'productId': productId,
      'images': images?.map((x) => x).toList(),
      'video': video,
      'videoUrl': videoUrl,
      'title': title,
      'secondaryTitle': secondaryTitle,
      'brand': brand,
      'cateId': cateId,
      'unit': unit,
      'isOnSale': isOnSale,
      'isAddon': isAddon,
      'addonSale': addonSale,
      'orderBy': orderBy,
      'description': description,
      'formats': formats.map((x) => x.toMap()).toList(),
      'attributes': attributes?.map((x) => x.toMap()).toList(),
      'addons': addons?.map((x) => x.toMap()).toList(),
      'expiredDate': expiredDate,
      'deletedVideo': deletedVideo,
      'videoId': videoId,
    };
  }

  factory CreateProductRequest.fromMap(Map<String, dynamic> map) {
    return CreateProductRequest(
      productId: map['productId'] != null ? map['productId'] as String : null,
      images: List<File>.from(map['images'].map((x) => File(x))),
      video: map['video'] != null ? File(map['video']) : null,
      videoUrl: map['videoUrl'] != null ? map['videoUrl'] as String : null,
      title: map['title'] as String,
      secondaryTitle: map['secondaryTitle'] as String,
      brand: map['brand'] as String,
      cateId: map['cateId'] as String,
      unit: map['unit'] as String,
      isOnSale: map['isOnSale'] as String,
      isAddon: map['isAddon'] as String,
      addonSale: map['addonSale'] as String,
      orderBy: map['orderBy'] as String,
      description:
          map['description'] != null ? map['description'] as String : null,
      formats: List<IFormatRequest>.from(
        (map['formats'] as List<int>).map<IFormatRequest>(
          (x) => IFormatRequest.fromMap(x as Map<String, dynamic>),
        ),
      ),
      attributes: map['attributes'] != null
          ? List<Attribute>.from(
              (map['attributes'] as List<int>).map<Attribute?>(
                (x) => Attribute.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      addons: map['addons'] != null
          ? List<IAddonRequest>.from(
              (map['addons'] as List<int>).map<IAddonRequest?>(
                (x) => IAddonRequest.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      expiredDate: map['expiredDate'] as String,
      deletedVideo:
          map['deletedVideo'] != null ? map['deletedVideo'] as String : null,
      videoId: map['videoId'] != null ? map['videoId'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory CreateProductRequest.fromJson(String source) =>
      CreateProductRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'CreateProductRequest(productId: $productId, images: $images, video: $video, videoUrl: $videoUrl, title: $title, secondaryTitle: $secondaryTitle, brand: $brand, cateId: $cateId, unit: $unit, isOnSale: $isOnSale, isAddon: $isAddon, addonSale: $addonSale, orderBy: $orderBy, description: $description, formats: $formats, attributes: $attributes, addons: $addons, expiredDate: $expiredDate, deletedVideo: $deletedVideo, videoId: $videoId)';
  }

  @override
  bool operator ==(covariant CreateProductRequest other) {
    if (identical(this, other)) return true;

    return other.productId == productId &&
        listEquals(other.images, images) &&
        other.video == video &&
        other.videoUrl == videoUrl &&
        other.title == title &&
        other.secondaryTitle == secondaryTitle &&
        other.brand == brand &&
        other.cateId == cateId &&
        other.unit == unit &&
        other.isOnSale == isOnSale &&
        other.isAddon == isAddon &&
        other.addonSale == addonSale &&
        other.orderBy == orderBy &&
        other.description == description &&
        listEquals(other.formats, formats) &&
        listEquals(other.attributes, attributes) &&
        listEquals(other.addons, addons) &&
        other.expiredDate == expiredDate &&
        other.deletedVideo == deletedVideo &&
        other.videoId == videoId;
  }

  @override
  int get hashCode {
    return productId.hashCode ^
        images.hashCode ^
        video.hashCode ^
        videoUrl.hashCode ^
        title.hashCode ^
        secondaryTitle.hashCode ^
        brand.hashCode ^
        cateId.hashCode ^
        unit.hashCode ^
        isOnSale.hashCode ^
        isAddon.hashCode ^
        addonSale.hashCode ^
        orderBy.hashCode ^
        description.hashCode ^
        formats.hashCode ^
        attributes.hashCode ^
        addons.hashCode ^
        expiredDate.hashCode ^
        deletedVideo.hashCode ^
        videoId.hashCode;
  }
}

class Photo {
  String image_id;
  String name;
  String type;
  String sort;
  String photo;

  Photo({
    required this.image_id,
    required this.name,
    required this.type,
    required this.sort,
    required this.photo,
  });

  Photo copyWith({
    String? image_id,
    String? name,
    String? type,
    String? sort,
    String? photo,
  }) {
    return Photo(
      image_id: image_id ?? this.image_id,
      name: name ?? this.name,
      type: type ?? this.type,
      sort: sort ?? this.sort,
      photo: photo ?? this.photo,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'image_id': image_id,
      'name': name,
      'type': type,
      'sort': sort,
      'photo': photo,
    };
  }

  factory Photo.fromMap(Map<String, dynamic> map) {
    return Photo(
      image_id: map['image_id'] as String,
      name: map['name'] as String,
      type: map['type'] as String,
      sort: map['sort'] as String,
      photo: map['photo'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory Photo.fromJson(String source) =>
      Photo.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Photo(image_id: $image_id, name: $name, type: $type, sort: $sort, photo: $photo)';
  }

  @override
  bool operator ==(covariant Photo other) {
    if (identical(this, other)) return true;

    return other.image_id == image_id &&
        other.name == name &&
        other.type == type &&
        other.sort == sort &&
        other.photo == photo;
  }

  @override
  int get hashCode {
    return image_id.hashCode ^
        name.hashCode ^
        type.hashCode ^
        sort.hashCode ^
        photo.hashCode;
  }
}

class IAddonRequest {
  String product_id;
  String product_title;
  String addon_code;

  IAddonRequest({
    required this.product_id,
    required this.product_title,
    required this.addon_code,
  });

  IAddonRequest copyWith({
    String? productId,
    String? productTitle,
    String? addonCode,
  }) {
    return IAddonRequest(
      product_id: product_id,
      product_title: product_title,
      addon_code: addon_code,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'product_id': product_id,
      'product_title': product_title,
      'addon_code': addon_code,
    };
  }

  factory IAddonRequest.fromMap(Map<String, dynamic> map) {
    return IAddonRequest(
      product_id: map['product_id'] as String,
      product_title: map['product_title'] as String,
      addon_code: map['addon_code'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory IAddonRequest.fromJson(String source) =>
      IAddonRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'IAddonRequest(product_id: $product_id, product_title: $product_title, addon_code: $addon_code)';

  @override
  bool operator ==(covariant IAddonRequest other) {
    if (identical(this, other)) return true;

    return other.product_id == product_id &&
        other.product_title == product_title &&
        other.addon_code == addon_code;
  }

  @override
  int get hashCode =>
      product_id.hashCode ^ product_title.hashCode ^ addon_code.hashCode;
}

class IFormatRequest {
  String? format_id;
  String? barcode;
  String? name;
  String? price;
  String? package_price;
  String? old_price;
  String? sale_sku;
  String? sale_type;
  String? totalItem;

  IFormatRequest({
    this.format_id,
    this.barcode,
    this.name,
    this.price,
    this.package_price,
    this.old_price,
    this.sale_sku,
    this.sale_type,
    this.totalItem,
  });

  IFormatRequest copyWith({
    String? format_id,
    String? barcode,
    String? name,
    String? price,
    String? package_price,
    String? old_price,
    String? sale_sku,
    String? sale_type,
    String? totalItem,
  }) {
    return IFormatRequest(
      format_id: format_id ?? this.format_id,
      barcode: barcode ?? this.barcode,
      name: name ?? this.name,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      old_price: old_price ?? this.old_price,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_type: sale_type ?? this.sale_type,
      totalItem: totalItem ?? this.totalItem,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'format_id': format_id,
      'barcode': barcode,
      'name': name,
      'price': price,
      'package_price': package_price,
      'old_price': old_price,
      'sale_sku': sale_sku,
      'sale_type': sale_type,
      'totalItem': totalItem,
    };
  }

  factory IFormatRequest.fromMap(Map<String, dynamic> map) {
    return IFormatRequest(
      format_id: map['format_id'] != null ? map['format_id'] as String : null,
      barcode: map['barcode'] != null ? map['barcode'] as String : null,
      name: map['name'] != null ? map['name'] as String : null,
      price: map['price'] != null ? map['price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      old_price: map['old_price'] != null ? map['old_price'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      totalItem: map['totalItem'] != null ? map['totalItem'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory IFormatRequest.fromJson(String source) =>
      IFormatRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'IFormatRequest(format_id: $format_id, barcode: $barcode, name: $name, price: $price, package_price: $package_price, old_price: $old_price, sale_sku: $sale_sku, sale_type: $sale_type, totalItem: $totalItem)';
  }

  @override
  bool operator ==(covariant IFormatRequest other) {
    if (identical(this, other)) return true;

    return other.format_id == format_id &&
        other.barcode == barcode &&
        other.name == name &&
        other.price == price &&
        other.package_price == package_price &&
        other.old_price == old_price &&
        other.sale_sku == sale_sku &&
        other.sale_type == sale_type &&
        other.totalItem == totalItem;
  }

  @override
  int get hashCode {
    return format_id.hashCode ^
        barcode.hashCode ^
        name.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        old_price.hashCode ^
        sale_sku.hashCode ^
        sale_type.hashCode ^
        totalItem.hashCode;
  }
}
