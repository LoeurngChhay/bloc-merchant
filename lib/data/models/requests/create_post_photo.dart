// ignore_for_file: public_member_api_docs, sort_constructors_first
class CreatePostPhotoRequest {
  final String title;
  final String productId;
  final List<String> images;

  CreatePostPhotoRequest({
    required this.title,
    required this.productId,
    required this.images,
  });

  @override
  String toString() =>
      'CreatePostPhotoRequest(title: $title, productId: $productId, images: $images)';
}
