// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

class CreatePostVideoRequest {
  final String title;
  final String productId;
  final File? video;
  final File? cover;

  CreatePostVideoRequest(
      {required this.title, required this.productId, this.video, this.cover});

  @override
  String toString() =>
      'CreatePostVideoRequest(title: $title, productId: $productId, video: $video, cover: $cover)';
}
