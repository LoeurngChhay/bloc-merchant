// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class LoginRequest {
  String? mobile;
  String? passwd;

  LoginRequest({
    this.mobile,
    this.passwd,
  });

  LoginRequest copyWith({
    String? mobile,
    String? passwd,
  }) {
    return LoginRequest(
      mobile: mobile ?? this.mobile,
      passwd: passwd ?? this.passwd,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'mobile': mobile,
      'passwd': passwd,
    };
  }

  factory LoginRequest.fromMap(Map<String, dynamic> map) {
    return LoginRequest(
      mobile: map['mobile'] != null ? map['mobile'] as String : null,
      passwd: map['passwd'] != null ? map['passwd'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginRequest.fromJson(String source) =>
      LoginRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'LoginRequest(mobile: $mobile, passwd: $passwd)';

  @override
  bool operator ==(covariant LoginRequest other) {
    if (identical(this, other)) return true;

    return other.mobile == mobile && other.passwd == passwd;
  }

  @override
  int get hashCode => mobile.hashCode ^ passwd.hashCode;
}
