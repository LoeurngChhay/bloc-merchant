import 'dart:io';

class FileEntity {
  String? filename;
  String? paramater;
  File? file;

  FileEntity({
    this.filename,
    this.paramater,
    this.file,
  });
}
