// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: non_constant_identifier_names

class CreateCategoryRequest {
  String? title;
  int? orderby;
  int? showType;
  String? stime;
  String? ltime;
  String? icon;
  String? isRelease;
  String? releaseDate;
  String? isExpired;
  String? expiredDate;

  CreateCategoryRequest({
    this.title,
    this.orderby,
    this.showType,
    this.stime,
    this.ltime,
    this.icon,
    this.isRelease,
    this.releaseDate,
    this.isExpired,
    this.expiredDate,
  });

  @override
  String toString() {
    return 'CreateCategoryRequest(title: $title, orderby: $orderby, showType: $showType, stime: $stime, ltime: $ltime, icon: $icon, isRelease: $isRelease, releaseDate: $releaseDate, isExpired: $isExpired, expiredDate: $expiredDate)';
  }
}
