// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class ProductList {
  int? cateId;
  List<Product>? items;
  int? total;
  int? limit;

  ProductList({
    this.cateId,
    this.items,
    this.total,
    this.limit,
  });

  ProductList copyWith({
    int? cateId,
    List<Product>? items,
    int? total,
    int? limit,
  }) {
    return ProductList(
      cateId: cateId ?? this.cateId,
      items: items ?? this.items,
      total: total ?? this.total,
      limit: limit ?? this.limit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'cateId': cateId,
      'items': items?.map((x) => x.toMap()).toList(),
      'total': total,
      'limit': limit,
    };
  }

  factory ProductList.fromMap(Map<String, dynamic> map) {
    return ProductList(
      cateId: map['cateId'] != null ? map['cateId'] as int : null,
      items: map['items'] != null
          ? List<Product>.from(
              (map['items'] as List<dynamic>).map<Product?>(
                (x) => Product.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      total: map['total'] != null ? map['total'] as int : null,
      limit: map['limit'] != null ? map['limit'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductList.fromJson(String source) =>
      ProductList.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductList(cateId: $cateId, items: $items, total: $total, limit: $limit)';
  }

  @override
  bool operator ==(covariant ProductList other) {
    if (identical(this, other)) return true;

    return other.cateId == cateId &&
        listEquals(other.items, items) &&
        other.total == total &&
        other.limit == limit;
  }

  @override
  int get hashCode {
    return cateId.hashCode ^ items.hashCode ^ total.hashCode ^ limit.hashCode;
  }
}

class Product {
  String? product_id;
  String? shop_id;
  String? cateId;
  String? title;
  String? secondary_title;
  String? thumb;
  num? price;
  num? package_price;
  String? sales;
  String? sale_type;
  String? sale_sku;
  String? sale_count;
  String? orderby;
  String? closed;
  String? is_onsale;
  String? unit;
  String? is_format;
  String? is_addon;
  String? addon_sale;
  String? bar_code;
  String? is_spec;
  List<ProductFormat>? formats;
  List<ProductAttribute>? attributes;

  Product({
    this.product_id,
    this.shop_id,
    this.cateId,
    this.title,
    this.secondary_title,
    this.thumb,
    this.price,
    this.package_price,
    this.sales,
    this.sale_type,
    this.sale_sku,
    this.sale_count,
    this.orderby,
    this.closed,
    this.is_onsale,
    this.unit,
    this.is_format,
    this.is_addon,
    this.addon_sale,
    this.bar_code,
    this.is_spec,
    this.formats,
    this.attributes,
  });

  Product copyWith({
    String? product_id,
    String? shop_id,
    String? cateId,
    String? title,
    String? secondary_title,
    String? thumb,
    num? price,
    num? package_price,
    String? sales,
    String? sale_type,
    String? sale_sku,
    String? sale_count,
    String? orderby,
    String? closed,
    String? is_onsale,
    String? unit,
    String? is_format,
    String? is_addon,
    String? addon_sale,
    String? bar_code,
    String? is_spec,
    List<ProductFormat>? formats,
    List<ProductAttribute>? attributes,
  }) {
    return Product(
      product_id: product_id ?? this.product_id,
      shop_id: shop_id ?? this.shop_id,
      cateId: cateId ?? this.cateId,
      title: title ?? this.title,
      secondary_title: secondary_title ?? this.secondary_title,
      thumb: thumb ?? this.thumb,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      sales: sales ?? this.sales,
      sale_type: sale_type ?? this.sale_type,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      orderby: orderby ?? this.orderby,
      closed: closed ?? this.closed,
      is_onsale: is_onsale ?? this.is_onsale,
      unit: unit ?? this.unit,
      is_format: is_format ?? this.is_format,
      is_addon: is_addon ?? this.is_addon,
      addon_sale: addon_sale ?? this.addon_sale,
      bar_code: bar_code ?? this.bar_code,
      is_spec: is_spec ?? this.is_spec,
      formats: formats ?? this.formats,
      attributes: attributes ?? this.attributes,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'product_id': product_id,
      'shop_id': shop_id,
      'cateId': cateId,
      'title': title,
      'secondary_title': secondary_title,
      'thumb': thumb,
      'price': price,
      'package_price': package_price,
      'sales': sales,
      'sale_type': sale_type,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'orderby': orderby,
      'closed': closed,
      'is_onsale': is_onsale,
      'unit': unit,
      'is_format': is_format,
      'is_addon': is_addon,
      'addon_sale': addon_sale,
      'bar_code': bar_code,
      'is_spec': is_spec,
      'formats': formats?.map((x) => x.toMap()).toList(),
      'attributes': attributes?.map((x) => x.toMap()).toList(),
    };
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cateId: map['cateId'] != null ? map['cateId'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      secondary_title: map['secondary_title'] != null
          ? map['secondary_title'] as String
          : null,
      thumb: map['thumb'] != null ? map['thumb'] as String : null,
      price: map['price'] != null ? map['price'] as num : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as num : null,
      sales: map['sales'] != null ? map['sales'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      closed: map['closed'] != null ? map['closed'] as String : null,
      is_onsale: map['is_onsale'] != null ? map['is_onsale'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      is_format: map['is_format'] != null ? map['is_format'] as String : null,
      is_addon: map['is_addon'] != null ? map['is_addon'] as String : null,
      addon_sale:
          map['addon_sale'] != null ? map['addon_sale'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      is_spec: map['is_spec'] != null ? map['is_spec'] as String : null,
      formats: map['formats'] != null
          ? List<ProductFormat>.from(
              (map['formats'] as List<dynamic>).map<ProductFormat?>(
                (x) => ProductFormat.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      attributes: map['attributes'] != null
          ? List<ProductAttribute>.from(
              (map['attributes'] as List<dynamic>).map<ProductAttribute?>(
                (x) => ProductAttribute.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Product.fromJson(String source) =>
      Product.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Product(product_id: $product_id, shop_id: $shop_id, cateId: $cateId, title: $title, secondary_title: $secondary_title, thumb: $thumb, price: $price, package_price: $package_price, sales: $sales, sale_type: $sale_type, sale_sku: $sale_sku, sale_count: $sale_count, orderby: $orderby, closed: $closed, is_onsale: $is_onsale, unit: $unit, is_format: $is_format, is_addon: $is_addon, addon_sale: $addon_sale, bar_code: $bar_code, is_spec: $is_spec, formats: $formats, attributes: $attributes)';
  }

  @override
  bool operator ==(covariant Product other) {
    if (identical(this, other)) return true;

    return other.product_id == product_id &&
        other.shop_id == shop_id &&
        other.cateId == cateId &&
        other.title == title &&
        other.secondary_title == secondary_title &&
        other.thumb == thumb &&
        other.price == price &&
        other.package_price == package_price &&
        other.sales == sales &&
        other.sale_type == sale_type &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.orderby == orderby &&
        other.closed == closed &&
        other.is_onsale == is_onsale &&
        other.unit == unit &&
        other.is_format == is_format &&
        other.is_addon == is_addon &&
        other.addon_sale == addon_sale &&
        other.bar_code == bar_code &&
        other.is_spec == is_spec &&
        listEquals(other.formats, formats) &&
        listEquals(other.attributes, attributes);
  }

  @override
  int get hashCode {
    return product_id.hashCode ^
        shop_id.hashCode ^
        cateId.hashCode ^
        title.hashCode ^
        secondary_title.hashCode ^
        thumb.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        sales.hashCode ^
        sale_type.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        orderby.hashCode ^
        closed.hashCode ^
        is_onsale.hashCode ^
        unit.hashCode ^
        is_format.hashCode ^
        is_addon.hashCode ^
        addon_sale.hashCode ^
        bar_code.hashCode ^
        is_spec.hashCode ^
        formats.hashCode ^
        attributes.hashCode;
  }
}

class ProductFormat {
  String? spec_id;
  String? product_id;
  String? price;
  String? package_price;
  String? spec_name;
  String? spec_photo;
  String? sale_sku;
  String? sale_count;
  String? sale_type;
  String? bar_code;
  String? old_price;

  ProductFormat({
    required this.spec_id,
    this.product_id,
    this.price,
    this.package_price,
    this.spec_name,
    this.spec_photo,
    this.sale_sku,
    this.sale_count,
    this.sale_type,
    this.bar_code,
    this.old_price,
  });

  ProductFormat copyWith({
    String? spec_id,
    String? product_id,
    String? price,
    String? package_price,
    String? spec_name,
    String? spec_photo,
    String? sale_sku,
    String? sale_count,
    String? sale_type,
    String? bar_code,
    String? old_price,
  }) {
    return ProductFormat(
      spec_id: spec_id ?? this.spec_id,
      product_id: product_id ?? this.product_id,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      spec_name: spec_name ?? this.spec_name,
      spec_photo: spec_photo ?? this.spec_photo,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      sale_type: sale_type ?? this.sale_type,
      bar_code: bar_code ?? this.bar_code,
      old_price: old_price ?? this.old_price,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'spec_id': spec_id,
      'product_id': product_id,
      'price': price,
      'package_price': package_price,
      'spec_name': spec_name,
      'spec_photo': spec_photo,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'sale_type': sale_type,
      'bar_code': bar_code,
      'old_price': old_price,
    };
  }

  factory ProductFormat.fromMap(Map<String, dynamic> map) {
    return ProductFormat(
      spec_id: map['spec_id'] != null ? map['spec_id'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      price: map['price'] != null ? map['price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      spec_name: map['spec_name'] != null ? map['spec_name'] as String : null,
      spec_photo:
          map['spec_photo'] != null ? map['spec_photo'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      old_price: map['old_price'] != null ? map['old_price'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductFormat.fromJson(String source) =>
      ProductFormat.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductFormat(spec_id: $spec_id, product_id: $product_id, price: $price, package_price: $package_price, spec_name: $spec_name, spec_photo: $spec_photo, sale_sku: $sale_sku, sale_count: $sale_count, sale_type: $sale_type, bar_code: $bar_code, old_price: $old_price)';
  }

  @override
  bool operator ==(covariant ProductFormat other) {
    if (identical(this, other)) return true;

    return other.spec_id == spec_id &&
        other.product_id == product_id &&
        other.price == price &&
        other.package_price == package_price &&
        other.spec_name == spec_name &&
        other.spec_photo == spec_photo &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.sale_type == sale_type &&
        other.bar_code == bar_code &&
        other.old_price == old_price;
  }

  @override
  int get hashCode {
    return spec_id.hashCode ^
        product_id.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        spec_name.hashCode ^
        spec_photo.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        sale_type.hashCode ^
        bar_code.hashCode ^
        old_price.hashCode;
  }
}

class ProductAttribute {
  String? key;
  List<dynamic>? val;

  ProductAttribute({
    this.key,
    this.val,
  });

  ProductAttribute copyWith({
    String? key,
    List<dynamic>? val,
  }) {
    return ProductAttribute(
      key: key ?? this.key,
      val: val ?? this.val,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'key': key,
      'val': val,
    };
  }

  factory ProductAttribute.fromMap(Map<String, dynamic> map) {
    return ProductAttribute(
      key: map['key'] != null ? map['key'] as String : null,
      val: map['val'] != null
          ? List<dynamic>.from((map['val'] as List<dynamic>))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductAttribute.fromJson(String source) =>
      ProductAttribute.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'ProductAttribute(key: $key, val: $val)';

  @override
  bool operator ==(covariant ProductAttribute other) {
    if (identical(this, other)) return true;

    return other.key == key && listEquals(other.val, val);
  }

  @override
  int get hashCode => key.hashCode ^ val.hashCode;
}
