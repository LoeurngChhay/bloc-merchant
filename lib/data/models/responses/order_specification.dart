// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class OrderSpecific {
  String? key;
  String? val;
  OrderSpecific({
    this.key,
    this.val,
  });

  OrderSpecific copyWith({
    String? key,
    String? val,
  }) {
    return OrderSpecific(
      key: key ?? this.key,
      val: val ?? this.val,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'key': key,
      'val': val,
    };
  }

  factory OrderSpecific.fromMap(Map<String, dynamic> map) {
    return OrderSpecific(
      key: map['key'] != null ? map['key'] as String : null,
      val: map['val'] != null ? map['val'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderSpecific.fromJson(String source) =>
      OrderSpecific.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'OrderSpecific(key: $key, val: $val)';

  @override
  bool operator ==(covariant OrderSpecific other) {
    if (identical(this, other)) return true;

    return other.key == key && other.val == val;
  }

  @override
  int get hashCode => key.hashCode ^ val.hashCode;
}
