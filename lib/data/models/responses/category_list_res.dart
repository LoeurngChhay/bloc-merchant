// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class CategoryList {
  List<Category>? items;
  int? total;
  int? limit;

  CategoryList({
    this.items,
    this.total,
    this.limit,
  });

  CategoryList copyWith({
    List<Category>? items,
    int? total,
    int? limit,
  }) {
    return CategoryList(
      items: items ?? this.items,
      total: total ?? this.total,
      limit: limit ?? this.limit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'items': items?.map((x) => x.toMap()).toList(),
      'total': total,
      'limit': limit,
    };
  }

  factory CategoryList.fromMap(Map<String, dynamic> map) {
    return CategoryList(
      items: map['items'] != null
          ? List<Category>.from(
              (map['items'] as List<dynamic>).map<Category?>(
                (x) => Category.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      total: map['total'] != null ? map['total'] as int : null,
      limit: map['limit'] != null ? map['limit'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryList.fromJson(String source) =>
      CategoryList.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'CategoryList(items: $items, total: $total, limit: $limit)';

  @override
  bool operator ==(covariant CategoryList other) {
    if (identical(this, other)) return true;

    return listEquals(other.items, items) &&
        other.total == total &&
        other.limit == limit;
  }

  @override
  int get hashCode => items.hashCode ^ total.hashCode ^ limit.hashCode;
}

class Category {
  String? shop_id;
  String? cate_id;
  String? title;
  String? icon;
  String? dateline;
  String? orderby;
  String? show_type;
  String? item_count;

  Category({
    this.shop_id,
    this.cate_id,
    this.title,
    this.icon,
    this.dateline,
    this.orderby,
    this.show_type,
    this.item_count,
  });

  Category copyWith({
    String? shop_id,
    String? cate_id,
    String? title,
    String? icon,
    String? dateline,
    String? orderby,
    String? show_type,
    String? item_count,
  }) {
    return Category(
      shop_id: shop_id ?? this.shop_id,
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      icon: icon ?? this.icon,
      dateline: dateline ?? this.dateline,
      orderby: orderby ?? this.orderby,
      show_type: show_type ?? this.show_type,
      item_count: item_count ?? this.item_count,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'cate_id': cate_id,
      'title': title,
      'icon': icon,
      'dateline': dateline,
      'orderby': orderby,
      'show_type': show_type,
      'item_count': item_count,
    };
  }

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      icon: map['icon'] != null ? map['icon'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      show_type: map['show_type'] != null ? map['show_type'] as String : null,
      item_count:
          map['item_count'] != null ? map['item_count'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Category.fromJson(String source) =>
      Category.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Category(shop_id: $shop_id, cate_id: $cate_id, title: $title, icon: $icon, dateline: $dateline, orderby: $orderby, show_type: $show_type, item_count: $item_count)';
  }

  @override
  bool operator ==(covariant Category other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.cate_id == cate_id &&
        other.title == title &&
        other.icon == icon &&
        other.dateline == dateline &&
        other.orderby == orderby &&
        other.show_type == show_type &&
        other.item_count == item_count;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        cate_id.hashCode ^
        title.hashCode ^
        icon.hashCode ^
        dateline.hashCode ^
        orderby.hashCode ^
        show_type.hashCode ^
        item_count.hashCode;
  }
}
