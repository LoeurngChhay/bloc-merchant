// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class HomeInfo {
  String? shop_id;
  String? operation_status;
  int? today_sale;
  int? yesterday_sale;
  String? pending_order;
  String? verify_name;
  String? title;
  String? logo;
  String? contact;
  String? mobile;
  String? addr;
  String? is_mall;

  HomeInfo({
    this.shop_id,
    this.operation_status,
    this.today_sale,
    this.yesterday_sale,
    this.pending_order,
    this.verify_name,
    this.title,
    this.logo,
    this.contact,
    this.mobile,
    this.addr,
    this.is_mall,
  });

  @override
  String toString() {
    return 'HomeInfo(shop_id: $shop_id, operation_status: $operation_status, today_sale: $today_sale, yesterday_sale: $yesterday_sale, pending_order: $pending_order, verify_name: $verify_name, title: $title, logo: $logo, contact: $contact, mobile: $mobile, addr: $addr, is_mall: $is_mall)';
  }

  HomeInfo copyWith({
    String? shop_id,
    String? operation_status,
    int? today_sale,
    int? yesterday_sale,
    String? pending_order,
    String? verify_name,
    String? title,
    String? logo,
    String? contact,
    String? mobile,
    String? addr,
    String? is_mall,
  }) {
    return HomeInfo(
      shop_id: shop_id ?? this.shop_id,
      operation_status: operation_status ?? this.operation_status,
      today_sale: today_sale ?? this.today_sale,
      yesterday_sale: yesterday_sale ?? this.yesterday_sale,
      pending_order: pending_order ?? this.pending_order,
      verify_name: verify_name ?? this.verify_name,
      title: title ?? this.title,
      logo: logo ?? this.logo,
      contact: contact ?? this.contact,
      mobile: mobile ?? this.mobile,
      addr: addr ?? this.addr,
      is_mall: is_mall ?? this.is_mall,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'operation_status': operation_status,
      'today_sale': today_sale,
      'yesterday_sale': yesterday_sale,
      'pending_order': pending_order,
      'verify_name': verify_name,
      'title': title,
      'logo': logo,
      'contact': contact,
      'mobile': mobile,
      'addr': addr,
      'is_mall': is_mall,
    };
  }

  factory HomeInfo.fromMap(Map<String, dynamic> map) {
    return HomeInfo(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      operation_status: map['operation_status'] != null
          ? map['operation_status'] as String
          : null,
      today_sale: map['today_sale'] != null ? map['today_sale'] as int : null,
      yesterday_sale:
          map['yesterday_sale'] != null ? map['yesterday_sale'] as int : null,
      pending_order:
          map['pending_order'] != null ? map['pending_order'] as String : null,
      verify_name:
          map['verify_name'] != null ? map['verify_name'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      logo: map['logo'] != null ? map['logo'] as String : null,
      contact: map['contact'] != null ? map['contact'] as String : null,
      mobile: map['mobile'] != null ? map['mobile'] as String : null,
      addr: map['addr'] != null ? map['addr'] as String : null,
      is_mall: map['is_mall'] != null ? map['is_mall'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory HomeInfo.fromJson(String source) =>
      HomeInfo.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool operator ==(covariant HomeInfo other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.operation_status == operation_status &&
        other.today_sale == today_sale &&
        other.yesterday_sale == yesterday_sale &&
        other.pending_order == pending_order &&
        other.verify_name == verify_name &&
        other.title == title &&
        other.logo == logo &&
        other.contact == contact &&
        other.mobile == mobile &&
        other.addr == addr &&
        other.is_mall == is_mall;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        operation_status.hashCode ^
        today_sale.hashCode ^
        yesterday_sale.hashCode ^
        pending_order.hashCode ^
        verify_name.hashCode ^
        title.hashCode ^
        logo.hashCode ^
        contact.hashCode ^
        mobile.hashCode ^
        addr.hashCode ^
        is_mall.hashCode;
  }
}
