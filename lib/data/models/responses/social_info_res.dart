// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class SocialInfo {
  String? social_uid;
  String? name;
  String? total_follow;
  String? total_view;
  String? total_like;

  SocialInfo({
    this.social_uid,
    this.name,
    this.total_follow,
    this.total_view,
    this.total_like,
  });

  SocialInfo copyWith({
    String? social_uid,
    String? name,
    String? total_follow,
    String? total_view,
    String? total_like,
  }) {
    return SocialInfo(
      social_uid: social_uid ?? this.social_uid,
      name: name ?? this.name,
      total_follow: total_follow ?? this.total_follow,
      total_view: total_view ?? this.total_view,
      total_like: total_like ?? this.total_like,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'social_uid': social_uid,
      'name': name,
      'total_follow': total_follow,
      'total_view': total_view,
      'total_like': total_like,
    };
  }

  factory SocialInfo.fromMap(Map<String, dynamic> map) {
    return SocialInfo(
      social_uid:
          map['social_uid'] != null ? map['social_uid'] as String : null,
      name: map['name'] != null ? map['name'] as String : null,
      total_follow:
          map['total_follow'] != null ? map['total_follow'] as String : null,
      total_view:
          map['total_view'] != null ? map['total_view'] as String : null,
      total_like:
          map['total_like'] != null ? map['total_like'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory SocialInfo.fromJson(String source) =>
      SocialInfo.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'SocialInfo(social_uid: $social_uid, name: $name, total_follow: $total_follow, total_view: $total_view, total_like: $total_like)';
  }

  @override
  bool operator ==(covariant SocialInfo other) {
    if (identical(this, other)) return true;

    return other.social_uid == social_uid &&
        other.name == name &&
        other.total_follow == total_follow &&
        other.total_view == total_view &&
        other.total_like == total_like;
  }

  @override
  int get hashCode {
    return social_uid.hashCode ^
        name.hashCode ^
        total_follow.hashCode ^
        total_view.hashCode ^
        total_like.hashCode;
  }
}
