// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_addon.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_video.dart';

class ProductDetail {
  String? product_id;
  String? shop_id;
  String? cate_id;
  String? title;
  String? secondary_title;
  String? photo;
  String? thumb;
  String? raw_photo;
  num? price;
  num? package_price;
  String? sales;
  String? sale_type;
  String? sale_sku;
  String? sale_count;
  String? orderby;
  String? closed;
  String? is_onsale;
  String? is_format;
  String? unit;
  String? addon_product;
  String? bar_code;
  String? total_item;
  List<Format>? formats;
  List<ProductImages>? images;
  List<Attribute>? attributes;
  String? description;
  String? is_addon;
  String? addon_sale;
  String? is_spec;
  String? is_favorite;
  String? expired_date;
  List<ProductAddons>? addons;
  ProductVideo? video;

  ProductDetail({
    this.product_id,
    this.shop_id,
    this.cate_id,
    this.title,
    this.secondary_title,
    this.photo,
    this.thumb,
    this.raw_photo,
    this.price,
    this.package_price,
    this.sales,
    this.sale_type,
    this.sale_sku,
    this.sale_count,
    this.orderby,
    this.closed,
    this.is_onsale,
    this.is_format,
    this.unit,
    this.addon_product,
    this.bar_code,
    this.total_item,
    this.formats,
    this.images,
    this.attributes,
    this.description,
    this.is_addon,
    this.addon_sale,
    this.is_spec,
    this.is_favorite,
    this.expired_date,
    this.addons,
    this.video,
  });

  ProductDetail copyWith({
    String? product_id,
    String? shop_id,
    String? cate_id,
    String? title,
    String? secondary_title,
    String? photo,
    String? thumb,
    String? raw_photo,
    num? price,
    num? package_price,
    String? sales,
    String? sale_type,
    String? sale_sku,
    String? sale_count,
    String? orderby,
    String? closed,
    String? is_onsale,
    String? is_format,
    String? unit,
    String? addon_product,
    String? bar_code,
    String? total_item,
    List<Format>? formats,
    List<ProductImages>? images,
    List<Attribute>? attributes,
    String? description,
    String? is_addon,
    String? addon_sale,
    String? is_spec,
    String? is_favorite,
    String? expired_date,
    List<ProductAddons>? addons,
    ProductVideo? video,
  }) {
    return ProductDetail(
      product_id: product_id ?? this.product_id,
      shop_id: shop_id ?? this.shop_id,
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      secondary_title: secondary_title ?? this.secondary_title,
      photo: photo ?? this.photo,
      thumb: thumb ?? this.thumb,
      raw_photo: raw_photo ?? this.raw_photo,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      sales: sales ?? this.sales,
      sale_type: sale_type ?? this.sale_type,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      orderby: orderby ?? this.orderby,
      closed: closed ?? this.closed,
      is_onsale: is_onsale ?? this.is_onsale,
      is_format: is_format ?? this.is_format,
      unit: unit ?? this.unit,
      addon_product: addon_product ?? this.addon_product,
      bar_code: bar_code ?? this.bar_code,
      total_item: total_item ?? this.total_item,
      formats: formats ?? this.formats,
      images: images ?? this.images,
      attributes: attributes ?? this.attributes,
      description: description ?? this.description,
      is_addon: is_addon ?? this.is_addon,
      addon_sale: addon_sale ?? this.addon_sale,
      is_spec: is_spec ?? this.is_spec,
      is_favorite: is_favorite ?? this.is_favorite,
      expired_date: expired_date ?? this.expired_date,
      addons: addons ?? this.addons,
      video: video ?? this.video,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'product_id': product_id,
      'shop_id': shop_id,
      'cate_id': cate_id,
      'title': title,
      'secondary_title': secondary_title,
      'photo': photo,
      'thumb': thumb,
      'raw_photo': raw_photo,
      'price': price,
      'package_price': package_price,
      'sales': sales,
      'sale_type': sale_type,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'orderby': orderby,
      'closed': closed,
      'is_onsale': is_onsale,
      'is_format': is_format,
      'unit': unit,
      'addon_product': addon_product,
      'bar_code': bar_code,
      'total_item': total_item,
      'formats': formats?.map((x) => x.toMap()).toList(),
      'images': images?.map((x) => x.toMap()).toList(),
      'attributes': attributes?.map((x) => x.toMap()).toList(),
      'description': description,
      'is_addon': is_addon,
      'addon_sale': addon_sale,
      'is_spec': is_spec,
      'is_favorite': is_favorite,
      'expired_date': expired_date,
      'addons': addons?.map((x) => x.toMap()).toList(),
      'video': video?.toMap(),
    };
  }

  factory ProductDetail.fromMap(Map<String, dynamic> map) {
    return ProductDetail(
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      secondary_title: map['secondary_title'] != null
          ? map['secondary_title'] as String
          : null,
      photo: map['photo'] != null ? map['photo'] as String : null,
      thumb: map['thumb'] != null ? map['thumb'] as String : null,
      raw_photo: map['raw_photo'] != null ? map['raw_photo'] as String : null,
      price: map['price'] != null ? map['price'] as num : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as num : null,
      sales: map['sales'] != null ? map['sales'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      closed: map['closed'] != null ? map['closed'] as String : null,
      is_onsale: map['is_onsale'] != null ? map['is_onsale'] as String : null,
      is_format: map['is_format'] != null ? map['is_format'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      addon_product:
          map['addon_product'] != null ? map['addon_product'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      total_item:
          map['total_item'] != null ? map['total_item'] as String : null,
      formats: map['formats'] != null
          ? List<Format>.from(
              (map['formats'] as List<dynamic>).map<Format?>(
                (x) => Format.fromMap(x as Map<String, dynamic>),
              ),
            )
          : [],
      images: map['images'] != null
          ? List<ProductImages>.from(
              (map['images'] as List<dynamic>).map<ProductImages?>(
                (x) => ProductImages.fromMap(x as Map<String, dynamic>),
              ),
            )
          : [],
      attributes: map['attributes'] != null
          ? List<Attribute>.from(
              (map['attributes'] as List<dynamic>).map<Attribute?>(
                (x) => Attribute.fromMap(x as Map<String, dynamic>),
              ),
            )
          : [],
      description:
          map['description'] != null ? map['description'] as String : null,
      is_addon: map['is_addon'] != null ? map['is_addon'] as String : null,
      addon_sale:
          map['addon_sale'] != null ? map['addon_sale'] as String : null,
      is_spec: map['is_spec'] != null ? map['is_spec'] as String : null,
      is_favorite:
          map['is_favorite'] != null ? map['is_favorite'] as String : null,
      expired_date: map['expired_date'] != null
          ? map['expired_date'] is int && (map['expired_date'] as int) == 0
              ? "0"
              : map['expired_date'] as String
          : null,
      addons: map['addons'] != null
          ? List<ProductAddons>.from(
              (map['addons'] as List<dynamic>).map<ProductAddons?>(
                (x) => ProductAddons.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      video: map['video'] is List && (map['video'] as List).isEmpty
          ? null
          : ProductVideo.fromMap(map['video'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductDetail.fromJson(String source) =>
      ProductDetail.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductDetail(product_id: $product_id, shop_id: $shop_id, cate_id: $cate_id, title: $title, secondary_title: $secondary_title, photo: $photo, thumb: $thumb, raw_photo: $raw_photo, price: $price, package_price: $package_price, sales: $sales, sale_type: $sale_type, sale_sku: $sale_sku, sale_count: $sale_count, orderby: $orderby, closed: $closed, is_onsale: $is_onsale, is_format: $is_format, unit: $unit, addon_product: $addon_product, bar_code: $bar_code, total_item: $total_item, formats: $formats, images: $images, attributes: $attributes, description: $description, is_addon: $is_addon, addon_sale: $addon_sale, is_spec: $is_spec, is_favorite: $is_favorite, expired_date: $expired_date, addons: $addons, video: $video)';
  }

  @override
  bool operator ==(covariant ProductDetail other) {
    if (identical(this, other)) return true;

    return other.product_id == product_id &&
        other.shop_id == shop_id &&
        other.cate_id == cate_id &&
        other.title == title &&
        other.secondary_title == secondary_title &&
        other.photo == photo &&
        other.thumb == thumb &&
        other.raw_photo == raw_photo &&
        other.price == price &&
        other.package_price == package_price &&
        other.sales == sales &&
        other.sale_type == sale_type &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.orderby == orderby &&
        other.closed == closed &&
        other.is_onsale == is_onsale &&
        other.is_format == is_format &&
        other.unit == unit &&
        other.addon_product == addon_product &&
        other.bar_code == bar_code &&
        other.total_item == total_item &&
        listEquals(other.formats, formats) &&
        listEquals(other.images, images) &&
        listEquals(other.attributes, attributes) &&
        other.description == description &&
        other.is_addon == is_addon &&
        other.addon_sale == addon_sale &&
        other.is_spec == is_spec &&
        other.is_favorite == is_favorite &&
        other.expired_date == expired_date &&
        listEquals(other.addons, addons) &&
        other.video == video;
  }

  @override
  int get hashCode {
    return product_id.hashCode ^
        shop_id.hashCode ^
        cate_id.hashCode ^
        title.hashCode ^
        secondary_title.hashCode ^
        photo.hashCode ^
        thumb.hashCode ^
        raw_photo.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        sales.hashCode ^
        sale_type.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        orderby.hashCode ^
        closed.hashCode ^
        is_onsale.hashCode ^
        is_format.hashCode ^
        unit.hashCode ^
        addon_product.hashCode ^
        bar_code.hashCode ^
        total_item.hashCode ^
        formats.hashCode ^
        images.hashCode ^
        attributes.hashCode ^
        description.hashCode ^
        is_addon.hashCode ^
        addon_sale.hashCode ^
        is_spec.hashCode ^
        is_favorite.hashCode ^
        expired_date.hashCode ^
        addons.hashCode ^
        video.hashCode;
  }
}
