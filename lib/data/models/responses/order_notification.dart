class OrderNotifcation {
  String? alert;
  String? sound;
  String? contentAvailable;
  String? priority;
  String? body;
  String? androidChannelId;

  OrderNotifcation(
      {this.alert,
      this.sound,
      this.contentAvailable,
      this.priority,
      this.body,
      this.androidChannelId});

  OrderNotifcation.fromJson(Map<String, dynamic> json) {
    alert = json['alert'];
    sound = json['sound'];
    contentAvailable = json['content-available'];
    priority = json['priority'];
    body = json['body'];
    androidChannelId = json['android_channel_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['alert'] = alert;
    data['sound'] = sound;
    data['content-available'] = contentAvailable;
    data['priority'] = priority;
    data['body'] = body;
    data['android_channel_id'] = androidChannelId;
    return data;
  }
}
