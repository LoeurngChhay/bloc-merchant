// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class ProductCategoryList {
  List<ProCategory?>? items;
  int? total;
  int? limit;

  ProductCategoryList({
    this.items,
    this.total,
    this.limit,
  });

  ProductCategoryList copyWith({
    List<ProCategory?>? items,
    int? total,
    int? limit,
  }) {
    return ProductCategoryList(
      items: items ?? this.items,
      total: total ?? this.total,
      limit: limit ?? this.limit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'items': items?.map((x) => x?.toMap()).toList(),
      'total': total,
      'limit': limit,
    };
  }

  factory ProductCategoryList.fromMap(Map<String, dynamic> map) {
    return ProductCategoryList(
      items: map['items'] != null
          ? List<ProCategory?>.from(
              (map['items'] as List<dynamic>).map<ProCategory?>(
                (x) => ProCategory?.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      total: map['total'] != null ? map['total'] as int : null,
      limit: map['limit'] != null ? map['limit'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductCategoryList.fromJson(String source) =>
      ProductCategoryList.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'ProductCategoryList(items: $items, total: $total, limit: $limit)';

  @override
  bool operator ==(covariant ProductCategoryList other) {
    if (identical(this, other)) return true;

    return listEquals(other.items, items) &&
        other.total == total &&
        other.limit == limit;
  }

  @override
  int get hashCode => items.hashCode ^ total.hashCode ^ limit.hashCode;
}

class ProCategory {
  String? shop_id;
  String? cate_id;
  String? title;
  String? icon;
  String? dateline;
  String? orderby;

  ProCategory({
    this.shop_id,
    this.cate_id,
    this.title,
    this.icon,
    this.dateline,
    this.orderby,
  });

  ProCategory copyWith({
    String? shop_id,
    String? cate_id,
    String? title,
    String? icon,
    String? dateline,
    String? orderby,
  }) {
    return ProCategory(
      shop_id: shop_id ?? this.shop_id,
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      icon: icon ?? this.icon,
      dateline: dateline ?? this.dateline,
      orderby: orderby ?? this.orderby,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'cate_id': cate_id,
      'title': title,
      'icon': icon,
      'dateline': dateline,
      'orderby': orderby,
    };
  }

  factory ProCategory.fromMap(Map<String, dynamic> map) {
    return ProCategory(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      icon: map['icon'] != null ? map['icon'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProCategory.fromJson(String source) =>
      ProCategory.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProCategory(shop_id: $shop_id, cate_id: $cate_id, title: $title, icon: $icon, dateline: $dateline, orderby: $orderby)';
  }

  @override
  bool operator ==(covariant ProCategory other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.cate_id == cate_id &&
        other.title == title &&
        other.icon == icon &&
        other.dateline == dateline &&
        other.orderby == orderby;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        cate_id.hashCode ^
        title.hashCode ^
        icon.hashCode ^
        dateline.hashCode ^
        orderby.hashCode;
  }
}
