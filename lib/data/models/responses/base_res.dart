// ignore_for_file: public_member_api_docs, sort_constructors_first
class BaseResponse<T> {
  final T? data;
  final int error;
  final String message;

  BaseResponse({
    this.data,
    required this.error,
    required this.message,
  });

  factory BaseResponse.fromMap(Map<String, dynamic> map, T data) {
    return BaseResponse<T>(
      data: data,
      error: map['error'] as int,
      message: map['message'] as String,
    );
  }

  @override
  String toString() =>
      'BaseResponse(data: $data, error: $error, message: $message)';
}
