// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class OrderList {
  List<Order>? items;
  int? total;
  int? limit;

  OrderList({
    required this.items,
    this.total,
    this.limit,
  });

  OrderList copyWith({
    List<Order>? items,
    int? total,
    int? limit,
  }) {
    return OrderList(
      items: items ?? this.items,
      total: total ?? this.total,
      limit: limit ?? this.limit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'items': items?.map((x) => x.toMap()).toList(),
      'total': total,
      'limit': limit,
    };
  }

  factory OrderList.fromMap(Map<String, dynamic> map) {
    return OrderList(
      items: List<Order>.from(
        (map['items'] as List<dynamic>).map<Order?>(
          (x) => Order.fromMap(x as Map<String, dynamic>),
        ),
      ),
      total: map['total'] != null ? map['total'] as int : null,
      limit: map['limit'] != null ? map['limit'] as int : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderList.fromJson(String source) =>
      OrderList.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'OrderList(items: $items, total: $total, limit: $limit)';

  @override
  bool operator ==(covariant OrderList other) {
    if (identical(this, other)) return true;

    return listEquals(other.items, items) &&
        other.total == total &&
        other.limit == limit;
  }

  @override
  int get hashCode => items.hashCode ^ total.hashCode ^ limit.hashCode;
}

class Order {
  String? order_id;
  String? order_time;
  Customer? customer;
  String? driver_id;
  Driver? driver;
  String? product_price;
  String? package_price;
  String? pei_time_label;
  String? queue_number;
  String? receiver_contact;
  String? receiver_mobile;
  String? reveiver_adrress;
  String? payment;
  String? payment_status;
  String? remark;
  String? order_status;
  String? order_type;
  String? order_type_label;
  String? order_status_label;
  String? pin_code;
  String? total_price;
  String? pei_amount;
  num? bag_price;
  num? discount_amount;
  num? vat_amount;
  num? commission_fee;
  num? shop_receive;

  Order({
    this.order_id,
    this.order_time,
    this.customer,
    this.driver_id,
    this.driver,
    this.product_price,
    this.package_price,
    this.pei_time_label,
    this.queue_number,
    this.receiver_contact,
    this.receiver_mobile,
    this.reveiver_adrress,
    this.payment,
    this.payment_status,
    this.remark,
    this.order_status,
    this.order_type,
    this.order_type_label,
    this.order_status_label,
    this.pin_code,
    this.total_price,
    this.pei_amount,
    this.bag_price,
    this.discount_amount,
    this.vat_amount,
    this.commission_fee,
    this.shop_receive,
  });

  Order copyWith({
    String? order_id,
    String? order_time,
    Customer? customer,
    String? driver_id,
    Driver? driver,
    String? product_price,
    String? package_price,
    String? pei_time_label,
    String? queue_number,
    String? receiver_contact,
    String? receiver_mobile,
    String? reveiver_adrress,
    String? payment,
    String? payment_status,
    String? remark,
    String? order_status,
    String? order_type,
    String? order_type_label,
    String? order_status_label,
    String? pin_code,
    String? total_price,
    String? pei_amount,
    num? bag_price,
    num? discount_amount,
    num? vat_amount,
    num? commission_fee,
    num? shop_receive,
  }) {
    return Order(
      order_id: order_id ?? this.order_id,
      order_time: order_time ?? this.order_time,
      customer: customer ?? this.customer,
      driver_id: driver_id ?? this.driver_id,
      driver: driver ?? this.driver,
      product_price: product_price ?? this.product_price,
      package_price: package_price ?? this.package_price,
      pei_time_label: pei_time_label ?? this.pei_time_label,
      queue_number: queue_number ?? this.queue_number,
      receiver_contact: receiver_contact ?? this.receiver_contact,
      receiver_mobile: receiver_mobile ?? this.receiver_mobile,
      reveiver_adrress: reveiver_adrress ?? this.reveiver_adrress,
      payment: payment ?? this.payment,
      payment_status: payment_status ?? this.payment_status,
      remark: remark ?? this.remark,
      order_status: order_status ?? this.order_status,
      order_type: order_type ?? this.order_type,
      order_type_label: order_type_label ?? this.order_type_label,
      order_status_label: order_status_label ?? this.order_status_label,
      pin_code: pin_code ?? this.pin_code,
      total_price: total_price ?? this.total_price,
      pei_amount: pei_amount ?? this.pei_amount,
      bag_price: bag_price ?? this.bag_price,
      discount_amount: discount_amount ?? this.discount_amount,
      vat_amount: vat_amount ?? this.vat_amount,
      commission_fee: commission_fee ?? this.commission_fee,
      shop_receive: shop_receive ?? this.shop_receive,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'order_id': order_id,
      'order_time': order_time,
      'customer': customer?.toMap(),
      'driver_id': driver_id,
      'driver': driver?.toMap(),
      'product_price': product_price,
      'package_price': package_price,
      'pei_time_label': pei_time_label,
      'queue_number': queue_number,
      'receiver_contact': receiver_contact,
      'receiver_mobile': receiver_mobile,
      'reveiver_adrress': reveiver_adrress,
      'payment': payment,
      'payment_status': payment_status,
      'remark': remark,
      'order_status': order_status,
      'order_type': order_type,
      'order_type_label': order_type_label,
      'order_status_label': order_status_label,
      'pin_code': pin_code,
      'total_price': total_price,
      'pei_amount': pei_amount,
      'bag_price': bag_price,
      'discount_amount': discount_amount,
      'vat_amount': vat_amount,
      'commission_fee': commission_fee,
      'shop_receive': shop_receive,
    };
  }

  factory Order.fromMap(Map<String, dynamic> map) {
    return Order(
      order_id: map['order_id'] != null ? map['order_id'] as String : null,
      order_time:
          map['order_time'] != null ? map['order_time'] as String : null,
      customer: map['customer'] != null
          ? Customer.fromMap(map['customer'] as Map<String, dynamic>)
          : null,
      driver_id: map['driver_id'] != null ? map['driver_id'] as String : null,
      driver: map['driver'] != null
          ? Driver.fromMap(map['driver'] as Map<String, dynamic>)
          : null,
      product_price:
          map['product_price'] != null ? map['product_price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      pei_time_label: map['pei_time_label'] != null
          ? map['pei_time_label'] as String
          : null,
      queue_number:
          map['queue_number'] != null ? map['queue_number'] as String : null,
      receiver_contact: map['receiver_contact'] != null
          ? map['receiver_contact'] as String
          : null,
      receiver_mobile: map['receiver_mobile'] != null
          ? map['receiver_mobile'] as String
          : null,
      reveiver_adrress: map['reveiver_adrress'] != null
          ? map['reveiver_adrress'] as String
          : null,
      payment: map['payment'] != null ? map['payment'] as String : null,
      payment_status: map['payment_status'] != null
          ? map['payment_status'] as String
          : null,
      remark: map['remark'] != null ? map['remark'] as String : null,
      order_status:
          map['order_status'] != null ? map['order_status'] as String : null,
      order_type:
          map['order_type'] != null ? map['order_type'] as String : null,
      order_type_label: map['order_type_label'] != null
          ? map['order_type_label'] as String
          : null,
      order_status_label: map['order_status_label'] != null
          ? map['order_status_label'] as String
          : null,
      pin_code: map['pin_code'] != null ? map['pin_code'] as String : null,
      total_price:
          map['total_price'] != null ? map['total_price'] as String : null,
      pei_amount:
          map['pei_amount'] != null ? map['pei_amount'] as String : null,
      bag_price: map['bag_price'] != null ? map['bag_price'] as num : null,
      discount_amount:
          map['discount_amount'] != null ? map['discount_amount'] as num : null,
      vat_amount: map['vat_amount'] != null ? map['vat_amount'] as num : null,
      commission_fee:
          map['commission_fee'] != null ? map['commission_fee'] as num : null,
      shop_receive:
          map['shop_receive'] != null ? map['shop_receive'] as num : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Order.fromJson(String source) =>
      Order.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Order(order_id: $order_id, order_time: $order_time, customer: $customer, driver_id: $driver_id, driver: $driver, product_price: $product_price, package_price: $package_price, pei_time_label: $pei_time_label, queue_number: $queue_number, receiver_contact: $receiver_contact, receiver_mobile: $receiver_mobile, reveiver_adrress: $reveiver_adrress, payment: $payment, payment_status: $payment_status, remark: $remark, order_status: $order_status, order_type: $order_type, order_type_label: $order_type_label, order_status_label: $order_status_label, pin_code: $pin_code, total_price: $total_price, pei_amount: $pei_amount, bag_price: $bag_price, discount_amount: $discount_amount, vat_amount: $vat_amount, commission_fee: $commission_fee, shop_receive: $shop_receive)';
  }

  @override
  bool operator ==(covariant Order other) {
    if (identical(this, other)) return true;

    return other.order_id == order_id &&
        other.order_time == order_time &&
        other.customer == customer &&
        other.driver_id == driver_id &&
        other.driver == driver &&
        other.product_price == product_price &&
        other.package_price == package_price &&
        other.pei_time_label == pei_time_label &&
        other.queue_number == queue_number &&
        other.receiver_contact == receiver_contact &&
        other.receiver_mobile == receiver_mobile &&
        other.reveiver_adrress == reveiver_adrress &&
        other.payment == payment &&
        other.payment_status == payment_status &&
        other.remark == remark &&
        other.order_status == order_status &&
        other.order_type == order_type &&
        other.order_type_label == order_type_label &&
        other.order_status_label == order_status_label &&
        other.pin_code == pin_code &&
        other.total_price == total_price &&
        other.pei_amount == pei_amount &&
        other.bag_price == bag_price &&
        other.discount_amount == discount_amount &&
        other.vat_amount == vat_amount &&
        other.commission_fee == commission_fee &&
        other.shop_receive == shop_receive;
  }

  @override
  int get hashCode {
    return order_id.hashCode ^
        order_time.hashCode ^
        customer.hashCode ^
        driver_id.hashCode ^
        driver.hashCode ^
        product_price.hashCode ^
        package_price.hashCode ^
        pei_time_label.hashCode ^
        queue_number.hashCode ^
        receiver_contact.hashCode ^
        receiver_mobile.hashCode ^
        reveiver_adrress.hashCode ^
        payment.hashCode ^
        payment_status.hashCode ^
        remark.hashCode ^
        order_status.hashCode ^
        order_type.hashCode ^
        order_type_label.hashCode ^
        order_status_label.hashCode ^
        pin_code.hashCode ^
        total_price.hashCode ^
        pei_amount.hashCode ^
        bag_price.hashCode ^
        discount_amount.hashCode ^
        vat_amount.hashCode ^
        commission_fee.hashCode ^
        shop_receive.hashCode;
  }
}

class Customer {
  String? uid;
  String? mobile;
  String? nickname;
  String? face;
  String? order_count;

  Customer({
    this.uid,
    this.mobile,
    this.nickname,
    this.face,
    this.order_count,
  });

  Customer copyWith({
    String? uid,
    String? mobile,
    String? nickname,
    String? face,
    String? order_count,
  }) {
    return Customer(
      uid: uid ?? this.uid,
      mobile: mobile ?? this.mobile,
      nickname: nickname ?? this.nickname,
      face: face ?? this.face,
      order_count: order_count ?? this.order_count,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'uid': uid,
      'mobile': mobile,
      'nickname': nickname,
      'face': face,
      'order_count': order_count,
    };
  }

  factory Customer.fromMap(Map<String, dynamic> map) {
    return Customer(
      uid: map['uid'] != null ? map['uid'] as String : null,
      mobile: map['mobile'] != null ? map['mobile'] as String : null,
      nickname: map['nickname'] != null ? map['nickname'] as String : null,
      face: map['face'] != null ? map['face'] as String : null,
      order_count:
          map['order_count'] != null ? map['order_count'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Customer.fromJson(String source) =>
      Customer.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Customer(uid: $uid, mobile: $mobile, nickname: $nickname, face: $face, order_count: $order_count)';
  }

  @override
  bool operator ==(covariant Customer other) {
    if (identical(this, other)) return true;

    return other.uid == uid &&
        other.mobile == mobile &&
        other.nickname == nickname &&
        other.face == face &&
        other.order_count == order_count;
  }

  @override
  int get hashCode {
    return uid.hashCode ^
        mobile.hashCode ^
        nickname.hashCode ^
        face.hashCode ^
        order_count.hashCode;
  }
}

class Driver {
  String? staff_id;
  String? name;
  String? mobile;
  String? face;
  String? lat;
  String? lng;

  Driver({
    this.staff_id,
    this.name,
    this.mobile,
    this.face,
    this.lat,
    this.lng,
  });

  Driver copyWith({
    String? staff_id,
    String? name,
    String? mobile,
    String? face,
    String? lat,
    String? lng,
  }) {
    return Driver(
      staff_id: staff_id ?? this.staff_id,
      name: name ?? this.name,
      mobile: mobile ?? this.mobile,
      face: face ?? this.face,
      lat: lat ?? this.lat,
      lng: lng ?? this.lng,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'staff_id': staff_id,
      'name': name,
      'mobile': mobile,
      'face': face,
      'lat': lat,
      'lng': lng,
    };
  }

  factory Driver.fromMap(Map<String, dynamic> map) {
    return Driver(
      staff_id: map['staff_id'] != null
          ? map['staff_id'] is int && (map['staff_id'] as int) == 0
              ? "0"
              : map['staff_id'] as String
          : null,
      name: map['name'] != null ? map['name'] as String : null,
      mobile: map['mobile'] != null ? map['mobile'] as String : null,
      face: map['face'] != null ? map['face'] as String : null,
      lat: map['lat'] != null
          ? map['lat'] is int && (map['lat'] as int) == 0
              ? "0"
              : map['lat'] as String
          : null,
      lng: map['lng'] != null
          ? map['lng'] is int && (map['lng'] as int) == 0
              ? "0"
              : map['lng'] as String
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Driver.fromJson(String source) =>
      Driver.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Driver(staff_id: $staff_id, name: $name, mobile: $mobile, face: $face, lat: $lat, lng: $lng)';
  }

  @override
  bool operator ==(covariant Driver other) {
    if (identical(this, other)) return true;

    return other.staff_id == staff_id &&
        other.name == name &&
        other.mobile == mobile &&
        other.face == face &&
        other.lat == lat &&
        other.lng == lng;
  }

  @override
  int get hashCode {
    return staff_id.hashCode ^
        name.hashCode ^
        mobile.hashCode ^
        face.hashCode ^
        lat.hashCode ^
        lng.hashCode;
  }
}
