// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class Format {
  String? spec_id;
  String? product_id;
  String? price;
  String? package_price;
  String? spec_name;
  String? spec_photo;
  String? sale_sku;
  String? sale_count;
  String? sale_type;
  String? bar_code;
  String? old_price;
  String? is_onsale;
  String? views;
  String? original_price;
  String? expired_date;
  String? total_item;
  String? dateline;
  String? unit;
  String? deleted;
  String? sort;
  String? ori_price;

  Format({
    this.spec_id,
    this.product_id,
    this.price,
    this.package_price,
    this.spec_name,
    this.spec_photo,
    this.sale_sku,
    this.sale_count,
    this.sale_type,
    this.bar_code,
    this.old_price,
    this.is_onsale,
    this.views,
    this.original_price,
    this.expired_date,
    this.total_item,
    this.dateline,
    this.unit,
    this.deleted,
    this.sort,
    this.ori_price,
  });

  Format copyWith({
    String? spec_id,
    String? product_id,
    String? price,
    String? package_price,
    String? spec_name,
    String? spec_photo,
    String? sale_sku,
    String? sale_count,
    String? sale_type,
    String? bar_code,
    String? old_price,
    String? is_onsale,
    String? views,
    String? original_price,
    String? expired_date,
    String? total_item,
    String? dateline,
    String? unit,
    String? deleted,
    String? sort,
    String? ori_price,
  }) {
    return Format(
      spec_id: spec_id ?? this.spec_id,
      product_id: product_id ?? this.product_id,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      spec_name: spec_name ?? this.spec_name,
      spec_photo: spec_photo ?? this.spec_photo,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      sale_type: sale_type ?? this.sale_type,
      bar_code: bar_code ?? this.bar_code,
      old_price: old_price ?? this.old_price,
      is_onsale: is_onsale ?? this.is_onsale,
      views: views ?? this.views,
      original_price: original_price ?? this.original_price,
      expired_date: expired_date ?? this.expired_date,
      total_item: total_item ?? this.total_item,
      dateline: dateline ?? this.dateline,
      unit: unit ?? this.unit,
      deleted: deleted ?? this.deleted,
      sort: sort ?? this.sort,
      ori_price: ori_price ?? this.ori_price,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'spec_id': spec_id,
      'product_id': product_id,
      'price': price,
      'package_price': package_price,
      'spec_name': spec_name,
      'spec_photo': spec_photo,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'sale_type': sale_type,
      'bar_code': bar_code,
      'old_price': old_price,
      'is_onsale': is_onsale,
      'views': views,
      'original_price': original_price,
      'expired_date': expired_date,
      'total_item': total_item,
      'dateline': dateline,
      'unit': unit,
      'deleted': deleted,
      'sort': sort,
      'ori_price': ori_price,
    };
  }

  factory Format.fromMap(Map<String, dynamic> map) {
    return Format(
      spec_id: map['spec_id'] != null ? map['spec_id'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      price: map['price'] != null ? map['price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      spec_name: map['spec_name'] != null ? map['spec_name'] as String : null,
      spec_photo:
          map['spec_photo'] != null ? map['spec_photo'] as String : null,
      sale_sku: map['sale_sku'] != null
          ? map['sale_sku'] is int
              ? "9999"
              : map['sale_sku'] as String
          : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      old_price: map['old_price'] != null ? map['old_price'] as String : null,
      is_onsale: map['is_onsale'] != null ? map['is_onsale'] as String : null,
      views: map['views'] != null ? map['views'] as String : null,
      original_price: map['original_price'] != null
          ? map['original_price'] as String
          : null,
      expired_date:
          map['expired_date'] != null ? map['expired_date'] as String : null,
      total_item:
          map['total_item'] != null ? map['total_item'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      deleted: map['deleted'] != null ? map['deleted'] as String : null,
      sort: map['sort'] != null ? map['sort'] as String : null,
      ori_price: map['ori_price'] != null ? map['ori_price'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Format.fromJson(String source) =>
      Format.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Format(spec_id: $spec_id, product_id: $product_id, price: $price, package_price: $package_price, spec_name: $spec_name, spec_photo: $spec_photo, sale_sku: $sale_sku, sale_count: $sale_count, sale_type: $sale_type, bar_code: $bar_code, old_price: $old_price, is_onsale: $is_onsale, views: $views, original_price: $original_price, expired_date: $expired_date, total_item: $total_item, dateline: $dateline, unit: $unit, deleted: $deleted, sort: $sort, ori_price: $ori_price)';
  }

  @override
  bool operator ==(covariant Format other) {
    if (identical(this, other)) return true;

    return other.spec_id == spec_id &&
        other.product_id == product_id &&
        other.price == price &&
        other.package_price == package_price &&
        other.spec_name == spec_name &&
        other.spec_photo == spec_photo &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.sale_type == sale_type &&
        other.bar_code == bar_code &&
        other.old_price == old_price &&
        other.is_onsale == is_onsale &&
        other.views == views &&
        other.original_price == original_price &&
        other.expired_date == expired_date &&
        other.total_item == total_item &&
        other.dateline == dateline &&
        other.unit == unit &&
        other.deleted == deleted &&
        other.sort == sort &&
        other.ori_price == ori_price;
  }

  @override
  int get hashCode {
    return spec_id.hashCode ^
        product_id.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        spec_name.hashCode ^
        spec_photo.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        sale_type.hashCode ^
        bar_code.hashCode ^
        old_price.hashCode ^
        is_onsale.hashCode ^
        views.hashCode ^
        original_price.hashCode ^
        expired_date.hashCode ^
        total_item.hashCode ^
        dateline.hashCode ^
        unit.hashCode ^
        deleted.hashCode ^
        sort.hashCode ^
        ori_price.hashCode;
  }
}
