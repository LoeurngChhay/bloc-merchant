// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class Attribute {
  String? attribute_id;
  String? title;
  String? name;
  List<String>? values;
  String? key;
  List<String>? val;

  Attribute({
    this.attribute_id,
    this.title,
    this.name,
    this.values,
    this.key,
    this.val,
  });

  Attribute copyWith({
    String? attribute_id,
    String? title,
    String? name,
    List<String>? values,
    String? key,
    List<String>? val,
  }) {
    return Attribute(
      attribute_id: attribute_id ?? this.attribute_id,
      title: title ?? this.title,
      name: name ?? this.name,
      values: values ?? this.values,
      key: key ?? this.key,
      val: val ?? this.val,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'attribute_id': attribute_id,
      'title': title,
      'name': name,
      'values': values?.map((x) => x).toList(),
      'key': key,
      'val': val?.map((x) => x).toList(),
    };
  }

  factory Attribute.fromMap(Map<String, dynamic> map) {
    return Attribute(
      attribute_id:
          map['attribute_id'] != null ? map['attribute_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      name: map['name'] != null ? map['name'] as String : null,
      values: map['values'] != null
          ? List<String>.from(
              (map['values'] as List<dynamic>)
                  .map<String?>((x) => x.toString()),
            )
          : [],
      key: map['key'] != null ? map['key'] as String : null,
      val: map['val'] != null
          ? List<String>.from(
              (map['val'] as List<dynamic>).map<String?>((x) => x.toString()),
            )
          : [],
    );
  }

  String toJson() => json.encode(toMap());

  factory Attribute.fromJson(String source) =>
      Attribute.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Attribute(attribute_id: $attribute_id, title: $title, name: $name, values: $values, key: $key, val: $val)';
  }

  @override
  bool operator ==(covariant Attribute other) {
    if (identical(this, other)) return true;

    return other.attribute_id == attribute_id &&
        other.title == title &&
        other.name == name &&
        listEquals(other.values, values) &&
        other.key == key &&
        listEquals(other.val, val);
  }

  @override
  int get hashCode {
    return attribute_id.hashCode ^
        title.hashCode ^
        name.hashCode ^
        values.hashCode ^
        key.hashCode ^
        val.hashCode;
  }
}
