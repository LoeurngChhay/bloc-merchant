// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:bloc_merchant_mobile_2/data/models/responses/order_addon.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_specification.dart';

class OrderDetail {
  String? driver_id;
  Driver? driver;
  Customer? customer;
  List<Product>? products;
  num? bag_price;
  num? discount_amount;
  num? vat_amount;
  num? commission_fee;
  num? shop_receive;
  String? pei_time_label;
  String? order_type;
  String? order_type_label;
  String? queue_number;
  String? payment;
  String? payment_status;
  String? receiver_contact;
  String? receiver_mobile;
  String? receiver_address;
  String? product_price;
  String? package_price;
  String? note;
  String? pin_code;
  String? total_price;
  String? pei_amount;
  String? order_status;
  String? order_status_label;

  OrderDetail({
    this.driver_id,
    this.driver,
    this.customer,
    this.products,
    this.bag_price,
    this.discount_amount,
    this.vat_amount,
    this.commission_fee,
    this.shop_receive,
    this.pei_time_label,
    this.order_type,
    this.order_type_label,
    this.queue_number,
    this.payment,
    this.payment_status,
    this.receiver_contact,
    this.receiver_mobile,
    this.receiver_address,
    this.product_price,
    this.package_price,
    this.note,
    this.pin_code,
    this.total_price,
    this.pei_amount,
    this.order_status,
    this.order_status_label,
  });

  OrderDetail copyWith({
    String? driver_id,
    Driver? driver,
    Customer? customer,
    List<Product>? products,
    num? bag_price,
    num? discount_amount,
    num? vat_amount,
    num? commission_fee,
    num? shop_receive,
    String? pei_time_label,
    String? order_type,
    String? order_type_label,
    String? queue_number,
    String? payment,
    String? payment_status,
    String? receiver_contact,
    String? receiver_mobile,
    String? receiver_address,
    String? product_price,
    String? package_price,
    String? note,
    String? pin_code,
    String? total_price,
    String? pei_amount,
    String? order_status,
    String? order_status_label,
  }) {
    return OrderDetail(
      driver_id: driver_id ?? this.driver_id,
      driver: driver ?? this.driver,
      customer: customer ?? this.customer,
      products: products ?? this.products,
      bag_price: bag_price ?? this.bag_price,
      discount_amount: discount_amount ?? this.discount_amount,
      vat_amount: vat_amount ?? this.vat_amount,
      commission_fee: commission_fee ?? this.commission_fee,
      shop_receive: shop_receive ?? this.shop_receive,
      pei_time_label: pei_time_label ?? this.pei_time_label,
      order_type: order_type ?? this.order_type,
      order_type_label: order_type_label ?? this.order_type_label,
      queue_number: queue_number ?? this.queue_number,
      payment: payment ?? this.payment,
      payment_status: payment_status ?? this.payment_status,
      receiver_contact: receiver_contact ?? this.receiver_contact,
      receiver_mobile: receiver_mobile ?? this.receiver_mobile,
      receiver_address: receiver_address ?? this.receiver_address,
      product_price: product_price ?? this.product_price,
      package_price: package_price ?? this.package_price,
      note: note ?? this.note,
      pin_code: pin_code ?? this.pin_code,
      total_price: total_price ?? this.total_price,
      pei_amount: pei_amount ?? this.pei_amount,
      order_status: order_status ?? this.order_status,
      order_status_label: order_status_label ?? this.order_status_label,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'driver_id': driver_id,
      'driver': driver?.toMap(),
      'customer': customer?.toMap(),
      'products': products?.map((x) => x.toMap()).toList(),
      'bag_price': bag_price,
      'discount_amount': discount_amount,
      'vat_amount': vat_amount,
      'commission_fee': commission_fee,
      'shop_receive': shop_receive,
      'pei_time_label': pei_time_label,
      'order_type': order_type,
      'order_type_label': order_type_label,
      'queue_number': queue_number,
      'payment': payment,
      'payment_status': payment_status,
      'receiver_contact': receiver_contact,
      'receiver_mobile': receiver_mobile,
      'receiver_address': receiver_address,
      'product_price': product_price,
      'package_price': package_price,
      'note': note,
      'pin_code': pin_code,
      'total_price': total_price,
      'pei_amount': pei_amount,
      'order_status': order_status,
      'order_status_label': order_status_label,
    };
  }

  factory OrderDetail.fromMap(Map<String, dynamic> map) {
    return OrderDetail(
      driver_id: map['driver_id'] != null ? map['driver_id'] as String : null,
      driver: map['driver'] != null
          ? Driver.fromMap(map['driver'] as Map<String, dynamic>)
          : null,
      customer: map['customer'] != null
          ? Customer.fromMap(map['customer'] as Map<String, dynamic>)
          : null,
      products: map['products'] != null
          ? List<Product>.from(
              (map['products'] as List<dynamic>).map<Product?>(
                (x) => Product.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      bag_price: map['bag_price'] != null ? map['bag_price'] as num : null,
      discount_amount:
          map['discount_amount'] != null ? map['discount_amount'] as num : null,
      vat_amount: map['vat_amount'] != null ? map['vat_amount'] as num : null,
      commission_fee:
          map['commission_fee'] != null ? map['commission_fee'] as num : null,
      shop_receive:
          map['shop_receive'] != null ? map['shop_receive'] as num : null,
      pei_time_label: map['pei_time_label'] != null
          ? map['pei_time_label'] as String
          : null,
      order_type:
          map['order_type'] != null ? map['order_type'] as String : null,
      order_type_label: map['order_type_label'] != null
          ? map['order_type_label'] as String
          : null,
      queue_number:
          map['queue_number'] != null ? map['queue_number'] as String : null,
      payment: map['payment'] != null ? map['payment'] as String : null,
      payment_status: map['payment_status'] != null
          ? map['payment_status'] as String
          : null,
      receiver_contact: map['receiver_contact'] != null
          ? map['receiver_contact'] as String
          : null,
      receiver_mobile: map['receiver_mobile'] != null
          ? map['receiver_mobile'] as String
          : null,
      receiver_address: map['receiver_address'] != null
          ? map['receiver_address'] as String
          : null,
      product_price:
          map['product_price'] != null ? map['product_price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      note: map['note'] != null ? map['note'] as String : null,
      pin_code: map['pin_code'] != null ? map['pin_code'] as String : null,
      total_price:
          map['total_price'] != null ? map['total_price'] as String : null,
      pei_amount:
          map['pei_amount'] != null ? map['pei_amount'] as String : null,
      order_status:
          map['order_status'] != null ? map['order_status'] as String : null,
      order_status_label: map['order_status_label'] != null
          ? map['order_status_label'] as String
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderDetail.fromJson(String source) =>
      OrderDetail.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'OrderDetail(driver_id: $driver_id, driver: $driver, customer: $customer, products: $products, bag_price: $bag_price, discount_amount: $discount_amount, vat_amount: $vat_amount, commission_fee: $commission_fee, shop_receive: $shop_receive, pei_time_label: $pei_time_label, order_type: $order_type, order_type_label: $order_type_label, queue_number: $queue_number, payment: $payment, payment_status: $payment_status, receiver_contact: $receiver_contact, receiver_mobile: $receiver_mobile, receiver_address: $receiver_address, product_price: $product_price, package_price: $package_price, note: $note, pin_code: $pin_code, total_price: $total_price, pei_amount: $pei_amount, order_status: $order_status, order_status_label: $order_status_label)';
  }

  @override
  bool operator ==(covariant OrderDetail other) {
    if (identical(this, other)) return true;

    return other.driver_id == driver_id &&
        other.driver == driver &&
        other.customer == customer &&
        listEquals(other.products, products) &&
        other.bag_price == bag_price &&
        other.discount_amount == discount_amount &&
        other.vat_amount == vat_amount &&
        other.commission_fee == commission_fee &&
        other.shop_receive == shop_receive &&
        other.pei_time_label == pei_time_label &&
        other.order_type == order_type &&
        other.order_type_label == order_type_label &&
        other.queue_number == queue_number &&
        other.payment == payment &&
        other.payment_status == payment_status &&
        other.receiver_contact == receiver_contact &&
        other.receiver_mobile == receiver_mobile &&
        other.receiver_address == receiver_address &&
        other.product_price == product_price &&
        other.package_price == package_price &&
        other.note == note &&
        other.pin_code == pin_code &&
        other.total_price == total_price &&
        other.pei_amount == pei_amount &&
        other.order_status == order_status &&
        other.order_status_label == order_status_label;
  }

  @override
  int get hashCode {
    return driver_id.hashCode ^
        driver.hashCode ^
        customer.hashCode ^
        products.hashCode ^
        bag_price.hashCode ^
        discount_amount.hashCode ^
        vat_amount.hashCode ^
        commission_fee.hashCode ^
        shop_receive.hashCode ^
        pei_time_label.hashCode ^
        order_type.hashCode ^
        order_type_label.hashCode ^
        queue_number.hashCode ^
        payment.hashCode ^
        payment_status.hashCode ^
        receiver_contact.hashCode ^
        receiver_mobile.hashCode ^
        receiver_address.hashCode ^
        product_price.hashCode ^
        package_price.hashCode ^
        note.hashCode ^
        pin_code.hashCode ^
        total_price.hashCode ^
        pei_amount.hashCode ^
        order_status.hashCode ^
        order_status_label.hashCode;
  }
}

class Product {
  String? order_id;
  String? product_id;
  String? product_title;
  String? product_name;
  String? bar_code;
  String? product_image;
  String? product_thumb;
  String? product_price;
  String? package_price;
  String? product_number;
  String? amount;
  String? spec_id;
  String? unit;
  List<OrderSpecific>? specification;
  String? product_prices;
  String? product_old_price;
  String? product_old_prices;
  String? huodong_title;
  String? cate_id;
  String? waimai_bl;
  String? discount_bl;
  List<OrderProductAddon>? product_addon;
  String? product_addon_price;
  String? is_hide;
  String? spec_name;
  String? discount_type;
  String? margin_bl;
  String? note;

  Product({
    this.order_id,
    this.product_id,
    this.product_title,
    this.product_name,
    this.bar_code,
    this.product_image,
    this.product_thumb,
    this.product_price,
    this.package_price,
    this.product_number,
    this.amount,
    this.spec_id,
    this.unit,
    this.specification,
    this.product_prices,
    this.product_old_price,
    this.product_old_prices,
    this.huodong_title,
    this.cate_id,
    this.waimai_bl,
    this.discount_bl,
    this.product_addon,
    this.product_addon_price,
    this.is_hide,
    this.spec_name,
    this.discount_type,
    this.margin_bl,
    this.note,
  });

  Product copyWith({
    String? order_id,
    String? product_id,
    String? product_title,
    String? product_name,
    String? bar_code,
    String? product_image,
    String? product_thumb,
    String? product_price,
    String? package_price,
    String? product_number,
    String? amount,
    String? spec_id,
    String? unit,
    List<OrderSpecific>? specification,
    String? product_prices,
    String? product_old_price,
    String? product_old_prices,
    String? huodong_title,
    String? cate_id,
    String? waimai_bl,
    String? discount_bl,
    List<OrderProductAddon>? product_addon,
    String? product_addon_price,
    String? is_hide,
    String? spec_name,
    String? discount_type,
    String? margin_bl,
    String? note,
  }) {
    return Product(
      order_id: order_id ?? this.order_id,
      product_id: product_id ?? this.product_id,
      product_title: product_title ?? this.product_title,
      product_name: product_name ?? this.product_name,
      bar_code: bar_code ?? this.bar_code,
      product_image: product_image ?? this.product_image,
      product_thumb: product_thumb ?? this.product_thumb,
      product_price: product_price ?? this.product_price,
      package_price: package_price ?? this.package_price,
      product_number: product_number ?? this.product_number,
      amount: amount ?? this.amount,
      spec_id: spec_id ?? this.spec_id,
      unit: unit ?? this.unit,
      specification: specification ?? this.specification,
      product_prices: product_prices ?? this.product_prices,
      product_old_price: product_old_price ?? this.product_old_price,
      product_old_prices: product_old_prices ?? this.product_old_prices,
      huodong_title: huodong_title ?? this.huodong_title,
      cate_id: cate_id ?? this.cate_id,
      waimai_bl: waimai_bl ?? this.waimai_bl,
      discount_bl: discount_bl ?? this.discount_bl,
      product_addon: product_addon ?? this.product_addon,
      product_addon_price: product_addon_price ?? this.product_addon_price,
      is_hide: is_hide ?? this.is_hide,
      spec_name: spec_name ?? this.spec_name,
      discount_type: discount_type ?? this.discount_type,
      margin_bl: margin_bl ?? this.margin_bl,
      note: note ?? this.note,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'order_id': order_id,
      'product_id': product_id,
      'product_title': product_title,
      'product_name': product_name,
      'bar_code': bar_code,
      'product_image': product_image,
      'product_thumb': product_thumb,
      'product_price': product_price,
      'package_price': package_price,
      'product_number': product_number,
      'amount': amount,
      'spec_id': spec_id,
      'unit': unit,
      'specification': specification?.map((x) => x.toMap()).toList(),
      'product_prices': product_prices,
      'product_old_price': product_old_price,
      'product_old_prices': product_old_prices,
      'huodong_title': huodong_title,
      'cate_id': cate_id,
      'waimai_bl': waimai_bl,
      'discount_bl': discount_bl,
      'product_addon': product_addon?.map((x) => x.toMap()).toList(),
      'product_addon_price': product_addon_price,
      'is_hide': is_hide,
      'spec_name': spec_name,
      'discount_type': discount_type,
      'margin_bl': margin_bl,
      'note': note,
    };
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      order_id: map['order_id'] != null ? map['order_id'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      product_title:
          map['product_title'] != null ? map['product_title'] as String : null,
      product_name:
          map['product_name'] != null ? map['product_name'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      product_image:
          map['product_image'] != null ? map['product_image'] as String : null,
      product_thumb:
          map['product_thumb'] != null ? map['product_thumb'] as String : null,
      product_price:
          map['product_price'] != null ? map['product_price'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      product_number: map['product_number'] != null
          ? map['product_number'] as String
          : null,
      amount: map['amount'] != null ? map['amount'] as String : null,
      spec_id: map['spec_id'] != null ? map['spec_id'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      specification: map['specification'] != null
          ? List<OrderSpecific>.from(
              (map['specification'] as List<dynamic>).map<OrderSpecific?>(
                (x) => OrderSpecific.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      product_prices: map['product_prices'] != null
          ? map['product_prices'] as String
          : null,
      product_old_price: map['product_old_price'] != null
          ? map['product_old_price'] as String
          : null,
      product_old_prices: map['product_old_prices'] != null
          ? map['product_old_prices'] as String
          : null,
      huodong_title:
          map['huodong_title'] != null ? map['huodong_title'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      waimai_bl: map['waimai_bl'] != null ? map['waimai_bl'] as String : null,
      discount_bl:
          map['discount_bl'] != null ? map['discount_bl'] as String : null,
      product_addon: map['product_addon'] != null
          ? List<OrderProductAddon>.from(
              (map['product_addon'] as List<dynamic>).map<OrderProductAddon?>(
                (x) => OrderProductAddon.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      product_addon_price: map['product_addon_price'] != null
          ? map['product_addon_price'] as String
          : null,
      is_hide: map['is_hide'] != null ? map['is_hide'] as String : null,
      spec_name: map['spec_name'] != null ? map['spec_name'] as String : null,
      discount_type:
          map['discount_type'] != null ? map['discount_type'] as String : null,
      margin_bl: map['margin_bl'] != null ? map['margin_bl'] as String : null,
      note: map['note'] != null ? map['note'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Product.fromJson(String source) =>
      Product.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Product(order_id: $order_id, product_id: $product_id, product_title: $product_title, product_name: $product_name, bar_code: $bar_code, product_image: $product_image, product_thumb: $product_thumb, product_price: $product_price, package_price: $package_price, product_number: $product_number, amount: $amount, spec_id: $spec_id, unit: $unit, specification: $specification, product_prices: $product_prices, product_old_price: $product_old_price, product_old_prices: $product_old_prices, huodong_title: $huodong_title, cate_id: $cate_id, waimai_bl: $waimai_bl, discount_bl: $discount_bl, product_addon: $product_addon, product_addon_price: $product_addon_price, is_hide: $is_hide, spec_name: $spec_name, discount_type: $discount_type, margin_bl: $margin_bl, note: $note)';
  }

  @override
  bool operator ==(covariant Product other) {
    if (identical(this, other)) return true;

    return other.order_id == order_id &&
        other.product_id == product_id &&
        other.product_title == product_title &&
        other.product_name == product_name &&
        other.bar_code == bar_code &&
        other.product_image == product_image &&
        other.product_thumb == product_thumb &&
        other.product_price == product_price &&
        other.package_price == package_price &&
        other.product_number == product_number &&
        other.amount == amount &&
        other.spec_id == spec_id &&
        other.unit == unit &&
        listEquals(other.specification, specification) &&
        other.product_prices == product_prices &&
        other.product_old_price == product_old_price &&
        other.product_old_prices == product_old_prices &&
        other.huodong_title == huodong_title &&
        other.cate_id == cate_id &&
        other.waimai_bl == waimai_bl &&
        other.discount_bl == discount_bl &&
        listEquals(other.product_addon, product_addon) &&
        other.product_addon_price == product_addon_price &&
        other.is_hide == is_hide &&
        other.spec_name == spec_name &&
        other.discount_type == discount_type &&
        other.margin_bl == margin_bl &&
        other.note == note;
  }

  @override
  int get hashCode {
    return order_id.hashCode ^
        product_id.hashCode ^
        product_title.hashCode ^
        product_name.hashCode ^
        bar_code.hashCode ^
        product_image.hashCode ^
        product_thumb.hashCode ^
        product_price.hashCode ^
        package_price.hashCode ^
        product_number.hashCode ^
        amount.hashCode ^
        spec_id.hashCode ^
        unit.hashCode ^
        specification.hashCode ^
        product_prices.hashCode ^
        product_old_price.hashCode ^
        product_old_prices.hashCode ^
        huodong_title.hashCode ^
        cate_id.hashCode ^
        waimai_bl.hashCode ^
        discount_bl.hashCode ^
        product_addon.hashCode ^
        product_addon_price.hashCode ^
        is_hide.hashCode ^
        spec_name.hashCode ^
        discount_type.hashCode ^
        margin_bl.hashCode ^
        note.hashCode;
  }
}
