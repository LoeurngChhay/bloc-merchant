// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class CategoryDetail {
  String? shop_id;
  String? cate_id;
  String? parent_id;
  String? title;
  dynamic settime;
  String? icon;
  String? dateline;
  String? orderby;
  String? show_type;
  String? is_margin;
  String? stime;
  String? ltime;
  String? is_release;
  String? release_date;
  String? is_expired;
  String? expired_date;

  CategoryDetail({
    this.shop_id,
    this.cate_id,
    this.parent_id,
    this.title,
    this.settime,
    this.icon,
    this.dateline,
    this.orderby,
    this.show_type,
    this.is_margin,
    this.stime,
    this.ltime,
    this.is_release,
    this.release_date,
    this.is_expired,
    this.expired_date,
  });

  CategoryDetail copyWith({
    String? shop_id,
    String? cate_id,
    String? parent_id,
    String? title,
    dynamic settime,
    String? icon,
    String? dateline,
    String? orderby,
    String? show_type,
    String? is_margin,
    String? stime,
    String? ltime,
    String? is_release,
    String? release_date,
    String? is_expired,
    String? expired_date,
  }) {
    return CategoryDetail(
      shop_id: shop_id ?? this.shop_id,
      cate_id: cate_id ?? this.cate_id,
      parent_id: parent_id ?? this.parent_id,
      title: title ?? this.title,
      settime: settime ?? this.settime,
      icon: icon ?? this.icon,
      dateline: dateline ?? this.dateline,
      orderby: orderby ?? this.orderby,
      show_type: show_type ?? this.show_type,
      is_margin: is_margin ?? this.is_margin,
      stime: stime ?? this.stime,
      ltime: ltime ?? this.ltime,
      is_release: is_release ?? this.is_release,
      release_date: release_date ?? this.release_date,
      is_expired: is_expired ?? this.is_expired,
      expired_date: expired_date ?? this.expired_date,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'cate_id': cate_id,
      'parent_id': parent_id,
      'title': title,
      'settime': settime,
      'icon': icon,
      'dateline': dateline,
      'orderby': orderby,
      'show_type': show_type,
      'is_margin': is_margin,
      'stime': stime,
      'ltime': ltime,
      'is_release': is_release,
      'release_date': release_date,
      'is_expired': is_expired,
      'expired_date': expired_date,
    };
  }

  factory CategoryDetail.fromMap(Map<String, dynamic> map) {
    return CategoryDetail(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      parent_id: map['parent_id'] != null ? map['parent_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      settime: map['settime'] as dynamic,
      icon: map['icon'] != null ? map['icon'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      show_type: map['show_type'] != null ? map['show_type'] as String : null,
      is_margin: map['is_margin'] != null ? map['is_margin'] as String : null,
      stime: map['stime'] != null ? map['stime'] as String : null,
      ltime: map['ltime'] != null ? map['ltime'] as String : null,
      is_release:
          map['is_release'] != null ? map['is_release'] as String : null,
      release_date:
          map['release_date'] != null ? map['release_date'] as String : null,
      is_expired:
          map['is_expired'] != null ? map['is_expired'] as String : null,
      expired_date:
          map['expired_date'] != null ? map['expired_date'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryDetail.fromJson(String source) =>
      CategoryDetail.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'CategoryDetail(shop_id: $shop_id, cate_id: $cate_id, parent_id: $parent_id, title: $title, settime: $settime, icon: $icon, dateline: $dateline, orderby: $orderby, show_type: $show_type, is_margin: $is_margin, stime: $stime, ltime: $ltime, is_release: $is_release, release_date: $release_date, is_expired: $is_expired, expired_date: $expired_date)';
  }

  @override
  bool operator ==(covariant CategoryDetail other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.cate_id == cate_id &&
        other.parent_id == parent_id &&
        other.title == title &&
        other.settime == settime &&
        other.icon == icon &&
        other.dateline == dateline &&
        other.orderby == orderby &&
        other.show_type == show_type &&
        other.is_margin == is_margin &&
        other.stime == stime &&
        other.ltime == ltime &&
        other.is_release == is_release &&
        other.release_date == release_date &&
        other.is_expired == is_expired &&
        other.expired_date == expired_date;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        cate_id.hashCode ^
        parent_id.hashCode ^
        title.hashCode ^
        settime.hashCode ^
        icon.hashCode ^
        dateline.hashCode ^
        orderby.hashCode ^
        show_type.hashCode ^
        is_margin.hashCode ^
        stime.hashCode ^
        ltime.hashCode ^
        is_release.hashCode ^
        release_date.hashCode ^
        is_expired.hashCode ^
        expired_date.hashCode;
  }
}
