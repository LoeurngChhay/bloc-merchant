// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class ProductEntity {
  String? product_id;
  String? shop_id;
  String? cate_id;
  String? title;
  String? secondary_title;
  String? thumb;
  num? price;
  num? package_price;
  String? sales;
  String? sale_type;
  String? sale_sku;
  String? sale_count;
  String? orderby;
  String? closed;
  String? is_onsale;
  String? unit;
  String? is_format;
  List<ProductFormatEntity>? formats;
  String? is_addon;
  String? addon_sale;
  String? bar_code;
  String? is_spec;
  String? is_favorite;
  List<dynamic>? attributes;

  ProductEntity({
    this.product_id,
    this.shop_id,
    this.cate_id,
    this.title,
    this.secondary_title,
    this.thumb,
    this.price,
    this.package_price,
    this.sales,
    this.sale_type,
    this.sale_sku,
    this.sale_count,
    this.orderby,
    this.closed,
    this.is_onsale,
    this.unit,
    this.is_format,
    this.formats,
    this.is_addon,
    this.addon_sale,
    this.bar_code,
    this.is_spec,
    this.is_favorite,
    this.attributes,
  });

  ProductEntity copyWith({
    String? product_id,
    String? shop_id,
    String? cate_id,
    String? title,
    String? secondary_title,
    String? thumb,
    num? price,
    num? package_price,
    String? sales,
    String? sale_type,
    String? sale_sku,
    String? sale_count,
    String? orderby,
    String? closed,
    String? is_onsale,
    String? unit,
    String? is_format,
    List<ProductFormatEntity>? formats,
    String? is_addon,
    String? addon_sale,
    String? bar_code,
    String? is_spec,
    String? is_favorite,
    List<dynamic>? attributes,
  }) {
    return ProductEntity(
      product_id: product_id ?? this.product_id,
      shop_id: shop_id ?? this.shop_id,
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      secondary_title: secondary_title ?? this.secondary_title,
      thumb: thumb ?? this.thumb,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      sales: sales ?? this.sales,
      sale_type: sale_type ?? this.sale_type,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      orderby: orderby ?? this.orderby,
      closed: closed ?? this.closed,
      is_onsale: is_onsale ?? this.is_onsale,
      unit: unit ?? this.unit,
      is_format: is_format ?? this.is_format,
      formats: formats ?? this.formats,
      is_addon: is_addon ?? this.is_addon,
      addon_sale: addon_sale ?? this.addon_sale,
      bar_code: bar_code ?? this.bar_code,
      is_spec: is_spec ?? this.is_spec,
      is_favorite: is_favorite ?? this.is_favorite,
      attributes: attributes ?? this.attributes,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'product_id': product_id,
      'shop_id': shop_id,
      'cate_id': cate_id,
      'title': title,
      'secondary_title': secondary_title,
      'thumb': thumb,
      'price': price,
      'package_price': package_price,
      'sales': sales,
      'sale_type': sale_type,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'orderby': orderby,
      'closed': closed,
      'is_onsale': is_onsale,
      'unit': unit,
      'is_format': is_format,
      'formats': formats?.map((x) => x.toMap()).toList(),
      'is_addon': is_addon,
      'addon_sale': addon_sale,
      'bar_code': bar_code,
      'is_spec': is_spec,
      'is_favorite': is_favorite,
      'attributes': attributes,
    };
  }

  factory ProductEntity.fromMap(Map<String, dynamic> map) {
    return ProductEntity(
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      secondary_title: map['secondary_title'] != null
          ? map['secondary_title'] as String
          : null,
      thumb: map['thumb'] != null ? map['thumb'] as String : null,
      price: map['price'] != null ? map['price'] as num : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as num : null,
      sales: map['sales'] != null ? map['sales'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      closed: map['closed'] != null ? map['closed'] as String : null,
      is_onsale: map['is_onsale'] != null ? map['is_onsale'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      is_format: map['is_format'] != null ? map['is_format'] as String : null,
      formats: map['formats'] != null
          ? List<ProductFormatEntity>.from(
              (map['formats'] as List<dynamic>).map<ProductFormatEntity?>(
                (x) => ProductFormatEntity.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      is_addon: map['is_addon'] != null ? map['is_addon'] as String : null,
      addon_sale:
          map['addon_sale'] != null ? map['addon_sale'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      is_spec: map['is_spec'] != null ? map['is_spec'] as String : null,
      is_favorite:
          map['is_favorite'] != null ? map['is_favorite'] as String : null,
      attributes: map['attributes'] != null
          ? List<dynamic>.from((map['attributes'] as List<dynamic>))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductEntity.fromJson(String source) =>
      ProductEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductEntity(product_id: $product_id, shop_id: $shop_id, cate_id: $cate_id, title: $title, secondary_title: $secondary_title, thumb: $thumb, price: $price, package_price: $package_price, sales: $sales, sale_type: $sale_type, sale_sku: $sale_sku, sale_count: $sale_count, orderby: $orderby, closed: $closed, is_onsale: $is_onsale, unit: $unit, is_format: $is_format, formats: $formats, is_addon: $is_addon, addon_sale: $addon_sale, bar_code: $bar_code, is_spec: $is_spec, is_favorite: $is_favorite, attributes: $attributes)';
  }

  @override
  bool operator ==(covariant ProductEntity other) {
    if (identical(this, other)) return true;

    return other.product_id == product_id &&
        other.shop_id == shop_id &&
        other.cate_id == cate_id &&
        other.title == title &&
        other.secondary_title == secondary_title &&
        other.thumb == thumb &&
        other.price == price &&
        other.package_price == package_price &&
        other.sales == sales &&
        other.sale_type == sale_type &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.orderby == orderby &&
        other.closed == closed &&
        other.is_onsale == is_onsale &&
        other.unit == unit &&
        other.is_format == is_format &&
        listEquals(other.formats, formats) &&
        other.is_addon == is_addon &&
        other.addon_sale == addon_sale &&
        other.bar_code == bar_code &&
        other.is_spec == is_spec &&
        other.is_favorite == is_favorite &&
        listEquals(other.attributes, attributes);
  }

  @override
  int get hashCode {
    return product_id.hashCode ^
        shop_id.hashCode ^
        cate_id.hashCode ^
        title.hashCode ^
        secondary_title.hashCode ^
        thumb.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        sales.hashCode ^
        sale_type.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        orderby.hashCode ^
        closed.hashCode ^
        is_onsale.hashCode ^
        unit.hashCode ^
        is_format.hashCode ^
        formats.hashCode ^
        is_addon.hashCode ^
        addon_sale.hashCode ^
        bar_code.hashCode ^
        is_spec.hashCode ^
        is_favorite.hashCode ^
        attributes.hashCode;
  }
}

class ProductFormatEntity {
  String? spec_id;
  String? product_id;
  double? price;
  double? package_price;
  String? spec_name;
  String? spec_photo;
  String? sale_sku;
  String? sale_count;
  String? sale_type;
  String? bar_code;
  String? old_price;

  ProductFormatEntity({
    required this.spec_id,
    this.product_id,
    this.price,
    this.package_price,
    this.spec_name,
    this.spec_photo,
    this.sale_sku,
    this.sale_count,
    this.sale_type,
    this.bar_code,
    this.old_price,
  });

  ProductFormatEntity copyWith({
    String? spec_id,
    String? product_id,
    double? price,
    double? package_price,
    String? spec_name,
    String? spec_photo,
    String? sale_sku,
    String? sale_count,
    String? sale_type,
    String? bar_code,
    String? old_price,
  }) {
    return ProductFormatEntity(
      spec_id: spec_id ?? this.spec_id,
      product_id: product_id ?? this.product_id,
      price: price ?? this.price,
      package_price: package_price ?? this.package_price,
      spec_name: spec_name ?? this.spec_name,
      spec_photo: spec_photo ?? this.spec_photo,
      sale_sku: sale_sku ?? this.sale_sku,
      sale_count: sale_count ?? this.sale_count,
      sale_type: sale_type ?? this.sale_type,
      bar_code: bar_code ?? this.bar_code,
      old_price: old_price ?? this.old_price,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'spec_id': spec_id,
      'product_id': product_id,
      'price': price,
      'package_price': package_price,
      'spec_name': spec_name,
      'spec_photo': spec_photo,
      'sale_sku': sale_sku,
      'sale_count': sale_count,
      'sale_type': sale_type,
      'bar_code': bar_code,
      'old_price': old_price,
    };
  }

  factory ProductFormatEntity.fromMap(Map<String, dynamic> map) {
    return ProductFormatEntity(
      spec_id: map['spec_id'] != null ? map['spec_id'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      price: map['price'] != null ? map['price'] as double : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as double : null,
      spec_name: map['spec_name'] != null ? map['spec_name'] as String : null,
      spec_photo:
          map['spec_photo'] != null ? map['spec_photo'] as String : null,
      sale_sku: map['sale_sku'] != null ? map['sale_sku'] as String : null,
      sale_count:
          map['sale_count'] != null ? map['sale_count'] as String : null,
      sale_type: map['sale_type'] != null ? map['sale_type'] as String : null,
      bar_code: map['bar_code'] != null ? map['bar_code'] as String : null,
      old_price: map['old_price'] != null ? map['old_price'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductFormatEntity.fromJson(String source) =>
      ProductFormatEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductFormatEntity(spec_id: $spec_id, product_id: $product_id, price: $price, package_price: $package_price, spec_name: $spec_name, spec_photo: $spec_photo, sale_sku: $sale_sku, sale_count: $sale_count, sale_type: $sale_type, bar_code: $bar_code, old_price: $old_price)';
  }

  @override
  bool operator ==(covariant ProductFormatEntity other) {
    if (identical(this, other)) return true;

    return other.spec_id == spec_id &&
        other.product_id == product_id &&
        other.price == price &&
        other.package_price == package_price &&
        other.spec_name == spec_name &&
        other.spec_photo == spec_photo &&
        other.sale_sku == sale_sku &&
        other.sale_count == sale_count &&
        other.sale_type == sale_type &&
        other.bar_code == bar_code &&
        other.old_price == old_price;
  }

  @override
  int get hashCode {
    return spec_id.hashCode ^
        product_id.hashCode ^
        price.hashCode ^
        package_price.hashCode ^
        spec_name.hashCode ^
        spec_photo.hashCode ^
        sale_sku.hashCode ^
        sale_count.hashCode ^
        sale_type.hashCode ^
        bar_code.hashCode ^
        old_price.hashCode;
  }
}
