// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:flutter/foundation.dart';

class ProductCategory {
  String? cate_id;
  String? title;
  String? icon;
  String? orderby;
  int? is_check;
  int? p_count;
  List<dynamic>? sub_cates;

  ProductCategory({
    this.cate_id,
    this.title,
    this.icon,
    this.orderby,
    this.is_check,
    this.p_count,
    this.sub_cates,
  });

  ProductCategory copyWith({
    String? cate_id,
    String? title,
    String? icon,
    String? orderby,
    int? is_check,
    int? p_count,
    List<dynamic>? sub_cates,
  }) {
    return ProductCategory(
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      icon: icon ?? this.icon,
      orderby: orderby ?? this.orderby,
      is_check: is_check ?? this.is_check,
      p_count: p_count ?? this.p_count,
      sub_cates: sub_cates ?? this.sub_cates,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'cate_id': cate_id,
      'title': title,
      'icon': icon,
      'orderby': orderby,
      'is_check': is_check,
      'p_count': p_count,
      'sub_cates': sub_cates,
    };
  }

  factory ProductCategory.fromMap(Map<String, dynamic> map) {
    return ProductCategory(
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      icon: map['icon'] != null ? map['icon'] as String : null,
      orderby: map['orderby'] != null ? map['orderby'] as String : null,
      is_check: map['is_check'] != null ? map['is_check'] as int : null,
      p_count: map['p_count'] != null ? map['p_count'] as int : null,
      sub_cates: map['sub_cates'] != null
          ? List<dynamic>.from((map['sub_cates'] as List<dynamic>))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductCategory.fromJson(String source) =>
      ProductCategory.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductCategory(cate_id: $cate_id, title: $title, icon: $icon, orderby: $orderby, is_check: $is_check, p_count: $p_count, sub_cates: $sub_cates)';
  }

  @override
  bool operator ==(covariant ProductCategory other) {
    if (identical(this, other)) return true;

    return other.cate_id == cate_id &&
        other.title == title &&
        other.icon == icon &&
        other.orderby == orderby &&
        other.is_check == is_check &&
        other.p_count == p_count &&
        listEquals(other.sub_cates, sub_cates);
  }

  @override
  int get hashCode {
    return cate_id.hashCode ^
        title.hashCode ^
        icon.hashCode ^
        orderby.hashCode ^
        is_check.hashCode ^
        p_count.hashCode ^
        sub_cates.hashCode;
  }
}
