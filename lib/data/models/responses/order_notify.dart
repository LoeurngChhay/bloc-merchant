// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class OrderNotify {
  String? shop_id;
  String? pendding_count;
  String? prepare_count;
  String? pickup_count;
  String? delivery_count;
  String? shop_status;
  OrderNotify({
    this.shop_id,
    this.pendding_count,
    this.prepare_count,
    this.pickup_count,
    this.delivery_count,
    this.shop_status,
  });

  OrderNotify copyWith({
    String? shop_id,
    String? pendding_count,
    String? prepare_count,
    String? pickup_count,
    String? delivery_count,
    String? shop_status,
  }) {
    return OrderNotify(
      shop_id: shop_id ?? this.shop_id,
      pendding_count: pendding_count ?? this.pendding_count,
      prepare_count: prepare_count ?? this.prepare_count,
      pickup_count: pickup_count ?? this.pickup_count,
      delivery_count: delivery_count ?? this.delivery_count,
      shop_status: shop_status ?? this.shop_status,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'pendding_count': pendding_count,
      'prepare_count': prepare_count,
      'pickup_count': pickup_count,
      'delivery_count': delivery_count,
      'shop_status': shop_status,
    };
  }

  factory OrderNotify.fromMap(Map<String, dynamic> map) {
    return OrderNotify(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      pendding_count: map['pendding_count'] != null
          ? map['pendding_count'] as String
          : null,
      prepare_count:
          map['prepare_count'] != null ? map['prepare_count'] as String : null,
      pickup_count:
          map['pickup_count'] != null ? map['pickup_count'] as String : null,
      delivery_count: map['delivery_count'] != null
          ? map['delivery_count'] as String
          : null,
      shop_status:
          map['shop_status'] != null ? map['shop_status'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderNotify.fromJson(String source) =>
      OrderNotify.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'OrderNotify(shop_id: $shop_id, pendding_count: $pendding_count, prepare_count: $prepare_count, pickup_count: $pickup_count, delivery_count: $delivery_count, shop_status: $shop_status)';
  }

  @override
  bool operator ==(covariant OrderNotify other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.pendding_count == pendding_count &&
        other.prepare_count == prepare_count &&
        other.pickup_count == pickup_count &&
        other.delivery_count == delivery_count &&
        other.shop_status == shop_status;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        pendding_count.hashCode ^
        prepare_count.hashCode ^
        pickup_count.hashCode ^
        delivery_count.hashCode ^
        shop_status.hashCode;
  }
}
