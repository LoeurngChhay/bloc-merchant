// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_category.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_detail.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_extra.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_unit.dart';

class GetProductData {
  List<ProductCategory>? categories;
  List<ProductUnit>? unit_list;
  List<Attribute>? attribute_list;
  List<ProductExtra>? extra_list;
  ProductDetail? product_detail;
  int? is_ecommerce;
  String? is_retail;

  GetProductData({
    this.categories,
    this.unit_list,
    this.attribute_list,
    this.extra_list,
    this.product_detail,
    this.is_ecommerce,
    this.is_retail,
  });

  GetProductData copyWith({
    List<ProductCategory>? categories,
    List<ProductUnit>? unit_list,
    List<Attribute>? attribute_list,
    List<ProductExtra>? extra_list,
    ProductDetail? productDetail,
    int? is_ecommerce,
    String? is_retail,
  }) {
    return GetProductData(
      categories: categories ?? this.categories,
      unit_list: unit_list ?? this.unit_list,
      attribute_list: attribute_list ?? this.attribute_list,
      extra_list: extra_list ?? this.extra_list,
      product_detail: product_detail ?? this.product_detail,
      is_ecommerce: is_ecommerce ?? this.is_ecommerce,
      is_retail: is_retail ?? this.is_retail,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'categories': categories?.map((x) => x.toMap()).toList(),
      'unit_list': unit_list?.map((x) => x.toMap()).toList(),
      'attribute_list': attribute_list?.map((x) => x.toMap()).toList(),
      'extra_list': extra_list?.map((x) => x.toMap()).toList(),
      'product_detail': product_detail?.toMap(),
      'is_ecommerce': is_ecommerce,
      'is_retail': is_retail,
    };
  }

  factory GetProductData.fromMap(Map<String, dynamic> map) {
    return GetProductData(
      categories: map['categories'] != null
          ? List<ProductCategory>.from(
              (map['categories'] as List<dynamic>).map<ProductCategory?>(
                (x) => ProductCategory.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      unit_list: map['unit_list'] != null
          ? List<ProductUnit>.from(
              (map['unit_list'] as List<dynamic>).map<ProductUnit?>(
                (x) => ProductUnit.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      attribute_list: map['attribute_list'] != null
          ? List<Attribute>.from(
              (map['attribute_list'] as List<dynamic>).map<Attribute?>(
                (x) => Attribute.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      extra_list: map['extra_list'] != null
          ? List<ProductExtra>.from(
              (map['extra_list'] as List<dynamic>).map<ProductExtra?>(
                (x) => ProductExtra.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
      product_detail: map['product_detail'] != null
          ? ProductDetail.fromMap(map['product_detail'] as Map<String, dynamic>)
          : null,
      is_ecommerce:
          map['is_ecommerce'] != null ? map['is_ecommerce'] as int : null,
      is_retail: map['is_retail'] != null ? map['is_retail'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory GetProductData.fromJson(String source) =>
      GetProductData.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'GetProductData(categories: $categories, unit_list: $unit_list, attribute_list: $attribute_list, extra_list: $extra_list, product_detail: $product_detail, is_ecommerce: $is_ecommerce, is_retail: $is_retail)';
  }

  @override
  bool operator ==(covariant GetProductData other) {
    if (identical(this, other)) return true;

    return listEquals(other.categories, categories) &&
        listEquals(other.unit_list, unit_list) &&
        listEquals(other.attribute_list, attribute_list) &&
        listEquals(other.extra_list, extra_list) &&
        other.product_detail == product_detail &&
        other.is_ecommerce == is_ecommerce &&
        other.is_retail == is_retail;
  }

  @override
  int get hashCode {
    return categories.hashCode ^
        unit_list.hashCode ^
        attribute_list.hashCode ^
        extra_list.hashCode ^
        product_detail.hashCode ^
        is_ecommerce.hashCode ^
        is_retail.hashCode;
  }
}
