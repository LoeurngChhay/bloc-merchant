// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';
import 'dart:io';

class ProductImages {
  String? image_id;
  String? product_id;
  String? photo;
  num? type;
  String? name;
  String? sort;
  String? dateline;
  File? file;
  double? aspect;

  ProductImages({
    this.image_id,
    this.product_id,
    this.photo,
    this.type,
    this.name,
    this.sort,
    this.dateline,
    this.file,
    this.aspect,
  });

  ProductImages copyWith({
    String? image_id,
    String? product_id,
    String? photo,
    num? type,
    String? name,
    String? sort,
    String? dateline,
    double? aspect,
  }) {
    return ProductImages(
      image_id: image_id ?? this.image_id,
      product_id: product_id ?? this.product_id,
      photo: photo ?? this.photo,
      type: type ?? this.type,
      name: name ?? this.name,
      sort: sort ?? this.sort,
      dateline: dateline ?? this.dateline,
      aspect: aspect ?? this.aspect,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'image_id': image_id,
      'product_id': product_id,
      'photo': photo,
      'type': type,
      'name': name,
      'sort': sort,
      'dateline': dateline,
      'aspect': aspect,
    };
  }

  factory ProductImages.fromMap(Map<String, dynamic> map) {
    return ProductImages(
      image_id: map['image_id'] != null ? map['image_id'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      photo: map['photo'] != null ? map['photo'] as String : null,
      type: map['type'] != null ? map['type'] as num : null,
      name: map['name'] != null ? map['name'] as String : null,
      sort: map['sort'] != null ? map['sort'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      aspect: map['aspect'] != null ? map['aspect'] as double : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductImages.fromJson(String source) =>
      ProductImages.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductImages(image_id: $image_id, product_id: $product_id, photo: $photo, type: $type, name: $name, sort: $sort, dateline: $dateline, aspect: $aspect)';
  }

  @override
  bool operator ==(covariant ProductImages other) {
    if (identical(this, other)) return true;

    return other.image_id == image_id &&
        other.product_id == product_id &&
        other.photo == photo &&
        other.type == type &&
        other.name == name &&
        other.sort == sort &&
        other.dateline == dateline &&
        other.aspect == aspect;
  }

  @override
  int get hashCode {
    return image_id.hashCode ^
        product_id.hashCode ^
        photo.hashCode ^
        type.hashCode ^
        name.hashCode ^
        sort.hashCode ^
        dateline.hashCode ^
        aspect.hashCode;
  }
}
