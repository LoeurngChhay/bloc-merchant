// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class LoginResponse {
  Shop? shop;
  String? token;
  String? public;

  LoginResponse({
    this.shop,
    this.token,
    this.public,
  });

  LoginResponse copyWith({
    Shop? shop,
    String? token,
    String? public,
  }) {
    return LoginResponse(
      shop: shop ?? this.shop,
      token: token ?? this.token,
      public: public ?? this.public,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop': shop?.toMap(),
      'token': token,
      'public': public,
    };
  }

  factory LoginResponse.fromMap(Map<String, dynamic> map) {
    return LoginResponse(
      shop: map['shop'] != null
          ? Shop.fromMap(map['shop'] as Map<String, dynamic>)
          : null,
      token: map['token'] != null ? map['token'] as String : null,
      public: map['public'] != null ? map['public'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromJson(String source) =>
      LoginResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'LoginResponse(shop: $shop, token: $token, public: $public)';

  @override
  bool operator ==(covariant LoginResponse other) {
    if (identical(this, other)) return true;

    return other.shop == shop && other.token == token && other.public == public;
  }

  @override
  int get hashCode => shop.hashCode ^ token.hashCode ^ public.hashCode;
}

class Shop {
  String? shop_id;
  String? title;
  String? logo;
  String? contact;
  String? mobile;
  String? addr;
  String? verify;
  String? isMall;

  Shop({
    this.shop_id,
    this.title,
    this.logo,
    this.contact,
    this.mobile,
    this.addr,
    this.verify,
    this.isMall,
  });

  Shop copyWith({
    String? shop_id,
    String? title,
    String? logo,
    String? contact,
    String? mobile,
    String? addr,
    String? verify,
    String? isMall,
  }) {
    return Shop(
      shop_id: shop_id ?? this.shop_id,
      title: title ?? this.title,
      logo: logo ?? this.logo,
      contact: contact ?? this.contact,
      mobile: mobile ?? this.mobile,
      addr: addr ?? this.addr,
      verify: verify ?? this.verify,
      isMall: isMall ?? this.isMall,
    );
  }

  factory Shop.fromString(String source) {
    final regex = RegExp(r'Shop\((.+?)\)');
    final match = regex.firstMatch(source);
    if (match != null) {
      final fieldsString = match.group(1);
      if (fieldsString != null) {
        final fields = _parseFields(fieldsString);
        return Shop(
          shop_id: fields['shop_id'] == 'null' ? null : fields['shop_id'],
          title: fields['title']!,
          logo: fields['logo']!,
          contact: fields['contact']!,
          mobile: fields['mobile']!,
          addr: fields['addr']!,
          verify: fields['verify']!,
          isMall: fields['isMall'],
        );
      }
    }
    throw FormatException('Invalid ShopEntity string format');
  }

  static Map<String, String?> _parseFields(String fieldsString) {
    final fields = <String, String?>{};
    final regex = RegExp(r'(\w+): ([^,]+)(?:, |$)');
    for (final match in regex.allMatches(fieldsString)) {
      fields[match.group(1)!] = match.group(2);
    }
    return fields;
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'title': title,
      'logo': logo,
      'contact': contact,
      'mobile': mobile,
      'addr': addr,
      'verify': verify,
      'isMall': isMall,
    };
  }

  factory Shop.fromMap(Map<String, dynamic> map) {
    return Shop(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      logo: map['logo'] != null ? map['logo'] as String : null,
      contact: map['contact'] != null ? map['contact'] as String : null,
      mobile: map['mobile'] != null ? map['mobile'] as String : null,
      addr: map['addr'] != null ? map['addr'] as String : null,
      verify: map['verify'] != null ? map['verify'] as String : null,
      isMall: map['isMall'] != null ? map['isMall'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Shop.fromJson(String source) =>
      Shop.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Shop(shop_id: $shop_id, title: $title, logo: $logo, contact: $contact, mobile: $mobile, addr: $addr, verify: $verify, isMall: $isMall)';
  }

  @override
  bool operator ==(covariant Shop other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.title == title &&
        other.logo == logo &&
        other.contact == contact &&
        other.mobile == mobile &&
        other.addr == addr &&
        other.verify == verify &&
        other.isMall == isMall;
  }

  @override
  int get hashCode {
    return shop_id.hashCode ^
        title.hashCode ^
        logo.hashCode ^
        contact.hashCode ^
        mobile.hashCode ^
        addr.hashCode ^
        verify.hashCode ^
        isMall.hashCode;
  }
}
