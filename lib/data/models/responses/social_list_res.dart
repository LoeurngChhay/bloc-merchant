// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_entity.dart';

class SocialAds {
  final List<AdsItem?> items;
  final int total;
  final int limit;

  SocialAds({
    required this.items,
    required this.total,
    required this.limit,
  });

  SocialAds copyWith({
    List<AdsItem?>? items,
    int? total,
    int? limit,
  }) {
    return SocialAds(
      items: items ?? this.items,
      total: total ?? this.total,
      limit: limit ?? this.limit,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'items': items.map((x) => x?.toMap()).toList(),
      'total': total,
      'limit': limit,
    };
  }

  factory SocialAds.fromMap(Map<String, dynamic> map) {
    return SocialAds(
      items: List<AdsItem?>.from(
        (map['items'] as List<dynamic>).map<AdsItem?>(
          (x) => AdsItem.fromMap(x as Map<String, dynamic>),
        ),
      ),
      total: map['total'] as int,
      limit: map['limit'] as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory SocialAds.fromJson(String source) =>
      SocialAds.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'SocialAds(items: $items, total: $total, limit: $limit)';

  @override
  bool operator ==(covariant SocialAds other) {
    if (identical(this, other)) return true;

    return listEquals(other.items, items) &&
        other.total == total &&
        other.limit == limit;
  }

  @override
  int get hashCode => items.hashCode ^ total.hashCode ^ limit.hashCode;
}

class AdsItem {
  String? postId;
  String? socialUid;
  String? type;
  String? title;
  String? dateline;
  String? views;
  String? likes;
  int? clicks;
  String? approval;
  String? link_shop;
  String? link_data;
  String? thumbnail;
  Shop? shop;
  ProductEntity? product;
  PostImageEntity? photoModel;
  PostVideoEntity? videoModel;

  AdsItem({
    required this.postId,
    this.socialUid,
    this.type,
    this.title,
    this.dateline,
    this.views,
    this.likes,
    this.clicks,
    this.approval,
    this.link_shop,
    this.link_data,
    this.thumbnail,
    this.shop,
    this.product,
    this.photoModel,
    this.videoModel,
  });

  AdsItem copyWith({
    String? postId,
    String? socialUid,
    String? type,
    String? title,
    String? dateline,
    String? views,
    String? likes,
    int? clicks,
    String? approval,
    String? link_shop,
    String? link_data,
    String? thumbnail,
    Shop? shop,
    ProductEntity? product,
    PostImageEntity? photoModel,
    PostVideoEntity? videoModel,
  }) {
    return AdsItem(
      postId: postId ?? this.postId,
      socialUid: socialUid ?? this.socialUid,
      type: type ?? this.type,
      title: title ?? this.title,
      dateline: dateline ?? this.dateline,
      views: views ?? this.views,
      likes: likes ?? this.likes,
      clicks: clicks ?? this.clicks,
      approval: approval ?? this.approval,
      link_shop: link_shop ?? this.link_shop,
      link_data: link_data ?? this.link_data,
      thumbnail: thumbnail ?? this.thumbnail,
      shop: shop ?? this.shop,
      product: product ?? this.product,
      photoModel: photoModel ?? this.photoModel,
      videoModel: videoModel ?? this.videoModel,
    );
  }

  Map<String, dynamic> toMap() {
    print('toMap : $photoModel');
    return {
      'post_id': postId,
      'social_uid': socialUid,
      'type': type,
      'title': title,
      'dateline': dateline,
      'views': views,
      'likes': likes,
      'clicks': clicks,
      'approval': approval,
      'link_shop': link_shop,
      'link_data': link_data,
      'thumbnail': thumbnail,
      'shop': shop?.toMap(),
      'product': product?.toMap(),
      'photo_model': photoModel?.toMap(),
      'video_model': videoModel?.toMap(),
    };
  }

  factory AdsItem.fromMap(Map<String, dynamic> map) {
    print(
        "map['photo_model'] :${map['photo_model']} ${map['photo_model'] is List && (map['photo_model'] as List).isEmpty}");
    return AdsItem(
      postId: map['post_id'] as String?,
      socialUid: map['social_uid'] as String?,
      type: map['type'] as String?,
      title: map['title'] as String?,
      dateline: map['dateline'] as String?,
      views: map['views'] as String?,
      likes: map['likes'] as String?,
      clicks: map['clicks'] as int?,
      approval: map['approval'] as String?,
      link_shop: map['link_shop'] as String?,
      link_data: map['link_data'] as String?,
      thumbnail: map['thumbnail'] as String?,
      shop: map['shop'] is List && (map['shop'] as List).isNotEmpty
          ? Shop.fromMap(map['shop'] as Map<String, dynamic>)
          : null,
      product: map['product'] is List && (map['product'] as List).isEmpty
          ? null
          : ProductEntity.fromMap(map['product'] as Map<String, dynamic>),
      photoModel: map['photo_model'] is List &&
              (map['photo_model'] as List).isEmpty
          ? null
          : PostImageEntity.fromMap(map['photo_model'] as Map<String, dynamic>),
      videoModel: map['video_model'] is List &&
              (map['video_model'] as List).isEmpty
          ? null
          : PostVideoEntity.fromMap(map['video_model'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory AdsItem.fromJson(String source) {
    print('source $source');

    return AdsItem.fromMap(json.decode(source) as Map<String, dynamic>);
  }

  @override
  String toString() {
    return 'AdsItem(postId: $postId, socialUid: $socialUid, type: $type, title: $title, dateline: $dateline, views: $views, likes: $likes, clicks: $clicks, approval: $approval, link_shop: $link_shop, link_data: $link_data, thumbnail: $thumbnail, shop: $shop, product: $product, photoModel: $photoModel, videoModel: $videoModel)';
  }

  @override
  bool operator ==(covariant AdsItem other) {
    if (identical(this, other)) return true;
    return postId == other.postId &&
        socialUid == other.socialUid &&
        type == other.type &&
        title == other.title &&
        dateline == other.dateline &&
        views == other.views &&
        likes == other.likes &&
        clicks == other.clicks &&
        approval == other.approval &&
        link_shop == other.link_shop &&
        link_data == other.link_data &&
        thumbnail == other.thumbnail &&
        shop == other.shop &&
        product == other.product &&
        photoModel == other.photoModel &&
        videoModel == other.videoModel;
  }

  @override
  int get hashCode {
    return postId.hashCode ^
        socialUid.hashCode ^
        type.hashCode ^
        title.hashCode ^
        dateline.hashCode ^
        views.hashCode ^
        likes.hashCode ^
        clicks.hashCode ^
        approval.hashCode ^
        link_shop.hashCode ^
        link_data.hashCode ^
        thumbnail.hashCode ^
        shop.hashCode ^
        product.hashCode ^
        photoModel.hashCode ^
        videoModel.hashCode;
  }
}

class PostImageEntity {
  final String? postId;
  final List<PostDataImageEntity>? images;

  PostImageEntity({
    required this.postId,
    this.images,
  });

  PostImageEntity copyWith({
    String? postId,
    List<PostDataImageEntity>? images,
  }) {
    return PostImageEntity(
      postId: postId ?? this.postId,
      images: images ?? this.images,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'post_id': postId,
      'images': images?.map((x) => x.toMap()).toList(),
    };
  }

  factory PostImageEntity.fromMap(Map<String, dynamic> map) {
    return PostImageEntity(
      postId: map['post_id'] as String?,
      images: List<PostDataImageEntity>.from((map['images'] as List<dynamic>?)
              ?.map((x) =>
                  PostDataImageEntity.fromMap(x as Map<String, dynamic>)) ??
          const []),
    );
  }

  String toJson() => json.encode(toMap());

  factory PostImageEntity.fromJson(String source) =>
      PostImageEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'PostImageEntity(postId: $postId, images: $images)';

  @override
  bool operator ==(covariant PostImageEntity other) {
    if (identical(this, other)) return true;
    return postId == other.postId && images == other.images;
  }

  @override
  int get hashCode => postId.hashCode ^ images.hashCode;
}

class PostDataImageEntity {
  final String? photoId;
  final String? imageUrl;
  final String? width;
  final String? height;
  final String? size;

  PostDataImageEntity({
    required this.photoId,
    this.imageUrl,
    this.width,
    this.height,
    this.size,
  });

  PostDataImageEntity copyWith({
    String? photoId,
    String? imageUrl,
    String? width,
    String? height,
    String? size,
  }) {
    return PostDataImageEntity(
      photoId: photoId ?? this.photoId,
      imageUrl: imageUrl ?? this.imageUrl,
      width: width ?? this.width,
      height: height ?? this.height,
      size: size ?? this.size,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'photoId': photoId,
      'imageUrl': imageUrl,
      'width': width,
      'height': height,
      'size': size,
    };
  }

  factory PostDataImageEntity.fromMap(Map<String, dynamic> map) {
    return PostDataImageEntity(
      photoId: map['photoId'] != null ? map['photoId'] as String : null,
      imageUrl: map['imageUrl'] != null ? map['imageUrl'] as String : null,
      width: map['width'] != null ? map['width'] as String : null,
      height: map['height'] != null ? map['height'] as String : null,
      size: map['size'] != null ? map['size'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory PostDataImageEntity.fromJson(String source) =>
      PostDataImageEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'PostDataImageEntity(photoId: $photoId, imageUrl: $imageUrl, width: $width, height: $height, size: $size)';
  }

  @override
  bool operator ==(covariant PostDataImageEntity other) {
    if (identical(this, other)) return true;

    return other.photoId == photoId &&
        other.imageUrl == imageUrl &&
        other.width == width &&
        other.height == height &&
        other.size == size;
  }

  @override
  int get hashCode {
    return photoId.hashCode ^
        imageUrl.hashCode ^
        width.hashCode ^
        height.hashCode ^
        size.hashCode;
  }
}

class PostVideoEntity {
  final String? postId;
  final String? videoId;
  final String? mp4Hls;
  final String? mp4Dash;
  final String? mp4Link;
  final String? preview;
  final String? mp4Width;
  final String? mp4Height;
  final String? mp4Size;

  PostVideoEntity({
    required this.postId,
    this.videoId,
    this.mp4Hls,
    this.mp4Dash,
    this.mp4Link,
    this.preview,
    this.mp4Width,
    this.mp4Height,
    this.mp4Size,
  });

  PostVideoEntity copyWith({
    String? postId,
    String? videoId,
    String? mp4Hls,
    String? mp4Dash,
    String? mp4Link,
    String? preview,
    String? mp4Width,
    String? mp4Height,
    String? mp4Size,
  }) {
    return PostVideoEntity(
      postId: postId ?? this.postId,
      videoId: videoId ?? this.videoId,
      mp4Hls: mp4Hls ?? this.mp4Hls,
      mp4Dash: mp4Dash ?? this.mp4Dash,
      mp4Link: mp4Link ?? this.mp4Link,
      preview: preview ?? this.preview,
      mp4Width: mp4Width ?? this.mp4Width,
      mp4Height: mp4Height ?? this.mp4Height,
      mp4Size: mp4Size ?? this.mp4Size,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'post_id': postId,
      'video_id': videoId,
      'mp4_hls': mp4Hls,
      'mp4_dash': mp4Dash,
      'mp4_link': mp4Link,
      'mp4_preview': preview,
      'mp4_width': mp4Width,
      'mp4_height': mp4Height,
      'mp4_size': mp4Size,
    };
  }

  factory PostVideoEntity.fromMap(Map<String, dynamic> map) {
    return PostVideoEntity(
      postId: map['post_id'] as String?,
      videoId: map['video_id'] as String?,
      mp4Hls: map['mp4_hls'] as String?,
      mp4Dash: map['mp4_dash'] as String?,
      mp4Link: map['mp4_link'] as String?,
      preview: map['mp4_preview'] as String?,
      mp4Width: map['mp4_width'] as String?,
      mp4Height: map['mp4_height'] as String?,
      mp4Size: map['mp4_size'] as String?,
    );
  }

  String toJson() => json.encode(toMap());

  factory PostVideoEntity.fromJson(String source) =>
      PostVideoEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'PostVideoEntity(postId: $postId, videoId: $videoId, mp4Hls: $mp4Hls, mp4Dash: $mp4Dash, mp4Link: $mp4Link, preview: $preview, mp4Width: $mp4Width, mp4Height: $mp4Height, mp4Size: $mp4Size)';
  }

  @override
  bool operator ==(covariant PostVideoEntity other) {
    if (identical(this, other)) return true;
    return postId == other.postId &&
        videoId == other.videoId &&
        mp4Hls == other.mp4Hls &&
        mp4Dash == other.mp4Dash &&
        mp4Link == other.mp4Link &&
        preview == other.preview &&
        mp4Width == other.mp4Width &&
        mp4Height == other.mp4Height &&
        mp4Size == other.mp4Size;
  }

  @override
  int get hashCode {
    return postId.hashCode ^
        videoId.hashCode ^
        mp4Hls.hashCode ^
        mp4Dash.hashCode ^
        mp4Link.hashCode ^
        preview.hashCode ^
        mp4Width.hashCode ^
        mp4Height.hashCode ^
        mp4Size.hashCode;
  }
}

class Shop {
  final String? shop_id;
  final String? title;
  final String? logo;
  Shop({
    this.shop_id,
    this.title,
    this.logo,
  });

  Shop copyWith({
    String? shop_id,
    String? title,
    String? logo,
  }) {
    return Shop(
      shop_id: shop_id ?? this.shop_id,
      title: title ?? this.title,
      logo: logo ?? this.logo,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'shop_id': shop_id,
      'title': title,
      'logo': logo,
    };
  }

  factory Shop.fromMap(Map<String, dynamic> map) {
    return Shop(
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      logo: map['logo'] != null ? map['logo'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Shop.fromJson(String source) =>
      Shop.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Shop(shop_id: $shop_id, title: $title, logo: $logo)';

  @override
  bool operator ==(covariant Shop other) {
    if (identical(this, other)) return true;

    return other.shop_id == shop_id &&
        other.title == title &&
        other.logo == logo;
  }

  @override
  int get hashCode => shop_id.hashCode ^ title.hashCode ^ logo.hashCode;
}

class ImageModelPost {
  String? photoId;
  String? imageUrl;
  String? width;
  String? height;
  String? size;

  ImageModelPost({
    this.photoId,
    this.imageUrl,
    this.width,
    this.height,
    this.size,
  });

  ImageModelPost copyWith({
    String? photoId,
    String? imageUrl,
    String? width,
    String? height,
    String? size,
  }) {
    return ImageModelPost(
      photoId: photoId ?? this.photoId,
      imageUrl: imageUrl ?? this.imageUrl,
      width: width ?? this.width,
      height: height ?? this.height,
      size: size ?? this.size,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'photoId': photoId,
      'imageUrl': imageUrl,
      'width': width,
      'height': height,
      'size': size,
    };
  }

  factory ImageModelPost.fromMap(Map<String, dynamic> map) {
    return ImageModelPost(
      photoId: map['photoId'] != null ? map['photoId'] as String : null,
      imageUrl: map['imageUrl'] != null ? map['imageUrl'] as String : null,
      width: map['width'] != null ? map['width'] as String : null,
      height: map['height'] != null ? map['height'] as String : null,
      size: map['size'] != null ? map['size'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ImageModelPost.fromJson(String source) =>
      ImageModelPost.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ImageModelPost(photoId: $photoId, imageUrl: $imageUrl, width: $width, height: $height, size: $size)';
  }

  @override
  bool operator ==(covariant ImageModelPost other) {
    if (identical(this, other)) return true;

    return other.photoId == photoId &&
        other.imageUrl == imageUrl &&
        other.width == width &&
        other.height == height &&
        other.size == size;
  }

  @override
  int get hashCode {
    return photoId.hashCode ^
        imageUrl.hashCode ^
        width.hashCode ^
        height.hashCode ^
        size.hashCode;
  }
}
