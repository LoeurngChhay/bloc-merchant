// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class ProductExtra {
  String? product_id;
  String? thumb;
  String? cate_id;
  String? title;
  String? secondary_title;
  String? price;
  String? unit;
  String? addon_code;
  String? dateline;

  ProductExtra({
    this.product_id,
    this.thumb,
    this.cate_id,
    this.title,
    this.secondary_title,
    this.price,
    this.unit,
    this.addon_code,
    this.dateline,
  });

  ProductExtra copyWith({
    String? product_id,
    String? thumb,
    String? cate_id,
    String? title,
    String? secondary_title,
    String? price,
    String? unit,
    String? addon_code,
    String? dateline,
  }) {
    return ProductExtra(
      product_id: product_id ?? this.product_id,
      thumb: thumb ?? this.thumb,
      cate_id: cate_id ?? this.cate_id,
      title: title ?? this.title,
      secondary_title: secondary_title ?? this.secondary_title,
      price: price ?? this.price,
      unit: unit ?? this.unit,
      addon_code: addon_code ?? this.addon_code,
      dateline: dateline ?? this.dateline,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'product_id': product_id,
      'thumb': thumb,
      'cate_id': cate_id,
      'title': title,
      'secondary_title': secondary_title,
      'price': price,
      'unit': unit,
      'addon_code': addon_code,
      'dateline': dateline,
    };
  }

  factory ProductExtra.fromMap(Map<String, dynamic> map) {
    return ProductExtra(
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      thumb: map['thumb'] != null ? map['thumb'] as String : null,
      cate_id: map['cate_id'] != null ? map['cate_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      secondary_title: map['secondary_title'] != null
          ? map['secondary_title'] as String
          : null,
      price: map['price'] != null ? map['price'] as String : null,
      unit: map['unit'] != null ? map['unit'] as String : null,
      addon_code:
          map['addon_code'] != null ? map['addon_code'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductExtra.fromJson(String source) =>
      ProductExtra.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductExtra(product_id: $product_id, thumb: $thumb, cate_id: $cate_id, title: $title, secondary_title: $secondary_title, price: $price, unit: $unit, addon_code: $addon_code, dateline: $dateline)';
  }

  @override
  bool operator ==(covariant ProductExtra other) {
    if (identical(this, other)) return true;

    return other.product_id == product_id &&
        other.thumb == thumb &&
        other.cate_id == cate_id &&
        other.title == title &&
        other.secondary_title == secondary_title &&
        other.price == price &&
        other.unit == unit &&
        other.addon_code == addon_code &&
        other.dateline == dateline;
  }

  @override
  int get hashCode {
    return product_id.hashCode ^
        thumb.hashCode ^
        cate_id.hashCode ^
        title.hashCode ^
        secondary_title.hashCode ^
        price.hashCode ^
        unit.hashCode ^
        addon_code.hashCode ^
        dateline.hashCode;
  }
}
