// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class OrderProductAddon {
  String? old_price;
  String? product_name;
  String? package_price;
  String? product_id;
  String? price;
  int? qty;
  String? thumb;
  OrderProductAddon({
    this.old_price,
    this.product_name,
    this.package_price,
    this.product_id,
    this.price,
    this.qty,
    this.thumb,
  });

  OrderProductAddon copyWith({
    String? old_price,
    String? product_name,
    String? package_price,
    String? product_id,
    String? price,
    int? qty,
    String? thumb,
  }) {
    return OrderProductAddon(
      old_price: old_price ?? this.old_price,
      product_name: product_name ?? this.product_name,
      package_price: package_price ?? this.package_price,
      product_id: product_id ?? this.product_id,
      price: price ?? this.price,
      qty: qty ?? this.qty,
      thumb: thumb ?? this.thumb,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'old_price': old_price,
      'product_name': product_name,
      'package_price': package_price,
      'product_id': product_id,
      'price': price,
      'qty': qty,
      'thumb': thumb,
    };
  }

  factory OrderProductAddon.fromMap(Map<String, dynamic> map) {
    return OrderProductAddon(
      old_price: map['old_price'] != null ? map['old_price'] as String : null,
      product_name:
          map['product_name'] != null ? map['product_name'] as String : null,
      package_price:
          map['package_price'] != null ? map['package_price'] as String : null,
      product_id:
          map['product_id'] != null ? map['product_id'] as String : null,
      price: map['price'] != null ? map['price'] as String : null,
      qty: map['qty'] != null ? map['qty'] as int : null,
      thumb: map['thumb'] != null ? map['thumb'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OrderProductAddon.fromJson(String source) =>
      OrderProductAddon.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'OrderProductAddon(old_price: $old_price, product_name: $product_name, package_price: $package_price, product_id: $product_id, price: $price, qty: $qty, thumb: $thumb)';
  }

  @override
  bool operator ==(covariant OrderProductAddon other) {
    if (identical(this, other)) return true;

    return other.old_price == old_price &&
        other.product_name == product_name &&
        other.package_price == package_price &&
        other.product_id == product_id &&
        other.price == price &&
        other.qty == qty &&
        other.thumb == thumb;
  }

  @override
  int get hashCode {
    return old_price.hashCode ^
        product_name.hashCode ^
        package_price.hashCode ^
        product_id.hashCode ^
        price.hashCode ^
        qty.hashCode ^
        thumb.hashCode;
  }
}
