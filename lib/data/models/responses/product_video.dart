// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class ProductVideo {
  String? video_id;
  String? thumbnail;
  String? mp4_uid;
  String? mp4_hls;
  String? mp4_dash;
  String? mp4_link;
  String? preview;
  String? width;
  String? height;
  String? size;

  ProductVideo({
    this.video_id,
    this.thumbnail,
    this.mp4_uid,
    this.mp4_hls,
    this.mp4_dash,
    this.mp4_link,
    this.preview,
    this.width,
    this.height,
    this.size,
  });

  ProductVideo copyWith({
    String? video_id,
    String? thumbnail,
    String? mp4_uid,
    String? mp4_hls,
    String? mp4_dash,
    String? mp4_link,
    String? preview,
    String? width,
    String? height,
    String? size,
  }) {
    return ProductVideo(
      video_id: video_id ?? this.video_id,
      thumbnail: thumbnail ?? this.thumbnail,
      mp4_uid: mp4_uid ?? this.mp4_uid,
      mp4_hls: mp4_hls ?? this.mp4_hls,
      mp4_dash: mp4_dash ?? this.mp4_dash,
      mp4_link: mp4_link ?? this.mp4_link,
      preview: preview ?? this.preview,
      width: width ?? this.width,
      height: height ?? this.height,
      size: size ?? this.size,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'video_id': video_id,
      'thumbnail': thumbnail,
      'mp4_uid': mp4_uid,
      'mp4_hls': mp4_hls,
      'mp4_dash': mp4_dash,
      'mp4_link': mp4_link,
      'preview': preview,
      'width': width,
      'height': height,
      'size': size,
    };
  }

  factory ProductVideo.fromMap(Map<String, dynamic> map) {
    return ProductVideo(
      video_id: map['video_id'] != null ? map['video_id'] as String : null,
      thumbnail: map['thumbnail'] != null ? map['thumbnail'] as String : null,
      mp4_uid: map['mp4_uid'] != null ? map['mp4_uid'] as String : null,
      mp4_hls: map['mp4_hls'] != null ? map['mp4_hls'] as String : null,
      mp4_dash: map['mp4_dash'] != null ? map['mp4_dash'] as String : null,
      mp4_link: map['mp4_link'] != null ? map['mp4_link'] as String : null,
      preview: map['preview'] != null ? map['preview'] as String : null,
      width: map['width'] != null ? map['width'] as String : null,
      height: map['height'] != null ? map['height'] as String : null,
      size: map['size'] != null ? map['size'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductVideo.fromJson(String source) =>
      ProductVideo.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductVideo(video_id: $video_id, thumbnail: $thumbnail, mp4_uid: $mp4_uid, mp4_hls: $mp4_hls, mp4_dash: $mp4_dash, mp4_link: $mp4_link, preview: $preview, width: $width, height: $height, size: $size)';
  }

  @override
  bool operator ==(covariant ProductVideo other) {
    if (identical(this, other)) return true;

    return other.video_id == video_id &&
        other.thumbnail == thumbnail &&
        other.mp4_uid == mp4_uid &&
        other.mp4_hls == mp4_hls &&
        other.mp4_dash == mp4_dash &&
        other.mp4_link == mp4_link &&
        other.preview == preview &&
        other.width == width &&
        other.height == height &&
        other.size == size;
  }

  @override
  int get hashCode {
    return video_id.hashCode ^
        thumbnail.hashCode ^
        mp4_uid.hashCode ^
        mp4_hls.hashCode ^
        mp4_dash.hashCode ^
        mp4_link.hashCode ^
        preview.hashCode ^
        width.hashCode ^
        height.hashCode ^
        size.hashCode;
  }
}
