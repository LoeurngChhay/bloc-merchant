// ignore_for_file: public_member_api_docs, sort_constructors_first, non_constant_identifier_names
import 'dart:convert';

class ProductUnit {
  String? unit_id;
  String? shop_id;
  String? title;
  String? dateline;
  String? deleted;

  ProductUnit({
    this.unit_id,
    this.shop_id,
    this.title,
    this.dateline,
    this.deleted,
  });

  ProductUnit copyWith({
    String? unit_id,
    String? shop_id,
    String? title,
    String? dateline,
    String? deleted,
  }) {
    return ProductUnit(
      unit_id: unit_id ?? this.unit_id,
      shop_id: shop_id ?? this.shop_id,
      title: title ?? this.title,
      dateline: dateline ?? this.dateline,
      deleted: deleted ?? this.deleted,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'unit_id': unit_id,
      'shop_id': shop_id,
      'title': title,
      'dateline': dateline,
      'deleted': deleted,
    };
  }

  factory ProductUnit.fromMap(Map<String, dynamic> map) {
    return ProductUnit(
      unit_id: map['unit_id'] != null ? map['unit_id'] as String : null,
      shop_id: map['shop_id'] != null ? map['shop_id'] as String : null,
      title: map['title'] != null ? map['title'] as String : null,
      dateline: map['dateline'] != null ? map['dateline'] as String : null,
      deleted: map['deleted'] != null ? map['deleted'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductUnit.fromJson(String source) =>
      ProductUnit.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'ProductUnit(unit_id: $unit_id, shop_id: $shop_id, title: $title, dateline: $dateline, deleted: $deleted)';
  }

  @override
  bool operator ==(covariant ProductUnit other) {
    if (identical(this, other)) return true;

    return other.unit_id == unit_id &&
        other.shop_id == shop_id &&
        other.title == title &&
        other.dateline == dateline &&
        other.deleted == deleted;
  }

  @override
  int get hashCode {
    return unit_id.hashCode ^
        shop_id.hashCode ^
        title.hashCode ^
        dateline.hashCode ^
        deleted.hashCode;
  }
}
