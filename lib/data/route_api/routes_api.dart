class HttpApi {
  static const String clientType = "MERCHANT";
  static const String xAccessToken = "x-access-token";
  static const String bundle = "com.bongtk.blocmerchant";
  static const String version = "3.0.230101";
  static const String baseUrl = "https://v3.bloc.asia/api.php?API=";
  static const String language = "EN";
  //url
  static const String login = "merchant/passport/login";
  static const String logout = "merchant/passport/logout";
  static const String info = "merchant/home/info";
  static const String setCloseStatus = "merchant/home/close_operation";
  static const String setOpenStatus = "merchant/home/open_operation";

  //category
  static const String getListCategory = "merchant/product/category/list_cate";
  static const String getCategory = "merchant/product/category/detail";
  static const String createCategory =
      "merchant/product/category/create_category";
  static const String updateCategory =
      "merchant/product/category/update_category";
  static const String deleteCategory =
      "merchant/product/category/delete_category";

  //product
  static const String getProductDetail = 'merchant/product/product/data_edit';
  static const String getProductNew = "merchant/product/product/data_new";
  static const String getProduct = "merchant/product/product/list_prod";
  static const String getToppingProduct =
      "merchant/product/product/list_prod_topping";
  static const String getProdCat = "merchant/product/product/list_cate";
  static const String createProduct = "merchant/product/product/create_product";
  static const String updateProduct = 'merchant/product/product/update_product';
  static const String changeProductStatus =
      'merchant/product/product/set_onsale_product';
  static const String deleteProduct = 'merchant/product/product/delete_product';
  static const String uploadImage = 'merchant/home/image_upload';

  //orders
  static const String orderNotify = "merchant/order/home/notify";
  static const String getListOrder = "merchant/order/order/list_order";
  static const String orderDetail = "merchant/order/order/detail";
  static const String cancelTheOrder = "merchant/order/order/cancel";
  static const String acceptTheOrder = "merchant/order/order/accept";
  //socials
  static const String socialHome = "merchant/ads/social/home";
  static const String postList = "merchant/ads/social/list";
  static const String postVideo = "merchant/ads/social/postvideo";
  static const String postPhoto = "merchant/ads/social/postphoto";
}
