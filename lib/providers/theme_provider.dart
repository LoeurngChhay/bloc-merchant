import 'package:bloc_merchant_mobile_2/constants/storage_key.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider extends ChangeNotifier {
  final prefs = SharedPreferences.getInstance();

  ThemeMode _themeMode = ThemeMode.system;
  ThemeMode get themeMode => _themeMode;
  set themeMode(ThemeMode newValue) {
    _themeMode = newValue;
    notifyListeners();
  }

  Future<void> onThemeChanged(ThemeMode mode) async {
    final sharePrefs = await prefs;
    sharePrefs.setInt(StorageKey.themeMode, mode.index);
  }

  ThemeMode getThemeMode(int? index) {
    switch (index) {
      case 0:
        return ThemeMode.system;
      case 1:
        return ThemeMode.light;
      case 2:
        return ThemeMode.dark;
      default:
        return ThemeMode.system;
    }
  }

  Future<void> ensureInitialization() async {
    try {
      final sharePref = await prefs;
      final themePrefs = sharePref.getInt(StorageKey.themeMode);
      themeMode = getThemeMode(themePrefs);
    } catch (e) {
      themeMode = ThemeMode.system;
    }
  }
}
