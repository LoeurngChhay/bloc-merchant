// import 'package:bluetooth_print/bluetooth_print_model.dart';
import 'dart:convert';
import 'dart:io';

import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:esc_pos_bluetooth_updated/esc_pos_bluetooth_updated.dart';
import 'package:flutter/material.dart';
import 'package:print_bluetooth_thermal/print_bluetooth_thermal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_bluetooth_basic_updated/src/bluetooth_device.dart';

class PrinterProvider extends ChangeNotifier {
  // void setBlueToothDevice(BluetoothDevice device) {
  //   deviceConnection = device;
  //   print('devei n provider : $device');
  //   notifyListeners();
  // }

  // BluetoothDevice? _deviceConnection;
  // BluetoothDevice? get deviceConnection => _deviceConnection;
  // set deviceConnection(BluetoothDevice? newDeviceConnection) {
  //   _deviceConnection = newDeviceConnection;

  //   notifyListeners();
  //   print('newDeviceConnection : ${newDeviceConnection?.name}');
  // }

  // List<BluetoothDevice> _listDeviceConnected = [];
  // List<BluetoothDevice> get listDeviceConnecteds => _listDeviceConnected;
  // set listDeviceConnecteds(List<BluetoothDevice> newDevice) {
  //   _listDeviceConnected = newDevice;
  //   print('newDevice  : $newDevice');
  //   notifyListeners();
  // }

  //-------- Wifi --------

  IPAddress? _printIpAddress;
  IPAddress? get printIpAddress => _printIpAddress;
  set printIpAddress(IPAddress? newIpAddress) {
    _printIpAddress = newIpAddress;
    notifyListeners();
  }

  //-------- Bluetooth --------

  PrinterBluetooth? deviceIos;
  BluetoothInfo? deviceAndr;

  void setBlueToothIOSDevice(PrinterBluetooth printer) {
    deviceIos = printer;
    notifyListeners();
  }

  void setBlueToothAndroDevice(BluetoothInfo printer) {
    deviceAndr = printer;
    notifyListeners();
  }

  Future<void> ensureInitialization() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      final jsonStringBlt = prefs.getString(StorageKeys.PRINTER_KEY);
      final prinertIP = prefs.getString(StorageKeys.PRINT_IP);

      if (jsonStringBlt != null) {
        Map<String, dynamic> jsonMap = jsonDecode(jsonStringBlt);

        if (Platform.isIOS) {
          BluetoothDevice device = BluetoothDevice.fromJson(jsonMap);

          deviceIos = PrinterBluetooth(device);
          print('printer prefs.getString 1  : $deviceIos ');
        } else if (Platform.isAndroid) {
          print('jsonMap["macAdress"] : ${jsonMap["macAdress"]}');

          deviceAndr = BluetoothInfo(
              name: jsonMap["name"], macAdress: jsonMap["macAdress"]);
        }
      }

      if (prinertIP != null) {
        printIpAddress = IPAddress.fromString(prinertIP);
        print('printer prefs.getString   : ${printIpAddress} ');
      }
    } catch (e) {
      print('error ensureInitialization : $e');
    }
  }
}

class IPAddress {
  String ipAddress;
  int port;

  IPAddress({required this.ipAddress, this.port = 9100});

  factory IPAddress.fromString(String input) {
    final parts = input.split(':');

    if (parts.length == 1) {
      return IPAddress(ipAddress: parts[0].trim(), port: 9100);
    } else if (parts.length == 2) {
      final ipAddress = parts[0].trim();
      final port = int.tryParse(parts[1].trim()) ?? 9100;
      return IPAddress(ipAddress: ipAddress, port: port);
    } else {
      throw ArgumentError('Invalid input format. Expected "ip:port".');
    }
  }
  @override
  String toString() {
    return '$ipAddress:$port';
  }
}
