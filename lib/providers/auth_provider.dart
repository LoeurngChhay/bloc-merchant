import 'package:bloc_merchant_mobile_2/data/models/responses/login_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/auth_repo.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider extends ChangeNotifier {
  final AuthRepo authRepo;
  final Function(String)? onTokenChanged;
  AuthProvider({required this.authRepo, this.onTokenChanged});

  bool get isLoggedIn => _token != null;

  bool _loadingLogout = false;
  bool get loadingLogout => _loadingLogout;
  set loadingLogout(bool newToken) {
    _loadingLogout = newToken;
    notifyListeners();
  }

  String? _token;
  String? get token => _token;
  set token(String? newToken) {
    _token = newToken;
    notifyListeners();
    if (onTokenChanged != null) {
      onTokenChanged!(newToken ?? "");
    }
  }

  String? _fcmToken;
  String? get fcmToken => _fcmToken;
  set fcmToken(String? newToken) {
    _fcmToken = newToken;
    notifyListeners();
  }

  Shop? _shop;
  Shop? get shop => _shop;
  set shop(Shop? newValue) {
    _shop = newValue;
    notifyListeners();
  }

  final _preferences = SharedPreferences.getInstance();

  Future<void> logout() async {
    loadingLogout = true;
    final pref = await _preferences;

    pref.remove(StorageKeys.TOKEN_KEY);
    await FirebaseMessaging.instance.deleteToken();
    pref.clear();

    loadingLogout = false;
  }

  Future<void> setShop(Shop shopValue) async {
    shop = shopValue;
  }

  Future<void> ensureInitialization() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      final tokenString = prefs.getString(StorageKeys.TOKEN_KEY);
      final fcmTokenString = prefs.getString(StorageKeys.FCM_KEY);
      final shopString = prefs.getString(StorageKeys.USER_KEY);

      // print('fcmTokenString : $fcmTokenString');
      if (shopString != null) {
        shop = Shop.fromString(shopString);
      }

      // print('Token  : $fcmTokenString');
      if (tokenString != null) {
        token = tokenString.toString();
        fcmToken = fcmTokenString.toString();
      }
    } catch (e) {
      print('error ensureInitialization : $e');
    }
  }
}
