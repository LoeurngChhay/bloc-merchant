import 'dart:io';

import 'package:bloc_merchant_mobile_2/app_container.dart';
import 'package:bloc_merchant_mobile_2/constants/language.dart';
import 'package:bloc_merchant_mobile_2/data/repos/auth_repo.dart';
import 'package:bloc_merchant_mobile_2/firebase_options.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/providers/route_observer_provider.dart';
import 'package:bloc_merchant_mobile_2/providers/theme_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
final RouteObserverProvider routeObserver = RouteObserverProvider();

@pragma('vm:entry-point')
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  await setupFlutterNotifications();
  showFlutterNotification(message);

  // print('Handling a background message ${message.data}');
}

late AndroidNotificationChannel channel;
bool isFlutterLocalNotificationsInitialized = false;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

Future<void> setupFlutterNotifications({RemoteMessage? message}) async {
  if (isFlutterLocalNotificationsInitialized) {
    return;
  }

  channel = const AndroidNotificationChannel(
    "bloc_merchant_notication", // id
    'BLOC Merchant Notification', // title
    description: 'For bloc merchant use only.', // description
    importance: Importance.high,
    playSound: true,
  );
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  isFlutterLocalNotificationsInitialized = true;
}

void showFlutterNotification(RemoteMessage message) {
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;

  if (notification != null && android != null && !kIsWeb) {
    BigTextStyleInformation bigTextStyleInformation = BigTextStyleInformation(
      notification.body!,
      htmlFormatBigText: true,
      contentTitle: notification.titleLocKey,
      htmlFormatContentTitle: true,
    );

    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.titleLocKey,
        notification.body,
        NotificationDetails(
          iOS: DarwinNotificationDetails(
            // sound: 'tickal.mp3',
            sound: message.data["sound"],
            presentAlert: true,
            presentBadge: true,
            presentBanner: true,
          ),
          android: AndroidNotificationDetails(
            // channel.id,
            message.data["android_channel_id"],
            channel.name,
            icon: "@mipmap/ic_launcher",
            channelDescription: channel.description,
            importance: Importance.max,
            styleInformation: bigTextStyleInformation,
            priority: Priority.max,
            playSound: true,
          ),
        ));
  }
}

Future<void> initializeNotificationSettings() async {
  if (Platform.isAndroid) {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.requestNotificationsPermission();
  } else {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(alert: true, badge: true, sound: true);
  }

  var initializationSettingsAndroid =
      const AndroidInitializationSettings("@mipmap/ic_launcher");
  var initializationSettingsIOS = const DarwinInitializationSettings(
    requestSoundPermission: true,
    requestBadgePermission: true,
    requestAlertPermission: true,
  );

  var initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
  );

  await flutterLocalNotificationsPlugin
      .initialize(initializationSettings,
          onDidReceiveNotificationResponse: onDidReceiveNotificationResponse)
      .then((value) => print("local notification initialized $value"));
}

void onDidReceiveNotificationResponse(
    NotificationResponse notificationResponse) async {
  // final String? payload = notificationResponse.payload;
  // print('notificationResponse.payload  : $payload ');
  if (notificationResponse.payload != null) {
    // debugPrint('notification payload: $payload');

    final currentRoute = routeObserver.currentRoute;
    String targetRoute = OrderView.routeName;

    // print("currentRoute  : $currentRoute");

    if (currentRoute != targetRoute) {
      navigatorKey.currentState?.pushNamed(targetRoute);
    }
  }

  ///Navigator
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized().catchError((e) {
    // print("err localize : ${e.toString()}");
  }).then((value) {
    // print("initialized localized");
  });

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  if (!kIsWeb) {
    await setupFlutterNotifications();
  }

  setupLocator();

  runApp(EasyLocalization(
      path: 'assets/translations',
      fallbackLocale: Locales.en,
      supportedLocales: const [Locales.en, Locales.km, Locales.ch],
      child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Future<void> initFirebase() async {
    final preferences = SharedPreferences.getInstance();
    final pref = await preferences;

    // final apnsToken = await FirebaseMessaging.instance.getAPNSToken();

    // print("apnsToken $apnsToken");
    // print("init firebase");
    // ignore: body_might_complete_normally_catch_error
    final token = await FirebaseMessaging.instance.getToken().catchError((e) {
      print("err fcm $e");
    });

    print("fcm id $token");
    pref.setString(StorageKeys.FCM_KEY, token.toString());

    FirebaseMessaging.instance
        .subscribeToTopic("Order")
        .then((_) => print("subscribe success Order"))
        .catchError((e) => print("subscribe failed ${e.toString()}"));

    FirebaseMessaging.onMessage.listen(
      (message) {
        showFlutterNotification(message);
      },
    );
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    final messageInitial = await FirebaseMessaging.instance.getInitialMessage();
    print('messageInitial : ${messageInitial?.data}');

    if (messageInitial != null) {
      _handleMessage(messageInitial);
    }

    FirebaseMessaging.onMessageOpenedApp.listen(
      (message) {
        print('onMessageOpenedApp : ${message.data}');
        _handleMessage(message);
      },
    );
  }

  void _handleMessage(RemoteMessage message) {
    final currentRoute = routeObserver.currentRoute;
    String targetRoute = OrderView.routeName;

    if (currentRoute != targetRoute) {
      navigatorKey.currentState?.pushNamed(targetRoute);
    }
  }

  Future<void> init() async {
    try {
      await initializeNotificationSettings();
      await initFirebase();
    } catch (e) {
      print("err init :${e.toString()}");
    }
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ThemeProvider()),
        ChangeNotifierProvider(
          create: (_) => AuthProvider(
              onTokenChanged: (token) {
                // print("on token changed : ${token}");
              },
              authRepo: locator<AuthRepo>()),
        ),
        ChangeNotifierProvider(
          create: (_) => PrinterProvider(),
        ),
      ],
      child: const AppContainer(),
    );
  }
}
