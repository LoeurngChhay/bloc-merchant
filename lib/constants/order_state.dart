abstract class OrderState {
  static const int newOrder = 0;
  static const int waiting = 1;
  static const int picker = 2;
  static const int dilivery = 3;
}

abstract class HistoryOrderState {
  static const int process = 2;
  static const int completed = 4;
  static const int cancel = -1;
}

abstract class CancelReason {
  static const int outOfStock = 0;
  static const int noChef = 1;
  static const int notAvailable = 2;
  static const int toManyOrders = 3;
}

abstract class OrderSoundState {
  static const String sound = "neworder";
  static const String cancel = "tickal";
}
