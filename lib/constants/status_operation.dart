abstract class StatusOperation {
  static const String close = "0";
  static const String open = "1";
}

abstract class CloseOperationType {
  static const int openTmr = 0;
  static const int openDate = 1;
  static const int closeTemp = 2;
}
