abstract class ProductStatus {
  static const String hide = "0";
  static const String show = "1";
  static const int url = 0;
  static const int file = 1;
}

abstract class FormatStatus {
  static const String standard = "0";
  static const String advance = "1";
  static const String unlimited = "0";
  static const String limited = "1";
}

abstract class ProductTypeStatus {
  static const String standard = "0";
  static const String topping = "1";
}
