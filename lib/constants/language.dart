import 'package:flutter/material.dart';

abstract class Locales {
  static const en = Locale('en', 'US');
  static const km = Locale('km', 'KH');
  static const ch = Locale('zh', 'CN');
}
