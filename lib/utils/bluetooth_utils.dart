import 'package:flutter/services.dart';

//this utils work config only android
class BluetoothUtils {
  static const MethodChannel _channel = MethodChannel('bluetooth');

  static Future<void> openBluetoothSettings() async {
    try {
      await _channel.invokeMethod('openBluetoothSettings');
    } on PlatformException catch (e) {
      print('Failed to open Bluetooth settings: ${e.message}');
    }
  }

  static Future<bool> checkBluetoothStatus() async {
    try {
      final bool isEnabled = await _channel.invokeMethod('isBluetoothEnabled');

      print('isEnabled :$isEnabled');
      return isEnabled;
    } on PlatformException catch (e) {
      print("Failed to get Bluetooth status: '${e.message}'.");
    }

    return false;
  }
}
