import 'dart:typed_data';
import 'dart:ui';

import 'package:bloc_merchant_mobile_2/data/models/responses/order_addon.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:flutter/material.dart' hide Image;

abstract class PrinterUtils {
  static Future<Uint8List> createImageFromText(String text,
      {int width = 300, int height = 100}) async {
    ByteData? byteData;

    try {
      final pictureRecorder = PictureRecorder();
      final canvas = Canvas(pictureRecorder);

      // Set a white background
      final paint = Paint()..color = Colors.white;
      canvas.drawRect(
          Rect.fromLTWH(0, 0, width.toDouble(), height.toDouble()), paint);

      // Draw the text
      final textPainter = TextPainter(
        text: TextSpan(
          text: text,
          style: const TextStyle(
              color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
        ),
        textDirection: TextDirection.ltr,
      );

      textPainter.layout(minWidth: 0, maxWidth: width.toDouble());
      textPainter.paint(
        canvas,
        const Offset(30, 15),
      );

      // End recording and convert to image
      final picture = pictureRecorder.endRecording();
      final image = await picture.toImage(width, height);

      // Convert the image to byte data
      byteData = await image.toByteData(format: ImageByteFormat.png);
      print("byteData  : ${byteData!.buffer.asUint8List()}");
    } catch (e) {
      print('error convert : $e');
    }
    return byteData!.buffer.asUint8List();
  }

  static Future<Uint8List?> createImageName(String text,
      {double? width = 300, double? height = 100}) async {
    try {
      PictureRecorder? recorder = PictureRecorder();

      Canvas canvas = Canvas(recorder);
      final Paint painter = Paint()
        ..color = Colors.white
        ..style = PaintingStyle.fill;
      canvas.drawPaint(painter);

      final TextPainter textPainter = TextPainter(
        text: TextSpan(
            text: text,
            style: const TextStyle(
                color: Colors.black,
                fontSize: 34,
                // fontWeight: FontWeight.bold,
                fontFamily: 'sans-serif')),
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr,
        maxLines: 3,
      )..layout(maxWidth: width!);

      final offset = Offset((width - textPainter.width) / 2, 0.0);

      if (textPainter.width <= width) {
        height = 60;
        textPainter.paint(canvas, offset);
      } else {
        height = 90;
        textPainter.paint(canvas, const Offset(0.0, 0.0));
      }

      Image canvasImage =
          await recorder.endRecording().toImage(width.toInt(), height.toInt());
      ByteData? canvasByteData =
          await canvasImage.toByteData(format: ImageByteFormat.png);
      Uint8List canvasUint8List = Uint8List.view(canvasByteData!.buffer);

      return canvasUint8List;
    } catch (err) {
      return null;
    }
  }

  static Future<Uint8List?> createImageProduct(
      {double? width, Product? item, OrderProductAddon? toppping}) async {
    try {
      double fontSize = 24;
      String fontFamily = "sans-serif";
      var style = TextStyle(
          fontSize: fontSize,
          color: Colors.black,
          fontFamily: fontFamily,
          fontStyle: FontStyle.normal);

      // double height = 90;
      double height =
          item?.product_addon != null && item!.product_addon!.isNotEmpty
              ? 100
              : 80;
      double titleX = 0;
      double qtyX = 350;
      double priceX = 280;
      int maxTitleChar = 17;

      // String title = "BlOc Testing តេស្តអក្សរខ្មែរ តេស្តអក្សរខ្មែរ";
      String title = item!.product_title.toString();
      String? customizeTextFirst = title.length >= maxTitleChar
          ? title.substring(0, maxTitleChar)
          : title;

      String? customizeTextLast = title.length >= maxTitleChar
          ? title.substring(maxTitleChar, title.length)
          : '';

      PictureRecorder? recorder = PictureRecorder();

      Canvas canvas = Canvas(recorder);

      final Paint painter = Paint()
        ..color = Colors.white
        ..style = PaintingStyle.fill;

      canvas.drawPaint(painter);

      // -------------- product name --------------

      final TextPainter titleFirstPainter = TextPainter(
        text: TextSpan(text: customizeTextFirst, style: style),
        textAlign: TextAlign.left, textDirection: TextDirection.ltr,
        maxLines: 1,
        //ellipsis: "..."
      )..layout(maxWidth: width! - 120);
      titleFirstPainter.paint(canvas, Offset(titleX, 0.0));

      if (title.length >= maxTitleChar) {
        final TextPainter titleLastPainter = TextPainter(
          text: TextSpan(text: customizeTextLast, style: style),
          textAlign: TextAlign.left, textDirection: TextDirection.ltr,
          maxLines: 1,
          //ellipsis: "..."
        )..layout(maxWidth: width - 120);
        double heihtOff = titleFirstPainter.height;

        titleLastPainter.paint(canvas, Offset(titleX, heihtOff));
      }

      // -------------- product qty --------------
      final TextPainter qtyPainter = TextPainter(
        text: TextSpan(
          text: "x${item.product_number}",
          style: style.copyWith(fontWeight: FontWeight.bold, fontSize: 28),
        ),
        textAlign: TextAlign.right, textDirection: TextDirection.ltr,
        maxLines: 2,
        //ellipsis: "..."
      )..layout(maxWidth: 80);

      double qtyWidth = width - qtyX;
      double newQtyOff = qtyX + (qtyWidth - qtyPainter.width);
      qtyPainter.paint(canvas, Offset(newQtyOff, 0));

      // -------------- product price --------------
      final TextPainter pricePainter = TextPainter(
        text: TextSpan(
            text: "${double.parse(item.product_price!).toStringAsFixed(2)}\$",
            style: style),
        textAlign: TextAlign.right,
        textDirection: TextDirection.ltr,
        maxLines: 1,
      )..layout(maxWidth: 100);

      double prWidth = width - priceX;
      double heihtOff = titleFirstPainter.height + 5;
      double newOff = priceX + (prWidth - pricePainter.width);
      pricePainter.paint(canvas, Offset(newOff, heihtOff));

      if (title.length >= maxTitleChar) {
        final TextPainter titleLastPainter = TextPainter(
          text: TextSpan(text: customizeTextLast, style: style),
          textAlign: TextAlign.left, textDirection: TextDirection.ltr,
          maxLines: 1,
          //ellipsis: "..."
        )..layout(maxWidth: width - 120);
        double heihtOff = titleFirstPainter.height;

        titleLastPainter.paint(canvas, Offset(titleX, heihtOff));
      }

      //topping
      double spaceY = 45; // spacing vertical
      for (var topping in item.product_addon!) {
        // -------------- topping title --------------
        final TextPainter toppingTitle = TextPainter(
          text: TextSpan(
            text: topping.product_name,
            style: style.copyWith(fontSize: 16),
          ),
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr,
          maxLines: 1,
        )..layout(
            maxWidth: width - 120,
          );

        // Paint the topping title
        toppingTitle.paint(
            canvas, Offset(titleX + 20, toppingTitle.height + spaceY));

        // -------------- topping price --------------
        final TextPainter toppingPrice = TextPainter(
          text: TextSpan(
            text: "${double.parse(topping.price!).toStringAsFixed(2)}\$",
            style: style.copyWith(fontSize: 16),
          ),
          textAlign: TextAlign.right,
          textDirection: TextDirection.ltr,
          maxLines: 1,
        )..layout(maxWidth: 100);

        double prWidth = width - priceX;
        double priceOffsetX = priceX + (prWidth - toppingPrice.width);

        // Paint the topping price
        toppingPrice.paint(
            canvas, Offset(priceOffsetX, toppingTitle.height + spaceY));

        spaceY += 20;
      }

      Image canvasImage =
          await recorder.endRecording().toImage(width.toInt(), height.toInt());
      ByteData? canvasByteData =
          await canvasImage.toByteData(format: ImageByteFormat.png);
      Uint8List canvasUint8List = Uint8List.view(canvasByteData!.buffer);

      return canvasUint8List;
    } catch (err) {
      return null;
    }
  }
}
