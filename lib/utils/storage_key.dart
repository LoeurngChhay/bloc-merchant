// ignore_for_file: constant_identifier_names

abstract class StorageKeys {
  static const TOKEN_KEY = "token";
  static const USER_KEY = "user";
  static const FCM_KEY = 'fcm';
  static const SHOP_KEY = 'shop';
  static const LANG_KEY = 'language';
  static const PRINTER_KEY = 'printer';
  static const PRINT_IP = 'print_ip';
}
