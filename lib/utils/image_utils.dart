import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:image/image.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class ImageUtils {
  static Future<CroppedFile?> cropImage({required File pickedFile}) async {
    final croppedFile = await ImageCropper().cropImage(
      sourcePath: pickedFile.path,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.black,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: true,
          activeControlsWidgetColor: Colors.red,
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
          ],
        ),
        IOSUiSettings(
          title: 'Cropper',
          minimumAspectRatio: 1,
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
          ],
        ),
      ],
    );

    return croppedFile;
  }

  static Future<double?> aspectRatio(File imageFile) async {
    final decodedImage = decodeImage(imageFile.readAsBytesSync());
    final aspect =
        decodedImage != null ? decodedImage.width / decodedImage.height : null;
    return aspect;
  }

  static Future<File> convertImageToFile(Image image, String path) async {
    final newPath = await croppedFilePath(path);
    final jpegBytes = encodeJpg(image);

    final convertedFile = await File(newPath).writeAsBytes(jpegBytes);
    await File(path).delete();
    return convertedFile;
  }

  static Future<String> croppedFilePath(String path) async {
    final tempDir = await getTemporaryDirectory();
    return join(
      tempDir.path,
      '${basenameWithoutExtension(path)}_compressed.jpg',
    );
  }

  static Future<File> downloadImage(String url) async {
    try {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      String savePath = join(appDocDir.path, url.split('/').last);

      Dio dio = Dio();

      await dio.download(url, savePath);

      return File(savePath);
    } catch (e) {
      throw Exception("Failed to download image: $e");
    }
  }

  static Future<CroppedFile?> cropImageUrl(String url) async {
    final imageFile = await ImageUtils.downloadImage(url);
    final cropFile = await ImageUtils.cropImage(pickedFile: imageFile);

    return cropFile;
  }
}
