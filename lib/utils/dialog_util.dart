import 'package:bloc_merchant_mobile_2/ui/themes/colors.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class DialogUtil {
  static Future<void> cusAlertDialog({
    required BuildContext context,
    required String message,
    int? millisecond,
    int? closeTime,
    bool pop = false,
    String? popMessage,
  }) async {
    Future.delayed(Duration(milliseconds: millisecond ?? 300), () {
      showDialog<String>(
          context: context,
          useSafeArea: false,
          builder: (context) {
            Future.delayed(Duration(milliseconds: closeTime ?? 1000), () {
              Navigator.pop(context);
            });

            return Center(
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: Spacing.sm, horizontal: Spacing.l),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onBackground,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text(
                  message,
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: Theme.of(context).colorScheme.background),
                ),
              ),
            );
          });
    }).then((value) {
      if (pop) {
        Navigator.pop(context, popMessage);
      }
    });
  }

  static Future<void> cusConfirmDialog({
    required BuildContext context,
    String confirm = 'Confirm',
    String? title,
    Function()? onPressed,
    Color? colors,
    IconData? icon,
  }) async {
    final theme = Theme.of(context);
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
            clipBehavior: Clip.hardEdge,
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.3,
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Container(
                    width: double.infinity,
                    height: 100,
                    color: colors ?? theme.colorScheme.primary,
                    child: Icon(
                      icon ?? Icons.check_circle_outline_rounded,
                      size: 72,
                      color: Colors.white,
                    )),
                const SizedBox(height: Spacing.l),
                Text("Are you sure?",
                    style: theme.textTheme.titleLarge
                        ?.copyWith(fontWeight: FontWeight.bold)),
                if (title != null)
                  Text(title, style: theme.textTheme.titleMedium),
                const SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("Cancel")),
                    const SizedBox(width: Spacing.l),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: redPrimary),
                        onPressed: onPressed,
                        child: Text(
                          confirm,
                          style: const TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )),
                  ],
                ),
                const SizedBox(height: Spacing.l),
              ]),
            ));
      },
    );
  }

  static Future<void> cusAlertSnackBar(BuildContext context, message,
      {int milliseconds = 600, bool isError = false}) async {
    final snackBar = SnackBar(
      width: MediaQuery.of(context).size.width - 32,
      padding: const EdgeInsets.symmetric(vertical: 8),
      duration: Duration(milliseconds: milliseconds),
      backgroundColor: isError ? Colors.red : Colors.black.withOpacity(0.7),
      content: GestureDetector(
        onTap: () {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
        child: Center(
          child: Text("$message",
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontWeight: FontWeight.w600, color: Colors.white)),
        ),
      ),
      behavior: SnackBarBehavior.floating,
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
