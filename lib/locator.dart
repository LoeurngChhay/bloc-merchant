import 'package:bloc_merchant_mobile_2/client_request.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/auth_repo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/category_repo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/home_repo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/product_repo.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton<AuthRepo>(() => AuthRepoImpl());
  locator.registerLazySingleton<ProductRepo>(() => ProductRepo());
  locator.registerLazySingleton<HomeRepo>(() => HomeRepoImpl());
  locator.registerLazySingleton<CategoryRepo>(() => CategoryRepoImpl());
  locator
      .registerLazySingleton<AdsRepo>(() => AdsRepo(request: locator<Dio>()));
  locator.registerLazySingleton<OrderRepo>(
      () => OrderRepoImpl(request: locator<Dio>()));
  locator.registerSingleton<Dio>(ClientRequest().dio);
}
