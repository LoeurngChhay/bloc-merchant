import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/route_api/routes_api.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'package:shared_preferences/shared_preferences.dart';

class ClientRequest {
  Dio dio = Dio();

  Future<String?> getToken() async {
    final SharedPreferences storage = await SharedPreferences.getInstance();
    final tokenResponse = storage.getString(StorageKeys.TOKEN_KEY);
    // print('getToken : $tokenResponse');

    if (tokenResponse != null) {
      return tokenResponse;
    }
    return "";
  }

  Future<String?> getFcmToken() async {
    final SharedPreferences storage = await SharedPreferences.getInstance();
    final tokenResponse = storage.getString(StorageKeys.FCM_KEY);

    if (tokenResponse != null) {
      return tokenResponse;
    }
    return "";
  }

  Future<String?> getRefreshToken() async {
    //   final SharedPreferences storage = await SharedPreferences.getInstance();
    //   final tokenResponse = storage.getString(StorageKeys.TOKEN_KEY);
    //   if (tokenResponse != null) {
    //     // print("getRefreshToken : ${Token.fromJson(tokenResponse).refresh_token}");
    //     return LoginResponse.fromJson(tokenResponse).refresh_token;
    //   }
    //   return "";
    // }

    // Future<String?> refreshToken() async {
    //   AuthRepo authRepo = AuthRepoImpl();

    //   final refreshToken = await getRefreshToken(); //get old refreshToken
    //   if (refreshToken!.isEmpty) {
    //     throw "";
    //   }
    //   final response = await authRepo.refreshToken(refreshToken);

    //   if (response.status == 401) {
    //     return response.message;
    //   }

    // return response.data!.token;
    return "";
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return dio.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }

  ClientRequest() {
    if (kDebugMode) {
      dio.interceptors
          .add(LogInterceptor(requestBody: true, responseBody: true));
    }

    // if (Platform.isAndroid) {
    //   (dio.httpClientAdapter as IOHttpClientAdapter).onHttpClientCreate =
    //       (client) {
    //     client.badCertificateCallback =
    //         (X509Certificate cert, String host, int port) => true;
    //     return client;
    //   };
    // }

    dio.options.baseUrl = HttpApi.baseUrl;
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest:
            (RequestOptions options, RequestInterceptorHandler handler) async {
          final token = await ClientRequest().getToken();
          final fcmToken = await ClientRequest().getFcmToken();

          // print('fcmToken : $fcmToken');

          if (token!.isNotEmpty) {
            options.headers['token'] = token;
            // options.headers['user-agent'] = "com.bongtk.blocmerchant";
            options.headers['Client-Type'] = HttpApi.clientType;
            options.headers['Client-Os'] =
                Platform.isAndroid ? "ANDROID" : "IOS";
            options.headers['Client-Bundle'] = HttpApi.bundle;
            options.headers['Client-Language'] = HttpApi.language;
            options.headers['Fcm-Id'] = fcmToken;
          }

          return handler.next(options);
        },
        onResponse: (Response response, ResponseInterceptorHandler handler) {
          // print('response.data .. : ${response.data}');
          if (response.data["error"] != 0) {
            return handler.reject(DioException(
                type: DioExceptionType.unknown,
                requestOptions: response.requestOptions,
                response: response,
                error: response,
                message: response.data['message']));
          }

          return handler.next(response);
        },
        onError: (DioException error, ErrorInterceptorHandler handler) {
          print("error response in CustomInterceptor : $error");
          return handler.next(error);
        },
      ),
    );
  }
}
