import 'package:bloc_merchant_mobile_2/constants/language.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageView extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  static const String routeName = '/language';
  const LanguageView({super.key});

  @override
  LanguageViewState createState() => LanguageViewState();
}

class LanguageViewState extends State<LanguageView>
    with SingleTickerProviderStateMixin {
  Size? screenSize;
  String change = "";
  final _preferences = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.language.tr(),
              style: const TextStyle(color: Colors.white)),
          centerTitle: true,
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          leading: InkWell(
            borderRadius: BorderRadius.circular(30.0),
            child: const Icon(Icons.arrow_back_ios, color: Colors.white),
            onTap: () {
              Navigator.pop(context, change);
            },
          ),
        ),
        backgroundColor: Colors.white,
        body: ListView(padding: const EdgeInsets.all(10), children: [
          Card(
            elevation: 0,
            margin: const EdgeInsets.all(0),
            child: ListTile(
              leading: CircleAvatar(
                radius: 24,
                child: Image.asset("assets/images/flags/khmer.png",
                    width: 100, height: 100, fit: BoxFit.fill),
              ),
              title: Text(LocaleKeys.km.tr()),
              subtitle: Text('km_l'.tr()),
              onTap: () async {
                changeLaungue(Locales.km, context);
              },
            ),
          ),
          const Divider(
            color: Colors.black12,
            height: 1.0,
            indent: 75,
          ),
          Card(
            elevation: 0,
            margin: const EdgeInsets.all(0),
            child: ListTile(
              leading: CircleAvatar(
                radius: 24,
                child: Image.asset("assets/images/flags/english.png",
                    width: 100, height: 100, fit: BoxFit.fill),
              ),
              title: Text(LocaleKeys.en.tr()),
              subtitle: Text(LocaleKeys.en_l.tr()),
              onTap: () async {
                changeLaungue(Locales.en, context);
              },
            ),
          ),
          const Divider(
            color: Colors.black12,
            height: 1.0,
            indent: 75,
          ),
          Card(
            elevation: 0,
            margin: const EdgeInsets.all(0),
            child: ListTile(
              leading: CircleAvatar(
                radius: 24,
                child: Image.asset("assets/images/flags/chinese.png",
                    width: 100, height: 100, fit: BoxFit.fill),
              ),
              title: Text(LocaleKeys.zh.tr()),
              subtitle: Text(LocaleKeys.zh_l.tr()),
              onTap: () async {
                changeLaungue(Locales.ch, context);
              },
            ),
          ),
          const Divider(
            color: Colors.black12,
            height: 1.0,
            indent: 75,
          ),
        ]));
  }

  Future<void> changeLaungue(Locale lang, BuildContext context) async {
    // final newLang = Locales(lang);

    await context.setLocale(lang);
    final prefs = await _preferences;

    prefs.setString(StorageKeys.LANG_KEY, lang.toString());

    // setState(() {
    //   change = lang;
    // });
  }
}
