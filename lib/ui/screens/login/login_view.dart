import 'package:bloc_merchant_mobile_2/data/repos/auth_repo.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/home_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/login/login_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatefulWidget {
  static const String routeName = '/login';
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final _phoneNumberController = TextEditingController();
  final _passwordController = TextEditingController();

  bool showPass = false;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ChangeNotifierProvider<LoginViewController>(
      create: (context) => LoginViewController(
          authRepo: locator<AuthRepo>(),
          authProvider: context.read<AuthProvider>(),
          onLoginError: (message) {
            showCupertinoDialog(
              barrierDismissible: true,
              context: context,
              builder: (context) => AlertDialog.adaptive(
                title: const Text("Login Fail"),
                content: Text(message.toString()),
              ),
            );
          },
          onLoginSucess: () {
            Navigator.of(context).pushReplacementNamed(HomeView.routeName);
          }),
      child: Consumer<LoginViewController>(
          builder: (context, valueController, child) {
        return Scaffold(
          body: GestureDetector(
            onTap: () =>
                FocusScope.of(context).requestFocus(FocusNode()), //remove focus
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.blue.shade100, Colors.black54],
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            tileMode: TileMode.repeated)),
                  ),
                  CustomPaint(
                    painter: LoginBackground(),
                    size: Size(MediaQuery.of(context).size.width,
                        MediaQuery.of(context).size.height),
                  ),
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    padding:
                        const EdgeInsets.symmetric(horizontal: Spacing.normal),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: Spacing.l),
                        Image.asset('assets/images/logo.png', height: 270),
                        const SizedBox(height: Spacing.l),
                        CusTextField(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: Spacing.normal),
                          controller: _phoneNumberController,
                          hintText: 'Phone Number',
                          keyboardType: TextInputType.number,
                          backgroundColors: Colors.white,
                          prefixIcon: Icon(Icons.phone_android_rounded,
                              color: Theme.of(context)
                                  .colorScheme
                                  .onBackground
                                  .withOpacity(0.8)),
                          suffixIcon: IconButton(
                              onPressed: () {
                                _phoneNumberController.clear();
                              },
                              icon: const Icon(Icons.close, size: 20)),
                        ),
                        const SizedBox(height: Spacing.normal),
                        CusTextField(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: Spacing.normal),
                          controller: _passwordController,
                          showPass: !showPass,
                          // labelText: 'Password',
                          backgroundColors: Colors.white,
                          hintText: 'Password',
                          prefixIcon: Icon(
                            Icons.lock_rounded,
                            color: Theme.of(context)
                                .colorScheme
                                .onBackground
                                .withOpacity(0.8),
                          ),
                          suffixIcon: IconButton(
                            icon: showPass
                                ? Icon(Icons.visibility_off,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onBackground
                                        .withOpacity(0.8))
                                : Icon(Icons.visibility,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onBackground
                                        .withOpacity(0.8)),
                            onPressed: () {
                              setState(() {
                                showPass = !showPass;
                              });
                            },
                          ),
                        ),
                        const SizedBox(height: Spacing.m),
                        Align(
                          alignment: Alignment.centerRight,
                          child: TextButton(
                              onPressed: () {},
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero),
                              child: Text(
                                "Forgot Password?",
                                style: theme.textTheme.titleMedium
                                    ?.copyWith(color: Colors.white),
                              )),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: FilledButton(
                                  style: FilledButton.styleFrom(
                                      fixedSize: const Size.fromHeight(45),
                                      backgroundColor: Colors.amber.shade700),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, HomeView.routeName);
                                  },
                                  child: const Text(
                                    'Quick Login',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                            const SizedBox(width: Spacing.normal),
                            Expanded(
                              child: FilledButton.icon(
                                  onPressed: valueController.loading
                                      ? () {}
                                      : () {
                                          // print(
                                          //     'number : ${_phoneNumberController.text}');
                                          // print(
                                          //     'nupassmber : ${_passwordController.text}');
                                          valueController.login(
                                              mobile:
                                                  _phoneNumberController.text,
                                              passwd: _passwordController.text);
                                        },
                                  style: FilledButton.styleFrom(
                                    fixedSize: const Size.fromHeight(45),
                                  ),
                                  icon: !valueController.loading
                                      ? const Icon(Icons.logout)
                                      : const SizedBox(
                                          height: 16,
                                          width: 16,
                                          child: CircularProgressIndicator(
                                            color: Colors.white,
                                            strokeWidth: 3,
                                          ),
                                        ),
                                  label: const Text(
                                    "Login",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )),
                            )
                          ],
                        ),
                        Container(
                            margin:
                                const EdgeInsets.symmetric(vertical: Spacing.m),
                            child: const Text('- OR -')),
                        Text(
                          'Call Us: 016 | 017 421 333',
                          style: theme.textTheme.titleLarge?.copyWith(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}

class LoginBackground extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var rect = const Offset(0.0, 0.0) & Size(size.width, size.height);
    var paint = Paint();
    paint
      ..color = Colors.black
      ..strokeWidth = 2.0
      ..shader = LinearGradient(
              colors: [Colors.blue, Colors.black.withOpacity(0.5)],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              tileMode: TileMode.clamp)
          .createShader(rect)
      ..style = PaintingStyle.fill;

    var path = Path();
    path.moveTo(size.width * 0, size.height);
    path.lineTo(size.width, size.height);
    var endPoint = Offset(0 * size.width / 4, 4 * size.height / 4);
    var ctlPoint = Offset(0.2 * size.width / 4, 3 * size.height / 4);
    path.quadraticBezierTo(ctlPoint.dx, ctlPoint.dy, endPoint.dx, endPoint.dy);

    paint.shader = LinearGradient(
            colors: [
          Colors.black.withOpacity(1.0),
          Colors.black.withOpacity(1.0)
        ],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            tileMode: TileMode.clamp)
        .createShader(rect);
    var path1 = Path();
    path1.moveTo(size.width * 1, size.height);
    path1.lineTo(size.width * 0, size.height);

    var endPoint1 = Offset(5 * size.width / 4, 4 * size.height / 4);
    var ctlPoint1 = Offset(4.5 * size.width / 4, 2.5 * size.height / 4);
    path1.quadraticBezierTo(
        ctlPoint1.dx, ctlPoint1.dy, endPoint1.dx, endPoint1.dy);

    canvas.drawPath(path1, paint);
    paint.shader = LinearGradient(
            colors: [
          Colors.blue.withOpacity(0.2),
          Colors.blue.withOpacity(0.3)
        ],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            tileMode: TileMode.clamp)
        .createShader(rect);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
