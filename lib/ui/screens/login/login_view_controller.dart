import 'package:bloc_merchant_mobile_2/data/repos/auth_repo.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class LoginViewController extends ChangeNotifier {
  final AuthRepo authRepo;
  final AuthProvider? authProvider;
  final Function()? onLoginSucess;
  final Function(String? message)? onLoginError;

  LoginViewController(
      {this.onLoginSucess,
      this.onLoginError,
      required this.authRepo,
      this.authProvider});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  Future<void> login({String? mobile, String? passwd}) async {
    loading = true;
    try {
      if (mobile != null) {
        if (mobile.startsWith('0')) {
          mobile = '855${mobile.substring(1)}';
        } else if (!mobile.startsWith('855')) {
          mobile = '855$mobile';
        }
      }

      final res = await authRepo.login(mobile: mobile, passwd: passwd);

      if (res.data?.shop != null) {
        authProvider?.setShop(res.data!.shop!);
      }

      if (onLoginSucess != null) {
        onLoginSucess!();
      }
    } catch (e) {
      print("Error lgoin : $e");
      if (e is DioException) {
        if (onLoginError != null) {
          onLoginError!(e.message);
        }
      }
    } finally {
      loading = false;
    }
  }
}
