import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/requests/create_product_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/get_product_data.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/data/repos/product_repo.dart';
import 'package:bloc_merchant_mobile_2/utils/image_utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CreateProductViewController extends ChangeNotifier {
  final ProductRepo productRepo;
  final String? productId;
  final Function(String)? onSuccess;
  final Function(String)? onError;
  final Function(GetProductData product)? getInitProduct;
  CreateProductViewController({
    required this.productRepo,
    this.productId,
    this.onSuccess,
    this.onError,
    this.getInitProduct,
  });

// ----- image -------
  List<File> imagesFile = [];

  List<ProductImages> _photos = [];
  List<ProductImages> get photos => _photos;
  set photos(List<ProductImages> value) {
    _photos = value;
  }

  // ----- video -------

  String? _video;
  String? get video => _video;
  set video(String? value) {
    _video = value;
    notifyListeners();
  }

  String? _videoId;
  String? get videoId => _videoId;
  set videoId(String? value) {
    _videoId = value;
    notifyListeners();
  }

  //--------------------

  Future<void> cropImage(String imagePath, {bool? isUrl = false}) async {
    File imageFile = File(imagePath);

    loadingImage = true;

    try {
      if (isUrl!) {
        imageFile = await ImageUtils.downloadImage(imagePath);
      }

      final cropFile = await ImageUtils.cropImage(pickedFile: imageFile);

      final findIndex = photos.indexWhere((e) => e.photo == imagePath);
      // final findFileIndex = imagesFile.indexWhere((e) => e.path == imagePath);

      if (cropFile != null) {
        final aspectRatio = await ImageUtils.aspectRatio(File(cropFile.path));
        if (findIndex != -1) {
          photos[findIndex] = ProductImages(
              photo: cropFile.path,
              type: 1,
              name: cropFile.path.split('/').last,
              aspect: aspectRatio);
        }

        imagesFile.add(File(cropFile.path));

        notifyListeners();
      }
    } catch (e) {
      print('Error cropImage : $e');
    } finally {
      loadingImage = false;
    }
  }

  Future<void> uploadImage({List<File> images = const []}) async {
    if (images.isNotEmpty) {
      //format photo info
      for (var e in images) {
        final aspectRatio = await ImageUtils.aspectRatio(File(e.path));

        photos.add(
          ProductImages(
              photo: e.path,
              type: 1,
              name: e.path.split('/').last,
              aspect: aspectRatio),
        );

        imagesFile.add(File(e.path));
      }
      notifyListeners();
    }
  }

  Future<void> uploadVideo() async {
    final ImagePicker picker = ImagePicker();
    final file = await picker.pickVideo(source: ImageSource.gallery);

    if (file != null) {
      video = file.path;
    }
  }

  void removeVideo() {
    video = null;
  }

  Future<void> create(CreateProductRequest req) async {
    req.photos = photos;
    req.images = imagesFile;
    req.videoId = videoId;
    req.videoUrl = video;

    if (video != null && videoId == null) {
      req.video = File(video!);
    }

    try {
      loading = true;

      final respose = await productRepo.actionProduct(req);

      if (onSuccess != null) {
        onSuccess!(respose.message);
      }
    } catch (e) {
      print("Error create product  : $e");
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loading = false;
    }
  }

  GetProductData? _productDetail;
  GetProductData? get productDetail => _productDetail;
  set productDetail(GetProductData? newValue) {
    _productDetail = newValue;
    notifyListeners();
  }

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  bool _loadingImage = false;
  bool get loadingImage => _loadingImage;
  set loadingImage(bool newValue) {
    _loadingImage = newValue;
    notifyListeners();
  }

  Future<void> getProductNew() async {
    loading = true;
    try {
      final response = await productRepo.getNewProduct();
      productDetail = response.data;
    } catch (e) {
      print('error getProduct : $e');
    } finally {
      loading = false;
    }
  }

  Future<void> getProductDetail() async {
    loading = true;

    try {
      final response = await productRepo.getProductDetail(productId.toString());
      productDetail = response.data;

      if (getInitProduct != null) {
        photos = productDetail?.product_detail?.images ?? [];
        video = productDetail?.product_detail?.video?.mp4_hls;
        videoId = productDetail?.product_detail?.video?.video_id;

        getInitProduct!(productDetail!);
      }
    } catch (e) {
      print("Erro getProductDetail : $e");
    } finally {
      loading = false;
    }
  }

  Future<void> callInitMethod() async {
    if (productId != null) {
      //call when update product
      getProductDetail();
    } else {
      //call when create product
      getProductNew();
    }
  }

// ---------- get image url (quill decription) ----------
  String? _imageUrl;
  String? get imageUrl => _imageUrl;
  set imageUrl(String? value) {
    _imageUrl = value;
    notifyListeners();
  }

  Future<void> getImageUrl(File photo) async {
    try {
      final res = await productRepo.uploadImage(photo);
      imageUrl = res.data;
    } catch (e) {
      // print('Error uploadImage : $e');

      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    }
  }
}
