import 'dart:io';

import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/constants/product_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_product_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/get_product_data.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_category.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_detail.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_extra.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_unit.dart';
import 'package:bloc_merchant_mobile_2/data/repos/product_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_add_extra.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_decription.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_format.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_info.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_setting.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_type.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/utils/delta_to_html.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart' as quill;
import 'package:flutter_quill_delta_from_html/flutter_quill_delta_from_html.dart';
import 'package:group_button/group_button.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CreateProductViewArguments {
  final String? productId;
  CreateProductViewArguments({this.productId});
}

class CreateProductView extends StatefulWidget {
  static const String routeName = '/create-product';
  final CreateProductViewArguments? args;

  const CreateProductView({super.key, this.args});

  @override
  State<CreateProductView> createState() => _CreateProductViewState();
}

class _CreateProductViewState extends State<CreateProductView> {
  String? productId;

  //Product type
  GroupButtonController saleTypeController = GroupButtonController();
  GroupButtonController productTypeController = GroupButtonController();
  bool isProductTopping = false;

  //Product Info
  final _titleTextController = TextEditingController();
  final _secondTitleController = TextEditingController();
  final _brandNameController = TextEditingController();
  final _productCateController = TextEditingController();
  final _unitTypeController = TextEditingController();
  ProductCategory? selectedCate;
  ProductUnit? selectedUnit;
  File? videoFile;
  String? videoPath;
  String? videoId;

  //Product format
  final _nameController = TextEditingController();
  final _priceController = TextEditingController();
  final _stockController = TextEditingController();
  final _barcodeController = TextEditingController();
  final _packFeeController = TextEditingController();
  final _totalItemController = TextEditingController();
  List<Format> formats = [];
  bool isFormatAdvance = false;
  bool isUnlimited = true;

  //Product Attribute
  List<Attribute>? attributeList = [];

  //Product Extra (Addon)
  List<ProductExtra>? addonsList = [];
  bool isExtra = false;

  //producut setting
  final _sortController = TextEditingController();
  final _expireDateController = TextEditingController();
  bool isOnSale = true;
  bool isExpire = false;

  //product desc
  String? description;
  final _describController = quill.QuillController.basic();

  void onCreateProduct(CreateProductViewController valueController) async {
    CreateProductRequest? req;

    List<IFormatRequest> formatReq = [];

    if (!isFormatAdvance) {
      // print('isUnlimited : ${isUnlimited}');
      formatReq = [
        IFormatRequest(
          format_id: "0",
          name: _nameController.text,
          price: _priceController.text,
          old_price: _priceController.text,
          sale_sku: isUnlimited ? "9999" : _stockController.text,
          package_price: _packFeeController.text,
          barcode: _barcodeController.text,
          sale_type:
              isUnlimited ? FormatStatus.unlimited : FormatStatus.limited,
        )
      ];
    } else {
      for (var e in formats) {
        final item = IFormatRequest(
            barcode: e.bar_code,
            format_id: e.spec_id,
            name: e.spec_name,
            old_price: e.old_price,
            package_price: e.package_price,
            price: e.price,
            sale_sku: e.sale_sku.toString(),
            sale_type: e.sale_type,
            totalItem: e.total_item);

        formatReq.add(item);
      }
    }

    final isAddon = !isProductTopping
        ? ProductTypeStatus.standard
        : ProductTypeStatus.topping;

    final addonSale =
        !isProductTopping ? "0" : saleTypeController.selectedIndex.toString();

    List<IAddonRequest> reqAddonList = [];

    if (addonsList != null) {
      for (var e in addonsList!) {
        reqAddonList.add(IAddonRequest(
          product_id: e.product_id.toString(),
          product_title: e.title.toString(),
          addon_code: e.addon_code.toString(),
        ));
      }
    }

    final deltaJson = _describController.document.toDelta().toJson();
    final descHtml = DeltaToHTML.encodeJson(deltaJson);

    // print('descText : $descHtml');

    req = CreateProductRequest(
      productId: productId,
      brand: _brandNameController.text,
      title: _titleTextController.text,
      secondaryTitle: _secondTitleController.text,
      cateId: selectedCate?.cate_id ?? "",
      unit: _unitTypeController.text,
      description: descHtml,
      formats: formatReq,
      isOnSale: isOnSale ? ProductStatus.show : ProductStatus.hide,
      isAddon: isAddon,
      orderBy: _sortController.text,
      expiredDate: isExpire ? _expireDateController.text : '0',
      addons: isExtra ? reqAddonList : [],
      attributes: attributeList,
      addonSale: addonSale,
      video: videoFile,
      videoId: videoId,
      videoUrl: videoPath,
    );

    valueController.create(req);
  }

  void initializeProduct(GetProductData product) {
    ProductDetail? productDetail = product.product_detail;

    // print('productDetail : ${productDetail}');

    if (productDetail != null) {
      productId = productDetail.product_id;

      final cate = product.categories
          ?.where((e) => e.cate_id == productDetail.cate_id)
          .toList();

      ///Product Type
      int isAddon = int.parse(productDetail.is_addon!);
      int addonSale = int.parse(productDetail.addon_sale!);

      isProductTopping = isAddon == 1;
      productTypeController.selectIndex(isAddon);
      saleTypeController.selectIndex(addonSale);

      ///Product Info
      _titleTextController.text = productDetail.title ?? "";
      _secondTitleController.text = productDetail.secondary_title ?? "";
      _productCateController.text = (cate != null ? cate.first.title : "")!;
      _unitTypeController.text = productDetail.unit ?? "";
      videoId = productDetail.video?.video_id;
      videoPath = productDetail.video?.mp4_hls;

      final findCate = product.categories
          ?.where((e) => e.cate_id == productDetail.cate_id)
          .toList();

      if (findCate != null) {
        selectedCate = findCate.first;
      }

      if (selectedCate != null) {
        selectedCate = cate?.first;
      }

      // Product Format
      isFormatAdvance = productDetail.is_format == FormatStatus.advance;
      final formatList = productDetail.formats;

      if (isFormatAdvance) {
        if (formatList != null && formatList.isNotEmpty) {
          formats = formatList;
        }
      } else {
        _priceController.text = productDetail.price.toString();
        _barcodeController.text = productDetail.bar_code.toString();
        _packFeeController.text = productDetail.package_price.toString();

        if (productDetail.sale_type == FormatStatus.unlimited) {
          isUnlimited = true;
        } else {
          isUnlimited = false;
          _stockController.text = productDetail.sale_sku.toString();
        }
      }

      ///Product Attribtute
      if (productDetail.attributes!.isNotEmpty) {
        for (var e in productDetail.attributes!) {
          attributeList?.add(Attribute(name: e.key, values: e.val));
        }
      }

      ///Product Extra
      if (productDetail.addons!.isNotEmpty) {
        for (var e in productDetail.addons!) {
          addonsList?.add(ProductExtra(
            addon_code: productDetail.addon_product,
            cate_id: e.cate_id,
            dateline: "",
            price: e.price.toString(),
            product_id: e.product_id,
            secondary_title: e.secondary_title,
            thumb: e.thumb,
            title: e.title,
            unit: e.unit,
          ));
        }
        isExtra = true;
      }

      ///Product Setting
      isOnSale = productDetail.is_onsale == ProductStatus.show;
      _sortController.text = productDetail.orderby ?? "50";
      _expireDateController.text = productDetail.expired_date != "0"
          ? productDetail.expired_date.toString()
          : "";
      isExpire = productDetail.expired_date != "0";

      if (productDetail.description != null) {
        final jsonDoc =
            HtmlToDelta().convert(productDetail.description!).toJson();

        _describController.document = quill.Document.fromJson(jsonDoc);
      }
    }

    setState(() {});
  }

  @override
  void initState() {
    _sortController.text = "50";
    productTypeController.selectIndex(0);

    super.initState();
  }

  @override
  void dispose() {
    //product Type
    productTypeController.dispose();
    saleTypeController.dispose();

    //product info
    _titleTextController.dispose();
    _secondTitleController.dispose();
    _brandNameController.dispose();
    _productCateController.dispose();
    _unitTypeController.dispose();

    //Product format
    _nameController.dispose();
    _priceController.dispose();
    _stockController.dispose();
    _barcodeController.dispose();
    _packFeeController.dispose();
    _totalItemController.dispose();

    //setting
    _sortController.dispose();
    _expireDateController.dispose();

    //desc
    _describController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CreateProductViewController(
        productRepo: locator<ProductRepo>(),
        productId: widget.args?.productId,
        onSuccess: (message) {
          DialogUtil.cusAlertSnackBar(context, message)
              .then((value) => Navigator.pop(context, AsyncStatus.success));
        },
        onError: (message) {
          DialogUtil.cusAlertSnackBar(context, message,
              isError: true, milliseconds: 2000);
        },
        getInitProduct: initializeProduct,
      )..callInitMethod(),
      child: Consumer<CreateProductViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            leading: const CusBackButton(color: Colors.white),
            title: Text(
              widget.args?.productId != null
                  ? LocaleKeys.edit_product.tr()
                  : LocaleKeys.new_product.tr(),
              style: const TextStyle(color: Colors.white),
            ),
          ),
          body: viewController.loading
              ? const SizedBox(
                  width: double.infinity,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ))
              : GestureDetector(
                  onTap: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                  },
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(Spacing.s),
                    child: Column(
                      children: [
                        ProductType(
                          productTypeController: productTypeController,
                          saleTypeController: saleTypeController,
                          onItemCallBack: (index) {
                            isProductTopping = index == 1;

                            if (index == 1 && isFormatAdvance) {
                              isFormatAdvance = false;
                            }

                            setState(() {});
                          },
                        ),
                        const SizedBox(height: Spacing.s),
                        ProductInfo(
                          titleTextController: _titleTextController,
                          secondTitleController: _secondTitleController,
                          brandNameController: _brandNameController,
                          productCateController: _productCateController,
                          unitTypeController: _unitTypeController,
                          viewController: viewController,
                          callbackSelectCate: (cate) {
                            setState(() {
                              selectedCate = cate;
                            });
                          },
                          callbackSelectUnit: (unit) {
                            setState(() {
                              selectedUnit = unit;
                            });
                          },
                        ),
                        const SizedBox(height: Spacing.s),
                        ProductFormat(
                          formats: formats,
                          nameController: _nameController,
                          priceController: _priceController,
                          stockController: _stockController,
                          barcodeController: _barcodeController,
                          packFeeController: _packFeeController,
                          totalItemController: _totalItemController,
                          isFormatAdvance: isFormatAdvance,
                          onChangedSwitch: isProductTopping
                              ? null
                              : (value) {
                                  setState(() {
                                    isFormatAdvance = value;
                                  });
                                },
                          onUnlimited: (value) {
                            isUnlimited = value;
                          },
                          isUnlimited: isUnlimited,
                          callbackList: (formatList) {
                            // print('callbackList : ${formatList}');
                            setState(() {
                              formats = formatList;
                            });
                          },
                        ),
                        const SizedBox(height: Spacing.s),
                        if (!isProductTopping && !viewController.loading) ...[
                          ProductAttribute(
                              attributeList: attributeList ?? [],
                              attributesTemplate: viewController
                                      .productDetail?.attribute_list ??
                                  []),
                          const SizedBox(height: Spacing.s),
                          ProductAddExtra(
                            selectedItem: addonsList,
                            extraList:
                                viewController.productDetail?.extra_list ?? [],
                            onSwitch: (value) {
                              setState(() {
                                isExtra = value;
                              });
                            },
                          ),
                          const SizedBox(height: Spacing.s),
                        ],
                        ProductSetting(
                          sortController: _sortController,
                          expireDateController: _expireDateController,
                          saleStatus: isOnSale,
                          onSubmit: (date) {
                            date as DateTime;
                            final formatDate =
                                DateFormat('dd-MM-yyyy').format(date);
                            _expireDateController.text = formatDate.toString();
                            setState(() {});

                            // print('_selectedDate : $_selectedDate');

                            Navigator.pop(context);
                          },
                          swithStatus: (value) {
                            setState(() {
                              isOnSale = value;
                            });
                          },
                          onExiredDate: (value) {
                            setState(() {
                              isExpire = value;
                            });
                          },
                        ),
                        const SizedBox(height: Spacing.s),
                        ProductDescription(
                          viewController: viewController,
                          controller: _describController,
                          // initText: viewController
                          //     .productDetail?.product_detail?.description,
                        ),
                        const SizedBox(height: Spacing.s),
                        SizedBox(
                          width: double.infinity,
                          child: FilledButton.icon(
                              onPressed: () {
                                onCreateProduct(viewController);
                              },
                              icon: viewController.loading
                                  ? const SizedBox(
                                      height: 18,
                                      width: 18,
                                      child: CircularProgressIndicator(
                                          color: Colors.white),
                                    )
                                  : const SizedBox(),
                              label: Text(productId != null
                                  ? "Update Now"
                                  : "Create Now")),
                        ),
                        const SizedBox(height: 40),
                      ],
                    ),
                  ),
                ),
        );
      }),
    );
  }
}
