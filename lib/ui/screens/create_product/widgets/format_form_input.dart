import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class FormatFormInput extends StatefulWidget {
  final TextEditingController priceController;
  final TextEditingController stockController;
  final TextEditingController barcodeController;
  final TextEditingController packFeeController;
  final TextEditingController totalItemController;
  final TextEditingController? nameController;
  final Function(bool)? onUnlimited;
  final bool? isDialog;
  final bool? isRetail;
  final bool? isUnlimited;
  final String? id;
  final List<Format>? formats;
  final Map<String, dynamic>? request;
  final Function()? onCancel;
  final Function()? onSave;

  const FormatFormInput({
    super.key,
    required this.priceController,
    required this.stockController,
    required this.barcodeController,
    required this.packFeeController,
    required this.totalItemController,
    this.nameController,
    this.isDialog = false,
    this.isRetail = false, //isRetail === "1"
    this.onUnlimited,
    this.id,
    this.formats = const [],
    this.isUnlimited = true,
    this.request,
    this.onSave,
    this.onCancel,
  });

  @override
  State<FormatFormInput> createState() => _FormatFormInputState();
}

class _FormatFormInputState extends State<FormatFormInput> {
  bool isUnlimited = true;
  String stock = "";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void fillEditValue() {
    if (widget.formats!.isNotEmpty) {
      final format =
          widget.formats?.where((e) => e.spec_id == widget.id).toList().first;

      widget.nameController?.text = format?.spec_name ?? "";
      widget.packFeeController.text = format?.package_price ?? "";
      widget.priceController.text = format?.price ?? "";
      widget.stockController.text =
          isUnlimited ? "" : format?.sale_sku?.toString() ?? "";
      widget.barcodeController.text = format?.bar_code?.toString() ?? "";
      widget.totalItemController.text = format?.total_item ?? "";
    }
  }

  @override
  void initState() {
    isUnlimited = widget.isUnlimited ?? isUnlimited;

    if (widget.id != null) {
      fillEditValue();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final border = OutlineInputBorder(
        borderSide: BorderSide(
            width: 1, color: theme.colorScheme.onBackground.withOpacity(0.5)));
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.isDialog! && widget.nameController != null) ...[
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                child: Text(LocaleKeys.p_name.tr())),
            SizedBox(
              height: 40,
              child: CusTextField(
                controller: widget.nameController!,
                contentPadding: const EdgeInsets.all(Spacing.s),
                hintText: LocaleKeys.p_name.tr(),
                border: border,
                focusedBorder: border,
                enabledBorder: border,
              ),
            ),
            const SizedBox(height: Spacing.s),
          ],
          if (widget.isDialog!)
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                child: Text(LocaleKeys.p_price.tr())),
          Row(
            children: [
              if (widget.isDialog != null && !widget.isDialog!)
                SizedBox(width: 75, child: Text(LocaleKeys.p_price.tr())),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 40,
                  child: CusTextField(
                      controller: widget.priceController,
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                      contentPadding: const EdgeInsets.all(Spacing.s),
                      hintText: LocaleKeys.p_price.tr(),
                      border: border,
                      focusedBorder: border,
                      enabledBorder: border,
                      isValid: true,
                      validMes: 'This field is required*.',
                      suffixIcon: const Icon(Icons.attach_money_sharp)),
                ),
              )
            ],
          ),
          const SizedBox(height: Spacing.s),
          if (widget.isDialog!)
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                child: Text(LocaleKeys.p_stock.tr())),
          Row(
            children: [
              if (widget.isDialog != null && !widget.isDialog!)
                SizedBox(width: 75, child: Text(LocaleKeys.p_stock.tr())),
              Expanded(
                  child: Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      height: 40,
                      child: CusTextField(
                        controller: widget.stockController,
                        keyboardType: TextInputType.number,
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        hintText: isUnlimited
                            ? LocaleKeys.p_unlimited.tr()
                            : LocaleKeys.p_add_stock.tr(),
                        border: border,
                        focusedBorder: border,
                        enabledBorder: border,
                        enabled: !isUnlimited,
                        onChange: (text) {
                          stock = text;
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: Spacing.s),
                  if (widget.isDialog != null && widget.isDialog!) ...[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          isUnlimited = !isUnlimited;
                        });

                        if (isUnlimited) {
                          widget.stockController.text = "";
                        } else {
                          widget.stockController.text = stock;
                        }

                        if (widget.onUnlimited != null) {
                          widget.onUnlimited!(isUnlimited);
                        }
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(Spacing.xs)),
                        child: Icon(
                            isUnlimited
                                ? Icons.check_box_outlined
                                : Icons.check_box_outline_blank,
                            color: Colors.white),
                      ),
                    ),
                  ] else
                    Expanded(
                      child: SizedBox(
                        height: 40,
                        child: FilledButton.icon(
                            style: FilledButton.styleFrom(
                                padding: EdgeInsets.zero,
                                backgroundColor: Colors.green,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(Spacing.s))),
                            onPressed: () {
                              setState(() {
                                isUnlimited = !isUnlimited;
                              });

                              if (isUnlimited) {
                                widget.stockController.text = "";
                              } else {
                                widget.stockController.text = stock;
                              }

                              if (widget.onUnlimited != null) {
                                widget.onUnlimited!(isUnlimited);
                              }
                            },
                            icon: Icon(
                              isUnlimited
                                  ? Icons.check_box_outlined
                                  : Icons.check_box_outline_blank,
                              color: Colors.white,
                            ),
                            label: widget.isDialog != null && widget.isDialog!
                                ? const SizedBox()
                                : Text(
                                    LocaleKeys.p_unlimited.tr(),
                                    style: TextStyle(color: Colors.white),
                                  )),
                      ),
                    )
                ],
              ))
            ],
          ),
          const SizedBox(height: Spacing.s),
          if (widget.isDialog!)
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                child: Text(LocaleKeys.p_packaging_fee.tr())),
          Row(
            children: [
              if (widget.isDialog != null && !widget.isDialog!)
                SizedBox(
                    width: 75,
                    child: Text("${LocaleKeys.p_packaging_fee.tr()} :")),
              Expanded(
                  flex: 2,
                  child: SizedBox(
                    height: 40,
                    child: CusTextField(
                      controller: widget.packFeeController,
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                      contentPadding: const EdgeInsets.all(Spacing.s),
                      hintText: LocaleKeys.p_packaging_fee.tr(),
                      border: border,
                      focusedBorder: border,
                      enabledBorder: border,
                    ),
                  ))
            ],
          ),
          const SizedBox(height: Spacing.s),
          if (widget.isDialog!)
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                child: Text(LocaleKeys.p_barcode.tr())),
          Row(
            children: [
              if (widget.isDialog != null && !widget.isDialog!)
                SizedBox(width: 75, child: Text(LocaleKeys.p_barcode.tr())),
              Expanded(
                  flex: 2,
                  child: SizedBox(
                    height: 40,
                    child: CusTextField(
                      controller: widget.barcodeController,
                      contentPadding: const EdgeInsets.all(Spacing.s),
                      hintText: LocaleKeys.p_barcode.tr(),
                      border: border,
                      focusedBorder: border,
                      enabledBorder: border,
                    ),
                  ))
            ],
          ),
          const SizedBox(height: Spacing.s),
          if (widget.isDialog! && widget.isRetail!)
            Container(
                margin: const EdgeInsets.only(bottom: Spacing.xs),
                width: 75,
                child: const Text("Total Item")),
          if (widget.isRetail!)
            Row(
              children: [
                if (widget.isDialog != null && !widget.isDialog!)
                  const SizedBox(width: 75, child: Text("Total Item")),
                Expanded(
                  flex: 2,
                  child: SizedBox(
                      height: 40,
                      child: CusTextField(
                        controller: widget.totalItemController,
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        hintText: "Total Item",
                        border: border,
                        focusedBorder: border,
                        enabledBorder: border,
                      )),
                )
              ],
            ),
        ],
      ),
    );
  }
}
