// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/dialog/attribute_dialog.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';

class ProductAttribute extends StatefulWidget {
  final List<Attribute> attributesTemplate;
  final List<Attribute> attributeList;

  const ProductAttribute({
    super.key,
    this.attributesTemplate = const [],
    required this.attributeList,
  });

  @override
  State<ProductAttribute> createState() => _ProductAttributeState();
}

class _ProductAttributeState extends State<ProductAttribute> {
  // List<Attribute> widget.attributeList = [];

  void openAttributeDialog({int? index}) {
    // initialTextField(index: index);
    showDialog(
      context: context,
      builder: (context) {
        return AttributeDialog(
          onSave: (attValue) {
            setState(() {
              widget.attributeList.add(attValue);
            });
          },
        );
      },
    );
  }

  void openAttributeTemplate() {
    showDialog(
        context: context,
        builder: (context) {
          return AttributeTemplate(
            attributeList: widget.attributesTemplate,
            selectedItem: widget.attributeList,
            onSelectTemp: (selectedAttr) {
              //TO_CHECK
              //  List<String>? values =
              //     selectedAttr.values!.isNotEmpty ? selectedAttr.values : [];
              // final item = Attribute(
              //     attribute_id: selectedAttr.attribute_id,
              //     title: selectedAttr.title,
              //     name: selectedAttr.name ?? '',
              //     values: values);

              final item = selectedAttr;
//
              if (widget.attributeList.contains(item)) {
                widget.attributeList.removeWhere((e) => e.name == item.name);
              } else {
                widget.attributeList.add(item);
              }

              // print("widget.attributeList : ${widget.attributeList}");

              setState(() {});
              Navigator.pop(context);
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        color: theme.colorScheme.background,
        borderRadius: BorderRadius.circular(Spacing.s),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Text(LocaleKeys.p_item_attributes.tr(),
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              const Spacer(),
              CusIconButton(
                height: 35,
                width: 35,
                onPressed: () {
                  openAttributeDialog();
                },
                icon: const Icon(
                  Icons.add,
                  color: Colors.green,
                ),
              ),
              const SizedBox(width: Spacing.s),
              SizedBox(
                height: 35,
                child: FilledButton(
                    onPressed: () {
                      openAttributeTemplate();
                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Colors.orange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        )),
                    child: Text(LocaleKeys.p_template.tr())),
              ),
            ],
          ),
          Visibility(
            visible: widget.attributeList.isEmpty,
            child: Container(
              height: 80,
              margin: const EdgeInsets.symmetric(vertical: Spacing.s),
              decoration: BoxDecoration(color: Colors.grey[300]),
              alignment: Alignment.center,
              child: Text("${LocaleKeys.no_extra_has_been_selected.tr()}..."),
            ),
          ),
          ...List.generate(
              widget.attributeList.length,
              (index) => Container(
                    clipBehavior: Clip.antiAlias,
                    margin: const EdgeInsets.only(top: Spacing.normal),
                    // height: 250,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Spacing.s),
                        boxShadow: [
                          BoxShadow(
                            color:
                                theme.colorScheme.onBackground.withOpacity(0.3),
                            blurRadius: 5,
                            offset: const Offset(1, 0.5),
                            spreadRadius: 1,
                          )
                        ]),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: double.infinity,
                          clipBehavior: Clip.antiAlias,
                          height: 50,
                          padding:
                              const EdgeInsets.symmetric(horizontal: Spacing.m),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(
                                width: 0.2,
                                color: theme.colorScheme.onBackground,
                              ))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.attributeList[index].name.toString(),
                                  style: theme.textTheme.titleMedium
                                      ?.copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                              InkWell(
                                  onTap: () {
                                    widget.attributeList.removeAt(index);
                                    setState(() {});
                                  },
                                  child: const Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ))
                            ],
                          ),
                        ),
                        Container(
                            color: Colors.grey[300],
                            child: GridView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              padding: const EdgeInsets.all(Spacing.m),
                              itemCount:
                                  widget.attributeList[index].values?.length,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      mainAxisSpacing: Spacing.s,
                                      crossAxisSpacing: Spacing.s,
                                      mainAxisExtent: 45,
                                      crossAxisCount: 2),
                              itemBuilder: (context, indexValue) {
                                final attrValue = widget
                                    .attributeList[index].values![indexValue];
                                return Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: Spacing.normal),
                                  decoration:
                                      const BoxDecoration(color: Colors.white),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(attrValue.toString()),
                                      InkWell(
                                        onTap: () {
                                          if (widget.attributeList[index]
                                                  .values!.length ==
                                              1) {
                                            widget.attributeList
                                                .removeAt(index);
                                          } else {
                                            widget.attributeList[index].values =
                                                widget
                                                    .attributeList[index].values
                                                    ?.where((element) {
                                              return element !=
                                                  widget.attributeList[index]
                                                      .values?[indexValue];
                                            }).toList();

                                            print(
                                                'After Removei attribute value : ${widget.attributeList[index]} ');
                                          }
                                          setState(() {});
                                        },
                                        child: const Icon(
                                          Icons.close,
                                          color: Colors.red,
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },
                            ))
                      ],
                    ),
                  ))
        ],
      ),
    );
  }
}

class AttributeType {
  final String name;
  final List<String?>? value;

  AttributeType({
    required this.name,
    required this.value,
  });

  @override
  String toString() => 'AttributeType(name: $name, value: $value)';
}
