import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:flutter/material.dart';

class ItemAttribute extends StatefulWidget {
  const ItemAttribute({super.key});

  @override
  State<ItemAttribute> createState() => _ItemAttributeState();
}

class _ItemAttributeState extends State<ItemAttribute> {
  final _attributeNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [CusTextField(controller: _attributeNameController)],
        )
      ],
    );
  }
}
