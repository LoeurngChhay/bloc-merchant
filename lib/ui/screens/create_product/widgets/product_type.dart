import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_radio_tile.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';

class ProductType extends StatefulWidget {
  final Function(int selectedIndex)? onItemCallBack;
  final Function(int selectedIndex)? onSaleCallBack;
  final GroupButtonController productTypeController;
  final GroupButtonController saleTypeController;

  const ProductType({
    super.key,
    this.onItemCallBack,
    this.onSaleCallBack,
    required this.productTypeController,
    required this.saleTypeController,
  });

  @override
  State<ProductType> createState() => _ProductTypeState();
}

class _ProductTypeState extends State<ProductType> {
  final optionButtons = ["Standard", "Topping"];
  final saleTypeButtons = ["For Sale", "Only Topping"];

  bool formatAdvanced = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Spacing.s),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            LocaleKeys.p_type.tr(),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: Spacing.s),
          Row(
            children: [
              Text(LocaleKeys.i_type.tr()),
              const SizedBox(width: Spacing.l),
              Expanded(
                child: GroupButton(
                  controller: widget.productTypeController,
                  buttons: optionButtons,
                  buttonIndexedBuilder: (selected, index, context) {
                    return CusRadioTile(
                      isSelected:
                          widget.productTypeController.selectedIndex == index,
                      title: optionButtons[index],
                      groupValue: widget.productTypeController.selectedIndex,
                      value: index,
                      onTap: () {
                        setState(() {
                          widget.productTypeController.selectIndex(index);
                          if (index == 1) {
                            widget.saleTypeController.selectIndex(0);
                          } else {
                            formatAdvanced = false;
                          }
                        });

                        if (widget.onItemCallBack != null) {
                          widget.onItemCallBack!(index);
                        }
                      },
                    );
                  },
                  options: const GroupButtonOptions(
                    groupingType: GroupingType.wrap,
                    direction: Axis.horizontal,
                    mainGroupAlignment: MainGroupAlignment.start,
                    crossGroupAlignment: CrossGroupAlignment.start,
                    groupRunAlignment: GroupRunAlignment.start,
                    textPadding: EdgeInsets.all(0),
                    elevation: 1,
                  ),
                  isRadio: true,
                  enableDeselect: false,
                ),
              )
            ],
          ),
          const SizedBox(height: Spacing.s),
          Visibility(
            visible: widget.productTypeController.selectedIndex == 1,
            child: Row(
              children: [
                Text(LocaleKeys.p_sale_type.tr()),
                const SizedBox(width: Spacing.l),
                Expanded(
                  child: GroupButton(
                    controller: widget.saleTypeController,
                    buttons: saleTypeButtons,
                    buttonIndexedBuilder: (selected, index, context) {
                      return CusRadioTile(
                        isSelected:
                            widget.saleTypeController.selectedIndex == index,
                        title: saleTypeButtons[index],
                        groupValue: widget.saleTypeController.selectedIndex,
                        value: index,
                        onTap: () {
                          // unFocus();
                          setState(() {
                            widget.saleTypeController.selectIndex(index);
                            if (index == 0) {
                              widget.saleTypeController.selectIndex(0);
                            } else {
                              formatAdvanced = false;
                            }
                          });
                        },
                      );
                    },
                    options: const GroupButtonOptions(
                      groupingType: GroupingType.wrap,
                      direction: Axis.horizontal,
                      mainGroupAlignment: MainGroupAlignment.start,
                      crossGroupAlignment: CrossGroupAlignment.start,
                      groupRunAlignment: GroupRunAlignment.start,
                      textPadding: EdgeInsets.all(0),
                      elevation: 1,
                    ),
                    isRadio: true,
                    enableDeselect: false,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
