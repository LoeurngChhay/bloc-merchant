import 'package:bloc_merchant_mobile_2/constants/product_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/format_form_input.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/product_format_table.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ProductFormat extends StatefulWidget {
  final List<Format> formats;
  final bool? isFormatAdvance;
  final bool? isUnlimited;

  final TextEditingController nameController;
  final TextEditingController priceController;
  final TextEditingController stockController;
  final TextEditingController barcodeController;
  final TextEditingController packFeeController;
  final TextEditingController totalItemController;
  final Function(List<Format>)? callbackList;
  final Function(bool)? onChangedSwitch;
  final Function(bool)? onUnlimited;

  const ProductFormat({
    super.key,
    required this.formats,
    this.isFormatAdvance = false,
    required this.nameController,
    required this.priceController,
    required this.stockController,
    required this.barcodeController,
    required this.packFeeController,
    required this.totalItemController,
    this.onChangedSwitch,
    this.isUnlimited = true,
    this.onUnlimited,
    this.callbackList,
  });

  @override
  State<ProductFormat> createState() => _ProductFormatState();
}

class _ProductFormatState extends State<ProductFormat> {
  // List<Format> format = [];
  bool unlimited = true;

  void resetTextEditor() {
    widget.nameController.text = "";
    widget.priceController.text = "";
    widget.stockController.text = "";
    widget.barcodeController.text = "";
    widget.packFeeController.text = "";
    widget.totalItemController.text = "";
  }

  void openFormFormatDialog({String? id}) {
    if (id != null) {
      unlimited = widget.formats
              .where((e) => e.spec_id == id)
              .toList()
              .first
              .sale_type ==
          FormatStatus.unlimited;
    }

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Spacing.m)),
        title: Text(LocaleKeys.advanced_format.tr()),
        content: SingleChildScrollView(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FormatFormInput(
                  formats: widget.formats,
                  id: id,
                  nameController: widget.nameController,
                  priceController: widget.priceController,
                  stockController: widget.stockController,
                  barcodeController: widget.barcodeController,
                  packFeeController: widget.packFeeController,
                  totalItemController: widget.totalItemController,
                  isUnlimited: unlimited,
                  onUnlimited: (value) {
                    setState(() {
                      unlimited = value;
                    });
                  },
                  isDialog: true,
                ),
              ],
            ),
          ),
        ),
        actions: [
          FilledButton(
              onPressed: () {
                Navigator.pop(context);
                resetTextEditor();
              },
              style: FilledButton.styleFrom(backgroundColor: Colors.red),
              child: Text(
                LocaleKeys.cancel.tr(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              )),
          FilledButton(
              onPressed: () {
                if (id != null) {
                  final index = widget.formats
                      .indexWhere((e) => e.spec_id == id.toString());

                  widget.formats[index] = Format(
                    spec_id: id.toString(),
                    spec_name: widget.nameController.text,
                    price: widget.priceController.text,
                    old_price: widget.priceController.text,
                    sale_type: unlimited
                        ? FormatStatus.unlimited
                        : FormatStatus.limited,
                    sale_sku: unlimited ? "9999" : widget.stockController.text,
                    package_price: widget.packFeeController.text,
                    bar_code: widget.barcodeController.text,
                    total_item: widget.totalItemController.text,
                  );
                } else {
                  widget.formats.add(
                    Format(
                      spec_id: (widget.formats.length + 1).toString(),
                      spec_name: widget.nameController.text,
                      price: widget.priceController.text,
                      old_price: widget.priceController.text,
                      sale_type: unlimited
                          ? FormatStatus.unlimited
                          : FormatStatus.limited,
                      sale_sku:
                          unlimited ? "9999" : widget.stockController.text,
                      package_price: widget.packFeeController.text,
                      bar_code: widget.barcodeController.text,
                      total_item: widget.totalItemController.text,
                    ),
                  );
                }

                if (widget.callbackList != null) {
                  //CHECK AGAIN
                  widget.callbackList!(widget.formats);
                }

                setState(() {});
                Navigator.pop(context);
                resetTextEditor();
              },
              child: Text(
                LocaleKeys.save.tr(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        color: theme.colorScheme.background,
        borderRadius: BorderRadius.circular(Spacing.s),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                LocaleKeys.p_format.tr(),
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Spacer(),
              Text(
                widget.isFormatAdvance!
                    ? "(${LocaleKeys.advanced.tr()})"
                    : '(${LocaleKeys.standard.tr()})',
                style: TextStyle(
                    color: widget.isFormatAdvance! ? Colors.blue : null),
              ),
              const SizedBox(width: Spacing.xs),
              SizedBox(
                height: 35,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Switch(
                      value: widget.isFormatAdvance!,
                      onChanged: widget.onChangedSwitch),
                ),
              )
            ],
          ),
          if (widget.isFormatAdvance!) ...[
            ProductItemFormatList(
              formatList: widget.formats,
              onEdit: (id) {
                openFormFormatDialog(id: id);
              },
              onDelete: (id) {
                widget.formats.removeWhere((e) => e.spec_id == id);
                setState(() {});
              },
            ),
            const SizedBox(height: Spacing.s),
            ElevatedButton(
                onPressed: openFormFormatDialog,
                child: Text(LocaleKeys.p_add_new.tr()))
          ] else ...[
            FormatFormInput(
              priceController: widget.priceController,
              stockController: widget.stockController,
              barcodeController: widget.barcodeController,
              totalItemController: widget.totalItemController,
              packFeeController: widget.packFeeController,
              onUnlimited: widget.onUnlimited,
              isUnlimited: widget.isUnlimited,
            ),
          ]
        ],
      ),
    );
  }
}
