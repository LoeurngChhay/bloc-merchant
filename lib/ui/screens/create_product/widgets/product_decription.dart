import 'dart:io';

import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:flutter_quill_extensions/flutter_quill_extensions.dart';
import 'package:image_picker/image_picker.dart';

class ProductDescription extends StatefulWidget {
  final QuillController controller;
  final CreateProductViewController viewController;

  const ProductDescription(
      {super.key, required this.controller, required this.viewController});

  @override
  State<ProductDescription> createState() => _ProductDescriptionState();
}

class _ProductDescriptionState extends State<ProductDescription> {
  final ImagePicker _picker = ImagePicker();
  bool loading = false;

  Future<void> _pickImage() async {
    try {
      final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
      loading = true;
      showLoading();
      if (image != null && mounted) {
        final File file = File(image.path);

        await widget.viewController.getImageUrl(file);
        _addImageToEditor();
      }
    } catch (e) {
      // Handle any errors here
      print("Error picking image: $e");
    } finally {
      if (loading) {
        loading = false;
        Navigator.pop(context);
      }
    }
  }

  void _addImageToEditor() {
    final imageUrl = widget.viewController.imageUrl;
    if (imageUrl != null) {
      final index = widget.controller.selection.baseOffset;
      final length = widget.controller.selection.extentOffset -
          widget.controller.selection.baseOffset;

      widget.controller.replaceText(
        index,
        length,
        BlockEmbed.image(imageUrl),
        TextSelection.collapsed(offset: index + 1),
      );
    }
  }

  void showLoading() {
    if (!loading) return;
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
            insetPadding: EdgeInsets.zero,
            contentPadding: EdgeInsets.zero,
            backgroundColor: Colors.white,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            content: const SizedBox(
              height: 150,
              width: 150,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 60,
                    width: 60,
                    child: CircularProgressIndicator(strokeWidth: 5),
                  )
                ],
              ),
            ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: QuillToolbar.simple(
              configurations: QuillSimpleToolbarConfigurations(
                  controller: widget.controller,
                  toolbarSectionSpacing: 0,
                  showColorButton: false,
                  showUnderLineButton: true,
                  showRedo: false,
                  showUndo: false,
                  showFontFamily: false,
                  showQuote: false,
                  showBackgroundColorButton: false,
                  showClipboardPaste: false,
                  showClipboardCopy: false,
                  showClipboardCut: false,
                  showSubscript: false,
                  showCodeBlock: false,
                  showInlineCode: false,
                  showSearchButton: false,
                  showSuperscript: false,
                  showStrikeThrough: false,
                  showDividers: false,
                  showClearFormat: false,
                  showListCheck: false,
                  showIndent: false,
                  showLink: false,
                  showHeaderStyle: false,
                  showFontSize: false,
                  showAlignmentButtons: true,
                  toolbarSize: 36,
                  sectionDividerSpace: 8,
                  toolbarIconAlignment: WrapAlignment.start,
                  toolbarIconCrossAlignment: WrapCrossAlignment.center,
                  customButtons: [
                    QuillToolbarCustomButtonOptions(
                        onPressed: _pickImage,
                        icon:
                            const Icon(Icons.photo_size_select_actual_outlined))
                  ],
                  decoration: BoxDecoration(
                    border: Border.all(),
                  )),
            ),
          ),
          Container(
            constraints: const BoxConstraints(minHeight: 300),
            decoration: BoxDecoration(
                border: Border.all(), borderRadius: BorderRadius.circular(8)),
            child: QuillEditor.basic(
              configurations: QuillEditorConfigurations(
                  padding: const EdgeInsets.all(8),
                  controller: widget.controller,
                  embedBuilders: FlutterQuillEmbeds.editorBuilders()),
            ),
          )
        ],
      ),
    );
  }
}
