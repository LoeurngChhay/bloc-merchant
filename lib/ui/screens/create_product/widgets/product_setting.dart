import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class ProductSetting extends StatefulWidget {
  final TextEditingController sortController;
  final TextEditingController expireDateController;
  final Function(Object?)? onSubmit;
  final Function(bool)? swithStatus;
  final Function(bool)? onExiredDate;
  final bool? saleStatus;

  const ProductSetting({
    super.key,
    required this.sortController,
    required this.expireDateController,
    this.onSubmit,
    this.swithStatus,
    this.saleStatus = false,
    this.onExiredDate,
  });

  @override
  State<ProductSetting> createState() => _ProductSettingState();
}

class _ProductSettingState extends State<ProductSetting> {
  bool isExpireDate = false;
  bool isOnSale = false;

  void openDatePicker() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.width * 0.9,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: SfDateRangePicker(
                // onSelectionChanged: _onSelectionChanged,
                selectionMode: DateRangePickerSelectionMode.single,
                showActionButtons: true,
                initialSelectedDate: DateTime.now(),
                onCancel: () => Navigator.pop(context),
                onSubmit: widget.onSubmit),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    setState(() {
      isOnSale = widget.saleStatus!;
      isExpireDate = widget.expireDateController.text != "";
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final border = OutlineInputBorder(
        borderSide: BorderSide(
            width: 1, color: theme.colorScheme.onBackground.withOpacity(0.3)));
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        color: theme.colorScheme.background,
        borderRadius: BorderRadius.circular(Spacing.s),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(flex: 1, child: Text(LocaleKeys.p_sale_status.tr())),
              Expanded(
                  flex: 2,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 35,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Switch(
                            value: isOnSale,
                            onChanged: (value) {
                              setState(() {
                                isOnSale = value;
                              });

                              if (widget.swithStatus != null) {
                                widget.swithStatus!(value);
                              }
                            },
                          ),
                        ),
                      ),
                      const SizedBox(width: Spacing.s),
                      Text(
                        isOnSale
                            ? "(${LocaleKeys.on_sale.tr()})"
                            : "(${LocaleKeys.not_show.tr()})",
                        style: TextStyle(color: isOnSale ? Colors.blue : null),
                      )
                    ],
                  ))
            ],
          ),
          const SizedBox(height: Spacing.s),
          Row(
            children: [
              Expanded(flex: 1, child: Text(LocaleKeys.sort.tr())),
              Expanded(
                flex: 2,
                child: SizedBox(
                  height: 40,
                  child: CusTextField(
                    controller: widget.sortController,
                    hintText: LocaleKeys.sort.tr(),
                    contentPadding: const EdgeInsets.all(Spacing.s),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(6)),
                    enabledBorder: border,
                    focusedBorder: border,
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: Spacing.s),
          SizedBox(
            height: 40,
            child: Row(
              children: [
                Expanded(flex: 1, child: Text(LocaleKeys.p_expired_date.tr())),
                Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isExpireDate = !isExpireDate;
                            });

                            if (widget.onExiredDate != null) {
                              widget.onExiredDate!(isExpireDate);
                            }
                          },
                          child: Icon(isExpireDate
                              ? Icons.check_box_outlined
                              : Icons.check_box_outline_blank),
                        ),
                        const SizedBox(width: Spacing.s),
                        Visibility(
                          visible: isExpireDate,
                          child: Expanded(
                            child: GestureDetector(
                              onTap: openDatePicker,
                              child: Container(
                                height: 40,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: Spacing.s),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  border: Border.all(
                                      width: 1,
                                      color: theme.colorScheme.onBackground
                                          .withOpacity(0.6)),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(widget.expireDateController.text != ''
                                        ? widget.expireDateController.text
                                        : "Select Date"),
                                    const Icon(Icons.calendar_month)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
