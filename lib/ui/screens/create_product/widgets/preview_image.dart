import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/utils/image_utils.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:image_picker/image_picker.dart';
import 'package:image/image.dart';

class PreviewImage extends StatefulWidget {
  final List<XFile> files;
  const PreviewImage({super.key, required this.files});

  @override
  State<PreviewImage> createState() => _PreviewImageState();
}

class _PreviewImageState extends State<PreviewImage> {
  // List<XFile> images = [];
  List<ProductImages> images = [];

  void cropImage(File file) async {
    final croppedFile = await ImageUtils.cropImage(pickedFile: file);

    if (croppedFile != null) {
      final findIndex = images.indexWhere((e) => e.photo == file.path);

      images[findIndex] = ProductImages(
        photo: croppedFile.path,
        type: 1,
        name: croppedFile.path.split('/').last,
        aspect: 1,
      );
      setState(() {});
    }
  }

  void initImages() {
    for (var e in widget.files) {
      final file = File(e.path);
      final decodedImage = decodeImage(file.readAsBytesSync());

      //get aspect ratio
      final aspectRatio = decodedImage != null
          ? decodedImage.width / decodedImage.height
          : null;

      final image = ProductImages(
        photo: e.path,
        type: 1,
        name: e.path.split('/').last,
        aspect: aspectRatio,
      );
      //set images
      images.add(image);
    }
  }

  @override
  void initState() {
    initImages();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Preview Image"),
        actions: [
          TextButton(
              onPressed: () {
                final formatImage = images.where((e) => e.aspect == 1).toList();

                Navigator.pop(context, formatImage);
              },
              child: const Text("Next")),
          const SizedBox(width: Spacing.normal),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
              childAspectRatio: 3 / 4,
              padding: const EdgeInsets.all(8),
              children: images.map((e) {
                return Column(
                  children: [
                    AspectRatio(
                      aspectRatio: 1,
                      child: Stack(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: e.aspect != 1
                                  ? Border.all(color: Colors.red, width: 2)
                                  : null,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: FileImage(File(e.photo!)),
                              ),
                            ),
                          ),
                          if (e.aspect != 1)
                            const Positioned(
                                top: 4,
                                left: 4,
                                child: Icon(
                                  Icons.warning_rounded,
                                  color: Colors.red,
                                  size: 32,
                                )),
                          Positioned(
                            top: 4,
                            right: 4,
                            child: InkWell(
                              onTap: () {
                                // widget.productImages!.remove(productImage);

                                images.remove(e);
                                setState(() {});
                              },
                              child: Container(
                                height: 30,
                                width: 30,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.black.withOpacity(0.4)),
                                child: const Icon(
                                  Icons.close,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 8),
                    SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                            onPressed: () {
                              if (e.photo != null) {
                                cropImage(File(e.photo!));
                              }
                            },
                            child: const Text("Crop Image")))
                  ],
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}
