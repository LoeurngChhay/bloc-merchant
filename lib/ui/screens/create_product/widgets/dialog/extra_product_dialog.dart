import 'package:bloc_merchant_mobile_2/data/models/responses/product_extra.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:flutter/material.dart';

class ExtraProductDialog extends StatefulWidget {
  final List<ProductExtra?> extraList;
  final List<ProductExtra?>? selectedProduct;
  final Function(ProductExtra)? onTap;
  const ExtraProductDialog(
      {super.key, required this.extraList, this.selectedProduct, this.onTap});

  @override
  State<ExtraProductDialog> createState() => _ExtraProductDialogState();
}

class _ExtraProductDialogState extends State<ExtraProductDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Select Product Extra"),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      insetPadding: const EdgeInsets.all(Spacing.m),
      contentPadding:
          const EdgeInsets.symmetric(horizontal: 0, vertical: Spacing.normal),
      shape: OutlineInputBorder(borderRadius: BorderRadius.circular(Spacing.m)),
      content: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.5,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 45,
                color: Theme.of(context).colorScheme.primary,
                padding: const EdgeInsets.all(Spacing.s),
                child: const Row(
                  children: [
                    SizedBox(
                        width: 60,
                        child: Text('Image',
                            style: TextStyle(fontWeight: FontWeight.bold))),
                    SizedBox(width: Spacing.normal),
                    Text('Item Title (Price & Format)',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              if (widget.extraList.isEmpty) const EmptyScreen(),
              if (widget.extraList.isNotEmpty)
                Expanded(
                    child: ListView.separated(
                  itemCount: widget.extraList.length,
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.s),
                  itemBuilder: (context, index) {
                    return ExtraProductListTile(
                      extraItem: widget.extraList[index]!,
                      isCheck: widget.selectedProduct != null
                          ? widget.selectedProduct?.any((e) =>
                              e?.product_id ==
                              widget.extraList[index]?.product_id)
                          : false,
                      onTap: () {
                        if (widget.onTap != null) {
                          widget.onTap!(widget.extraList[index]!);
                          setState(() {});
                        }
                      },
                    );
                  },
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: Spacing.s),
                ))
            ],
          )),
      actions: [
        FilledButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: FilledButton.styleFrom(backgroundColor: Colors.red),
            child: const Text(
              "Cancel",
              style: TextStyle(fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}

class ExtraProductListTile extends StatefulWidget {
  final ProductExtra extraItem;
  final Function()? onTap;
  final bool? isCheck;
  final Widget? icon;

  const ExtraProductListTile(
      {super.key,
      required this.extraItem,
      this.onTap,
      this.isCheck = false,
      this.icon});

  @override
  State<ExtraProductListTile> createState() => _ExtraProductListTileState();
}

class _ExtraProductListTileState extends State<ExtraProductListTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: widget.onTap,
      contentPadding: const EdgeInsets.symmetric(horizontal: Spacing.xs),
      leading: SizedBox(
          height: 55,
          width: 55,
          child: TransparentImage(url: widget.extraItem.thumb)),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  "ID: ${widget.extraItem.product_id}",
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Text(widget.extraItem.title.toString())
        ],
      ),
      subtitle: Row(
        children: [
          const SizedBox(height: 25, child: VerticalDivider(width: Spacing.xs)),
          const SizedBox(width: Spacing.s),
          Expanded(
            child: RichText(
              text: TextSpan(
                  style: const TextStyle(color: Colors.black),
                  children: [
                    TextSpan(
                        text: '\$ ${widget.extraItem.price}',
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                    const TextSpan(text: ' / '),
                    TextSpan(
                        text: widget.extraItem.unit,
                        style: TextStyle(color: Colors.amber.shade700)),
                  ]),
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
      trailing: widget.icon ??
          CusIconButton(
              onPressed: widget.onTap,
              style:
                  BoxDecoration(border: Border.all(color: Colors.transparent)),
              icon: Icon(
                widget.isCheck!
                    ? Icons.check_box_rounded
                    : Icons.check_box_outline_blank_rounded,
                color: widget.isCheck! ? Colors.blue : null,
              )),
    );
  }
}
