import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:flutter/material.dart';

class AttributeDialog extends StatefulWidget {
  final Function(Attribute)? onSave;

  const AttributeDialog({super.key, this.onSave});

  @override
  State<AttributeDialog> createState() => _AttributeDialogState();
}

class _AttributeDialogState extends State<AttributeDialog> {
  final _formKey = GlobalKey<FormState>();
  List<Widget> attributeFieldList = [];
  final _attributeNameController = TextEditingController();
  List<TextEditingController> allControllers = [];
  List<String> initAttributeValues = ['', '', '', ''];

  void initialTextField() {
    // ignore: unused_local_variable
    for (var e in initAttributeValues) {
      final controller = TextEditingController();
      attributeFieldList.add(_textField(controller));
      allControllers.add(controller);
    }
  }

  void _addTextField() {
    final controller = TextEditingController();

    attributeFieldList = [...attributeFieldList, _textField(controller)];
    allControllers = [...allControllers, controller];

    setState(() {});
  }

  void _removeTextField(TextEditingController controller) {
    setState(() {
      int index = allControllers.indexOf(controller);
      if (index != -1) {
        allControllers.removeAt(index);
        attributeFieldList.removeAt(index);
        controller.dispose();
      }
    });
  }

  @override
  void initState() {
    initialTextField();
    super.initState();
  }

  @override
  void dispose() {
    _attributeNameController.dispose();
    allControllers.forEach((controller) => controller.dispose());
    attributeFieldList.clear();
    allControllers.clear();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      insetPadding: const EdgeInsets.all(Spacing.m),
      contentPadding: const EdgeInsets.all(Spacing.s),
      actionsPadding:
          const EdgeInsets.only(right: Spacing.m, bottom: Spacing.m),
      shape: OutlineInputBorder(borderRadius: BorderRadius.circular(Spacing.m)),
      title: const Text("Add Attribute"),
      content: contentDialog(),
      actions: actionDialog(),
    );
  }

  Widget contentDialog() {
    return Form(
      key: _formKey,
      child: Card(
        color: Theme.of(context).colorScheme.background,
        surfaceTintColor: Theme.of(context).colorScheme.background,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  controller: _attributeNameController,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      enabledBorder:
                          OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: 'Enter Attribute Name...',
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: Spacing.m)),
                  validator: (value) {
                    if (value == null || value.isEmpty || value == '') {
                      return "This field is required.";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: Spacing.m),
                if (attributeFieldList.isNotEmpty)
                  ...List.generate(attributeFieldList.length,
                      (index) => attributeFieldList[index]),
                Container(
                    margin: const EdgeInsets.all(Spacing.s),
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        onPressed: () {
                          _addTextField();
                        },
                        child: const Text("Add New")))
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> actionDialog() {
    return [
      FilledButton(
          onPressed: () {
            Navigator.pop(context);
            // resetTextEditor();
            _attributeNameController.text = "";
            attributeFieldList.clear();
            allControllers.clear();
          },
          style: FilledButton.styleFrom(backgroundColor: Colors.red),
          child: const Text(
            "Cancel",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
      FilledButton(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              List<String> initAttributeValues = [];
              for (var e in allControllers) {
                initAttributeValues = [...initAttributeValues, e.text];
                initAttributeValues.removeWhere((e) => e == '');
              }
              final attributeItem = Attribute(
                  name: _attributeNameController.text,
                  values: initAttributeValues);

              if (widget.onSave != null) {
                widget.onSave!(attributeItem);
              }

              Navigator.pop(context);
            }
          },
          child: const Text(
            "Save",
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
    ];
  }

  Widget _textField(TextEditingController controller) {
    final border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(2),
        borderSide:
            BorderSide(width: 0.5, color: Colors.black.withOpacity(0.3)));

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.s),
      margin: const EdgeInsets.only(bottom: Spacing.s),
      height: 45,
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(
              horizontal: Spacing.m, vertical: Spacing.xs),
          enabledBorder: border,
          border: border,
          hintText: "Enter Value",
          suffixIcon: IconButton(
            onPressed: () {
              _removeTextField(controller);
            },
            icon: const Icon(
              Icons.close,
              color: Colors.red,
            ),
          ),
        ),
      ),
    );
  }
}

class AttributeTemplate extends StatelessWidget {
  final List<Attribute?>? attributeList;
  final List<Attribute>? selectedItem;
  final Function(Attribute value)? onSelectTemp;

  const AttributeTemplate(
      {super.key,
      required this.attributeList,
      this.onSelectTemp,
      this.selectedItem});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AlertDialog(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      insetPadding: const EdgeInsets.all(Spacing.m),
      contentPadding: const EdgeInsets.all(Spacing.s),
      shape: OutlineInputBorder(borderRadius: BorderRadius.circular(Spacing.m)),
      title: const Text("Select Attributes"),
      content: SizedBox(
        height: MediaQuery.of(context).size.height * 0.4,
        width: MediaQuery.of(context).size.width,
        child: ListView.builder(
          itemCount: attributeList?.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            bool isSelected = selectedItem!.contains(attributeList![index]);

            print("select : ${selectedItem?.map((e) => e.attribute_id)}");

            return InkWell(
              onTap: () {
                if (onSelectTemp != null && attributeList != null) {
                  onSelectTemp!(attributeList![index]!);
                }
              },
              child: Card(
                // color: isSelected ? Colors.blue.withOpacity(0.1) : null,
                surfaceTintColor: isSelected
                    ? null
                    : theme.colorScheme.onBackground.withOpacity(0.03),
                margin: const EdgeInsets.all(Spacing.s),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(Spacing.normal),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            attributeList![index]!.name.toString(),
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(width: Spacing.xs),
                          Text(
                            "(${attributeList![index]!.title.toString()})",
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      const SizedBox(height: Spacing.m),
                      if (attributeList![index]!.values!.isNotEmpty)
                        Wrap(
                            spacing: Spacing.s,
                            runSpacing: Spacing.s,
                            children: attributeList![index]!
                                .values!
                                .map(
                                  (e) => Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: Spacing.normal,
                                        vertical: Spacing.s),
                                    decoration: BoxDecoration(
                                      color: theme.colorScheme.onBackground
                                          .withOpacity(0.1),
                                      borderRadius:
                                          BorderRadius.circular(Spacing.s),
                                    ),
                                    child: Text(e.toString()),
                                  ),
                                )
                                .toList())
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
      actions: [
        FilledButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: FilledButton.styleFrom(backgroundColor: Colors.red),
            child: const Text(
              "Cancel",
              style: TextStyle(fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}
