import 'package:bloc_merchant_mobile_2/constants/language.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_extra.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/dialog/extra_product_dialog.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ProductAddExtra extends StatefulWidget {
  final List<ProductExtra?> extraList;
  final List<ProductExtra>? selectedItem;
  final Function(bool)? onSwitch;
  const ProductAddExtra(
      {super.key,
      required this.extraList,
      this.selectedItem = const [],
      this.onSwitch});

  @override
  State<ProductAddExtra> createState() => _ProductAddExtraState();
}

class _ProductAddExtraState extends State<ProductAddExtra> {
  bool isAddTopping = false;
  // final List<Extra?> widget.selectedItem = [];

  void openAddExtraDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return ExtraProductDialog(
            extraList: widget.extraList,
            selectedProduct: widget.selectedItem,
            onTap: (item) {
              setState(() {
                if (widget.selectedItem!
                    .any((e) => e.product_id == item.product_id)) {
                  widget.selectedItem
                      ?.removeWhere((e) => e.product_id == item.product_id);
                } else {
                  widget.selectedItem?.add(item);
                }
              });
            });
      },
    );
  }

  @override
  void initState() {
    setState(() {
      isAddTopping = widget.selectedItem!.isNotEmpty;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        color: theme.colorScheme.background,
        borderRadius: BorderRadius.circular(Spacing.s),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "${LocaleKeys.p_add_topping.tr()} / ${LocaleKeys.p_extra_item.tr()}",
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Spacer(),
              Text(
                isAddTopping
                    ? "(${LocaleKeys.have_topping.tr()})"
                    : "(${LocaleKeys.no.tr()})",
                style: TextStyle(color: isAddTopping ? Colors.blue : null),
              ),
              const SizedBox(width: Spacing.s),
              SizedBox(
                height: 35,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Switch(
                    value: isAddTopping,
                    onChanged: (value) => setState(() {
                      isAddTopping = value;

                      if (widget.onSwitch != null) {
                        widget.onSwitch!(isAddTopping);
                      }
                    }),
                  ),
                ),
              )
            ],
          ),
          Visibility(
            visible: isAddTopping && widget.selectedItem!.isEmpty,
            child: Container(
              height: 80,
              margin: const EdgeInsets.symmetric(vertical: Spacing.s),
              decoration: BoxDecoration(color: Colors.grey[300]),
              alignment: Alignment.center,
              child: Text("${LocaleKeys.no_extra_has_been_selected.tr()}..."),
            ),
          ),
          if (isAddTopping && widget.selectedItem!.isNotEmpty) ...[
            Container(
              height: 45,
              margin: const EdgeInsets.only(top: Spacing.s),
              color: Theme.of(context).colorScheme.primary,
              padding: const EdgeInsets.all(Spacing.s),
              child: Row(
                children: [
                  SizedBox(
                    width: 60,
                    child: Text(LocaleKeys.image.tr(),
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  const SizedBox(width: Spacing.normal),
                  Text(
                      '${LocaleKeys.p_title.tr()} (${LocaleKeys.p_price.tr()} & ${LocaleKeys.format.tr()})',
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            ...List.generate(
                widget.selectedItem!.length,
                (index) => ExtraProductListTile(
                      extraItem: widget.selectedItem![index],
                      isCheck: false,
                      icon: CusIconButton(
                        onPressed: () {
                          final id = widget.selectedItem?[index].product_id;
                          widget.selectedItem
                              ?.removeWhere((e) => e.product_id == id);

                          setState(() {});
                        },
                        style: BoxDecoration(
                            border: Border.all(color: Colors.transparent)),
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                      ),
                    ))
          ],
          Visibility(
            visible: isAddTopping,
            child: Container(
              margin: const EdgeInsets.only(top: Spacing.s),
              height: 35,
              child: FilledButton.icon(
                  onPressed: () {
                    openAddExtraDialog();
                  },
                  style: FilledButton.styleFrom(
                      backgroundColor: theme.colorScheme.onBackground,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(Spacing.xs))),
                  icon: const Icon(Icons.add),
                  label: Text(
                    LocaleKeys.add_item.tr(),
                    style: context.locale == Locales.km
                        ? const TextStyle(fontWeight: FontWeight.bold)
                        : null,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
