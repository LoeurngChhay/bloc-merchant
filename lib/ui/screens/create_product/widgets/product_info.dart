import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_category.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_unit.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/bottom_modal_product_cate.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/gallary_assets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:video_player/video_player.dart';

class ProductInfo extends StatefulWidget {
  final TextEditingController titleTextController;
  final TextEditingController secondTitleController;
  final TextEditingController brandNameController;
  final TextEditingController productCateController;
  final TextEditingController unitTypeController;
  final CreateProductViewController viewController;
  final Function(List<ProductImages> image)? callbackUploadImg;
  final Function(String? videoFile)? callbackUploadVideo;
  final Function(ProductCategory cate)? callbackSelectCate;
  final Function(ProductUnit unit)? callbackSelectUnit;

  const ProductInfo({
    super.key,
    required this.titleTextController,
    required this.secondTitleController,
    required this.brandNameController,
    required this.productCateController,
    required this.unitTypeController,
    required this.viewController,
    this.callbackUploadImg,
    this.callbackUploadVideo,
    this.callbackSelectCate,
    this.callbackSelectUnit,
  });

  @override
  State<ProductInfo> createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  late VideoPlayerController _videoController;
  String? cateTitle;
  String? unitTitle;
  bool loadingVideo = false;
  String? selectedCrop;

  void openBottomSheetModal({
    bool isUnitType = false,
    List<ProductCategory>? itemList,
    List<ProductUnit>? unitList,
  }) {
    showModalBottomSheet(
      context: context,
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.8),
      shape: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(Spacing.normal))),
      builder: (context) {
        return BottomModalProductCate(
          itemList: itemList,
          unitList: unitList,
          onPressedCate: (selectedItem) {
            widget.productCateController.text = selectedItem.title.toString();

            if (widget.callbackSelectCate != null) {
              widget.callbackSelectCate!(selectedItem);
            }
            setState(() {});
            Navigator.pop(context);
          },
          onPressedUnit: (selectedItem) {
            widget.unitTypeController.text = selectedItem.title.toString();

            if (widget.callbackSelectUnit != null) {
              widget.callbackSelectUnit!(selectedItem);
            }
            setState(() {});
            Navigator.pop(context);
          },
        );
      },
    );
  }

  void videoControllerInitial() async {
    await widget.viewController.uploadVideo();
    final video = widget.viewController.video;

    if (video != null) {
      final file = File(video);

      _videoController = VideoPlayerController.file(file);

      loadingVideo = true;
      await _videoController.initialize();

      _videoController.play();
      _videoController.setLooping(true);
      loadingVideo = false;

      setState(() {});
    }
  }

  @override
  void initState() {
    if (widget.productCateController.text != '' &&
        widget.unitTypeController.text != '') {
      cateTitle = widget.productCateController.text;
      unitTitle = widget.unitTypeController.text;
    }

    final video = widget.viewController.video;

    if (video != null) {
      _videoController = VideoPlayerController.networkUrl(Uri.parse(video));
      _videoController.initialize().then((value) {
        setState(() {
          _videoController.setLooping(true);
        });
      });
    }

    setState(() {});

    super.initState();
  }

  @override
  void dispose() {
    final video = widget.viewController.video;
    if (video != null) {
      _videoController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final border = OutlineInputBorder(
        borderSide: BorderSide(width: 1, color: Colors.black.withOpacity(0.6)));

    final productImages = widget.viewController.photos;
    final video = widget.viewController.video;
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Spacing.s),
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(LocaleKeys.p_image.tr())),
          Container(
            height: 150,
            alignment: Alignment.center,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: productImages.length + 1,
              itemBuilder: (context, index) {
                //show button for upload image
                if (index == productImages.length || productImages.isEmpty) {
                  if (productImages.length < 5) {
                    return GestureDetector(
                      // onTap: widget.viewController.uploadImage,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AssetDialog(
                                enableMultiple: true,
                                limit: 5 - productImages.length,
                                enableCrop: true,
                                type: RequestType.image,
                                onNextClick: (imageFiles, type) async {
                                  Navigator.pop(context);
                                  widget.viewController
                                      .uploadImage(images: imageFiles);
                                },
                              ),
                            ));
                      },
                      child: Container(
                          height: 150,
                          width: 150,
                          clipBehavior: Clip.antiAlias,
                          padding: const EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.circular(Spacing.normal),
                          ),
                          child: productImages.isNotEmpty &&
                                  index == productImages.length
                              ? const Icon(Icons.add, size: 40)
                              : Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Icon(CupertinoIcons.camera_fill,
                                        color: Colors.grey),
                                    const SizedBox(height: Spacing.s),
                                    Text(LocaleKeys.click_here_to_upload.tr(),
                                        textAlign: TextAlign.center)
                                  ],
                                )),
                    );
                  } else {
                    return const SizedBox();
                  }
                }

                final productImage = productImages[index];
                final isWorngAspect =
                    productImage.aspect != 1 && productImage.type == 1;

                //show image
                return GestureDetector(
                  onTap: widget.viewController.loadingImage
                      ? null
                      : () async {
                          setState(() {
                            selectedCrop = productImage.photo;
                          });

                          widget.viewController.cropImage(productImage.photo!,
                              isUrl: productImage.type == 0);
                        },
                  child: Stack(
                    clipBehavior: Clip.hardEdge,
                    children: [
                      Container(
                        height: 150,
                        width: 150,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Spacing.normal),
                          border: isWorngAspect
                              ? Border.all(width: 2, color: Colors.red)
                              : Border.all(width: 1, color: Colors.grey),
                          image: productImage.type == 0
                              ? DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(productImage.photo!),
                                )
                              : DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(File(productImage.photo!)),
                                ),
                        ),
                      ),
                      if (isWorngAspect)
                        const Positioned(
                            top: 4,
                            left: 4,
                            child: Icon(
                              Icons.warning_rounded,
                              color: Colors.red,
                              size: 32,
                            )),
                      Positioned(
                        top: Spacing.xs,
                        right: Spacing.xs,
                        child: InkWell(
                          onTap: () {
                            if (widget.viewController.loadingImage == false) {
                              productImages.remove(productImage);
                              setState(() {});
                            }
                          },
                          child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black.withOpacity(0.4)),
                              child: const Icon(
                                Icons.close,
                                color: Colors.white,
                              )),
                        ),
                      ),
                      if (widget.viewController.loadingImage &&
                          selectedCrop == productImage.photo)
                        Positioned.fill(
                            child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(Spacing.normal),
                          ),
                          child: const SizedBox(
                            height: 18,
                            width: 18,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ))
                    ],
                  ),
                );
              },
              separatorBuilder: (context, index) =>
                  const SizedBox(width: Spacing.normal),
            ),
          ),
          const SizedBox(height: Spacing.s),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(LocaleKeys.p_video.tr())),
          Row(
            mainAxisAlignment: video != null
                ? MainAxisAlignment.center
                : MainAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width *
                      (video != null ? 0.5 : 0.8),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: video != null
                                  ? () {
                                      if (_videoController.value.isPlaying) {
                                        _videoController.pause();
                                      } else {
                                        _videoController.play();
                                      }
                                      setState(() {});
                                    }
                                  : videoControllerInitial,
                              child: Stack(
                                children: [
                                  Container(
                                    clipBehavior: Clip.hardEdge,
                                    decoration: BoxDecoration(
                                      color: Colors.grey.shade200,
                                      border: video == null
                                          ? Border.all(
                                              width: 1, color: Colors.grey)
                                          : null,
                                      borderRadius:
                                          BorderRadius.circular(Spacing.normal),
                                    ),
                                    child: video != null
                                        ? loadingVideo
                                            ? const SizedBox(
                                                height: 150,
                                                width: 150,
                                                child: Center(
                                                  child:
                                                      CircularProgressIndicator(),
                                                ),
                                              )
                                            : Stack(
                                                children: [
                                                  AspectRatio(
                                                      aspectRatio:
                                                          _videoController.value
                                                              .aspectRatio,
                                                      child: VideoPlayer(
                                                        _videoController,
                                                      )),
                                                  if (!_videoController
                                                      .value.isPlaying)
                                                    Positioned(
                                                      top: 60,
                                                      bottom: 60,
                                                      left: 70,
                                                      right: 70,
                                                      child: Container(
                                                        height: 40,
                                                        width: 40,
                                                        decoration:
                                                            BoxDecoration(
                                                                color: Colors
                                                                    .black
                                                                    .withOpacity(
                                                                        0.4),
                                                                shape: BoxShape
                                                                    .circle),
                                                        child: const Icon(
                                                          Icons
                                                              .play_arrow_rounded,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                ],
                                              )
                                        : SizedBox(
                                            height: 150,
                                            width: 150,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                const Icon(
                                                  Icons.videocam_rounded,
                                                  size: 40,
                                                  color: Colors.grey,
                                                ),
                                                const SizedBox(
                                                    height: Spacing.xs),
                                                SizedBox(
                                                  width: 120,
                                                  child: Text(
                                                    LocaleKeys
                                                        .click_here_to_upload
                                                        .tr(),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                  ),
                                  if (video != null)
                                    Positioned(
                                        top: 8,
                                        right: 8,
                                        child: InkWell(
                                          onTap: () {
                                            widget.viewController.removeVideo();
                                            _videoController.dispose();
                                          },
                                          child: Container(
                                            height: 30,
                                            width: 30,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.red,
                                            ),
                                            child: const Icon(
                                              Icons.close,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ))
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: Spacing.s),
                          if (video == null)
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${LocaleKeys.p_ratio.tr()}: 16:9',
                                  style: const TextStyle(fontSize: 12),
                                ),
                                Text(
                                  '${LocaleKeys.p_video_duration.tr()}: 29 ${LocaleKeys.second.tr()}',
                                  style: const TextStyle(fontSize: 12),
                                ),
                                Text(
                                  '${LocaleKeys.p_file_type.tr()}: MP4',
                                  style: const TextStyle(fontSize: 12),
                                ),
                              ],
                            )
                        ],
                      ),
                      if (video != null) ...[
                        const SizedBox(height: Spacing.m),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                            onTap: () {
                              _videoController.dispose();
                              videoControllerInitial();
                            },
                            child: Text(
                              LocaleKeys.change.tr(),
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                        ),
                      ]
                    ],
                  )),
            ],
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              controller: widget.titleTextController,
              contentPadding: const EdgeInsets.all(Spacing.s),
              hintText: LocaleKeys.p_title.tr(),
              border: border,
              focusedBorder: border,
              enabledBorder: border,
            ),
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              controller: widget.secondTitleController,
              contentPadding: const EdgeInsets.all(Spacing.s),
              hintText: LocaleKeys.p_secondary_title.tr(),
              border: border,
              focusedBorder: border,
              enabledBorder: border,
            ),
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              controller: widget.brandNameController,
              contentPadding: const EdgeInsets.all(Spacing.s),
              hintText: LocaleKeys.p_brand_name.tr(),
              border: border,
              focusedBorder: border,
              enabledBorder: border,
            ),
          ),
          const SizedBox(height: Spacing.normal),
          Row(
            children: [
              Expanded(
                  flex: 2,
                  child: SizedBox(
                    height: 45,
                    child: CusTextField(
                        onTap: () {
                          openBottomSheetModal(
                            itemList:
                                widget.viewController.productDetail!.categories,
                          );
                        },
                        controller: widget.productCateController,
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        hintText: LocaleKeys.select_category.tr(),
                        labelText: LocaleKeys.p_category.tr(),
                        border: border,
                        readOnly: true,
                        focusedBorder: border,
                        enabledBorder: border,
                        suffixIcon: const Icon(Icons.arrow_drop_down)),
                  )),
              const SizedBox(width: Spacing.m),
              Expanded(
                  flex: 1,
                  child: SizedBox(
                    height: 45,
                    child: CusTextField(
                        onTap: () => openBottomSheetModal(
                            isUnitType: true,
                            unitList:
                                widget.viewController.productDetail?.unit_list),
                        controller: widget.unitTypeController,
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        hintText: LocaleKeys.select_unit_type.tr(),
                        labelText: LocaleKeys.p_unit.tr(),
                        border: border,
                        readOnly: true,
                        focusedBorder: border,
                        enabledBorder: border,
                        suffixIcon: const Icon(Icons.arrow_drop_down)),
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
