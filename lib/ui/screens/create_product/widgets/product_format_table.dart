import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ProductItemFormatList extends StatefulWidget {
  final List<Format> formatList;
  final Function(String id)? onEdit;
  final Function(String id)? onDelete;
  const ProductItemFormatList(
      {super.key, required this.formatList, this.onEdit, this.onDelete});

  @override
  State<ProductItemFormatList> createState() => _ProductItemFormatListState();
}

class _ProductItemFormatListState extends State<ProductItemFormatList> {
  double actionSize = 60;
  double tableHeaderH = 35;
  double nameW = 100;
  double priceW = 80;
  double actionW = 40;

  @override
  Widget build(BuildContext context) {
    const headerStyle = TextStyle(fontWeight: FontWeight.bold);

    return SizedBox(
      height: 35 * (widget.formatList.length + 1),
      child: ListView(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        children: [
          Column(
            children: [
              Container(
                color: Colors.amber,
                height: 35,
                padding: const EdgeInsets.symmetric(horizontal: Spacing.m),
                child: Row(
                  children: [
                    Container(
                      width: actionW * 2,
                      alignment: Alignment.center,
                      child: Text(LocaleKeys.action.tr(), style: headerStyle),
                    ),
                    Container(
                      width: nameW,
                      alignment: Alignment.centerLeft,
                      child: Text(LocaleKeys.p_name.tr(), style: headerStyle),
                    ),
                    Container(
                      width: priceW,
                      alignment: Alignment.center,
                      child: Text(LocaleKeys.p_price.tr(), style: headerStyle),
                    ),
                    Container(
                      width: priceW,
                      alignment: Alignment.center,
                      child: Text(LocaleKeys.qty.tr(), style: headerStyle),
                    ),
                    Container(
                      width: priceW,
                      alignment: Alignment.center,
                      child:
                          Text(LocaleKeys.p_pack_fee.tr(), style: headerStyle),
                    ),

                    Container(
                      width: nameW,
                      margin: const EdgeInsets.only(left: Spacing.m),
                      alignment: Alignment.centerLeft,
                      child:
                          Text(LocaleKeys.p_barcode.tr(), style: headerStyle),
                    ),
                    // Container(
                    //   width: priceW,
                    //   alignment: Alignment.center,
                    //   child: Text('Total Item', style: headerStyle),
                    // ),
                  ],
                ),
              ),
              ...widget.formatList.map((e) {
                return SizedBox(
                  height: 35,
                  child: Row(
                    children: [
                      InkWell(
                          onTap: () {
                            if (widget.onEdit != null) {
                              widget.onEdit!(e.spec_id!);
                            }
                          },
                          child: SizedBox(
                              width: actionW,
                              height: 33,
                              child:
                                  const Icon(Icons.edit, color: Colors.amber))),
                      SizedBox(
                          height: 35,
                          child: VerticalDivider(
                            endIndent: 4,
                            indent: 4,
                            color: Colors.black.withOpacity(0.5),
                          )),
                      InkWell(
                          onTap: () {
                            if (widget.onDelete != null) {
                              widget.onDelete!(e.spec_id!);
                            }
                          },
                          child: SizedBox(
                              height: 33,
                              width: actionW,
                              child:
                                  const Icon(Icons.delete, color: Colors.red))),
                      Container(
                        width: nameW,
                        // color: Colors.red,
                        alignment: Alignment.centerLeft,
                        child: Text(e.spec_name.toString(),
                            overflow: TextOverflow.ellipsis),
                      ),
                      Container(
                        width: priceW,
                        alignment: Alignment.center,
                        child: Text(
                            e.price == ""
                                ? "${e.price}"
                                : '\$ ${double.parse(e.price.toString()).toStringAsFixed(2)}',
                            overflow: TextOverflow.ellipsis),
                      ),
                      Container(
                        width: priceW,
                        alignment: Alignment.center,
                        child: Text(
                            e.sale_sku == "9999"
                                ? "Unlimited"
                                : e.sale_sku.toString(),
                            overflow: TextOverflow.ellipsis),
                      ),
                      Container(
                        width: priceW,
                        alignment: Alignment.center,
                        child: Text(
                            e.package_price == ""
                                ? "\$0.00"
                                : '\$ ${double.parse(e.package_price.toString()).toStringAsFixed(2)}',
                            overflow: TextOverflow.ellipsis),
                      ),
                      Container(
                        width: nameW,
                        margin: const EdgeInsets.only(left: Spacing.m),
                        alignment: Alignment.centerLeft,
                        child: Text(e.bar_code.toString(),
                            overflow: TextOverflow.ellipsis),
                      ),
                      // Container(
                      //   width: priceW,
                      //   alignment: Alignment.center,
                      //   child: const Text('1', overflow: TextOverflow.ellipsis),
                      // ),
                    ],
                  ),
                );
              }),
            ],
          )
        ],
      ),
    );
  }
}
