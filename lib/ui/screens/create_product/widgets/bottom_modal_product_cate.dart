import 'package:bloc_merchant_mobile_2/data/models/responses/product_category.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_unit.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:bloc_merchant_mobile_2/utils/debounce.dart';
import 'package:flutter/material.dart';

class BottomModalProductCate extends StatefulWidget {
  final Function(ProductCategory)? onPressedCate;
  final Function(ProductUnit)? onPressedUnit;

  final List<ProductCategory>? itemList;
  final List<ProductUnit>? unitList;

  const BottomModalProductCate({
    super.key,
    this.itemList,
    this.unitList,
    this.onPressedCate,
    this.onPressedUnit,
  });

  @override
  State<BottomModalProductCate> createState() => _BottomModalProductCateState();
}

class _BottomModalProductCateState extends State<BottomModalProductCate> {
  final debouncer = Debouncer();
  List<ProductCategory> cateList = [];
  List<ProductUnit> unitList = [];
  final _searchController = TextEditingController();
  String? unitText;

  void onSearching() {
    final searchText = _searchController.text.toLowerCase();

    if (widget.unitList != null) {
      unitList = widget.unitList!
          .where((item) => item.title!.toLowerCase().contains(searchText))
          .toList();
    } else {
      cateList = widget.itemList!
          .where((item) => item.title!.toLowerCase().contains(searchText))
          .toList();
    }
    setState(() {});
  }

  @override
  void initState() {
    _searchController.addListener(
      () {
        debouncer.run(() {
          onSearching();
        });
      },
    );
    setState(() {
      cateList = widget.itemList ?? [];
      unitList = widget.unitList ?? [];
    });
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding:
          const EdgeInsets.symmetric(horizontal: Spacing.normal, vertical: 0),
      height: MediaQuery.of(context).size.height * 0.8,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(22),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: Spacing.normal),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                    widget.unitList != null
                        ? "Product Unit"
                        : "Product Category",
                    style: theme.textTheme.titleMedium
                        ?.copyWith(fontWeight: FontWeight.bold)),
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.close_rounded))
              ],
            ),
          ),
          SizedBox(
            height: 45,
            child: TextFormField(
              controller: _searchController,
              decoration: InputDecoration(
                  hintText: "Search...",
                  contentPadding: EdgeInsets.zero,
                  prefixIcon: const Icon(Icons.search),
                  suffixIcon: _searchController.text.isEmpty
                      ? null
                      : InkWell(
                          onTap: () {
                            _searchController.clear();
                          },
                          child: const Icon(Icons.close),
                        ),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
          ),
          const SizedBox(height: Spacing.normal),
          if (widget.unitList != null) ...[
            const Text(
              "Don't have ?",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Row(
              children: [
                SizedBox(
                  height: 42,
                  width: MediaQuery.of(context).size.width / 2,
                  child: CusTextField(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    hintText: "Unit",
                    onChange: (text) {
                      unitText = text;
                    },
                  ),
                ),
                const SizedBox(width: Spacing.m),
                FilledButton(
                    onPressed: () {
                      final unit = ProductUnit(title: unitText);

                      widget.onPressedUnit!(unit);
                    },
                    style: FilledButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8))),
                    child: const Text("Add"))
              ],
            ),
          ],
          // const SizedBox(height: Spacing.normal),
          Expanded(
              child: ListView.separated(
            itemCount:
                widget.unitList != null ? unitList.length : cateList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  if (cateList.isNotEmpty) {
                    widget.onPressedCate!(cateList[index]);

                    return;
                  }

                  if (unitList.isNotEmpty) {
                    widget.onPressedUnit!(unitList[index]);

                    return;
                  }
                },
                child: Container(
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width,
                    height: 45,
                    alignment: Alignment.centerLeft,
                    child: Text(
                        widget.unitList != null
                            ? unitList[index].title.toString()
                            : cateList[index].title.toString(),
                        textAlign: TextAlign.start,
                        style: theme.textTheme.titleMedium)),
              );
            },
            separatorBuilder: (context, index) =>
                const SizedBox(height: Spacing.s),
          ))
        ],
      ),
    );
  }
}
