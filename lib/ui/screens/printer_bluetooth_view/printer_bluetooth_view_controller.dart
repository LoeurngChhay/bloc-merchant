import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/login_res.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_exception.dart';
import 'package:bloc_merchant_mobile_2/utils/printer_utils.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:dio/dio.dart';
import 'package:esc_pos_bluetooth_updated/esc_pos_bluetooth_updated.dart';
import 'package:esc_pos_utils_updated/esc_pos_utils_updated.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:image/image.dart';
import 'package:flutter/services.dart';
import 'package:print_bluetooth_thermal/print_bluetooth_thermal.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrinterBluetoothViewController extends ChangeNotifier {
  final PrinterProvider? printerProvider;
  final Function(String msg)? onSuccessPrint;
  final Function(String msg)? onErrorPrint;
  final Function()? openSetting;

  PrinterBluetoothViewController({
    this.printerProvider,
    this.onSuccessPrint,
    this.onErrorPrint,
    this.openSetting,
  });

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  final _preferences = SharedPreferences.getInstance();

  bool _isScanning = true;
  bool get isScanning => _isScanning;
  set isScanning(bool value) {
    _isScanning = value;
    notifyListeners();
  }

  bool _onProgress = false;
  bool get onProgress => _onProgress;
  set onProgress(bool value) {
    _onProgress = value;
    notifyListeners();
  }

  bool _isConnecting = false;
  bool get isConnecting => _isConnecting;
  set isConnecting(bool value) {
    _isConnecting = value;
    notifyListeners();
  }

  bool _isConnected = false;
  bool get isConnected =>
      _isConnected || deviceIOS != null || deviceAndr != null;
  set isConnected(bool value) {
    _isConnected = value;
    notifyListeners();
  }

  Future<void> initPrinterBluetooth() async {
    if (Platform.isIOS) {
      initIOS();
      return;
    }

    //----------------------

    if (Platform.isAndroid) {
      initAndorid();
      return;
    }
  }

  CustomException? _errorPrinter;
  CustomException? get errorPrinter => _errorPrinter;
  set errorPrinter(CustomException? value) {
    _errorPrinter = value;
    notifyListeners();
  }

//--------- IOS ----------
  List<PrinterBluetooth> _devicesIOS = [];
  List<PrinterBluetooth> get devices => _devicesIOS;
  set devicesIOS(List<PrinterBluetooth> value) {
    _devicesIOS = value;
    notifyListeners();
  }

  PrinterBluetooth? _deviceIOS;
  PrinterBluetooth? get deviceIOS => _deviceIOS ?? printerProvider?.deviceIos;
  set deviceIOS(PrinterBluetooth? value) {
    _deviceIOS = value;

    notifyListeners();
  }

  Future<void> startScanIOS() async {
    printerManager.startScan(const Duration(seconds: 2));
  }

  Future<void> initIOS() async {
    final connectedPrint = printerProvider?.deviceIos;

    if (connectedPrint != null) {
      _devicesIOS.add(connectedPrint);
      deviceIOS = connectedPrint;
    }
    isScanning = true;

    try {
      await startScanIOS();
      printerManager.scanResults.listen((printers) async {
        devicesIOS = printers;
      });
    } catch (e) {
      print("Error Ios printer : $e");
      if (e is DioException) {
        errorPrinter = CustomException(e.message.toString());
      }
    } finally {
      Future<dynamic>.delayed(const Duration(seconds: 5)).then((value) {
        isScanning = false;
      });
    }
  }

  Future<void> connectIOSDevice(PrinterBluetooth printer) async {
    final prefs = await _preferences;
    isConnecting = true;

    final deviceJson = ''' {
      "name": "${printer.name}",
      "address": "${printer.address}",
      "type": ${printer.type},
      "connected": false
    }''';

    printerManager.selectPrinter(printer);
    deviceIOS = printer;
    printerProvider?.deviceIos = deviceIOS;
    prefs.setString(StorageKeys.PRINTER_KEY, deviceJson);
    printerProvider?.setBlueToothIOSDevice(printer);
    isConnecting = false;
  }

  //--------- Android ----------

  List<BluetoothInfo> _deviceListAndr = [];
  List<BluetoothInfo> get deviceListAndr => _deviceListAndr;
  set deviceListAndr(List<BluetoothInfo> newDevice) {
    _deviceListAndr = newDevice;
    notifyListeners();
  }

  BluetoothInfo? _deviceAndr;
  BluetoothInfo? get deviceAndr => _deviceAndr ?? printerProvider?.deviceAndr;
  set deviceAndr(BluetoothInfo? value) {
    _deviceAndr = value;

    notifyListeners();
  }

  Future<void> initAndorid() async {
    final device = printerProvider?.deviceAndr;
    if (device != null) {
      deviceAndr = device;
      deviceListAndr.add(device);
    }

    isScanning = true;

    try {
      final enable = await PrintBluetoothThermal.bluetoothEnabled;
      final permission =
          await PrintBluetoothThermal.isPermissionBluetoothGranted;
      print('$permission, Bluetooth enable : $enable');
      if (!enable && openSetting != null) {
        openSetting!();

        throw "Bluetooth is disable, please enable bluetooth.";
      }

      print("permission s : $permission");

      if (!permission) {
        throw "No nearby permissoin.";
      }

      final List<BluetoothInfo> listResult =
          await PrintBluetoothThermal.pairedBluetooths;

      print('listResult : $enable');
      deviceListAndr = listResult;
    } catch (e) {
      print('Error Android Scann : $e');
      if (e is DioException) {
        errorPrinter = CustomException(e.message.toString());
      }
    } finally {
      Future<dynamic>.delayed(const Duration(seconds: 4)).then((value) {
        isScanning = false;
      });
    }
  }

  Future<void> connectAndrDevice(BluetoothInfo value) async {
    // final name = value.name;
    final address = value.macAdress;
    isConnecting = true;

    try {
      final prefs = await _preferences;

      final connectionStatus = await PrintBluetoothThermal.connectionStatus;

      if (connectionStatus) await PrintBluetoothThermal.disconnect;

      final isConnect =
          await PrintBluetoothThermal.connect(macPrinterAddress: address);

      if (isConnect) {
        final deviceJson = ''' {
            "name": "${value.name}",
            "macAdress": "${value.macAdress}"
          }''';

        deviceAndr = value;
        printerProvider?.setBlueToothAndroDevice(value);
        prefs.setString(StorageKeys.PRINTER_KEY, deviceJson);
      } else {
        if (onErrorPrint != null) {
          onErrorPrint!('Make sure "${value.name}" is turned on and in range.');
        }
      }
    } catch (e) {
      print('Error Connect Android ; $e');
    } finally {
      isConnecting = false;
    }
  }

  //----------------------------

  List<Uint8List> _textIMg = [];
  List<Uint8List> get textIMg => _textIMg;
  set textIMg(List<Uint8List> value) {
    _textIMg = value;
    notifyListeners();
  }

  Future<void> printTest() async {
    onProgress = true;
    try {
      final printFormat = await testTicket();

      if (Platform.isIOS) {
        //ios
        final PosPrintResult res =
            await printerManager.printTicket(printFormat, chunkSizeBytes: 40);

        if (res == PosPrintResult.timeout ||
            res == PosPrintResult.printerNotSelected) {
          await printerManager.disconnect();
          await connectIOSDevice(_deviceIOS!);
          await printerManager.printTicket(printFormat, chunkSizeBytes: 40);
        }
      } else {
        //android

        bool connetionStatus = await PrintBluetoothThermal.connectionStatus;

        if (connetionStatus) {
          await PrintBluetoothThermal.writeBytes(printFormat);
        } else {
          print('deviceAndr L: $deviceAndr');
          connectAndrDevice(deviceAndr!);

          throw "the printer is disconnected ($connetionStatus)";
        }
      }
    } catch (e) {
      print("erorr Pirnter : $e");
      deviceIOS = null;
      deviceAndr = null;

      // print("viewController.deviceAndr : $deviceIOS, $deviceAndr");

      if (onErrorPrint != null) {
        onErrorPrint!(e.toString());
      }
    } finally {
      onProgress = false;
    }
  }

  Future<void> startPrinter({
    OrderDetail? orderDetail,
    String? orderId,
    String? time,
    Shop? shop,
    bool forShop = false,
  }) async {
    onProgress = true;

    final printFormat = await tickerFormat(orderDetail,
        orderId: orderId, time: time, shop: shop, forShop: forShop);

    try {
      if (Platform.isIOS) {
        //ios

        await printerManager.printTicket(printFormat,
            chunkSizeBytes: 60, queueSleepTimeMs: 40);
      } else {
        //android

        bool conexionStatus = await PrintBluetoothThermal.connectionStatus;
        if (conexionStatus) {
          await PrintBluetoothThermal.writeBytes(printFormat);
        }
      }
    } catch (e) {
      print("error Printer : $e");
      deviceIOS = null;
      deviceAndr = null;
      if (e is DioException) {
        if (onErrorPrint != null) {
          onErrorPrint!(e.message.toString());
        }
      }
    } finally {
      onProgress = false;
    }
  }

  Future<List<int>> tickerFormat(
    OrderDetail? orderDetail, {
    PaperSize? paperSize = PaperSize.mm58,
    String? orderId,
    String? time,
    bool forShop = false,
    Shop? shop,
  }) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(paperSize!, profile);

    List<int> bytes = [];

    String shopTitle = shop?.title ?? "";

    final shopImg =
        await PrinterUtils.createImageName(shopTitle, width: 300, height: 60);

    Image? shopName = decodeImage(shopImg!);
    if (Platform.isIOS) {
      final resizedImage = copyResize(shopName!,
          width: 300, height: 60, interpolation: Interpolation.nearest);
      final bytesimg = Uint8List.fromList(encodeJpg(resizedImage));
      shopName = decodeImage(bytesimg);
    }

    bytes += generator.image(shopName!);

    bytes += generator.text("BLOC #${shop?.shop_id}");
    bytes += generator.row([
      PosColumn(
          text: "Order: #$orderId",
          width: 12,
          styles: const PosStyles.defaults())
    ]);
    bytes += generator.text("Time: $time", styles: const PosStyles.defaults());
    bytes += generator.hr();
    if (orderDetail != null) {
      for (var item in orderDetail.products!) {
        Uint8List? covertToImg =
            await PrinterUtils.createImageProduct(width: 350, item: item);

        Image? product = decodeImage(covertToImg!);
        if (Platform.isIOS) {
          final resizedImage = copyResize(product!,
              width: 372, height: 100, interpolation: Interpolation.nearest);
          final bytesimg = Uint8List.fromList(encodeJpg(resizedImage));
          product = decodeImage(bytesimg);
        }

        bytes += generator.image(product!);
      }
    }

    bytes += generator.feed(2);
    bytes += generator.hr();

    double subtotal = double.parse(orderDetail?.product_price ?? "0");
    double discount = (orderDetail?.discount_amount ?? 0).toDouble();
    double parkageFee = double.parse(orderDetail?.package_price ?? "0");
    double vatAmount = (orderDetail?.vat_amount ?? 0).toDouble();
    // double deliveryFee = item!.delivery_fee!;
    // double bagFee = item!.bag_price!;
    double total = ((subtotal + parkageFee) - discount) + vatAmount;

    //------ sub total
    bytes += generator.row([
      PosColumn(
          text: 'SUB-TOTAL:', width: 8, styles: const PosStyles.defaults()),
      PosColumn(
          text: "${subtotal.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ package fee
    bytes += generator.row([
      PosColumn(
          text: 'Packaging Fee:', width: 8, styles: const PosStyles.defaults()),
      PosColumn(
          text: "${parkageFee.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ Discount
    bytes += generator.row([
      PosColumn(text: 'Discount:', width: 8),
      PosColumn(
          text: "${orderDetail?.discount_amount?.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ vat
    bytes += generator.row([
      PosColumn(text: 'Vat:', width: 8),
      PosColumn(
          text: "${orderDetail?.vat_amount?.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ vat
    bytes += generator.row([
      PosColumn(text: 'Total:', width: 8),
      PosColumn(
          text:
              "${forShop ? orderDetail?.shop_receive?.toStringAsFixed(2) : total.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    bytes += generator.feed(2);
    bytes += generator.text("TEL: 016 | 017 421 333",
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));
    bytes += generator.text("** Thank you for using BLOC**",
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));

    bytes += generator.cut();

    return bytes;
  }

  Future<List<int>> testTicket({Uint8List? testImage}) async {
    // printerManager.selectPrinter(printer);
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm58, profile);
    List<int> bytes = [];

    final item = await PrinterUtils.createImageName('តេស្តបោះពុម្ព',
        width: 300, height: 80);

    Image? imageKm = decodeImage(item!);
    if (Platform.isIOS) {
      final resizedImage = copyResize(imageKm!,
          width: 300, height: 60, interpolation: Interpolation.nearest);
      final bytesimg = Uint8List.fromList(encodeJpg(resizedImage));
      imageKm = decodeImage(bytesimg);
    }

    bytes += generator.image(imageKm!);

    bytes += generator.text('Print Test',
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));

    bytes += generator.text("列印測試",
        containsChinese: true,
        styles: const PosStyles(
          align: PosAlign.center,
        ));

    bytes += generator.cut();
    return bytes;
  }

  void disposed() async {
    //----- IOS -----
    if (_devicesIOS.isNotEmpty) {
      printerManager.dispose();
      return;
    }

    //----- Android -----
    if (_deviceListAndr.isNotEmpty) {
      await PrintBluetoothThermal.disconnect;
      return;
    }
  }
}
