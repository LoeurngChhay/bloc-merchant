import 'dart:async';
import 'dart:io';

import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_bluetooth_view/printer_bluetooth_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/utils/bluetooth_utils.dart';
import 'package:esc_pos_bluetooth_updated/esc_pos_bluetooth_updated.dart';
import 'package:flutter/material.dart';
import 'package:print_bluetooth_thermal/print_bluetooth_thermal.dart';
import 'package:provider/provider.dart';
import 'package:permission_handler/permission_handler.dart';

class PrinterBluetoothView extends StatefulWidget {
  static const String routeName = '/printer-bluetooth';

  const PrinterBluetoothView({super.key});

  @override
  State<PrinterBluetoothView> createState() => _PrinterBluetoothViewState();
}

class _PrinterBluetoothViewState extends State<PrinterBluetoothView> {
  late PrinterBluetoothViewController _printerViewController;
  BluetoothInfo? deviceAndr;
  PrinterBluetooth? deviceIOS;

  void showPermissionDialog(String message) {
    showAdaptiveDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Icon(Icons.nearby_error),
          content: Text(message),
          actions: [
            ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("Cancel")),
            FilledButton(
                onPressed: () async {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  await Future.delayed(const Duration(milliseconds: 300));
                  await openAppSettings().then((value) {
                    print("Back from setting");
                    _printerViewController.initAndorid();
                  });
                },
                child: const Text("Open Setting")),
          ],
        );
      },
    );
  }

  void checkPermission() async {
    try {
      final statusScan = await Permission.bluetoothScan.request();

      await Permission.bluetooth.request();
      await Permission.bluetoothConnect.request().then((permission) {
        if (permission.isGranted) _printerViewController.initAndorid();
      });

      if (statusScan.isDenied || statusScan.isPermanentlyDenied) {
        print('bluetoothScan view: $statusScan');

        showPermissionDialog(
            "Allow App to find, connect to, and determine the relative position of nearby devices?");
      }
    } catch (e) {
      print('Error Bluetooth Permission. $e');
    }
  }

  void openBluetooth() {
    showDialog(
      context: context,
      builder: (ctxDialog) {
        return AlertDialog(
          title: const Text("Bluetooth"),
          content:
              const Text("This app requires Bluetooth to connect to device."),
          actions: [
            ElevatedButton(
                onPressed: () async {
                  BluetoothUtils.checkBluetoothStatus()
                      .then((value) => Navigator.pop(context, value));
                },
                child: const Text("Cancel")),
            FilledButton(
                onPressed: () async {
                  await BluetoothUtils.openBluetoothSettings();
                  print('back from bluetooth setting ');

                  BluetoothUtils.checkBluetoothStatus()
                      .then((value) => Navigator.pop(context, value));
                },
                child: const Text("Open Setting")),
          ],
        );
      },
    ).then((value) {
      if (value != null && value == true) {
        _printerViewController.initAndorid();
      }
    });
  }

  @override
  void initState() {
    if (Platform.isAndroid) {
      checkPermission();
    }

    super.initState();
  }

  @override
  void dispose() {
    _printerViewController.disposed();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PrinterBluetoothViewController(
        printerProvider: context.read<PrinterProvider>(),
        onErrorPrint: (message) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text("Connection Unsuccessfull"),
                content: Text(message),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text("Ok"))
                ],
              );
            },
          );
        },
        openSetting: () {
          openBluetooth();
        },
      )..initPrinterBluetooth(),
      child: Consumer<PrinterBluetoothViewController>(
          builder: (context, viewController, child) {
        _printerViewController = viewController;
        return Scaffold(
            appBar: AppBar(
              title: const Text("Printer"),
              actions: [
                InkWell(
                    onTap: () {
                      if (viewController.isConnected) {}
                      viewController.printTest();

                      // final mappedRes = fakeOrderDetail as Map<String, dynamic>;
                      // final response = BaseResponse.fromMap(
                      //     mappedRes, OrderDetail.fromMap(mappedRes["data"]));
                      // final productDetial = response.data;
                      //   viewController.testPrinterImage(productDetial!);
                    },
                    child: viewController.onProgress
                        ? const SizedBox(
                            height: 18,
                            width: 18,
                            child: CircularProgressIndicator(),
                          )
                        : const SizedBox(
                            height: 45,
                            width: 45,
                            child: Icon(Icons.print_rounded))),
                const SizedBox(width: Spacing.l)
              ],
            ),
            body: RefreshIndicator(
              onRefresh: () async {
                viewController.initPrinterBluetooth();
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Container(
                  color: Colors.transparent,
                  constraints: BoxConstraints(
                      minHeight: MediaQuery.of(context).size.height -
                          MediaQuery.of(context).viewPadding.top -
                          AppBar().preferredSize.height),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: Platform.isIOS
                        ? childrenIOS(viewController)
                        : childrenAndroid(viewController),
                  ),
                ),
              ),
            ));
      }),
    );
  }

  List<Widget> childrenIOS(PrinterBluetoothViewController viewController) {
    return [
      if (viewController.isScanning)
        Container(
            margin: const EdgeInsets.only(top: Spacing.normal),
            alignment: Alignment.center,
            child: const CircularProgressIndicator())
      else if (viewController.devices.isEmpty) ...[
        const SizedBox(height: 100),
        SizedBox(
            height: 142,
            width: 142,
            child: Image.asset('assets/images/printer.png')),
        const SizedBox(
            width: double.infinity,
            child: Text(
              "There is no printer here.",
              textAlign: TextAlign.center,
            )),
        const SizedBox(height: Spacing.m),
        ElevatedButton(
            onPressed: () {
              viewController.initIOS();
            },
            child: const Text("Scan again")),
        const SizedBox(height: 100),
      ] else
        ...viewController.devices.map((e) => ListTile(
              onTap: viewController.isConnecting && deviceIOS == e
                  ? null
                  : () {
                      deviceIOS = e;

                      viewController.connectIOSDevice(e);
                    },
              leading: viewController.isConnecting
                  ? const SizedBox(
                      height: 18,
                      width: 18,
                      child: CircularProgressIndicator(),
                    )
                  : viewController.deviceIOS != null &&
                          viewController.deviceIOS?.address == e.address
                      ? const Icon(
                          Icons.check_box_rounded,
                          color: Colors.blue,
                        )
                      : const Icon(Icons.check_box_outline_blank),
              title: Text(e.name.toString()),
              subtitle: Text(e.address.toString()),
              trailing: InkWell(
                  onTap: () {
                    if (viewController.deviceAndr != null &&
                        viewController.deviceAndr?.macAdress == e.address) {
                      viewController.printTest();
                    }
                  },
                  child: const Icon(Icons.print_rounded)),
            )),
      // if (viewController.textIMg != null) Image.memory(viewController.textIMg!)
    ];
  }

  List<Widget> childrenAndroid(PrinterBluetoothViewController viewController) {
    return [
      if (viewController.isScanning)
        Container(
            margin: const EdgeInsets.only(top: Spacing.normal),
            alignment: Alignment.center,
            child: const CircularProgressIndicator())
      else if (viewController.deviceListAndr.isEmpty) ...[
        const SizedBox(height: 100),
        SizedBox(
            height: 142,
            width: 142,
            child: Image.asset('assets/images/printer.png')),
        const SizedBox(
            width: double.infinity,
            child: Text(
              "There is no printer here.",
              textAlign: TextAlign.center,
            )),
        const SizedBox(height: Spacing.m),
        ElevatedButton(
            onPressed: () {
              viewController.initAndorid();
            },
            child: const Text("Scan again")),
        const SizedBox(height: 100),
      ] else
        ...viewController.deviceListAndr.map((e) => ListTile(
              onTap: () async {
                deviceAndr = e;
                viewController.connectAndrDevice(e);
              },
              leading: viewController.isConnecting && deviceAndr == e
                  ? const SizedBox(
                      height: 18,
                      width: 18,
                      child: CircularProgressIndicator(),
                    )
                  : viewController.deviceAndr != null &&
                          viewController.deviceAndr?.macAdress == e.macAdress
                      ? const Icon(
                          Icons.check_box_rounded,
                          color: Colors.blue,
                        )
                      : const Icon(Icons.check_box_outline_blank),
              title: Text(e.name.toString()),
              subtitle: Text(e.macAdress.toString()),
              focusColor: Colors.blue,
              trailing: InkWell(
                  onTap: () {
                    if (viewController.deviceAndr != null &&
                        viewController.deviceAndr?.macAdress == e.macAdress) {
                      viewController.printTest();
                    }
                  },
                  child: const Icon(Icons.print_rounded)),
            )),
    ];
  }
}
