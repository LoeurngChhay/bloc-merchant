import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/widgets/ads_action_button.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/widgets/ads_info.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:flutter/material.dart';

class AdsImageDetail extends StatefulWidget {
  final AdsItem post;
  const AdsImageDetail({super.key, required this.post});

  @override
  State<AdsImageDetail> createState() => _AdsImageDetailState();
}

class _AdsImageDetailState extends State<AdsImageDetail> {
  bool isFullScreen = false;

  void openMoreOption() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.2,
          color: Colors.black87,
          child: ListView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(Spacing.normal),
            children: [
              Container(
                height: 72,
                width: 72,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.9),
                    shape: BoxShape.circle),
                child: const Icon(
                  Icons.delete,
                  size: 32,
                  color: Colors.red,
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(child: SizedBox()),
        Align(
          alignment: Alignment.center,
          child: TransparentImage(
            url: widget.post.thumbnail,
            fit: BoxFit.contain,
          ),
        ),
        //app bar
        Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.all(16),
          child: SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const CusBackButton(color: Colors.white),
                const Text(
                  "Detail",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w600),
                ),
                InkWell(
                  onTap: openMoreOption,
                  child: const Icon(
                    Icons.more_horiz,
                    color: Colors.white,
                    size: 32,
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
            right: 8,
            bottom: 16 + MediaQuery.of(context).viewPadding.bottom,
            child: Column(
              children: [
                AnimatedOpacity(
                  opacity: isFullScreen ? 0 : 1,
                  duration: const Duration(milliseconds: 300),
                  child: AdsActionButton(
                    onPressLike: () {},
                    onPressAds: () {},
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isFullScreen = !isFullScreen;
                    });
                  },
                  child: Container(
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      shape: BoxShape.circle,
                      border: Border.all(width: 3, color: Colors.white),
                    ),
                    child: isFullScreen
                        ? const Icon(
                            Icons.close_fullscreen,
                            color: Colors.white,
                            size: 24,
                          )
                        : const Icon(
                            Icons.open_in_full,
                            color: Colors.white,
                            size: 24,
                          ),
                  ),
                ),
              ],
            )),
        Positioned(
          bottom: 16,
          left: 8,
          right: 80,
          child: AnimatedOpacity(
              opacity: isFullScreen ? 0 : 1,
              duration: const Duration(milliseconds: 300),
              child: AdsInfo(post: widget.post)),
        ),
      ],
    );
  }
}
