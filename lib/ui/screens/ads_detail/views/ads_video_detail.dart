import 'package:bloc_merchant_mobile_2/constants/post_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads/ads_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/widgets/ads_action_button.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/widgets/ads_info.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class AdsVideoDetail extends StatefulWidget {
  final AdsItem post;
  const AdsVideoDetail({super.key, required this.post});

  @override
  State<AdsVideoDetail> createState() => _AdsVideoDetailState();
}

class _AdsVideoDetailState extends State<AdsVideoDetail> {
  late VideoPlayerController _videoController;

  bool isVideoInitial = false;
  bool isFullScreen = false;

  void showAlertDialog() {
    showAdaptiveDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Row(
            children: [
              Icon(
                Icons.error,
                color: Colors.red,
              ),
              SizedBox(width: Spacing.xs),
              Text("Error"),
            ],
          ),
          content: const Text("Fail to play video."),
          actions: [
            FilledButton(
                style: FilledButton.styleFrom(backgroundColor: Colors.red),
                onPressed: () {
                  Navigator.popUntil(
                      context, ModalRoute.withName(AdsView.routeName));
                },
                child: const Text("Close"))
          ],
        );
      },
    );
  }

  void initVideo() async {
    try {
      String? url = widget.post.videoModel?.mp4Hls;

      print('url : $url');

      if (url != null) {
        _videoController = VideoPlayerController.networkUrl(Uri.parse(url));

        await _videoController.initialize();
        _videoController.play();
        _videoController.setLooping(true);

        setState(() {
          isVideoInitial = true;
        });
      }
    } catch (e) {
      showAlertDialog();
    }
  }

  @override
  void initState() {
    if (widget.post.type == PostType.video) {
      initVideo();
    }
    super.initState();
  }

  @override
  void dispose() {
    if (widget.post.type == PostType.video) {
      _videoController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (_videoController.value.isPlaying) {
          _videoController.pause();
        } else {
          _videoController.play();
        }

        setState(() {});
      },
      child: Stack(
        children: [
          Positioned.fill(
            child: Container(
              color: Colors.black,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: !isVideoInitial
                ? const SizedBox(child: CircularProgressIndicator())
                : AspectRatio(
                    aspectRatio: _videoController.value.aspectRatio,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: VideoPlayer(_videoController),
                    ),
                  ),
          ),
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.all(16),
            child: const SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CusBackButton(color: Colors.white),
                  Text(
                    "Detail",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                        fontWeight: FontWeight.w600),
                  ),
                  InkWell(
                    child: Icon(
                      Icons.more_horiz,
                      color: Colors.white,
                      size: 32,
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 16,
            left: 8,
            right: 80,
            child: AnimatedOpacity(
                opacity: isFullScreen ? 0 : 1,
                duration: const Duration(milliseconds: 300),
                child: AdsInfo(post: widget.post)),
          ),
          Positioned(
            bottom: 16 + MediaQuery.of(context).viewPadding.bottom,
            right: 8,
            child: Column(
              children: [
                AnimatedOpacity(
                  opacity: isFullScreen ? 0 : 1,
                  duration: const Duration(milliseconds: 300),
                  child: AdsActionButton(
                    onPressLike: () {},
                    onPressAds: () {},
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isFullScreen = !isFullScreen;
                    });
                  },
                  child: Container(
                    width: 40,
                    height: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      shape: BoxShape.circle,
                      border: Border.all(width: 3, color: Colors.white),
                    ),
                    child: isFullScreen
                        ? const Icon(
                            Icons.close_fullscreen,
                            color: Colors.white,
                            size: 24,
                          )
                        : const Icon(
                            Icons.open_in_full,
                            color: Colors.white,
                            size: 24,
                          ),
                  ),
                ),
              ],
            ),
          ),
          if (isVideoInitial && !_videoController.value.isPlaying)
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.4),
                    shape: BoxShape.circle),
                child: const Icon(
                  Icons.play_arrow_rounded,
                  color: Colors.white,
                  size: 42,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
