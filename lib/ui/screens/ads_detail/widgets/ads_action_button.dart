import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class AdsActionButton extends StatefulWidget {
  final Function()? onPressLike;
  final Function()? onPressAds;

  const AdsActionButton({super.key, this.onPressLike, this.onPressAds});

  @override
  State<AdsActionButton> createState() => _AdsActionButtonState();
}

class _AdsActionButtonState extends State<AdsActionButton> {
  bool isLike = false;
  int like = 0;
  int view = 199;
  int ads = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            // print("like click");
            if (widget.onPressLike != null) {
              widget.onPressLike!();
              setState(() {
                isLike = !isLike;
              });
            }
          },
          child: Icon(Icons.favorite,
              color: isLike ? Colors.red : Colors.white, size: 40),
        ),
        Text(
          ads.toString(),
          style: const TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
        ),
        const SizedBox(height: Spacing.normal),
        const Icon(Icons.remove_red_eye_sharp, color: Colors.white, size: 40),
        Text(
          ads.toString(),
          style: const TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
        ),
        const SizedBox(height: Spacing.normal),
        InkWell(
          onTap: () {
            // print("ads click");
            if (widget.onPressAds != null) {
              widget.onPressAds!();
            }
            setState(() {
              ads++;
            });
          },
          child: const Icon(Icons.ads_click, color: Colors.white, size: 40),
        ),
        Text(
          ads.toString(),
          style: const TextStyle(
              fontSize: 18, fontWeight: FontWeight.w600, color: Colors.white),
        ),
        const SizedBox(height: Spacing.normal),
      ],
    );
  }
}
