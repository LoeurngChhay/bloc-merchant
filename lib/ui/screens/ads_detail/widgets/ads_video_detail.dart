// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:smooth_video_progress/smooth_video_progress.dart';
// import 'package:video_player/video_player.dart';

// import 'ads_product_detail_widget.dart';
// import 'ads_right_detail_widget.dart';
// import 'ads_text_detail_widget.dart';

// class AdsVideoDetailWidget extends StatefulWidget {
//     final PostEntity? post;
//     // ignore: prefer_const_constructors_in_immutables
//     AdsVideoDetailWidget(this.post, {Key? key}) : super(key: key);

// 	@override
// 	AdsVideoDetailWidgetState createState() => AdsVideoDetailWidgetState();
// }

// class AdsVideoDetailWidgetState extends State<AdsVideoDetailWidget> {
//     VideoPlayerController? playerController;
//     Size fileDimension = Size.zero;
//     bool? isLoading = true;
//     bool? aniVisible = true;
//     bool? isPlaying = false;
//     Size? screenSize;

//     @override
//     void initState() {
//         super.initState();
//         setState(() {
//             isLoading = true;
//             aniVisible = true;
//             isPlaying = false;
//         });

//         try{
//             // ignore: unnecessary_null_comparison
//             if(widget.post!.video_model != null && widget.post!.video_model!.mp4_hls != "") {
//                 playerController = VideoPlayerController.networkUrl(Uri.parse(widget.post!.video_model!.mp4_hls!), videoPlayerOptions: VideoPlayerOptions(allowBackgroundPlayback: true), formatHint: VideoFormat.hls);
//                 playerController?.initialize().then((_) {
//                     fileDimension = playerController?.value.size ?? Size.zero;
//                     playerController?.play();
//                     playerController?.setLooping(true);
//                 });

//                 playerController!.addListener(() {
//                     if(playerController != null){
//                         if (playerController!.value.isPlaying && playerController!.value.isInitialized){
//                             if(mounted) {
//                                 setState(() {
//                                     isLoading = false;
//                                 });
//                             }
//                         }

//                         if (playerController!.value.isPlaying){
//                             if(mounted) {
//                                 setState(() {
//                                     isPlaying = true;
//                                 });
//                             }
//                         }
//                     }
//                 });
//             }else{
//                 // MsgWidget().errMsg("There is not video file to view");
//                 Navigator.pop(context);
//             }
//         } catch (err) {
//             // MsgWidget().errMsg("There is not video file to view");
//             setState(() {
//                 isLoading = true;
//             });
//         }
//     }


//     @override
//     void dispose() {
//         if(playerController != null){
//             playerController?.pause();
//         }
//         if(playerController != null){
//             playerController?.dispose();
//         }
//         super.dispose();
//     }

//     @override
//     Widget build(BuildContext context) {
//         screenSize = MediaQuery.of(context).size;

//         return isLoading == false ? Container(
//             alignment: Alignment.topCenter,
//             color: Colors.black,
//             width: screenSize!.width,
//             height: screenSize!.height,
//             child: Stack(
//                 children: [
//                     Container(
//                         alignment: Alignment.center,
//                         child: InkWell(
//                             onTap: () async {
//                                 if(playerController!.value.isPlaying) {
//                                     playerController?.pause();
//                                 }else{
//                                     playerController?.play();
//                                 }
//                             },  
//                             child: AspectRatio(
//                                 aspectRatio: playerController!.value.aspectRatio,
//                                 child: VideoPlayer(playerController!),
//                             ),
//                         )
//                     ),
//                     Center(
//                         child: isPlaying! ? Container() : Container(
//                             alignment: Alignment.center,
//                             width: 50,
//                             height: 50,
//                             decoration: BoxDecoration(
//                                 color: Colors.white54,
//                                 borderRadius: BorderRadius.circular(60),
//                             ),
//                             child: GestureDetector(
//                                 onTap: () async {
//                                     if(isPlaying!) {
//                                         playerController?.pause();
//                                     }else{
//                                         playerController?.play();
//                                     }
//                                 },  
//                                 child: Container(
//                                     width: 50,
//                                     height: 50,
//                                     alignment: Alignment.center,
//                                     decoration:  BoxDecoration(
//                                         color: Colors.transparent,
//                                         shape: BoxShape.circle,
//                                         border: Border.all(width: 2, color: Colors.white),
//                                     ),
//                                     child: const Icon(
//                                         Icons.play_arrow_outlined,
//                                         color: Colors.white,
//                                     ),
//                                 ),          
//                             ),
//                         )
//                     ),
//                     Positioned(
//                         left: 10,
//                         bottom: 30,
//                         child: AnimatedOpacity(
//                             opacity: aniVisible! ? 1.0 : 0.0,
//                             duration: const Duration(milliseconds: 100),
//                             child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                     AdsProductDetailWidget(widget.post, width: screenSize!.width),
//                                     widget.post!.title != "" ? const SizedBox(height: 10) : Container(),
//                                     widget.post!.title != "" ? AdsTextDetailWidget(widget.post, width: screenSize!.width) : Container()
//                                 ],
//                             ),
//                         ),
//                     ),
//                     Positioned(
//                         right: 20,
//                         bottom: 40,
//                         child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.center,
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: [
//                                 AdsRightDetailWidget(widget.post, aniVisible: aniVisible),
//                                 Container(
//                                     alignment: Alignment.center,
//                                     width: 40,
//                                     height: 40,
//                                     child: GestureDetector(
//                                         onTap: () async {
//                                             setState(() {
//                                                 aniVisible = !aniVisible!;
//                                             });
//                                         },  
//                                         child: Container(
//                                             width: 40,
//                                             height: 40,
//                                             alignment: Alignment.center,
//                                             decoration:  BoxDecoration(
//                                                 color: Colors.white.withOpacity(0.2),
//                                                 shape: BoxShape.circle,
//                                                 border: Border.all(width: 3, color: Colors.white),
//                                             ),
//                                             child: aniVisible! ? const Icon(
//                                                 Icons.close_fullscreen,
//                                                 color: Colors.white,
//                                                 size: 24,
//                                             ) : const Icon(
//                                                 Icons.open_in_full,
//                                                 color: Colors.white,
//                                                 size: 24,
//                                             ),
//                                         ),          
//                                     ),
//                                 )
//                             ],
//                         ),
//                     ),
//                     Positioned(
//                         bottom: 0,
//                         left: 0,
//                         width: screenSize!.width,
//                         child: Container(
//                             alignment: Alignment.center,
//                             height: 30,
//                             child: playerController!.value.isInitialized ? SmoothVideoProgress(
//                                 builder: (context, position, duration, child) => Slider(
//                                     onChangeStart: (_) => playerController!.pause(),
//                                     onChangeEnd: (_) => playerController!.play(),
//                                     onChanged: (value) => playerController!.seekTo(Duration(milliseconds: value.toInt())),
//                                     value: position.inMilliseconds.toDouble(),
//                                     min: 0,
//                                     max: duration.inMilliseconds.toDouble(),
//                                 ), 
//                                 controller: playerController!,
//                             ) : Container(),
//                         )
//                     )
//                     // Positioned(
//                     //     bottom: 30,
//                     //     left: 0,
//                     //     width: screenSize!.width,
//                     //     child: Container(
//                     //         alignment: Alignment.topCenter,
//                     //         height: 30,
//                     //         child: playerController!.value.isInitialized ? VideoProgressIndicator(
//                     //             playerController!,
//                     //             allowScrubbing: true,
//                     //             colors: const VideoProgressColors(
//                     //                 backgroundColor: Colors.white,
//                     //                 playedColor: Color(0xff18BAE8),
//                     //             ),
//                     //             padding: const EdgeInsets.only(top: 5, bottom: 5),
//                     //         ) : Container(),
//                     //     )
//                     // )
//                 ],
//             ),
//         ) : Container(
//             alignment: Alignment.center,
//             color: Colors.black,
//             child: Stack(
//                 children: [
//                     Container(
//                         alignment: Alignment.center,
//                         width: screenSize!.width,
//                         height: screenSize!.height,
//                         child: widget.post!.thumbnail != "" ? CachedNetworkImage(
//                             imageUrl: widget.post!.thumbnail!,
//                             imageBuilder: (context, imageProvider) => Container(
//                                 decoration: BoxDecoration(
//                                     image: DecorationImage(
//                                         image: imageProvider,
//                                         fit: BoxFit.cover,
//                                     )
//                                 ),
//                             ),
//                             placeholder: (context, url) => const CircularProgressIndicator(),
//                             errorWidget: (context, url, error) => Image.asset('assets/images/noimage.png'),
//                             fit: BoxFit.fitHeight,
//                         ) : Image.asset("assets/images/noimage.png"),
//                     ),
//                     Center(
//                         child: Container(
//                             padding: const EdgeInsets.all(6),
//                             decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.circular(4),
//                                 color: Colors.black26
//                             ),
//                             child: const CircularProgressIndicator(),
//                         )
//                     )
//                 ]
//             )
//         );
//     }

// }
