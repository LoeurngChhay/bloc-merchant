import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';

class AdsInfo extends StatefulWidget {
  final AdsItem post;
  const AdsInfo({super.key, required this.post});

  @override
  State<AdsInfo> createState() => _AdsInfoState();
}

class _AdsInfoState extends State<AdsInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.post.product != null)
          Container(
            width: (MediaQuery.of(context).size.width) - 80,
            height: 80,
            padding: const EdgeInsets.all(Spacing.s),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[200]),
            child: Row(
              children: [
                Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          blurRadius: 5,
                          blurStyle: BlurStyle.normal,
                          color: Colors.black.withOpacity(0.3),
                          offset: const Offset(1.5, 1),
                          spreadRadius: 0.5)
                    ]),
                    child: TransparentImage(url: widget.post.product?.thumb)),
                const SizedBox(width: Spacing.s),
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.post.product?.title ?? "",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Text(
                        " \$ ${widget.post.product?.price ?? "0.00"}",
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.red,
                            fontSize: 16),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        if (widget.post.title != null) ...[
          const SizedBox(height: Spacing.m),
          ReadMoreText(
            widget.post.title.toString(),
            trimMode: TrimMode.Line,
            trimLines: 1,
            lessStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
            trimCollapsedText: 'Show more',
            trimExpandedText: 'Show less',
            style: const TextStyle(
                color: Colors.white, fontWeight: FontWeight.w600),
            moreStyle: const TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ],
        SizedBox(height: MediaQuery.of(context).viewPadding.bottom)
      ],
    );
  }
}
