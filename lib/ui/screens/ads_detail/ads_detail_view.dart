import 'package:bloc_merchant_mobile_2/constants/post_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/views/ads_image_detail.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/views/ads_video_detail.dart';
import 'package:flutter/material.dart';

class AdsDetailViewArgument {
  final AdsItem post;

  const AdsDetailViewArgument({required this.post});
}

class AdsDetailView extends StatefulWidget {
  static const String routeName = '/ads-detail';
  final AdsDetailViewArgument? arg;

  const AdsDetailView({super.key, this.arg});

  @override
  State<AdsDetailView> createState() => _AdsDetailViewState();
}

class _AdsDetailViewState extends State<AdsDetailView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: widget.arg?.post.type == PostType.video
            ? AdsVideoDetail(post: widget.arg!.post)
            : AdsImageDetail(post: widget.arg!.post));
  }
}
