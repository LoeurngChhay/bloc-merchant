import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_category_req.dart';
import 'package:bloc_merchant_mobile_2/data/repos/category_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/category/category_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:time_picker_spinner_pop_up/time_picker_spinner_pop_up.dart';

const buttonAction = ButtonType(fullDay: 0, hide: 1, customize: 2);

//
class ButtonType {
  final int fullDay;
  final int hide;
  final int customize;

  const ButtonType(
      {required this.fullDay, required this.hide, required this.customize});
}

class CreateCategoryView extends StatefulWidget {
  final String? id;
  const CreateCategoryView({super.key, this.id});

  @override
  State<CreateCategoryView> createState() => _CreateCategoryViewState();
}

class _CreateCategoryViewState extends State<CreateCategoryView> {
  final _cateTitleController = TextEditingController();
  final _sortController = TextEditingController();
  final _releaseController = TextEditingController();
  final _expiredController = TextEditingController();

  bool isCustomize = false;
  bool isReleaseDate = false;
  bool isExpiredDate = false;

//time picker
  // TimePickerSpinnerController openTimeController =
  //     TimePickerSpinnerController();
  // TimePickerSpinnerController closeTimeController =
  //     TimePickerSpinnerController();
  int selectedButton = buttonAction.fullDay;

  DateTime openTime = DateTime.now();
  DateTime closeTime = DateTime.now();
  DateTime releaseDate = DateTime.now();
  DateTime exipredDate = DateTime.now().add(const Duration(days: 7));

  Future<void> _selectExpiredDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: exipredDate,
        firstDate: DateTime(1960),
        lastDate: DateTime(DateTime.now().year + 10));
    if (picked != null && picked != exipredDate) {
      String dateFormat = DateFormat("dd-MM-yyyy").format(picked);
      setState(() {
        exipredDate = picked;
      });

      _expiredController.text = dateFormat;
    }
  }

  Future<void> _selectReleasedDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: releaseDate,
        firstDate: DateTime(1960),
        lastDate: DateTime(DateTime.now().year + 10));
    if (picked != null && picked != exipredDate) {
      String dateFormat = DateFormat("dd-MM-yyyy").format(picked);
      setState(() {
        releaseDate = picked;
      });

      _releaseController.text = dateFormat;
    }
  }

  void invalidTextFeildDialog(BuildContext context, String alertMessage) {
    showAdaptiveDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Row(
            children: [
              Icon(
                Icons.error,
                color: Colors.red,
              ),
              SizedBox(width: Spacing.xs),
              Text("Error"),
            ],
          ),
          content: Text(alertMessage),
          actions: [
            FilledButton(
                style: FilledButton.styleFrom(backgroundColor: Colors.red),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("Close"))
          ],
        );
      },
    );
  }

  void showDialogConfrom() {
    showDialog(
        context: context,
        builder: (ctxDialog) => AlertDialog(
            contentPadding: EdgeInsets.zero,
            clipBehavior: Clip.antiAlias,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    padding: const EdgeInsets.all(Spacing.s),
                    height: 140,
                    width: double.infinity,
                    color: Colors.blue,
                    child: const Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.check_circle, color: Colors.white, size: 64),
                        SizedBox(height: Spacing.m),
                        Text(
                          "Successful",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        )
                      ],
                    )),
                Container(
                    padding: const EdgeInsets.all(Spacing.normal),
                    child: const Text(
                      'Do you want to create another product?',
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    )),
                Padding(
                  padding: const EdgeInsets.all(Spacing.m),
                  child: Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.pop(ctxDialog, false);
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.grey[300],
                            ),
                            child: const Text(
                              "No",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600),
                            )),
                      ),
                      const SizedBox(width: Spacing.m),
                      Expanded(
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.pop(ctxDialog, true);
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.blue),
                            child: const Text(
                              "Yes",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            )),
                      ),
                    ],
                  ),
                )
              ],
            ))).then((value) {
      if (value is bool) {
        if (value) {
          resetTextField();
        } else {
          Navigator.pop(context, AsyncStatus.success);
        }
      }
    });
  }

  void resetTextField() {
    _cateTitleController.text = '';
    _sortController.text = '50';
  }

  @override
  void initState() {
    _sortController.text = '50';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ChangeNotifierProvider<CategoryViewController>(
      create: widget.id == null
          ? (context) => CategoryViewController(
              categoryRepo: locator<CategoryRepo>(),
              onError: (message) {
                invalidTextFeildDialog(context, message);
              },
              onSuccess: (message) {
                DialogUtil.cusAlertSnackBar(context, message).then((value) {
                  Navigator.pop(context, AsyncStatus.success);
                });
              })
          : (context) => CategoryViewController(
              categoryRepo: locator<CategoryRepo>(),
              setInitValue: (detail) {
                _cateTitleController.text = detail.title ?? "";
                _sortController.text = detail.orderby ?? "";

                selectedButton = int.parse(detail.show_type!);

                if (detail.show_type == buttonAction.customize.toString()) {
                  // openTime = DateFormat.Hm().parse(detail.release_date!);
                  // closeTime = DateFormat.Hm().parse(detail.ltime!);

                  if (detail.is_release == "1") {
                    isReleaseDate = true;
                    _releaseController.text = detail.release_date.toString();
                  }

                  if (detail.expired_date == "1") {
                    isExpiredDate = true;
                    _expiredController.text = detail.expired_date.toString();
                  }
                }
              },
              onError: (message) {
                invalidTextFeildDialog(context, message);
              },
              onSuccess: (message) async {
                DialogUtil.cusAlertSnackBar(context, message).then((value) {
                  Navigator.pop(context, AsyncStatus.success);
                });
              })
            ..getCategoryDetail(widget.id!),
      child: Consumer<CategoryViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            leading: const CusBackButton(color: Colors.white),
            title: Text(
              LocaleKeys.Category.tr(),
              style: const TextStyle(color: Colors.white),
            ),
            centerTitle: true,
            actions: [
              SizedBox(
                height: 35,
                child: FilledButton(
                    onPressed: viewController.loadingAction
                        ? null
                        : () {
                            CreateCategoryRequest req = CreateCategoryRequest();
                            // String sTime = DateFormat.Hm().format(openTime);
                            // String lTime = DateFormat.Hm().format(closeTime);

                            //--------- validate -------------

                            if (_cateTitleController.text == '') {
                              invalidTextFeildDialog(
                                  context, "Please input the category title");
                              return;
                            }
                            if (_sortController.text == '') {
                              invalidTextFeildDialog(
                                  context, "Please input the category sort");
                              return;
                            }

                            req = CreateCategoryRequest(
                              showType: selectedButton,
                              title: _cateTitleController.text,
                              orderby: int.parse(
                                _sortController.text,
                              ),
                            );

                            //-------------------------------

                            // if (selectedButton == buttonAction.customize) {
                            //   req = CreateCategoryRequest(
                            //       title: _cateTitleController.text,
                            //       orderby: int.parse(_sortController.text),
                            //       isRelease: isReleaseDate ? "1" : "0",
                            //       releaseDate: _releaseController.text,
                            //       isExpired: isExpiredDate ? "1" : "0",
                            //       expiredDate: _expiredController.text,
                            //       stime: sTime,
                            //       ltime: lTime,
                            //       showType: selectedButton);
                            // } else {
                            //   print('selectedButton  : $selectedButton');
                            // req = CreateCategoryRequest(
                            //   showType: selectedButton,
                            //   title: _cateTitleController.text,
                            //   orderby: int.parse(
                            //     _sortController.text,
                            //   ),
                            // );
                            // }

                            //---------------request--------

                            if (widget.id != null) {
                              viewController.updateCategory(widget.id!, req);
                            } else {
                              viewController.createCategory(req);
                            }
                            //------------------------------
                          },
                    style: FilledButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(Spacing.xs))),
                    child: Row(
                      children: [
                        if (viewController.loadingAction)
                          Container(
                            height: 14,
                            width: 14,
                            margin: const EdgeInsets.only(right: Spacing.s),
                            child: const CircularProgressIndicator(
                              color: Colors.white,
                              strokeWidth: 2,
                            ),
                          ),
                        Text(
                          LocaleKeys.submit.tr(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
              ),
              const SizedBox(width: Spacing.s)
            ],
          ),
          body: viewController.loading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : GestureDetector(
                  onTap: () {
                    FocusManager.instance.primaryFocus?.unfocus();
                  },
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(Spacing.m),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: Spacing.s),
                            SizedBox(
                              height: 45,
                              child: CusTextField(
                                  controller: _cateTitleController,
                                  labelText: LocaleKeys.category_title.tr(),
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: Spacing.m),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          const BorderSide(color: Colors.blue),
                                      borderRadius:
                                          BorderRadius.circular(Spacing.xs)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: theme.colorScheme.onBackground
                                              .withOpacity(0.5)),
                                      borderRadius:
                                          BorderRadius.circular(Spacing.xs)),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      _cateTitleController.clear();
                                    },
                                    icon: const Icon(Icons.close_rounded),
                                  )),
                            ),
                            const SizedBox(height: Spacing.normal),
                            SizedBox(
                              height: 45,
                              child: CusTextField(
                                controller: _sortController,
                                labelText: LocaleKeys.sort.tr(),
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: Spacing.m),
                                focusedBorder: OutlineInputBorder(
                                    borderSide:
                                        const BorderSide(color: Colors.blue),
                                    borderRadius:
                                        BorderRadius.circular(Spacing.xs)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: theme.colorScheme.onBackground
                                            .withOpacity(0.5))),
                              ),
                            ),
                            const SizedBox(height: Spacing.normal),
                            Row(
                              children: [
                                FilledButton(
                                    style: FilledButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: Spacing.l,
                                            vertical: Spacing.s),
                                        backgroundColor: selectedButton ==
                                                buttonAction.fullDay
                                            ? Colors.blue
                                            : Colors.grey),
                                    onPressed: () {
                                      setState(() {
                                        isCustomize = false;
                                        selectedButton = buttonAction.fullDay;
                                      });
                                    },
                                    child: Text(LocaleKeys.show_full_day.tr())),
                                const SizedBox(width: Spacing.s),
                                FilledButton(
                                    onPressed: () {
                                      setState(() {
                                        selectedButton = buttonAction.hide;
                                      });
                                    },
                                    style: FilledButton.styleFrom(
                                        backgroundColor:
                                            selectedButton == buttonAction.hide
                                                ? Colors.blue
                                                : Colors.grey),
                                    child: Text(LocaleKeys.hide.tr())),
                                // const SizedBox(width: Spacing.s),
                                // FilledButton(
                                //     onPressed: () {
                                //       setState(() {
                                //         isCustomize = true;
                                //         selectedButton = buttonAction.customize;
                                //       });
                                //     },
                                //     style: FilledButton.styleFrom(
                                //         backgroundColor:
                                //             selectedButton == buttonAction.customize
                                //                 ? Colors.blue
                                //                 : Colors.grey),
                                //     child: const Text("Custoimize"))
                              ],
                            ),
                            // Divider(
                            //   color:
                            //       theme.colorScheme.onBackground.withOpacity(0.1),
                            // ),
                            // if (selectedButton == buttonAction.customize) ...[
                            //   Text(
                            //     "Customize Text",
                            //     style: Theme.of(context)
                            //         .textTheme
                            //         .titleMedium
                            //         ?.copyWith(fontWeight: FontWeight.bold),
                            //   ),
                            //   const SizedBox(height: Spacing.normal),
                            //   Row(
                            //     children: [
                            //       const Text(
                            //         "Open:",
                            //         style: TextStyle(
                            //             color: Colors.blue,
                            //             fontWeight: FontWeight.bold),
                            //       ),
                            //       const SizedBox(width: 10),
                            //       Expanded(
                            //         child: TimePickerSpinnerPopUp(
                            //           mode: CupertinoDatePickerMode.time,
                            //           initTime: openTime,
                            //           barrierColor: Colors
                            //               .black12, //Barrier Color when pop up show
                            //           onChange: (dateTime) {
                            //             // Implement your logic with select dateTime
                            //             setState(() {
                            //               openTime = dateTime;
                            //             });
                            //           },
                            //           controller: openTimeController,
                            //         ),
                            //       ),
                            //       const SizedBox(width: 10),
                            //       const Text("Close: ",
                            //           style: TextStyle(
                            //               fontWeight: FontWeight.bold,
                            //               color: Colors.blue)),
                            //       const SizedBox(width: 10),
                            //       Expanded(
                            //         child: TimePickerSpinnerPopUp(
                            //           mode: CupertinoDatePickerMode.time,

                            //           initTime: closeTime,
                            //           barrierColor: Colors
                            //               .black12, //Barrier Color when pop up show
                            //           onChange: (dateTime) {
                            //             setState(() {
                            //               closeTime = dateTime;
                            //             });
                            //           },
                            //           controller: closeTimeController,
                            //         ),
                            //       )
                            //     ],
                            //   ),
                            //   const SizedBox(height: Spacing.normal),
                            //   Text(
                            //     "Optional Setting (Schedule Show)",
                            //     style: theme.textTheme.titleMedium
                            //         ?.copyWith(fontWeight: FontWeight.bold),
                            //   ),
                            //   const SizedBox(height: Spacing.normal),
                            //   Row(
                            //     children: [
                            //       GestureDetector(
                            //           onTap: () {
                            //             setState(() {
                            //               isReleaseDate = !isReleaseDate;
                            //             });

                            //             if (isReleaseDate) {
                            //               _releaseController.text =
                            //                   DateFormat("dd-MM-yyyy")
                            //                       .format(releaseDate);
                            //             } else {
                            //               _releaseController.text = "";
                            //             }
                            //           },
                            //           child: isReleaseDate
                            //               ? const Icon(Icons.check_box_rounded,
                            //                   color: Colors.blue)
                            //               : const Icon(
                            //                   Icons.check_box_outline_blank)),
                            //       const SizedBox(width: Spacing.m),
                            //       const Text("Release Date"),
                            //       const SizedBox(width: Spacing.m),
                            //       Expanded(
                            //           child: SizedBox(
                            //         height: 42,
                            //         child: CusTextField(
                            //           controller: _releaseController,
                            //           readOnly: true,
                            //           onTap: isReleaseDate
                            //               ? () {
                            //                   _selectReleasedDate(context);
                            //                 }
                            //               : null,
                            //           contentPadding: const EdgeInsets.symmetric(
                            //               horizontal: 10),
                            //           focusedBorder: OutlineInputBorder(
                            //               borderRadius: BorderRadius.circular(4)),
                            //           enabledBorder: OutlineInputBorder(
                            //               borderSide: BorderSide(
                            //                   color: isReleaseDate
                            //                       ? theme.colorScheme.onBackground
                            //                       : theme.colorScheme.onBackground
                            //                           .withOpacity(0.4)),
                            //               borderRadius: BorderRadius.circular(4)),
                            //           hintText: "Select Date",
                            //           suffixIcon: Icon(
                            //             Icons.date_range,
                            //             color: theme.colorScheme.onBackground
                            //                 .withOpacity(0.4),
                            //           ),
                            //         ),
                            //       ))
                            //     ],
                            //   ),
                            //   const SizedBox(height: Spacing.normal),
                            //   Row(
                            //     children: [
                            //       GestureDetector(
                            //           onTap: () {
                            //             setState(() {
                            //               isExpiredDate = !isExpiredDate;
                            //             });

                            //             if (isExpiredDate) {
                            //               _expiredController.text =
                            //                   DateFormat("dd-MM-yyyy")
                            //                       .format(exipredDate);
                            //             } else {
                            //               _expiredController.text = "";
                            //             }
                            //           },
                            //           child: isExpiredDate
                            //               ? const Icon(Icons.check_box_rounded,
                            //                   color: Colors.blue)
                            //               : const Icon(
                            //                   Icons.check_box_outline_blank)),
                            //       const SizedBox(width: Spacing.m),
                            //       const Text("Expired Date"),
                            //       const SizedBox(width: Spacing.m),
                            //       Expanded(
                            //           child: SizedBox(
                            //         height: 42,
                            //         child: CusTextField(
                            //           controller: _expiredController,
                            //           readOnly: true,
                            //           onTap: isExpiredDate
                            //               ? () {
                            //                   _selectExpiredDate(context);
                            //                 }
                            //               : null,
                            //           contentPadding: const EdgeInsets.symmetric(
                            //               horizontal: 10),
                            //           focusedBorder: OutlineInputBorder(
                            //               borderRadius: BorderRadius.circular(4)),
                            //           enabledBorder: OutlineInputBorder(
                            //               borderSide: BorderSide(
                            //                   color: isExpiredDate
                            //                       ? theme.colorScheme.onBackground
                            //                       : theme.colorScheme.onBackground
                            //                           .withOpacity(0.4)),
                            //               borderRadius: BorderRadius.circular(4)),
                            //           hintText: "Select Date",
                            //           suffixIcon: Icon(
                            //             Icons.date_range,
                            //             color: theme.colorScheme.onBackground
                            //                 .withOpacity(0.4),
                            //           ),
                            //         ),
                            //       ))
                            //     ],
                            //   ),
                            // ]
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        );
      }),
    );
  }
}
