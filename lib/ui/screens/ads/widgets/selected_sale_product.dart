import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SelectedSaleProduct extends StatelessWidget {
  final Product product;
  const SelectedSaleProduct({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      minLeadingWidth: 40,
      contentPadding: const EdgeInsets.symmetric(horizontal: Spacing.s),
      leading: TransparentImage(
        url: product.thumb,
      ),
      title: Text(
        "ID: ${product.product_id}",
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        product.title.toString(),
      ),
    );
  }
}
