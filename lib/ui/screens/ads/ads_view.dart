import 'dart:io';

import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads/ads_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads_detail/ads_detail_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/image/create_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/views/video_post_preview.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class AdsView extends StatefulWidget {
  static const String routeName = '/ads';
  const AdsView({super.key});

  @override
  State<AdsView> createState() => _AdsViewState();
}

class _AdsViewState extends State<AdsView> {
  double headerHeight = 270;
  bool headerWhite = false;
  bool get wantKeepAlive => true;
  EasyRefreshController refreshController = EasyRefreshController(
    controlFinishRefresh: false,
    controlFinishLoad: false,
  );
  Size? screenSize;
  bool isLoading = false;
  final ImagePicker _picker = ImagePicker();
  int pageNum = 0;
  List<String> imagesPath = [];

  Future<void> pickUpPost(AdsViewController viewController) async {
    showModalBottomSheet(
      context: context,
      clipBehavior: Clip.antiAlias,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.3,
          padding: const EdgeInsets.all(Spacing.m),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 60,
                  padding: const EdgeInsets.all(Spacing.xs),
                  decoration: BoxDecoration(
                      color: Theme.of(context)
                          .colorScheme
                          .onBackground
                          .withOpacity(0.2),
                      shape: BoxShape.circle),
                  child: const Icon(Icons.close_rounded, size: 32.0),
                ),
              ),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox.fromSize(
                            size: const Size(72, 72),
                            child: ClipOval(
                                child: Material(
                                    color: Colors.black.withOpacity(0.8),
                                    child: InkWell(
                                      splashColor: Colors.black,
                                      onTap: () async {
                                        Navigator.of(context).pop();

                                        photoPickup(viewController);
                                      },
                                      child: const Icon(Icons.photo,
                                          size: 40, color: Colors.white),
                                    )))),
                        const SizedBox(height: 5),
                        const Text("Photo",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold))
                      ],
                    ),
                  ),
                  const SizedBox(width: 20),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox.fromSize(
                            size: const Size(72, 72),
                            child: ClipOval(
                                child: Material(
                                    color: Colors.black.withOpacity(0.8),
                                    child: InkWell(
                                      splashColor: Colors.black,
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        videoPickup(viewController);
                                      },
                                      child: const Icon(Icons.video_library,
                                          size: 40, color: Colors.white),
                                    )))),
                        const SizedBox(height: 5),
                        const Text("Video",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold))
                      ],
                    ),
                  ),
                ],
              )),
              const SizedBox(height: 40)
            ],
          ),
        );
      },
    );
  }

  Future<void> photoPickup(AdsViewController viewController) async {
    try {
      //remove limit 5
      final List<XFile> images = await _picker.pickMultiImage();

      if (images.isNotEmpty) {
        setState(() {
          imagesPath = images.map((e) => e.path).toList();
        });

        final result = await Navigator.pushNamed(
            context, CreatePostView.routeName,
            arguments: CreatePostArgument(images: imagesPath) as AsyncStatus?);

        if (result == AsyncStatus.success) {
          viewController.getAdsList();
        }
      }
    } catch (e) {
      print('Error Multipel Image : $e');
    }
  }

  Future<void> videoPickup(AdsViewController viewController) async {
    try {
      final video = await _picker.pickVideo(source: ImageSource.gallery);

      if (video != null) {
        File file = File(video.path);
        final result = await Navigator.pushNamed(
            context, VideoPostPreview.routeName,
            arguments: CreatePostArgument(images: [], video: file));

        print('result : $result');

        if (result is AsyncStatus) {
          AsyncStatus status = result;

          if (status == AsyncStatus.success) {
            viewController.getAdsList();
          }
        }
      }
    } on Exception catch (e) {
      print("Error picker file : $e");
    }
  }

  void showAlertDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog.adaptive(
          content: const Text("Something wrong with post"),
          actions: [
            FilledButton(
                style: FilledButton.styleFrom(backgroundColor: Colors.red),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("Cancel"))
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) =>
          AdsViewController(adsRepo: locator<AdsRepo>())..getAdsList(),
      child: Consumer<AdsViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          body: NestedScrollView(
              physics: viewController.postList.isEmpty
                  ? const NeverScrollableScrollPhysics()
                  : const AlwaysScrollableScrollPhysics(),
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [
                  SliverAppBar(
                    pinned: true,
                    stretch: true,
                    expandedHeight: headerHeight,
                    elevation: 0,
                    backgroundColor: headerWhite ? Colors.black : Colors.black,
                    snap: false,
                    leading:
                        BackButton(onPressed: () => {Navigator.pop(context)}),
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background:
                          buildAppBarBackground(context, viewController),
                    ),
                  )
                ];
              },
              body: EasyRefresh(
                controller: refreshController,
                onRefresh: () async {
                  viewController.getAdsList(refresh: true);
                },
                child: viewController.loading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : viewController.postList.isEmpty
                        ? EmptyScreen(
                            title: 'Empty Post',
                            onRefresh: () {
                              viewController.getAdsList();
                            },
                          )
                        : GridView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            itemCount: viewController.postList.length,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    mainAxisSpacing: 5,
                                    crossAxisSpacing: 5,
                                    crossAxisCount: 3),
                            itemBuilder: (context, index) {
                              final post = viewController.postList[index];

                              return Container(
                                constraints:
                                    const BoxConstraints(minHeight: 120),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  border:
                                      Border.all(width: 1, color: Colors.grey),
                                ),
                                child: GestureDetector(
                                  onTap: () async {
                                    if (post != null) {
                                      Navigator.pushNamed(
                                          context, AdsDetailView.routeName,
                                          arguments: AdsDetailViewArgument(
                                              post: post));
                                    } else {
                                      showAlertDialog();
                                    }
                                  },
                                  child: Stack(
                                    children: [
                                      Container(
                                        child: post?.thumbnail != ""
                                            ? CachedNetworkImage(
                                                imageUrl:
                                                    post!.thumbnail.toString(),
                                                imageBuilder:
                                                    (context, imageProvider) =>
                                                        Container(
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.cover,
                                                  )),
                                                ),
                                                placeholder: (context, url) =>
                                                    const Center(
                                                        child: SizedBox(
                                                            width: 24,
                                                            height: 24,
                                                            child:
                                                                CircularProgressIndicator())),
                                                errorWidget: (context, url,
                                                        error) =>
                                                    Image.asset(
                                                        'assets/images/no_img.jpeg'),
                                                fit: BoxFit.fitHeight,
                                              )
                                            : Image.asset(
                                                "assets/images/no_img.jpeg"),
                                      ),
                                      post?.type == "video"
                                          ? Positioned.fill(
                                              child: Align(
                                                alignment: Alignment.center,
                                                child: Container(
                                                  width: 32,
                                                  height: 32,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: Colors.transparent,
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        width: 2,
                                                        color: Colors.white),
                                                  ),
                                                  child: const Icon(
                                                    Icons.play_arrow_rounded,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
              )),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton:
              // socialer != null
              FloatingActionButton.extended(
            backgroundColor: Colors.blue.shade300,
            foregroundColor: Colors.white,
            onPressed: () {
              // Respond to button press
              pickUpPost(viewController);
            },
            icon: const Icon(Icons.add),
            label: const Text('POST',
                style: TextStyle(fontWeight: FontWeight.bold)),
          ),
        );
      }),
    );
  }

  Widget buildAppBarBackground(
      BuildContext context, AdsViewController viewController) {
    final user = context.read<AuthProvider>().shop;
    final socialInfo = viewController.socialInfo;
    return SafeArea(
      child: Container(
          color: Colors.blue.shade300,
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(top: 40),
                    child: Container(
                        width: 100,
                        height: 100,
                        clipBehavior: Clip.antiAlias,
                        decoration: const BoxDecoration(
                          // color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        padding: const EdgeInsets.all(Spacing.xs),
                        child: CircleAvatar(
                          child: user != null && user.logo != ''
                              ? ClipOval(
                                  child: CachedNetworkImage(
                                    imageUrl: user.logo.toString(),
                                    placeholder: (context, url) =>
                                        const CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Image.asset(
                                            'assets/images/no_img.jpeg'),
                                    fit: BoxFit.cover,
                                    height: 100,
                                    width: 100,
                                  ),
                                )
                              : ClipOval(
                                  child: Image.asset(
                                    "assets/images/no_img.jpeg",
                                    fit: BoxFit.contain,
                                    height: 100,
                                    width: 100,
                                  ),
                                ),
                        ))),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: Container()),
                    Container(
                        alignment: Alignment.center,
                        height: 60,
                        width: 88,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                                viewController.loading
                                    ? "..."
                                    : socialInfo!.total_follow.toString(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 26,
                                    color: Colors.white)),
                            const Text("FOLLOWERS",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Colors.white)),
                          ],
                        )),
                    Container(
                        alignment: Alignment.center,
                        height: 60,
                        width: 88,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                                viewController.loading
                                    ? "..."
                                    : socialInfo!.total_view.toString(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 26,
                                    color: Colors.white)),
                            const Text("VIEWS",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Colors.white)),
                          ],
                        )),
                    Container(
                        alignment: Alignment.center,
                        height: 60,
                        width: 88,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                                viewController.loading
                                    ? "..."
                                    : socialInfo!.total_like.toString(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 26,
                                    color: Colors.white)),
                            const Text("LIKES",
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    color: Colors.white)),
                          ],
                        )),
                    Expanded(child: Container()),
                  ],
                ),
                const SizedBox(height: Spacing.normal),
              ])),
    );
  }
}
