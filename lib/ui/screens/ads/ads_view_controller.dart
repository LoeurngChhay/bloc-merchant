import 'package:bloc_merchant_mobile_2/data/models/responses/social_info_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/social_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_exception.dart';
import 'package:flutter/material.dart';

class AdsViewController extends ChangeNotifier {
  final AdsRepo adsRepo;

  AdsViewController({required this.adsRepo});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  SocialInfo? _socialInfo;
  SocialInfo? get socialInfo => _socialInfo;
  set socialInfo(SocialInfo? newValue) {
    _socialInfo = newValue;
    notifyListeners();
  }

  List<AdsItem?> _postList = [];
  List<AdsItem?> get postList => _postList;
  set postList(List<AdsItem?> getValue) {
    _postList = getValue;
    notifyListeners();
  }

  CustomException? _errorCate;
  CustomException? get errorCate => _errorCate;
  set errorCate(CustomException? newValue) {
    _errorCate = newValue;
    notifyListeners();
  }

  Future<String> getSocailInfo({bool? refresh = false}) async {
    if (!refresh!) {
      loading = true;
    }

    try {
      final response = await adsRepo.getSocailInfo();
      socialInfo = response.data;

      return response.data!.social_uid.toString();
    } catch (e) {
      print("error get social info : $e");
      loading = false;
    }

    return '';
  }

  Future<void> getAdsList({bool? refresh = false}) async {
    String socialUid = await getSocailInfo(refresh: refresh);

    if (socialUid != '') {
      try {
        final res = await adsRepo.getAdsList(socialId: socialUid);
        print('res getAdsList : $res');
        postList = res.data?.items ?? [];
      } catch (e) {
        print("erro get post list : $e");
      } finally {
        loading = false;
      }
    }
  }
}
