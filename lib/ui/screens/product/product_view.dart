import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/constants/product_status.dart';
import 'package:bloc_merchant_mobile_2/data/repos/product_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/category/category_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/product_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/widgets/catgory_list_card.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/widgets/product_list_tile.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductViewArgument {
  final bool? hasSelected;

  ProductViewArgument({this.hasSelected = false});
}

class ProductView extends StatefulWidget {
  static const String routeName = '/product';
  final ProductViewArgument? args;

  const ProductView({super.key, this.args});

  @override
  State<ProductView> createState() => _ProductViewState();
}

class _ProductViewState extends State<ProductView> {
  final ScrollController? _categoriesController = ScrollController();
  final _scrollController = ScrollController();
  ProductViewController? _productViewController;

  final _searchController = TextEditingController();
  int onSelectedIndex = 0;
  String cateId = "0";
  bool onSale = false;
  int pageNum = 1;

  void initialController() {
    final nextPage = 0.9 * _scrollController.position.maxScrollExtent;

    if (_scrollController.position.pixels > nextPage) {
      _productViewController?.getProductList(cateId: cateId);
    }
  }

  @override
  void initState() {
    _scrollController.addListener(initialController);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _categoriesController?.dispose();
    _scrollController.dispose();
    _scrollController.removeListener(initialController);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProductViewController>(
      create: (context) => ProductViewController(
        productRepo: locator<ProductRepo>(),
        onSuccess: (message) {
          DialogUtil.cusAlertSnackBar(context, message);
        },
        onError: (message) {
          DialogUtil.cusAlertSnackBar(context, message,
                  isError: true, milliseconds: 10000)
              .then((value) => Navigator.pop(context, AsyncStatus.success));
        },
      )..initMethod(),
      child: Consumer<ProductViewController>(
          builder: (context, viewController, child) {
        _productViewController = viewController;

        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            leading: const CusBackButton(color: Colors.white),
            title: Text(
              LocaleKeys.products.tr(),
              style: TextStyle(color: Colors.white),
            ),
            actions: [
              GestureDetector(
                onTap: () async {
                  final statusRes = await Navigator.pushNamed(
                      context, CreateProductView.routeName) as AsyncStatus?;

                  if (statusRes != null && statusRes == AsyncStatus.success) {
                    viewController.getProductList(isRefresh: true);

                    setState(() {
                      onSelectedIndex = 0;
                      _categoriesController!.animateTo(0,
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.easeInOut);
                    });
                  }
                },
                child: Container(
                  height: 30,
                  width: 30,
                  margin: const EdgeInsets.only(right: Spacing.normal),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border.all(width: 1, color: Colors.white)),
                  child: const Icon(Icons.add, color: Colors.white),
                ),
              )
            ],
          ),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(Spacing.s),
                  decoration: const BoxDecoration(
                    color: Colors.black,
                  ),
                  width: double.infinity,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              height: 45,
                              child: CusTextField(
                                hintText: "${LocaleKeys.search.tr()}...",
                                controller: _searchController,
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: Spacing.normal, vertical: 0),
                                backgroundColors: Colors.white,
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.circular(Spacing.s)),
                              ),
                            ),
                          ),
                          const SizedBox(width: Spacing.s),
                          SizedBox(
                            height: 45,
                            child: FilledButton(
                              onPressed: () {
                                viewController.getProductList(
                                    search: _searchController.text,
                                    reset: true);
                              },
                              style: FilledButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(Spacing.s))),
                              child: Text(
                                LocaleKeys.search.tr(),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: Spacing.s),
                      SizedBox(
                        height: 45,
                        child: Row(
                          children: [
                            CusIconButton(
                              height: 40,
                              width: 40,
                              onPressed: () async {
                                await Navigator.pushNamed(
                                    context, CategoryView.routeName);

                                viewController.getProductCategoryList();
                              },
                              icon: const Icon(
                                Icons.list,
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.all(Spacing.s),
                              style: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(Spacing.xs),
                                  border: Border.all(
                                      color: Colors.white.withOpacity(0.6))),
                            ),
                            const SizedBox(width: Spacing.m),
                            Expanded(
                                child: SizedBox(
                              height: 40,
                              child: ListView.separated(
                                scrollDirection: Axis.horizontal,
                                controller: _categoriesController,
                                itemCount: viewController.cateList.length + 1,
                                itemBuilder: (context, index) {
                                  bool isSelected = onSelectedIndex == index;

                                  return CategoryListCard(
                                    title: index == 0
                                        ? LocaleKeys.all.tr()
                                        : viewController
                                            .cateList[index - 1]!.title
                                            .toString(),
                                    textColor: isSelected ? Colors.white : null,
                                    color: isSelected
                                        ? Colors.orange
                                        : Colors.white,
                                    fontWeight:
                                        isSelected ? FontWeight.bold : null,
                                    onPressed: () {
                                      setState(() {
                                        onSelectedIndex = index;
                                        cateId = index == 0
                                            ? "0"
                                            : viewController
                                                    .cateList[index - 1]!
                                                    .cate_id ??
                                                "0";
                                      });

                                      viewController.getProductList(
                                          cateId: cateId, reset: true);
                                    },
                                  );
                                },
                                separatorBuilder: (context, index) =>
                                    const SizedBox(width: Spacing.s),
                              ),
                            ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Colors.grey.shade300,
                  padding: const EdgeInsets.all(Spacing.s),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        child: Text(LocaleKeys.image.tr(),
                            style:
                                const TextStyle(fontWeight: FontWeight.bold)),
                      ),
                      const SizedBox(width: Spacing.normal),
                      Text(
                          '${LocaleKeys.i_title.tr()} (${LocaleKeys.p_price.tr()} & ${LocaleKeys.format.tr()})',
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                      const Spacer(),
                      Text(LocaleKeys.action.tr(),
                          style: const TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                Expanded(
                  child: viewController.loadingInitial
                      ? Container(
                          alignment: Alignment.center,
                          margin:
                              const EdgeInsets.symmetric(vertical: Spacing.s),
                          child: const CircularProgressIndicator(),
                        )
                      : EasyRefresh(
                          clipBehavior: Clip.none,
                          // resetAfterRefresh: true,
                          onRefresh: () async {
                            //onRefresh

                            viewController.getProductList(
                                isRefresh: true, cateId: cateId);
                          },
                          fit: StackFit.expand,
                          child: viewController.productList.isEmpty
                              ? EmptyScreen(
                                  onRefresh: () {
                                    viewController.getProductList(
                                        cateId: cateId, isRefresh: true);
                                  },
                                )
                              : ListView.builder(
                                  controller: _scrollController,
                                  itemCount:
                                      viewController.productList.length + 1,
                                  itemBuilder: (context, index) {
                                    if (index ==
                                        viewController.productList.length) {
                                      if (viewController.loadingMore) {
                                        return const Center(
                                            child: CircularProgressIndicator());
                                      } else {
                                        return const SizedBox();
                                      }
                                    }
                                    return ProductListTile(
                                      onSelected: widget.args != null &&
                                              widget.args!.hasSelected!
                                          ? () {
                                              final product = viewController
                                                  .productList[index];

                                              Navigator.pop(context, product);
                                            }
                                          : null,
                                      loading: viewController.loadingDelete,
                                      product:
                                          viewController.productList[index],
                                      onChangeStatus: (value) {
                                        String productId = viewController
                                            .productList[index].product_id
                                            .toString();
                                        viewController.changeProductStatus(
                                          productId: productId,
                                          showType: value
                                              ? ProductStatus.show
                                              : ProductStatus.hide,
                                        );
                                      },
                                      onDelete: viewController.loadingDelete
                                          ? null
                                          : () {
                                              // viewController.
                                              String? productId = viewController
                                                  .productList[index]
                                                  .product_id;

                                              if (productId != null) {
                                                DialogUtil.cusConfirmDialog(
                                                  context: context,
                                                  title:
                                                      'Do you want to delete?',
                                                  colors: Colors.red,
                                                  icon: Icons.delete,
                                                  onPressed: () async {
                                                    await viewController
                                                        .deleteProduct(
                                                            productId)
                                                        .then((value) {
                                                      Navigator.pop(context);
                                                      Navigator.pop(context);
                                                    });
                                                  },
                                                );
                                              }
                                            },
                                      onEdit: () async {
                                        final status =
                                            await Navigator.pushNamed(context,
                                                CreateProductView.routeName,
                                                arguments:
                                                    CreateProductViewArguments(
                                                        productId:
                                                            viewController
                                                                .productList[
                                                                    index]
                                                                .product_id));

                                        if (status is AsyncStatus &&
                                            status == AsyncStatus.success) {
                                          setState(() {
                                            viewController.getProductList(
                                                isRefresh: true,
                                                cateId: cateId);
                                          });
                                        }
                                      },
                                    );
                                  }),
                        ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
