import 'dart:async';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_cate_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/product_repo.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_exception.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class ProductViewController extends ChangeNotifier {
  final Function(String message)? onSuccess;
  final Function(String message)? onError;
  final ProductRepo productRepo;

  ProductViewController(
      {required this.productRepo, this.onSuccess, this.onError});

  int _page = 1;
  int get page => _page;
  set page(int value) {
    _page = value;
    notifyListeners();
  }

  void initMethod() {
    getProductCategoryList();
    getProductList();
  }

  bool _hasMore = true;
  bool get hasMore => _hasMore;
  set hasMore(bool newValue) {
    _hasMore = newValue;
    notifyListeners();
  }

  bool get loadingMore => _page > 1 && loadingPage;
  bool get loadingInitial => _loadingPage && _page == 1 && _productList.isEmpty;

  bool _loadingPage = true;
  bool get loadingPage => _loadingPage;
  set loadingPage(bool newValue) {
    _loadingPage = newValue;
    notifyListeners();
  }

  Exception? _error;
  Exception? get error => _error;
  set error(Exception? newValue) {
    _error = newValue;
    notifyListeners();
  }

  bool _loadingCate = false;
  bool get loadingCate => _loadingCate;
  set loadingCate(bool newValue) {
    _loadingCate = newValue;
    notifyListeners();
  }

  bool _loadingDelete = false;
  bool get loadingDelete => _loadingDelete;
  set loadingDelete(bool newValue) {
    _loadingDelete = newValue;
    notifyListeners();
  }

  List<Product> _productList = [];
  List<Product> get productList => _productList;
  set productList(List<Product> newList) {
    _productList = newList;
    notifyListeners();
  }

  CancelToken? _cancelToken;

  List<ProCategory?> _cateList = [];
  List<ProCategory?> get cateList => _cateList;
  set cateList(List<ProCategory?> newList) {
    _cateList = newList;
    notifyListeners();
  }

  void resetPagination() {
    page = 1;
    hasMore = true;
  }

  // void getProductByCate(String cateId) async {
  //   productList.clear();
  //   getProductList(cateId: cateId, isRefresh: true);
  // }

  Future<void> getProductList(
      {bool isRefresh = false,
      String? cateId,
      String? search,
      bool reset = false}) async {
    if (isRefresh) {
      resetPagination();
    }

    if (reset) {
      productList.clear();
      resetPagination();
    }

    if (!hasMore || loadingMore) return;

    try {
      loadingPage = true;

      if (_cancelToken != null && !_cancelToken!.isCancelled) {
        _cancelToken?.cancel();
      }

      _cancelToken = CancelToken();

      final reponse = await productRepo.getProductList(
          search: search,
          page: _page.toString(),
          cateId: cateId,
          cancelToken: _cancelToken);

      final items = reponse.data!.items;
      hasMore = items!.isNotEmpty;

      if (_page == 1) {
        productList = items;
      } else {
        productList.addAll(items);
      }
      _page++;
      loadingPage = false;
    } catch (e) {
      print('error product : $e ');
      if (e is DioException) {
        if (e.type == DioExceptionType.cancel) {
          print('Error Product cancel token : ${e.type}');
          return;
        }
        CustomException(e.message.toString());
      }
      loadingPage = false;
    }
  }

  Future<void> getProductCategoryList() async {
    loadingCate = true;

    try {
      final response = await productRepo.getProductCategoryList();

      cateList = response.data?.items ?? [];
    } catch (e) {
      // print('error product cat: $e ');
      error = CustomException(e.toString());
    } finally {
      loadingCate = false;
    }
  }

  Future<void> changeProductStatus(
      {String? productId, String? showType}) async {
    try {
      await productRepo.changeProductStatus(
          productId: productId, showType: showType);
    } catch (e) {
      print('Error changeStatues : $e');
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    }
  }

  Future<void> deleteProduct(String id) async {
    loadingDelete = true;
    try {
      final response = await productRepo.deleteProduct(id);

      String? deletedId = response.data;

      if (deletedId != null) {
        productList.removeWhere((e) => e.product_id == deletedId);
        notifyListeners();

        if (onSuccess != null) {
          onSuccess!(response.message);
        }
      }
    } catch (e) {
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingDelete = false;
    }
  }
}
