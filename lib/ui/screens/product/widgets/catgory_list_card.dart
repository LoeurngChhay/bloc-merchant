import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class CategoryListCard extends StatelessWidget {
  final Color? color;
  final Color? textColor;
  final String title;
  final FontWeight? fontWeight;
  final Function()? onPressed;

  const CategoryListCard({
    super.key,
    this.color,
    this.textColor,
    this.onPressed,
    required this.title,
    this.fontWeight,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: Spacing.m),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(Spacing.s),
        ),
        child: Text(
          title,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: textColor ?? Colors.black, fontWeight: fontWeight),
        ),
      ),
    );
  }
}
