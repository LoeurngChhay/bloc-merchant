import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:popover/popover.dart';

class ProductListTile extends StatefulWidget {
  final Product product;
  final bool? loading;
  final Function()? onSelected;
  final Function()? onEdit;
  final Function()? onDelete;
  final Function(bool)? onChangeStatus;

  const ProductListTile({
    super.key,
    required this.product,
    this.loading = false,
    this.onSelected,
    this.onEdit,
    this.onDelete,
    this.onChangeStatus,
  });
  @override
  State<ProductListTile> createState() => _ProductListTileState();
}

class _ProductListTileState extends State<ProductListTile> {
  bool onSale = false;

  initOnSale() {
    setState(() {
      onSale = widget.product.is_onsale == "1";
    });
  }

  @override
  void didUpdateWidget(covariant ProductListTile oldWidget) {
    if (oldWidget.product != widget.product) {
      initOnSale();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    initOnSale();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: Spacing.s),
      leading: SizedBox(
          height: 55,
          width: 55,
          child: TransparentImage(url: widget.product.thumb)),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  "ID: ${widget.product.product_id}",
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.xs),
                decoration: BoxDecoration(
                    color: onSale
                        ? Colors.green.withOpacity(0.1)
                        : Colors.red.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(Spacing.xs),
                    border:
                        Border.all(color: onSale ? Colors.green : Colors.red)),
                child: Text(
                  onSale ? "On Sale" : "On Close",
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: onSale ? Colors.green.shade600 : Colors.red),
                ),
              ),
              const SizedBox(width: Spacing.s),
              SizedBox(
                height: 40,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Switch(
                      value: onSale,
                      activeColor: Colors.green,
                      inactiveThumbColor: Colors.white,
                      inactiveTrackColor: Colors.red,
                      trackOutlineColor: MaterialStateProperty.resolveWith(
                          (Set<MaterialState> states) {
                        const Set<MaterialState> interactiveStates =
                            <MaterialState>{
                          MaterialState.pressed,
                          MaterialState.hovered,
                          MaterialState.focused,
                        };
                        if (states.any(interactiveStates.contains)) {
                          return Colors.blue;
                        }
                        return Colors.transparent;
                      }),
                      onChanged: (bool value) {
                        // This is called when the user toggles the switch.
                        setState(() {
                          onSale = value;
                        });

                        if (widget.onChangeStatus != null) {
                          widget.onChangeStatus!(value);
                        }
                      }),
                ),
              ),
            ],
          ),
          Text(widget.product.title.toString())
        ],
      ),
      subtitle: Row(
        children: [
          const SizedBox(height: 25, child: VerticalDivider(width: Spacing.xs)),
          const SizedBox(width: Spacing.s),
          RichText(
            text: TextSpan(
                style: const TextStyle(color: Colors.black),
                children: [
                  TextSpan(
                      text: '\$ ${widget.product.price}',
                      style: const TextStyle(
                          color: Colors.red, fontWeight: FontWeight.bold)),
                  const TextSpan(text: ' / '),
                  TextSpan(
                      text: widget.product.unit,
                      style: TextStyle(color: Colors.amber.shade700)),
                  const TextSpan(text: ' | '),
                  TextSpan(text: '${LocaleKeys.p_qty.tr()}: '),
                  TextSpan(
                      text: widget.product.sale_sku,
                      style: const TextStyle(color: Colors.blue)),
                ]),
          ),
        ],
      ),
      trailing: widget.onSelected != null
          ? FilledButton(
              style: FilledButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: Spacing.xs)),
              onPressed: widget.onSelected,
              child: Text(
                "Select",
                style: Theme.of(context).textTheme.bodySmall?.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ))
          : CusIconButton(
              onPressed: () {
                showPopover(
                  context: context,
                  bodyBuilder: (ctxPopover) => Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(ctxPopover);

                          if (widget.onEdit != null) {
                            widget.onEdit!();
                          }
                        },
                        child: Container(
                          height: 48,
                          color: Colors.amber,
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          alignment: Alignment.center,
                          child: Text(
                            LocaleKeys.edit.tr(),
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: widget.onDelete,
                          child: Container(
                            height: 48,
                            width: 70,
                            color: Colors.red,
                            alignment: Alignment.center,
                            child: widget.loading!
                                ? const SizedBox(
                                    height: 16,
                                    width: 16,
                                    child: CircularProgressIndicator(),
                                  )
                                : Text(
                                    LocaleKeys.delete.tr(),
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  arrowHeight: 0,
                  arrowWidth: 0,
                  direction: PopoverDirection.top,
                  transition: PopoverTransition.scale,
                  barrierColor: Colors.transparent,
                  arrowDyOffset: 60,
                  arrowDxOffset: 120,
                  width: 150,
                  height: 48,
                );
              },
              style:
                  BoxDecoration(border: Border.all(color: Colors.transparent)),
              icon: const Icon(Icons.more_vert),
            ),
    );
  }
}
