import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_bluetooth_view/printer_bluetooth_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_network_view.dart/printer_network_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_svg.dart';

class PrinterView extends StatefulWidget {
  static const routeName = '/printer';
  const PrinterView({super.key});

  @override
  State<PrinterView> createState() => _PrinterViewState();
}

class _PrinterViewState extends State<PrinterView> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    double printerCardSizeMobile = 155;

    return Scaffold(
      appBar: AppBar(title: Text(LocaleKeys.printer_view.tr())),
      body: SingleChildScrollView(
        primary: false,
        physics: const AlwaysScrollableScrollPhysics(),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.75,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: printerCardSizeMobile,
                    height: printerCardSizeMobile,
                    child: ElevatedButton(
                      onPressed: () {
                        // showCupertinoDialog(
                        //     context: context,
                        //     builder: (context) => const PrinterNetworkView());

                        Navigator.pushNamed(
                            context, PrinterNetworkView.routeName);
                      },
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          padding: const EdgeInsets.all(Spacing.s),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(Spacing.l1)),
                          backgroundColor: theme.colorScheme.background,
                          surfaceTintColor: theme.colorScheme.background),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              width: double.infinity,
                              alignment: Alignment.centerRight,
                              child: const Icon(Icons.wifi)),
                          const CusSvg(
                            pathName: 'assets/svgs/menu/print.svg',
                            size: 80,
                          ),
                          const SizedBox(height: Spacing.s),
                          Text(
                            LocaleKeys.wifi_printer.tr(),
                            overflow: TextOverflow.ellipsis,
                            style: theme.textTheme.titleMedium,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(width: Spacing.normal),
                  SizedBox(
                    width: printerCardSizeMobile,
                    height: printerCardSizeMobile,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(
                            context, PrinterBluetoothView.routeName);
                      },
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          padding: const EdgeInsets.all(Spacing.s),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(Spacing.l1)),
                          backgroundColor: theme.colorScheme.background,
                          surfaceTintColor: theme.colorScheme.background),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              width: double.infinity,
                              alignment: Alignment.centerRight,
                              child: const Icon(Icons.bluetooth)),
                          const CusSvg(
                            pathName: 'assets/svgs/menu/print.svg',
                            size: 80,
                          ),
                          const SizedBox(height: Spacing.s),
                          Text(
                            LocaleKeys.bluetooth_printer.tr(),
                            overflow: TextOverflow.ellipsis,
                            style: theme.textTheme.titleMedium,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
