import 'package:bloc_merchant_mobile_2/data/repos/home_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/home_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/grid_menu.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/home_header.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeView extends StatefulWidget {
  static const String routeName = "/home";
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  double appBarMaxHeight = 0.0;
  double appBarSize = 180;
  bool openShop = false;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ChangeNotifierProvider<HomeViewController>(
      create: (context) => HomeViewController(
        homeRepo: locator<HomeRepo>(),
      )..getHomeInfo(),
      child: Consumer<HomeViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: PreferredSize(
              preferredSize:
                  Size.fromHeight(280 + MediaQuery.of(context).viewPadding.top),
              child: HomeHeader(
                status: viewController.homeInfo?.operation_status,
              )),
          body: RefreshIndicator(
            onRefresh: () async {
              viewController.getHomeInfo();
            },
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                children: [
                  const SizedBox(height: 32),
                  const GridMenu(),
                  Container(
                    padding: const EdgeInsets.all(Spacing.m),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      LocaleKeys.your_activities.tr(),
                      style: theme.textTheme.titleLarge
                          ?.copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
