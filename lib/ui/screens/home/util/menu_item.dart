import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads/ads_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/setting/setting_view.dart';
import 'package:flutter/material.dart';

final menuItems = [
  Menu(
      name: LocaleKeys.order,
      icon: Icons.assignment_sharp,
      routeName: OrderView.routeName),
  Menu(
      name: LocaleKeys.products,
      icon: Icons.menu_book,
      routeName: ProductView.routeName),
  // Menu(name: LocaleKeys.reports.tr(), icon: Icons.signal_cellular_alt, routeName: ''),
  // Menu(name: LocaleKeys.promotion.tr(), icon: Icons.discount_outlined, routeName: ''),
  // Menu(
  //     name: LocaleKeys.rewards.tr(),
  //     icon: Icons.workspace_premium_outlined,
  //     routeName: ''),
  Menu(
      name: LocaleKeys.ads,
      icon: Icons.campaign_outlined,
      routeName: AdsView.routeName),
  // Menu(
  //     name: LocaleKeys.comment.tr(),
  //     icon: Icons.campaign_outlined,
  //     routeName: ProductTestView.routeName),
  Menu(
      name: LocaleKeys.setting,
      icon: Icons.miscellaneous_services_outlined,
      routeName: SettingView.routeName),
  Menu(
      name: LocaleKeys.logout, icon: Icons.logout_rounded, routeName: 'logout'),
];

class Menu {
  final String name;
  final IconData icon;
  final String routeName;

  Menu({
    required this.name,
    required this.icon,
    required this.routeName,
  });
}
