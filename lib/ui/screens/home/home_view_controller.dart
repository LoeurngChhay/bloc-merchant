import 'package:bloc_merchant_mobile_2/constants/status_operation.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/home_info_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/home_repo.dart';
import 'package:flutter/material.dart';

class HomeViewController extends ChangeNotifier {
  final HomeRepo homeRepo;
  final Function()? onError;
  final Function()? onSucces;

  HomeViewController({required this.homeRepo, this.onError, this.onSucces});

  bool _isOpen = true;
  bool get isOpen => _isOpen;
  set isOpen(bool newValue) {
    _isOpen = newValue;
    notifyListeners();
  }

  bool _loadingOperation = false;
  bool get loadingOperation => _loadingOperation;
  set loadingOperation(bool newValue) {
    _loadingOperation = newValue;
    notifyListeners();
  }

  Future<void> openOperation() async {
    loadingOperation = true;
    try {
      final response = await homeRepo.openOperation();
      isOpen = response.data == StatusOperation.open;

      if (onSucces != null) {
        onSucces!();
      }
    } catch (e) {
      print('error open operation : $e');
      if (onError != null) {
        onError!();
      }
    } finally {
      loadingOperation = false;
    }
  }

  Future<void> closeOperation({String? status, String? date}) async {
    loadingOperation = true;
    try {
      final response =
          await homeRepo.closeOperation(status: status, date: date);
      isOpen = response.data == StatusOperation.open;

      if (onSucces != null) {
        onSucces!();
      }
    } catch (e) {
      print('error close operation : $e');
      if (onError != null) {
        onError!();
      }
    } finally {
      loadingOperation = false;
    }
  }

  bool _loadingHomeInfo = false;
  bool get loadingHomeInfo => _loadingHomeInfo;
  set loadingHomeInfo(bool newValue) {
    _loadingHomeInfo = newValue;
    notifyListeners();
  }

  HomeInfo? _homeInfo;
  HomeInfo? get homeInfo => _homeInfo;
  set homeInfo(HomeInfo? newValue) {
    _homeInfo = newValue;
    notifyListeners();
  }

  Future<void> getHomeInfo() async {
    loadingHomeInfo = true;

    try {
      final response = await homeRepo.getHomeInfo();
      homeInfo = response.data;
    } catch (e) {
      print("error home info : $e");
    } finally {
      loadingHomeInfo = false;
    }
  }
}
