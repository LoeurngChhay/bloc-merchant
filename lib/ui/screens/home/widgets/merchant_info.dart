import 'package:bloc_merchant_mobile_2/data/models/responses/home_info_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class MerchantInfo extends StatelessWidget {
  final HomeInfo? homeInfo;

  const MerchantInfo({
    super.key,
    this.homeInfo,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(Spacing.normal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${LocaleKeys.welcome.tr()},",
                        style: theme.textTheme.titleMedium
                            ?.copyWith(color: Colors.white)),
                    Text(homeInfo == null ? "..." : homeInfo!.title.toString(),
                        style: theme.textTheme.titleLarge
                            ?.copyWith(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              homeInfo != null
                  ? CircleAvatar(
                      radius: 72 / 2,
                      foregroundImage: NetworkImage(homeInfo!.logo!))
                  : const CircleAvatar(
                      radius: 72 / 2,
                      foregroundImage: AssetImage('assets/images/no_img.jpeg')),
            ],
          ),
          const SizedBox(height: Spacing.s),
          Text(
            homeInfo == null ? "..." : homeInfo!.addr.toString(),
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
            style: theme.textTheme.titleMedium
                ?.copyWith(fontSize: 12, color: theme.colorScheme.background),
          )
        ],
      ),
    );
  }
}
