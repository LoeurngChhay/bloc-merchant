import 'package:bloc_merchant_mobile_2/constants/status_operation.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class DialogCloseOperation extends StatefulWidget {
  const DialogCloseOperation({super.key});

  @override
  State<DialogCloseOperation> createState() => _DialogCloseOperationState();
}

class _DialogCloseOperationState extends State<DialogCloseOperation> {
  int selectedType = 0;

  String openDate = DateFormat('dd-MM-yyyy')
      .format(DateTime.now().add(const Duration(days: 1)));

  void openDatePicker() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.width * 0.9,
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: SfDateRangePicker(
                // onSelectionChanged: _onSelectionChanged,
                selectionMode: DateRangePickerSelectionMode.single,
                showActionButtons: true,
                initialSelectedDate: DateTime.now(),
                onCancel: () => Navigator.pop(context),
                onSubmit: (date) {
                  date as DateTime;
                  final formatDate = DateFormat('dd-MM-yyyy').format(date);
                  openDate = formatDate.toString();
                  setState(() {});

                  Navigator.pop(context);
                }),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    bool isOnDate = selectedType == CloseOperationType.openDate;
    return AlertDialog(
      contentPadding: const EdgeInsets.all(Spacing.m),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Spacing.m)),
      content: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Reason for closing?",
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.amber[600]),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Icon(Icons.close_rounded, size: 22),
                )
              ],
            ),
            const SizedBox(height: Spacing.normal),
            Text(
              'Type of Closing Operation',
              style: theme.textTheme.titleMedium
                  ?.copyWith(fontWeight: FontWeight.bold, color: Colors.blue),
            ),
            const SizedBox(height: Spacing.m),
            Row(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedType = CloseOperationType.openTmr;
                    });
                  },
                  child: Container(
                    width: 110,
                    height: 35,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      color: selectedType == CloseOperationType.openTmr
                          ? Colors.blue.withOpacity(0.5)
                          : theme.colorScheme.onBackground.withOpacity(0.05),
                      borderRadius: BorderRadius.circular(Spacing.xs),
                      shape: BoxShape.rectangle,
                      border: Border.all(
                          width: 1,
                          color:
                              theme.colorScheme.onBackground.withOpacity(0.5)),
                    ),
                    child: TextButton.icon(
                      onPressed: null,
                      style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: Spacing.xs)),
                      icon: Icon(
                        selectedType == CloseOperationType.openTmr
                            ? Icons.check_box_outlined
                            : Icons.check_box_outline_blank,
                        size: 20,
                        color: theme.colorScheme.onBackground.withOpacity(0.5),
                      ),
                      label: Text(
                        "Open Back",
                        style: theme.textTheme.bodySmall?.copyWith(
                            color: theme.colorScheme.onBackground
                                .withOpacity(0.8)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: Spacing.s),
                const Expanded(flex: 2, child: Text("Same Time tomorrow")),
              ],
            ),
            const SizedBox(height: Spacing.s),
            Row(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedType = CloseOperationType.openDate;
                    });
                  },
                  child: Container(
                    width: 110,
                    height: 35,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      color: isOnDate
                          ? Colors.blue.withOpacity(0.5)
                          : theme.colorScheme.onBackground.withOpacity(0.05),
                      borderRadius: BorderRadius.circular(Spacing.xs),
                      shape: BoxShape.rectangle,
                      border: Border.all(
                          width: 1,
                          color:
                              theme.colorScheme.onBackground.withOpacity(0.5)),
                    ),
                    child: TextButton.icon(
                      onPressed: null,
                      style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: Spacing.xs)),
                      icon: Icon(
                        isOnDate
                            ? Icons.check_box_outlined
                            : Icons.check_box_outline_blank,
                        size: 20,
                        color: theme.colorScheme.onBackground.withOpacity(0.5),
                      ),
                      label: Text(
                        "Open on",
                        style: theme.textTheme.bodySmall?.copyWith(
                            color: theme.colorScheme.onBackground
                                .withOpacity(0.8)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: Spacing.s),
                Expanded(
                  child: GestureDetector(
                    onTap: isOnDate ? openDatePicker : null,
                    child: Container(
                      height: 40,
                      padding:
                          const EdgeInsets.symmetric(horizontal: Spacing.s),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        border: Border.all(
                            width: 1,
                            color:
                                Colors.black.withOpacity(isOnDate ? 0.6 : 0.3)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            openDate,
                            style: TextStyle(
                                color: isOnDate
                                    ? Colors.black
                                    : Colors.black.withOpacity(0.3)),
                          ),
                          Icon(Icons.calendar_month,
                              color: isOnDate
                                  ? Colors.black
                                  : Colors.black.withOpacity(0.3))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: Spacing.s),
            Row(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      selectedType = CloseOperationType.closeTemp;
                    });
                  },
                  child: Container(
                    width: 110,
                    height: 35,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      color: selectedType == CloseOperationType.closeTemp
                          ? Colors.blue.withOpacity(0.5)
                          : theme.colorScheme.onBackground.withOpacity(0.05),
                      borderRadius: BorderRadius.circular(Spacing.xs),
                      border: Border.all(
                          width: 1,
                          color:
                              theme.colorScheme.onBackground.withOpacity(0.5)),
                      shape: BoxShape.rectangle,
                    ),
                    child: TextButton.icon(
                      onPressed: null,
                      style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: Spacing.xs)),
                      icon: Icon(
                        selectedType == CloseOperationType.closeTemp
                            ? Icons.check_box_outlined
                            : Icons.check_box_outline_blank,
                        size: 20,
                        color: theme.colorScheme.onBackground.withOpacity(0.5),
                      ),
                      label: Text(
                        "Close",
                        style: theme.textTheme.bodySmall?.copyWith(
                            color: theme.colorScheme.onBackground
                                .withOpacity(0.8)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: Spacing.s),
                const Expanded(child: Text("Temporary")),
              ],
            ),
          ],
        ),
      ),
      actions: [
        FilledButton.icon(
            onPressed: () {
              Navigator.pop(context);
            },
            style: FilledButton.styleFrom(
                backgroundColor: Colors.grey[400],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Spacing.xs))),
            icon: const Icon(
              Icons.close,
              color: Colors.white,
            ),
            label: const Text("Cancel")),
        FilledButton.icon(
            onPressed: () {
              Navigator.pop(context, {
                'status': selectedType,
                'date': openDate,
              });
            },
            style: FilledButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Spacing.xs))),
            icon: const Icon(
              Icons.check,
              color: Colors.white,
            ),
            label: const Text(
              "Confirm",
              style: TextStyle(fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}
