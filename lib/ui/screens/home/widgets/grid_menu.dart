import 'package:bloc_merchant_mobile_2/ui/screens/home/util/menu_item.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/logout_dialog.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/colors.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class GridMenu extends StatefulWidget {
  final Function()? onLogout;
  const GridMenu({super.key, this.onLogout});

  @override
  State<GridMenu> createState() => _GridMenuState();
}

class _GridMenuState extends State<GridMenu> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.symmetric(horizontal: Spacing.normal),
        itemCount: menuItems.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            mainAxisSpacing: Spacing.m,
            crossAxisSpacing: Spacing.m,
            crossAxisCount: 3),
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              if (menuItems[index].routeName == "logout") {
                showDialog(
                  context: context,
                  builder: (context) => const LogoutDialog(),
                );
              } else {
                // Navigator.of(context).pushNamed(menuItems[index].routeName);
                Navigator.pushNamed(context, menuItems[index].routeName);
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(menuItems[index].icon,
                    color: menuItems[index].routeName == "logout"
                        ? redPrimary
                        : theme.colorScheme.onBackground,
                    size: 46),
                const SizedBox(height: Spacing.s),
                Text(
                  menuItems[index].name.tr(),
                  textAlign: TextAlign.center,
                  style: theme.textTheme.titleMedium,
                )
              ],
            ),
          );
        });
  }
}
