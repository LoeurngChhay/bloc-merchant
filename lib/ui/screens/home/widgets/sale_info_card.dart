import 'package:bloc_merchant_mobile_2/data/models/responses/home_info_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_svg.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class SalesInfoCard extends StatefulWidget {
  final HomeInfo? homeInfo;
  const SalesInfoCard({super.key, this.homeInfo});

  @override
  State<SalesInfoCard> createState() => _SalesInfoCardState();
}

class _SalesInfoCardState extends State<SalesInfoCard> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      // height: 60,
      margin: const EdgeInsets.all(Spacing.xs),
      padding: const EdgeInsets.all(6),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Spacing.s),
          gradient: LinearGradient(colors: [
            theme.colorScheme.primary.withOpacity(0.9),
            theme.colorScheme.primary.withOpacity(0.5),
          ])),
      child: Row(
        children: [
          const CusSvg(
            pathName: 'assets/svgs/wallet.svg',
            color: Colors.white,
            size: 49,
          ),
          const SizedBox(width: Spacing.xs),
          Expanded(
              child: Column(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  LocaleKeys.today_sales.tr(),
                  style: theme.textTheme.bodyLarge?.copyWith(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
                Text(
                  LocaleKeys.yesterday.tr(),
                  style: theme.textTheme.titleMedium
                      ?.copyWith(color: Colors.white),
                ),
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  "\$${widget.homeInfo == null ? 0 : widget.homeInfo?.today_sale}",
                  style: theme.textTheme.bodyLarge?.copyWith(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
                Text(
                  "\$${widget.homeInfo == null ? 0 : widget.homeInfo?.yesterday_sale}",
                  style: theme.textTheme.titleMedium
                      ?.copyWith(color: Colors.white),
                ),
              ]),
            ],
          ))
        ],
      ),
    );
  }
}
