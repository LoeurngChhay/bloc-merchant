import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/login/login_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/colors.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class LogoutDialog extends StatefulWidget {
  const LogoutDialog({super.key});

  @override
  State<LogoutDialog> createState() => _LogoutDialogState();
}

class _LogoutDialogState extends State<LogoutDialog> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Dialog(
        clipBehavior: Clip.hardEdge,
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.3,
          height: 265,
          child: Column(children: [
            Container(
                width: double.infinity,
                height: 100,
                color: theme.colorScheme.primary,
                child: const Icon(
                  Icons.check_circle_outline_rounded,
                  size: 72,
                  color: Colors.white,
                )),
            const SizedBox(height: Spacing.l),
            Text(LocaleKeys.are_you_sure.tr(),
                style: theme.textTheme.titleLarge
                    ?.copyWith(fontWeight: FontWeight.bold)),
            Text(LocaleKeys.do_you_want_to_logout.tr(),
                style: theme.textTheme.titleMedium),
            const SizedBox(height: Spacing.l),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: Spacing.normal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(padding: null),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          LocaleKeys.cancel.tr(),
                        )),
                  ),
                  const SizedBox(width: Spacing.l),
                  Expanded(
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: redPrimary),
                        onPressed: loading
                            ? () {}
                            : () {
                                setState(() {
                                  loading = true;
                                });

                                context
                                    .read<AuthProvider>()
                                    .logout()
                                    .then((value) {
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      LoginView.routeName, (route) => false);

                                  setState(() {
                                    loading = false;
                                  });
                                });
                              },
                        child: loading
                            ? const SizedBox(
                                height: 18,
                                width: 18,
                                child: CircularProgressIndicator(
                                    color: Colors.white),
                              )
                            : Text(
                                LocaleKeys.logout.tr(),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )),
                  ),
                ],
              ),
            )
          ]),
        ));
  }
}
