import 'package:bloc_merchant_mobile_2/constants/status_operation.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/home_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/dialog_close_operation.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/merchant_info.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/widgets/sale_info_card.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/colors.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class HomeHeader extends StatefulWidget {
  final String? status;
  const HomeHeader({super.key, this.status});

  @override
  State<HomeHeader> createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {
  bool openShop = false;

  void showCloseDialog(HomeViewController viewController) {
    showDialog(
      context: context,
      builder: (context) {
        return const DialogCloseOperation();
      },
    ).then((value) {
      if (value != null) {
        String status = value['status'].toString();
        String? date = value['date'];

        setState(() {
          openShop = false;
          viewController.closeOperation(status: status, date: date);
        });
      }
    });
  }

  @override
  void didUpdateWidget(covariant HomeHeader oldWidget) {
    if (widget.status != oldWidget.status) {
      if (widget.status != null) {
        setState(() {
          openShop = widget.status == StatusOperation.open;
        });
      }
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Consumer<HomeViewController>(
        builder: (context, viewController, child) {
      return SizedBox(
        height: 280 + MediaQuery.of(context).viewPadding.top,
        child: Stack(
          children: [
            Container(
              height: 200 + MediaQuery.of(context).viewPadding.top,
              width: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: openShop
                          ? [
                              theme.colorScheme.primary.withOpacity(0.9),
                              theme.colorScheme.primary.withOpacity(0.1),
                            ]
                          : [
                              redPrimary.withOpacity(0.9),
                              redPrimary.withOpacity(0.1),
                            ])),
              child: SafeArea(
                child: viewController.loadingHomeInfo
                    ? const MerchantInfo()
                    : MerchantInfo(homeInfo: viewController.homeInfo),
              ),
            ),
            Positioned(
                bottom: 0,
                left: Spacing.normal,
                right: Spacing.normal,
                child: Container(
                  width: double.infinity,
                  height: 130,
                  decoration: BoxDecoration(
                    color: theme.colorScheme.background,
                    borderRadius: BorderRadius.circular(Spacing.m),
                    boxShadow: [
                      BoxShadow(
                        color: theme.colorScheme.onBackground.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 8.0,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      )
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(Spacing.s),
                        child: Row(
                          children: [
                            Text(
                              LocaleKeys.shop_operation.tr(),
                              style: theme.textTheme.titleMedium,
                            ),
                            const SizedBox(width: Spacing.xs),
                            Text(
                              openShop
                                  ? '(${LocaleKeys.shop_status_open.tr()})'
                                  : '(${LocaleKeys.shop_status_close.tr()})',
                              style: theme.textTheme.titleMedium?.copyWith(
                                  fontWeight: FontWeight.bold,
                                  color: openShop ? Colors.green : Colors.red),
                            ),
                            const Spacer(),
                            viewController.loadingOperation ||
                                    viewController.loadingHomeInfo
                                ? Container(
                                    height: 40,
                                    width: 40,
                                    padding: const EdgeInsets.all(8),
                                    child: const CircularProgressIndicator())
                                : SizedBox(
                                    height: 40,
                                    child: Switch(
                                        value: openShop,
                                        activeColor: Colors.green,
                                        inactiveThumbColor: Colors.white,
                                        inactiveTrackColor: Colors.red,
                                        trackOutlineColor:
                                            MaterialStateProperty.resolveWith(
                                                (Set<MaterialState> states) {
                                          const Set<MaterialState>
                                              interactiveStates =
                                              <MaterialState>{
                                            MaterialState.pressed,
                                            MaterialState.hovered,
                                            MaterialState.focused,
                                          };
                                          if (states.any(
                                              interactiveStates.contains)) {
                                            return Colors.blue;
                                          }
                                          return Colors.transparent;
                                        }),
                                        onChanged: (bool value) {
                                          // print('$value openShop : $openShop');
                                          if (value) {
                                            viewController.openOperation();
                                            setState(() {
                                              openShop = value;
                                            });
                                          } else {
                                            showCloseDialog(viewController);
                                          }
                                        }),
                                  ),
                          ],
                        ),
                      ),
                      const Spacer(),
                      SalesInfoCard(
                        homeInfo: viewController.homeInfo,
                      )
                    ],
                  ),
                ))
          ],
        ),
      );
    });
  }
}
