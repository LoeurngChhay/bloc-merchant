import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/login_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/utils/esc_pos_network.dart';
import 'package:bloc_merchant_mobile_2/utils/printer_utils.dart';
import 'package:bloc_merchant_mobile_2/utils/storage_key.dart';
import 'package:dio/dio.dart';
import 'package:esc_pos_utils_updated/esc_pos_utils_updated.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:image/image.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrinterNetworkViewController extends ChangeNotifier {
  final PrinterProvider printerProvider;
  final Function()? onSuccess;
  final Function(String)? onError;
  final Function(String ip)? avaibleIPAddress;

  PrinterNetworkViewController({
    required this.printerProvider,
    this.onSuccess,
    this.onError,
    this.avaibleIPAddress,
  });

  final _preferences = SharedPreferences.getInstance();

  IPAddress? _storeIPAddress;
  IPAddress? get storeIPAddress => _storeIPAddress;
  set storeIPAddress(IPAddress? newVal) {
    _storeIPAddress = newVal;
    printerProvider.printIpAddress = newVal;

    notifyListeners();
  }

  bool _loadingScan = false;
  bool get loadingScan => _loadingScan;
  set loadingScan(bool newValue) {
    _loadingScan = newValue;
    notifyListeners();
  }

  Future<void> connectToPrinter(
      {IPAddress? ipAddress, OrderDetail? orderDetail}) async {
    ipAddress ??= printerProvider.printIpAddress;

    final pref = await _preferences;

    if (ipAddress != null) {
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      try {
        loadingScan = true;

        final PosPrintResult res =
            await printer.connect(ipAddress.ipAddress, port: ipAddress.port);

        if (res == PosPrintResult.success) {
          final printerAddress =
              IPAddress(ipAddress: ipAddress.ipAddress, port: ipAddress.port);
          pref.setString(StorageKeys.PRINT_IP, printerAddress.toString());

          if (orderDetail != null) {
            printReceipt(orderDetail, printer: printer);

            await Future.delayed(const Duration(seconds: 1), () {
              printer.disconnect();
            });
          } else {
            onSuccess!();
            storeIPAddress = ipAddress;
          }
        }

        if (res == PosPrintResult.timeout) {
          throw res.msg;
        }

        print('Print result: ${res.msg}');
      } catch (e) {
        print("connect unSuccessfully : $e");

        if (e is DioException) {
          onError!(e.message.toString());
        } else {
          onError!(e.toString());
        }
      } finally {
        loadingScan = false;
      }
    }
  }

  Future<void> startPrint(
    OrderDetail orderDetail, {
    IPAddress? ipAddress,
    String? orderId,
    bool forShop = false,
    Shop? shop,
    String? orderTime,
  }) async {
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);

    try {
      await printReceipt(
        orderId: orderId,
        time: orderTime,
        forShop: forShop,
        printer: printer,
        orderDetail,
        shop: shop,
      );
    } catch (e) {
      print('Error printer : $e');
    }
  }

  // void printReceipt(NetworkPrinter printer, {Uint8List? capturedImage}) {
  //   if (capturedImage != null) {
  //     print("Print image: $capturedImage");
  //     final image = decodeImage(capturedImage);
  //     printer.image(image!, align: PosAlign.center);
  //   }

  //   printer.feed(2);
  //   printer.cut();
  // }

  void init() {
    if (printerProvider.printIpAddress != null) {
      avaibleIPAddress!(printerProvider.printIpAddress!.ipAddress);
    }
  }

  Future<void> testPrint() async {
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);

    final item = await PrinterUtils.createImageName('តេស្តបោះពុម្ព',
        width: 400, height: 90);

    Image? imageKm = decodeImage(item!);

    printer.image(imageKm!);
    printer.text('Print Test',
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));
    printer.text("列印測試",
        containsChinese: true,
        styles: const PosStyles(
          align: PosAlign.center,
        ));

    printer.cut();
  }

  Future<void> printReceipt(
    OrderDetail? orderDetail, {
    required NetworkPrinter printer,
    String? orderId,
    String? time,
    bool forShop = false,
    Shop? shop,
  }) async {
    String shopTitle = shop?.title ?? "";

    final shopImg =
        await PrinterUtils.createImageName(shopTitle, width: 400, height: 60);

    Image? shopName = decodeImage(shopImg!);
    if (Platform.isIOS) {
      final resizedImage = copyResize(shopName!,
          width: 300, height: 60, interpolation: Interpolation.nearest);
      final bytesimg = Uint8List.fromList(encodeJpg(resizedImage));
      shopName = decodeImage(bytesimg);
    }

    printer.image(shopName!);

    printer.text("BLOC #${shop?.shop_id}");
    printer.row([
      PosColumn(
          text: "Order: #$orderId",
          width: 12,
          styles: const PosStyles.defaults())
    ]);
    printer.text("Time: $time", styles: const PosStyles.defaults());
    printer.hr();
    if (orderDetail != null) {
      for (var item in orderDetail.products!) {
        Uint8List? covertToImg =
            await PrinterUtils.createImageProduct(width: 350, item: item);

        Image? product = decodeImage(covertToImg!);
        if (Platform.isIOS) {
          final resizedImage = copyResize(product!,
              width: 372, height: 100, interpolation: Interpolation.nearest);
          final bytesimg = Uint8List.fromList(encodeJpg(resizedImage));
          product = decodeImage(bytesimg);
        }

        printer.image(product!);
      }
    }

    printer.feed(2);
    printer.hr();

    double subtotal = double.parse(orderDetail?.product_price ?? "0");
    double discount = (orderDetail?.discount_amount ?? 0).toDouble();
    double parkageFee = double.parse(orderDetail?.package_price ?? "0");
    double vatAmount = (orderDetail?.vat_amount ?? 0).toDouble();
    // double deliveryFee = item!.delivery_fee!;
    // double bagFee = item!.bag_price!;
    double total = ((subtotal + parkageFee) - discount) + vatAmount;

    //------ sub total
    printer.row([
      PosColumn(
          text: 'SUB-TOTAL:', width: 8, styles: const PosStyles.defaults()),
      PosColumn(
          text: "${subtotal.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ package fee
    printer.row([
      PosColumn(
          text: 'Packaging Fee:', width: 8, styles: const PosStyles.defaults()),
      PosColumn(
          text: "${parkageFee.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ Discount
    printer.row([
      PosColumn(text: 'Discount:', width: 8),
      PosColumn(
          text: "${orderDetail?.discount_amount?.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ vat
    printer.row([
      PosColumn(text: 'Vat:', width: 8),
      PosColumn(
          text: "${orderDetail?.vat_amount?.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    //------ vat
    printer.row([
      PosColumn(text: 'Total:', width: 8),
      PosColumn(
          text:
              "${forShop ? orderDetail?.shop_receive?.toStringAsFixed(2) : total.toStringAsFixed(2)} \$",
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
            width: PosTextSize.size1,
            height: PosTextSize.size1,
          )),
    ]);
    printer.feed(2);
    printer.text("TEL: 016 | 017 421 333",
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));
    printer.text("** Thank you for using BLOC**",
        styles: const PosStyles(
          align: PosAlign.center,
          width: PosTextSize.size1,
          height: PosTextSize.size1,
        ));

    printer.cut();
  }
}
