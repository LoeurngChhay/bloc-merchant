import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_network_view.dart/printer_network_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:screenshot/screenshot.dart';

class PrinterNetworkView extends StatefulWidget {
  static const String routeName = '/printer-network';
  // final List<CartItem>? cart;
  // final PreCheckout? preCheckout;
  // const PrinterNetworkView({super.key, this.cart, this.preCheckout});
  const PrinterNetworkView({super.key});

  @override
  State<PrinterNetworkView> createState() => _PrinterNetworkViewState();
}

class _PrinterNetworkViewState extends State<PrinterNetworkView>
    with TickerProviderStateMixin {
  final _ipAddressController = TextEditingController();
  final _portController = TextEditingController();
  ScreenshotController screenshotController = ScreenshotController();
  late final AnimationController _controller;

  // Uint8List? _imageFile;
  String base64Image = '';
  String? ipAddress;
  final info = NetworkInfo();

  final textStyle = const TextStyle(
      fontSize: 10, color: Colors.black, fontFamily: "Khmer OS BattamBang");

  void getIPAddress() async {
    final wifiIP = await info.getWifiIP();

    setState(() {
      ipAddress = wifiIP;
    });
  }

  void getInitIpAddress() {
    final printerProvider = context.read<PrinterProvider>();
    if (printerProvider.printIpAddress != null) {
      _ipAddressController.text = printerProvider.printIpAddress!.ipAddress;
      setState(() {});
    }
  }

  @override
  void initState() {
    _portController.text = '9100';
    _controller = AnimationController(vsync: this);
    getIPAddress();
    getInitIpAddress();

    super.initState();
  }

  DateTime now = DateTime.now();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ChangeNotifierProvider(
      create: (context) => PrinterNetworkViewController(
          printerProvider: context.read<PrinterProvider>(),
          onSuccess: () => showDialog(
                context: context,
                builder: (context) => const AlertDialog(
                  title: Text('Connection Success'),
                  content: Icon(
                    Icons.check_circle_outline_rounded,
                    size: 100,
                    color: Colors.green,
                  ),
                ),
              ).then((value) {
                Navigator.pop(context);
              }),
          onError: (message) => showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text('Connection Fail'),
                  content: Text(message),
                ),
              ),
          avaibleIPAddress: ((ipAddress) {
            return _ipAddressController.text = ipAddress;
          }))
        ..init(),
      child: Consumer<PrinterNetworkViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(LocaleKeys.wifi_printer.tr()),
            actions: [
              InkWell(
                onTap: () {
                  if (viewController.storeIPAddress != null) {
                    viewController.testPrint();
                  }
                },
                child: const Icon(Icons.print),
              ),
              const SizedBox(width: Spacing.normal),
            ],
          ),
          body: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.65,
              margin: const EdgeInsets.only(bottom: Spacing.s),
              padding: const EdgeInsets.all(Spacing.normal),
              child: Column(
                children: [
                  if (ipAddress != null)
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.all(8),
                        child: Row(
                          children: [
                            const Text('My Ip Address: ',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black)),
                            const SizedBox(width: Spacing.xs),
                            Text(ipAddress!,
                                style: const TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black)),
                            const Spacer(),
                            FilledButton(
                                onPressed: () {
                                  if (ipAddress != null) {
                                    setState(() {
                                      _ipAddressController.text = ipAddress!;
                                    });
                                  }
                                },
                                child: Text(LocaleKeys.fill.tr()))
                          ],
                        )),
                  Text(
                    LocaleKeys.add_printer_by_ip_address.tr(),
                    style: theme.textTheme.titleLarge
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: Spacing.l),
                  TextField(
                    controller: _ipAddressController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      constraints: const BoxConstraints(maxHeight: 45),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 20),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(Spacing.sm)),
                      hintText: "${LocaleKeys.please_input.tr()} IP",
                      hintStyle: TextStyle(
                        color: theme.colorScheme.onBackground.withOpacity(0.6),
                      ),
                    ),
                  ),
                  const SizedBox(height: Spacing.s),
                  TextField(
                    controller: _portController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        constraints: const BoxConstraints(maxHeight: 45),
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 20),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(Spacing.sm)),
                        hintStyle: TextStyle(
                          color:
                              theme.colorScheme.onBackground.withOpacity(0.6),
                        ),
                        hintText: '${LocaleKeys.please_input.tr()} Port'),
                  ),
                  const SizedBox(height: Spacing.l),
                  SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: theme.colorScheme.primary),
                          onPressed: () {
                            if (_ipAddressController.text != '') {
                              viewController.connectToPrinter(
                                ipAddress: IPAddress(
                                    ipAddress: _ipAddressController.text,
                                    port: int.parse(_portController.text)),
                              );
                            }
                          },
                          child: viewController.loadingScan
                              ? const SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: CircularProgressIndicator(
                                      color: Colors.white),
                                )
                              : Text(
                                  LocaleKeys.connect.tr(),
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                ))),
                ],
              )),
        );
      }),
    );
  }
}
