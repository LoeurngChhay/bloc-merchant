import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_notify_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_exception.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class OrderViewController extends ChangeNotifier {
  final OrderRepo orderRepo;
  final Function(String message, OrderAction action)? onSuccessAction;
  final Function(String message)? onErrorAction;
  OrderViewController({
    required this.orderRepo,
    this.onSuccessAction,
    this.onErrorAction,
  });

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newVal) {
    _loading = newVal;
    notifyListeners();
  }

  bool _loadingDetail = false;
  bool get loadingDetail => _loadingDetail;
  set loadingDetail(bool newVal) {
    _loadingDetail = newVal;
    notifyListeners();
  }

  bool _loadingAction = false;
  bool get loadingAction => _loadingAction;
  set loadingAction(bool newVal) {
    _loadingAction = newVal;
    notifyListeners();
  }

  bool _loadingReject = false;
  bool get loadingReject => _loadingReject;
  set loadingReject(bool newVal) {
    _loadingReject = newVal;
    notifyListeners();
  }

  CustomException? _error;
  CustomException? get error => _error;
  set error(CustomException? newValue) {
    _error = newValue;
    notifyListeners();
  }

  List<Order>? _orderList = [];
  List<Order>? get orderList => _orderList;
  set orderList(List<Order>? newValue) {
    _orderList = newValue;
    notifyListeners();
  }

  Map<String, OrderDetail> _orderDetailObj = {};
  Map<String, OrderDetail> get orderDetailObj => _orderDetailObj;
  set orderDetailObj(Map<String, OrderDetail> value) {
    _orderDetailObj = value;

    notifyListeners();
  }

  OrderNotify? _orderNotify;
  OrderNotify? get orderNotify => _orderNotify;
  set orderNotify(OrderNotify? value) {
    _orderNotify = value;
    notifyListeners();
  }

  Future<void> initMethod() async {
    getOrderNotify();
    getOrderList();
  }

  Future<void> getOrderNotify({bool refresh = false}) async {
    // if (!refresh) {
    //   loading = true;
    // }

    try {
      final response = await orderRepo.orderNotify();
      orderNotify = response.data;
    } catch (e) {
      print("Error order notify : $e");
    }
  }

  CancelToken? cancelToken;
  Future<void> getOrderList(
      {bool refresh = false,
      int? status = OrderState.newOrder,
      int? page}) async {
    if (!refresh) {
      loading = true;
    }

    try {
      if (cancelToken != null && !cancelToken!.isCancelled) {
        cancelToken?.cancel();
      }

      cancelToken = CancelToken();

      final response =
          await orderRepo.orderList(status: status, cancelToken: cancelToken);
      orderList = response.data != null ? response.data!.items : [];

      loading = false;
    } catch (e) {
      // print("error order List : $e");
      if (e is DioException) {
        if (e.type == DioExceptionType.cancel) {
          return;
        }
        loading = false;
      }
    }
  }

  Future<void> getDetail(String orderId) async {
    loadingDetail = true;
    try {
      final response = await orderRepo.orderDetail(orderId);

      if (response.data != null) {
        orderDetailObj.addAll({orderId: response.data!});
      }

      print('orderDetailObj : ${orderDetailObj.entries.map((e) => e.key)}');
    } catch (e) {
      print("Error detail : $e");
    } finally {
      loadingDetail = false;
    }
  }

  Future<void> acceptOrder(String orderId) async {
    loadingAction = true;

    // if (onSuccessAction != null) {
    //   onSuccessAction!("response.message", OrderAction.accept);
    // }
    try {
      final response = await orderRepo.orderAccept(orderId);

      if (onSuccessAction != null) {
        print('onsuccess ; ${response.message}');
        onSuccessAction!(response.message, OrderAction.accept);
      }
    } catch (e) {
      print('Error Accept Order ; $e');
      if (e is DioException) {
        if (onErrorAction != null) {
          onErrorAction!(e.message.toString());
        }
      }
    } finally {
      loadingAction = false;
    }
  }

  Future<void> cancelOrder(String orderId, String reason) async {
    loadingReject = true;

    try {
      final response = await orderRepo.orderCancel(orderId, reason);

      if (onSuccessAction != null) {
        onSuccessAction!(response.message, OrderAction.accept);
      }
    } catch (e) {
      print('Error Cancel Order ; $e');
      if (e is DioException) {
        if (onErrorAction != null) {
          onErrorAction!(e.message.toString());
        }
      }
    } finally {
      loadingReject = false;
    }
  }
}
