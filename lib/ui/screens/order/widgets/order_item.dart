import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/order_detail_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/widget/order_note.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderItem extends StatefulWidget {
  final Order item;
  final int state;
  final OrderDetail? orderDetail;
  final Function()? onAccept;
  final Function()? onCancel;
  final bool? loading;

  final OrderViewController orderViewController;

  const OrderItem({
    super.key,
    required this.state,
    required this.item,
    required this.orderViewController,
    this.orderDetail,
    this.onAccept,
    this.onCancel,
    this.loading = false,
  });

  @override
  State<OrderItem> createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  bool isOpenDetail = false;

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final item = widget.orderDetail;
    double subtotal = 0.00;
    double discount = 0.00;
    double parkageFee = 0.00;
    double vatAmount = 0.00;
    double total = 0.00;

    if (item != null) {
      final item = widget.orderDetail;
      subtotal = double.parse(item?.product_price ?? "0");
      discount = (item?.discount_amount ?? 0).toDouble();
      parkageFee = double.parse(item?.package_price ?? "0");
      vatAmount = (item?.vat_amount ?? 0).toDouble();
      // double deliveryFee = item!.delivery_fee!;
      // double bagFee = item!.bag_price!;
      total = ((subtotal + parkageFee) - discount) + vatAmount;
    }

    return Consumer<OrderViewController>(
      builder: (context, viewController, child) {
        return Container(
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(8)),
          margin: const EdgeInsets.symmetric(horizontal: Spacing.normal),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(Spacing.s),
                decoration: const BoxDecoration(
                  color: Colors.blue,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Text(
                              "${LocaleKeys.queue.tr()}:",
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.item.queue_number!,
                              style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            color: Colors.amberAccent,
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Text(
                            widget.item.order_time.toString(),
                            style: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "${LocaleKeys.order.tr()} #:",
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          widget.item.order_id!,
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(Spacing.normal),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Text("${LocaleKeys.customer.tr()}:"),
                        ),
                        Expanded(
                            flex: 2,
                            child: GestureDetector(
                              onTap: () {
                                _makePhoneCall(
                                  widget.item.customer!.mobile.toString(),
                                );
                              },
                              child: Text(
                                widget.item.customer!.mobile.toString(),
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            )),
                        const SizedBox(width: Spacing.s),
                        SizedBox(
                          width: 40,
                          height: 40,
                          child: CusIconButton(
                            onPressed: () {
                              _makePhoneCall(
                                widget.item.customer!.mobile.toString(),
                              );
                            },
                            icon: const Icon(
                              Icons.phone,
                              color: Colors.white,
                            ),
                            padding: const EdgeInsets.all(8),
                            style: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(Spacing.s),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: Spacing.m),
                    if (widget.item.driver != null &&
                        widget.item.driver_id != "0") ...[
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: Text("${LocaleKeys.driver.tr()}:")),
                          Expanded(
                            flex: 2,
                            child: GestureDetector(
                              onTap: () {
                                _makePhoneCall(
                                  widget.item.driver!.mobile.toString(),
                                );
                              },
                              child: RichText(
                                text: TextSpan(children: <TextSpan>[
                                  TextSpan(
                                    text: "${widget.item.driver!.name}\n",
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "${widget.item.driver!.mobile}",
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline,
                                    ),
                                  )
                                ]),
                              ),
                            ),
                          ),
                          const SizedBox(width: Spacing.s),
                          SizedBox(
                            width: 40,
                            height: 40,
                            child: CusIconButton(
                              onPressed: () {
                                _makePhoneCall(
                                  widget.item.driver!.mobile.toString(),
                                );
                              },
                              icon: const Icon(
                                Icons.phone,
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.all(8),
                              style: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(Spacing.s),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                    Divider(color: Colors.grey[400]),
                    if (widget.item.remark != "")
                      OrderNoteWidget(widget.item.remark),
                    ExpansionTile(
                      key: Key(widget.item.order_id.toString()),
                      onExpansionChanged: (value) {
                        setState(() => isOpenDetail = value);
                        if (value) {
                          widget.orderViewController
                              .getDetail(widget.item.order_id!);
                        }
                      },
                      // initiallyExpanded: true,
                      collapsedShape: const RoundedRectangleBorder(
                        side: BorderSide.none,
                      ),
                      shape: const RoundedRectangleBorder(
                        side: BorderSide.none,
                      ),
                      tilePadding: EdgeInsets.zero,
                      title: Text(
                        LocaleKeys.order_details.tr(),
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      trailing: Icon(
                        isOpenDetail
                            ? Icons.arrow_drop_up_rounded
                            : Icons.arrow_drop_down_rounded,
                        size: 32,
                      ),
                      children: viewController.loadingDetail
                          ? [
                              Container(
                                margin: const EdgeInsets.only(top: Spacing.s),
                                height: 24,
                                width: 24,
                                child: const CircularProgressIndicator(),
                              )
                            ]
                          : <Widget>[
                              if (widget.orderDetail != null)
                                ...widget.orderDetail!.products!.map((e) {
                                  // final productPrice = double.parse(e.product_price!) *
                                  //     double.parse(e.product_number!);
                                  return Column(
                                    children: [
                                      const SizedBox(height: Spacing.s),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                  horizontal: 3,
                                                  vertical: 1,
                                                ),
                                                margin: const EdgeInsets.only(
                                                  bottom: 3,
                                                ),
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(3),
                                                  border: Border.all(
                                                    width: 1,
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                  ),
                                                ),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      '${e.product_number}',
                                                      style: theme
                                                          .textTheme.titleLarge
                                                          ?.copyWith(
                                                        color: redPrimary,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                    const Text("x")
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                width: 45,
                                                height: 45,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(3),
                                                  border: Border.all(
                                                    width: 1,
                                                    color: Colors.grey
                                                        .withOpacity(0.2),
                                                  ),
                                                ),
                                                child: TransparentImage(
                                                  url: e.product_thumb,
                                                  borderRadius: 3,
                                                ),
                                              )
                                            ],
                                          ),
                                          const SizedBox(width: Spacing.s),
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        e.product_title
                                                            .toString(),
                                                        style: theme.textTheme
                                                            .bodyMedium
                                                            ?.copyWith(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      width: Spacing.m,
                                                    ),
                                                    Text(
                                                      '${e.product_price}\$',
                                                      style: theme
                                                          .textTheme.bodyMedium
                                                          ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                const SizedBox(
                                                  height: Spacing.s,
                                                ),
                                                ...e.specification!.map(
                                                  (e) => Text(
                                                      '• ${e.key}: ${e.val}'),
                                                ),
                                                const SizedBox(
                                                  height: Spacing.normal / 2,
                                                ),
                                                if (e.bar_code != "") ...[
                                                  SizedBox(
                                                    height: 60,
                                                    child: SfBarcodeGenerator(
                                                      value: e.bar_code,
                                                      showValue: true,
                                                    ),
                                                  ),
                                                ],
                                                ...e.product_addon!.map(
                                                  (e) {
                                                    return Padding(
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                        vertical: 3,
                                                      ),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            padding:
                                                                const EdgeInsets
                                                                    .symmetric(
                                                              horizontal:
                                                                  Spacing.s,
                                                              vertical:
                                                                  Spacing.xs,
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  Colors.green,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                Spacing.xs,
                                                              ),
                                                            ),
                                                            child: Text(
                                                              "ADD",
                                                              style: theme
                                                                  .textTheme
                                                                  .bodySmall
                                                                  ?.copyWith(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            width: 10,
                                                          ),
                                                          Text(
                                                            "x${e.qty}",
                                                            style: theme
                                                                .textTheme
                                                                .bodySmall
                                                                ?.copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            width: 10,
                                                          ),
                                                          Expanded(
                                                            child: Text(
                                                              e.product_name
                                                                  .toString(),
                                                              style: theme
                                                                  .textTheme
                                                                  .bodyMedium,
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            width:
                                                                Spacing.m * 2,
                                                          ),
                                                          Text(
                                                            '${e.price}\$',
                                                            style: theme
                                                                .textTheme
                                                                .bodyMedium,
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Divider(color: Colors.grey[400]),
                                    ],
                                  );
                                }),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("${LocaleKeys.subtotal.tr()}:"),
                                  Text(
                                    "${subtotal.toStringAsFixed(2)}\$",
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  )
                                ],
                              ),
                              const SizedBox(height: Spacing.m),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("${LocaleKeys.discount.tr()}:"),
                                  Text(
                                    "-${discount.toStringAsFixed(2)}\$",
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  )
                                ],
                              ),
                              const SizedBox(height: Spacing.m),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("${LocaleKeys.vat.tr()}:"),
                                  Text(
                                    "${item?.vat_amount}\$",
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  )
                                ],
                              ),
                              const SizedBox(height: Spacing.m),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "${LocaleKeys.total.tr()}:",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium
                                        ?.copyWith(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                  ),
                                  Text(
                                    "${total.toStringAsFixed(2)}\$",
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium
                                        ?.copyWith(
                                          fontWeight: FontWeight.bold,
                                        ),
                                  )
                                ],
                              ),
                              Divider(color: Colors.grey[400], height: 32),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    LocaleKeys.shop_receive.tr(),
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    '${item?.shop_receive?.toStringAsFixed(2)}\$',
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(height: 16),
                              Align(
                                alignment: Alignment.center,
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(
                                      context,
                                      OrderDetailView.routeName,
                                      arguments: OrderArguments(
                                        state: widget.state,
                                        orderId:
                                            widget.item.order_id.toString(),
                                        orderTime: widget.item.order_time,
                                      ),
                                    );
                                  },
                                  child: Text(
                                    LocaleKeys.see_more_details.tr(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black.withOpacity(0.8),
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(height: Spacing.normal),
                            ],
                    ),
                    if (widget.state == OrderState.newOrder) ...[
                      Divider(color: Colors.grey[400]),
                      Row(
                        children: [
                          TextButton(
                              onPressed: widget.onCancel,
                              child: viewController.loadingReject &&
                                      widget.loading!
                                  ? const SizedBox(
                                      height: 18,
                                      width: 18,
                                      child: CircularProgressIndicator(),
                                    )
                                  : Text(
                                      LocaleKeys.reject.tr(),
                                      style: const TextStyle(color: Colors.red),
                                    )),
                          const SizedBox(width: Spacing.normal),
                          Expanded(
                            child: FilledButton.icon(
                              style: FilledButton.styleFrom(
                                padding: const EdgeInsets.symmetric(
                                  vertical: Spacing.m,
                                ),
                              ),
                              onPressed: widget.onAccept,
                              icon: viewController.loadingAction &&
                                      widget.loading!
                                  ? const SizedBox(
                                      height: 24,
                                      width: 24,
                                      child: CircularProgressIndicator(
                                          color: Colors.white),
                                    )
                                  : const SizedBox(),
                              label: Text(
                                LocaleKeys.accept_order.tr(),
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      )
                    ] else
                      Container(
                        height: 60,
                        width: double.infinity,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          border: Border(
                            top: BorderSide(width: 2, color: Colors.blue),
                          ),
                        ),
                        child: Text(
                          "PIN: ${widget.item.order_id?.substring(2)}",
                          style: const TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
