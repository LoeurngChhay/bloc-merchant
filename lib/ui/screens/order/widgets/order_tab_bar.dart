import 'package:bloc_merchant_mobile_2/data/models/responses/order_notify_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as badges;

class OrderTabBar extends StatefulWidget {
  final TabController tabBarController;
  final OrderNotify? orderNotify;
  final Function(int index)? onTab;

  const OrderTabBar(
      {super.key,
      this.orderNotify,
      required this.tabBarController,
      this.onTab});

  @override
  State<OrderTabBar> createState() => _OrderTabBarState();
}

class _OrderTabBarState extends State<OrderTabBar> {
  final tabBarList = [
    TabBarType(icon: Icons.receipt_rounded, title: LocaleKeys.o_pending.tr()),
    TabBarType(
        icon: Icons.hourglass_top_rounded, title: LocaleKeys.o_prepare.tr()),
    TabBarType(
        icon: Icons.shopping_cart_rounded, title: LocaleKeys.o_pickup.tr()),
    TabBarType(icon: Icons.delivery_dining, title: LocaleKeys.o_delivery.tr()),
  ];

  int activeIndex = 0;
  @override
  Widget build(BuildContext context) {
    final notifyList = widget.orderNotify != null
        ? [
            widget.orderNotify?.pendding_count,
            widget.orderNotify?.prepare_count,
            widget.orderNotify?.pickup_count,
            widget.orderNotify?.delivery_count,
          ]
        : [];
    return TabBar(
        labelPadding: const EdgeInsets.all(0),
        padding: const EdgeInsets.symmetric(
            vertical: Spacing.m, horizontal: Spacing.s),
        indicatorSize: TabBarIndicatorSize.label,
        isScrollable: false,
        controller: widget.tabBarController,
        dividerColor: Colors.transparent,
        indicatorWeight: 0,
        labelColor: Colors.white,
        indicator: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(Spacing.s),
            border: Border.all(color: Colors.grey, width: 0)),
        onTap: widget.onTab,
        tabs: List.generate(tabBarList.length, (i) {
          return badges.Badge(
            showBadge: notifyList.isNotEmpty && notifyList[i] != "0",
            badgeStyle: badges.BadgeStyle(
                shape: badges.BadgeShape.square,
                borderRadius: BorderRadius.circular(30),
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 0)),
            position: badges.BadgePosition.topEnd(top: -10, end: 0),
            badgeContent: Text(
                notifyList.isNotEmpty && notifyList[i] != "0"
                    ? notifyList[i].toString()
                    : "",
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18)),
            child: Container(
              width: (MediaQuery.of(context).size.width / 4) - (Spacing.m),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Spacing.s),
                border: Border.all(width: 1, color: Colors.black),
              ),
              child: Tab(
                icon: Icon(tabBarList[i].icon, size: 30),
                child: Text(tabBarList[i].title),
              ),
            ),
          );
        }).toList());
  }
}

class TabBarType {
  final IconData icon;
  final String title;

  TabBarType({
    required this.icon,
    required this.title,
  });
}
