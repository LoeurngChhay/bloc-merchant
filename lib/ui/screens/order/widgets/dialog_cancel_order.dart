import 'package:flutter/material.dart';

Map<int, String> cancelLiss = {
  0: "Out of stock",
  1: "No Chef",
  2: "Not Available",
  3: "Shop Close",
  4: "To many orders",
};

class DialogCancelOrder extends StatefulWidget {
  final Function(int reason)? onSave;
  final Function()? onError;

  const DialogCancelOrder({super.key, this.onSave, this.onError});

  @override
  State<DialogCancelOrder> createState() => _DialogCancelOrderState();
}

class _DialogCancelOrderState extends State<DialogCancelOrder> {
  int cancelState = 0;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text("Cancelation Reason"),
      content: SizedBox(
        height: MediaQuery.of(context).size.height * 0.3,
        width: MediaQuery.of(context).size.width,
        child: Column(
            children: cancelLiss.entries
                .map((e) => SizedBox(
                      width: double.infinity,
                      child: FilledButton.icon(
                        onPressed: () {
                          setState(() {
                            cancelState = e.key;
                          });
                        },
                        style: FilledButton.styleFrom(
                            alignment: Alignment.centerLeft,
                            backgroundColor: cancelState == e.key
                                ? Colors.blue
                                : Colors.grey[200]),
                        icon: Icon(
                            cancelState == e.key
                                ? Icons.check_box_rounded
                                : Icons.check_box_outline_blank_rounded,
                            color: cancelState == e.key
                                ? Colors.white
                                : Colors.black),
                        label: Text(
                          e.value.toString(),
                          style: TextStyle(
                              color: cancelState == e.key
                                  ? Colors.white
                                  : Colors.black),
                        ),
                      ),
                    ))
                .toList()),
      ),
      actions: [
        FilledButton.icon(
            onPressed: () {
              Navigator.pop(context);
            },
            style: FilledButton.styleFrom(backgroundColor: Colors.red),
            icon: const Icon(Icons.close),
            label: const Text("Cancel")),
        FilledButton.icon(
            onPressed: () {
              if (widget.onSave != null) {
                widget.onSave!(cancelState);
              }
            },
            icon: const Icon(Icons.check),
            label: const Text("Save")),
      ],
    );
  }
}
