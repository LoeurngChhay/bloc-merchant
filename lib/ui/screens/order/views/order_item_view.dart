import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/widgets/dialog_cancel_order.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/widgets/order_item.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CancelReasonType {
  final String id;
  final String title;

  CancelReasonType({required this.id, required this.title});
}

class OrderItemView extends StatefulWidget {
  final Function()? onAccept;
  final Function()? refresh;
  final int state;
  const OrderItemView(
      {super.key, this.onAccept, this.refresh, required this.state});

  @override
  State<OrderItemView> createState() => _OrderItemViewState();
}

class _OrderItemViewState extends State<OrderItemView> {
  String? orderId;
  void openCancelOrder(OrderViewController viewController, id) {
    showDialog(
      context: context,
      builder: (context) {
        return DialogCancelOrder(
          onSave: (reason) {
            viewController.cancelOrder(id, reason.toString());
            // print("onCancel : $reason");
            Navigator.pop(context);
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderViewController>(
        builder: (context, viewController, child) {
      if (viewController.loading) {
        return const Center(child: CircularProgressIndicator());
      }

      if (viewController.orderList != null &&
          viewController.orderList!.isNotEmpty) {
        return RefreshIndicator(
          onRefresh: () async {
            viewController.getOrderNotify(refresh: true);
            viewController.getOrderList(refresh: true, status: widget.state);
          },
          child: ListView.separated(
            itemCount: viewController.orderList?.length ?? 0,
            itemBuilder: (context, index) {
              return OrderItem(
                state: widget.state,
                item: viewController.orderList![index],
                orderViewController: viewController,
                orderDetail: viewController.orderDetailObj[
                    viewController.orderList![index].order_id.toString()],
                loading: orderId == viewController.orderList![index].order_id,
                onAccept: () {
                  if (widget.onAccept != null) {
                    widget.onAccept!();
                  }
                  viewController.acceptOrder(
                      viewController.orderList![index].order_id.toString());
                },
                onCancel: () {
                  final id = viewController.orderList![index].order_id;
                  setState(() {
                    orderId = id;
                  });

                  openCancelOrder(viewController, id);
                },
              );
            },
            separatorBuilder: (context, index) =>
                const SizedBox(height: Spacing.normal),
          ),
        );
      }

      return EmptyScreen(
        title: LocaleKeys.no_orders.tr(),
        message: LocaleKeys.there_is_no_order_right_now_pleas_wait.tr(),
        onRefresh: () {
          viewController.getOrderNotify();
          viewController.getOrderList();
        },
      );
    });
  }
}
