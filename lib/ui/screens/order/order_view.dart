import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/order_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/views/order_item_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/widgets/order_tab_bar.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/order_history_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderView extends StatefulWidget {
  static const String routeName = '/order';

  const OrderView({super.key});

  @override
  State<OrderView> createState() => _OrderViewState();
}

class _OrderViewState extends State<OrderView>
    with SingleTickerProviderStateMixin {
  TabController? _tabBarController;
  late OrderViewController _orderViewController;

  @override
  void initState() {
    _tabBarController = TabController(length: 4, vsync: this);

    _tabBarController?.addListener(() {
      if (_tabBarController!.indexIsChanging) {
        _orderViewController.getOrderNotify();
        _orderViewController.getOrderList(status: _tabBarController?.index);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return ChangeNotifierProvider(
      create: (context) => OrderViewController(
        orderRepo: locator<OrderRepo>(),
        onSuccessAction: (message, action) {
          DialogUtil.cusAlertDialog(
              context: context, message: message, millisecond: 1000);
          if (action == OrderAction.accept) {
            _tabBarController?.animateTo(OrderState.waiting,
                duration: const Duration(milliseconds: 300),
                curve: Curves.easeInOut);
          }
        },
        onErrorAction: (message) {
          DialogUtil.cusAlertDialog(
              context: context, message: message, millisecond: 1000);
        },
      )..initMethod(),
      child: Consumer<OrderViewController>(
          builder: (context, viewController, child) {
        _orderViewController = viewController;
        return Scaffold(
          appBar: AppBar(
            leading: const CusBackButton(),
            title: Text(LocaleKeys.all_order.tr()),
            actions: [
              GestureDetector(
                onTap: () {
                  // _tabBarController?.animateTo(1);
                  // viewController.acceptOrder("0");
                  Navigator.pushNamed(context, OrderHistoryView.routeName);
                },
                child: Row(
                  children: [
                    Text(
                      LocaleKeys.o_history.tr(),
                      style: theme.textTheme.titleMedium
                          ?.copyWith(color: Colors.blue),
                    ),
                    const SizedBox(width: Spacing.s),
                    const Icon(
                      Icons.access_time_rounded,
                      color: Colors.blue,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: Spacing.s),
            ],
          ),
          body: DefaultTabController(
              initialIndex: 0,
              length: 4,
              child: Column(
                children: [
                  OrderTabBar(
                    orderNotify: viewController.orderNotify,
                    tabBarController: _tabBarController!,
                  ),
                  Expanded(
                    child: TabBarView(
                      controller: _tabBarController,
                      physics: const NeverScrollableScrollPhysics(),
                      children: const [
                        OrderItemView(state: OrderState.newOrder),
                        OrderItemView(state: OrderState.waiting),
                        OrderItemView(state: OrderState.picker),
                        OrderItemView(state: OrderState.dilivery),
                      ],
                    ),
                  )
                ],
              )),
        );
      }),
    );
  }
}
