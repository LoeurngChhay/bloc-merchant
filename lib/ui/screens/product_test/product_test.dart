// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:bloc_merchant_mobile_2/ui/screens/product_test/widgets/product_format_test.dart';
import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';
import 'package:provider/provider.dart';

import 'package:bloc_merchant_mobile_2/constants/product_status.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_product_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_attribute.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/widgets/product_info_test.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/widgets/product_type_test.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';

class ProductTestView extends StatefulWidget {
  static const String routeName = '/product-test';
  const ProductTestView({super.key});

  @override
  State<ProductTestView> createState() => _ProductTestViewState();
}

class _ProductTestViewState extends State<ProductTestView> {
  //form
  final _formKey = GlobalKey<FormState>();

  //Product type
  GroupButtonController saleTypeController = GroupButtonController();
  GroupButtonController productTypeController = GroupButtonController();
  bool isProductTopping = false;

  Map<String, dynamic> mapReq = {};
  CreateProductRequestTest _request = CreateProductRequestTest();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ProductTestViewController(),
      child: Consumer<ProductTestViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              leading: const CusBackButton(
                color: Colors.white,
              ),
              centerTitle: true,
              title: const Text(
                "Product Test",
                style: TextStyle(color: Colors.white),
              ),
              actions: [
                InkWell(
                  onTap: () {
                    // print('state : $_request');
                    viewController.createProduct(mapReq, _request);
                  },
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.white),
                    ),
                    child: const Icon(Icons.add, color: Colors.white),
                  ),
                ),
                const SizedBox(width: Spacing.normal),
              ],
            ),
            body: Form(
              key: _formKey,
              child: ListView(
                padding: const EdgeInsets.all(Spacing.sm),
                children: <Widget>[
                  ProductTypeTest(
                    productTypeController: productTypeController,
                    saleTypeController: saleTypeController,
                    onProductCallBack: (index) {
                      productTypeController.selectIndex(index);

                      mapReq.addAll({"isAdddon": index.toString()});

                      _request.isAddon = index.toString();

                      if (index == int.parse(ProductTypeStatus.topping)) {
                        saleTypeController.selectIndex(0);
                      }
                    },
                    onSaleCallBack: (index) {
                      saleTypeController.selectIndex(index);

                      mapReq.addAll({'addonSale': index.toString()});
                      _request.addonSale = index.toString();
                    },
                  ),
                  const SizedBox(height: Spacing.s),
                  ProductInfoTest(
                    request: mapReq,
                    productController: viewController,
                  ),
                  ProductFormatTest(
                    request: mapReq,
                    viewController: viewController,
                  ),
                  const SizedBox(height: Spacing.normal),
                  ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState?.validate() ?? false) {
                        // _formKey.currentState
                        //     ?.save(); // Trigger onSaved for all fields
                      }
                    },
                    child: const Text('Submit'),
                  ),
                ],
              ),
            ));
      }),
    );
  }
}

class CreateProductRequestTest {
  String? productId;
  File? video;
  String? videoUrl;
  String? videoId;
  String? title;
  String? secondaryTitle;
  String? brand;
  String? cateId;
  String? unit;
  String? isOnSale;
  String? isAddon;
  String? addonSale;
  String? orderBy;
  String? description;
  List<IFormatRequest>? formats;
  List<Attribute>? attributes;
  List<IAddonRequest>? addons;
  String? expiredDate;
  String? deletedVideo;
  List<File>? images;
  List<ProductImages>? photos;

  CreateProductRequestTest({
    this.productId,
    this.video,
    this.videoUrl,
    this.videoId,
    this.title,
    this.secondaryTitle,
    this.brand,
    this.cateId,
    this.unit,
    this.isOnSale,
    this.isAddon,
    this.addonSale,
    this.orderBy,
    this.description,
    this.formats,
    this.attributes,
    this.addons,
    this.expiredDate,
    this.deletedVideo,
    this.images,
    this.photos,
  });

  @override
  String toString() {
    return 'CreateProductRequestTest(productId: $productId, video: $video, videoUrl: $videoUrl, videoId: $videoId, title: $title, secondaryTitle: $secondaryTitle, brand: $brand, cateId: $cateId, unit: $unit, isOnSale: $isOnSale, isAddon: $isAddon, addonSale: $addonSale, orderBy: $orderBy, description: $description, formats: $formats, attributes: $attributes, addons: $addons, expiredDate: $expiredDate, deletedVideo: $deletedVideo, images: $images, photos: $photos)';
  }
}
