import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';

import 'package:flutter/material.dart';

class FormatFormInputTest extends StatefulWidget {
  final TextEditingController? priceController;
  final TextEditingController? stockController;
  final TextEditingController? barcodeController;
  final TextEditingController? packFeeController;
  final TextEditingController? totalItemController;
  final TextEditingController? nameController;
  final Function(bool)? onUnlimited;
  final bool? isDialog;
  final bool? isRetail;
  final bool? isUnlimited;
  final String? id;
  final List<Format>? formats;
  final Map<String, dynamic>? request;

  const FormatFormInputTest({
    super.key,
    this.priceController,
    this.stockController,
    this.barcodeController,
    this.packFeeController,
    this.totalItemController,
    this.nameController,
    this.isDialog = false,
    this.isRetail = false, //isRetail === "1"
    this.onUnlimited,
    this.id,
    this.formats = const [],
    this.isUnlimited = true,
    this.request,
  });

  @override
  State<FormatFormInputTest> createState() => _FormatFormInputTestState();
}

class _FormatFormInputTestState extends State<FormatFormInputTest> {
  bool isUnlimited = true;

  // void fillEditValue() {
  //   if (widget.formats!.isNotEmpty) {
  //     final format =
  //         widget.formats?.where((e) => e.spec_id == widget.id).toList().first;

  //     widget.nameController?.text = format?.spec_name ?? "";
  //     widget.packFeeController.text = format?.package_price ?? "";
  //     widget.priceController.text = format?.price ?? "";
  //     widget.stockController.text = format?.sale_sku?.toString() ?? "";
  //     widget.barcodeController.text = format?.bar_code?.toString() ?? "";
  //     widget.totalItemController.text = format?.total_item ?? "";
  //   }
  // }

  @override
  void initState() {
    isUnlimited = widget.isUnlimited ?? isUnlimited;

    if (widget.id != null) {
      // fillEditValue();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final border = OutlineInputBorder(
        borderSide: BorderSide(
            width: 1, color: theme.colorScheme.onBackground.withOpacity(0.5)));
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.isDialog! && widget.nameController != null) ...[
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              child: const Text("Name")),
          const CusTextField(
              contentPadding: const EdgeInsets.all(Spacing.s),
              hintText: "Name",
              suffixIcon: const Icon(Icons.attach_money_sharp)),
          const SizedBox(height: Spacing.s),
        ],
        if (widget.isDialog!)
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              child: const Text("Price")),
        Row(
          children: [
            if (widget.isDialog != null && !widget.isDialog!)
              const SizedBox(width: 75, child: Text("Price")),
            Expanded(
              flex: 2,
              child: CusTextField(
                hintText: "Price",
                border: border,
                focusedBorder: border,
                enabledBorder: border,
                suffixIcon: const Icon(Icons.attach_money_sharp),
                onChange: (value) {
                  widget.request?.addAll({"price": value});
                },
              ),
            )
          ],
        ),
        const SizedBox(height: Spacing.s),
        if (widget.isDialog!)
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              child: const Text("Stock")),
        Row(
          children: [
            if (widget.isDialog != null && !widget.isDialog!)
              const SizedBox(width: 75, child: Text("Stock")),
            Expanded(
                child: Row(
              children: [
                Expanded(
                  child: CusTextField(
                    hintText: isUnlimited ? "Unlimited" : "Add Stock",
                    border: border,
                    focusedBorder: border,
                    enabledBorder: border,
                    enabled: !isUnlimited,
                    onChange: (value) {
                      if (!isUnlimited) {
                        widget.request?.addAll({"sale_sku": value});
                      }
                    },
                  ),
                ),
                const SizedBox(width: Spacing.s),
                if (widget.isDialog != null && widget.isDialog!) ...[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isUnlimited = !isUnlimited;
                      });

                      if (widget.onUnlimited != null) {
                        widget.onUnlimited!(isUnlimited);
                      }
                    },
                    child: Container(
                      height: 48,
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(Spacing.xs)),
                      child: Icon(
                          isUnlimited
                              ? Icons.check_box_outlined
                              : Icons.check_box_outline_blank,
                          color: Colors.white),
                    ),
                  ),
                ] else
                  Expanded(
                    child: SizedBox(
                      height: 48,
                      child: FilledButton.icon(
                          style: FilledButton.styleFrom(
                              padding: EdgeInsets.zero,
                              backgroundColor: Colors.green,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.circular(Spacing.s))),
                          onPressed: () {
                            setState(() {
                              isUnlimited = !isUnlimited;
                            });

                            if (widget.onUnlimited != null) {
                              widget.onUnlimited!(isUnlimited);
                            }
                          },
                          icon: Icon(
                            isUnlimited
                                ? Icons.check_box_outlined
                                : Icons.check_box_outline_blank,
                            color: Colors.white,
                          ),
                          label: widget.isDialog != null && widget.isDialog!
                              ? const SizedBox()
                              : const Text(
                                  "Unlimited",
                                  style: TextStyle(color: Colors.white),
                                )),
                    ),
                  )
              ],
            ))
          ],
        ),
        const SizedBox(height: Spacing.s),
        if (widget.isDialog!)
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              child: const Text("Packaging fee")),
        Row(
          children: [
            if (widget.isDialog != null && !widget.isDialog!)
              const SizedBox(width: 75, child: Text("Packaging fee :")),
            Expanded(
                flex: 2,
                child: SizedBox(
                  height: 40,
                  child: CusTextField(
                    controller: widget.packFeeController,
                    contentPadding: const EdgeInsets.all(Spacing.s),
                    hintText: "Packaging fee",
                    border: border,
                    focusedBorder: border,
                    enabledBorder: border,
                  ),
                ))
          ],
        ),
        const SizedBox(height: Spacing.s),
        if (widget.isDialog!)
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              child: const Text("Barcode")),
        Row(
          children: [
            if (widget.isDialog != null && !widget.isDialog!)
              const SizedBox(width: 75, child: Text("Barcode")),
            Expanded(
                flex: 2,
                child: SizedBox(
                  height: 40,
                  child: CusTextField(
                    controller: widget.barcodeController,
                    contentPadding: const EdgeInsets.all(Spacing.s),
                    hintText: "Barcode",
                    border: border,
                    focusedBorder: border,
                    enabledBorder: border,
                  ),
                ))
          ],
        ),
        const SizedBox(height: Spacing.s),
        if (widget.isDialog! && widget.isRetail!)
          Container(
              margin: const EdgeInsets.only(bottom: Spacing.xs),
              width: 75,
              child: const Text("Total Item")),
        if (widget.isRetail!)
          Row(
            children: [
              if (widget.isDialog != null && !widget.isDialog!)
                const SizedBox(width: 75, child: Text("Total Item")),
              Expanded(
                flex: 2,
                child: SizedBox(
                    height: 40,
                    child: CusTextField(
                      controller: widget.totalItemController,
                      contentPadding: const EdgeInsets.all(Spacing.s),
                      hintText: "Total Item",
                      border: border,
                      focusedBorder: border,
                      enabledBorder: border,
                    )),
              )
            ],
          ),
      ],
    );
  }
}
