import 'package:bloc_merchant_mobile_2/data/models/responses/product_format.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/widgets/fomat_input_test.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class ProductFormatTest extends StatefulWidget {
  final List<Format>? formats;
  final bool? isFormatAdvance;
  final bool? isUnlimited;
  final TextEditingController? nameController;
  final TextEditingController? priceController;
  final TextEditingController? stockController;
  final TextEditingController? barcodeController;
  final TextEditingController? packFeeController;
  final TextEditingController? totalItemController;
  final Function(List<Format>)? callbackList;
  final Function(bool)? onChangedSwitch;
  final Function(bool)? onUnlimited;
  final ProductTestViewController? viewController;
  final Map<String, dynamic>? request;

  const ProductFormatTest({
    super.key,
    this.formats,
    this.isFormatAdvance = false,
    this.nameController,
    this.priceController,
    this.stockController,
    this.barcodeController,
    this.packFeeController,
    this.totalItemController,
    this.onChangedSwitch,
    this.isUnlimited = true,
    this.onUnlimited,
    this.callbackList,
    this.request,
    this.viewController,
  });

  @override
  State<ProductFormatTest> createState() => _ProductFormatTestState();
}

class _ProductFormatTestState extends State<ProductFormatTest> {
  // List<Format> format = [];
  bool unlimited = true;

  // void resetTextEditor() {
  //   widget.nameController.text = "";
  //   widget.priceController.text = "";
  //   widget.stockController.text = "";
  //   widget.barcodeController.text = "";
  //   widget.packFeeController.text = "";
  //   widget.totalItemController.text = "";
  // }

  // void openFormFormatDialog({String? id}) {
  //   if (id != null) {
  //     unlimited = widget.formats
  //             .where((e) => e.spec_id == id)
  //             .toList()
  //             .first
  //             .sale_type ==
  //         FormatStatus.unlimited;
  //   }

  //   showDialog(
  //     context: context,
  //     builder: (context) => AlertDialog(
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(Spacing.m)),
  //       title: const Text("Advanced format"),
  //       content: SingleChildScrollView(
  //         child: SizedBox(
  //           width: MediaQuery.of(context).size.width,
  //           child: Column(
  //             mainAxisSize: MainAxisSize.min,
  //             children: [
  //               FormatFormInput(
  //                 formats: widget.formats,
  //                 id: id,
  //                 nameController: widget.nameController,
  //                 priceController: widget.priceController,
  //                 stockController: widget.stockController,
  //                 barcodeController: widget.barcodeController,
  //                 packFeeController: widget.packFeeController,
  //                 totalItemController: widget.totalItemController,
  //                 isUnlimited: unlimited,
  //                 onUnlimited: (value) {
  //                   setState(() {
  //                     unlimited = value;
  //                   });
  //                 },
  //                 isDialog: true,
  //               ),
  //             ],
  //           ),
  //         ),
  //       ),
  //       actions: [
  //         FilledButton(
  //             onPressed: () {
  //               Navigator.pop(context);
  //               resetTextEditor();
  //             },
  //             style: FilledButton.styleFrom(backgroundColor: Colors.red),
  //             child: const Text(
  //               "Cancel",
  //               style: TextStyle(fontWeight: FontWeight.bold),
  //             )),
  //         FilledButton(
  //             onPressed: () {
  //               if (id != null) {
  //                 final index = widget.formats
  //                     .indexWhere((e) => e.spec_id == id.toString());

  //                 widget.formats[index] = Format(
  //                   spec_id: id.toString(),
  //                   spec_name: widget.nameController.text,
  //                   price: widget.priceController.text,
  //                   old_price: widget.priceController.text,
  //                   sale_type: unlimited
  //                       ? FormatStatus.unlimited
  //                       : FormatStatus.limited,
  //                   sale_sku: unlimited ? "9999" : widget.stockController.text,
  //                   package_price: widget.packFeeController.text,
  //                   bar_code: widget.barcodeController.text,
  //                   total_item: widget.totalItemController.text,
  //                 );
  //               } else {
  //                 widget.formats.add(
  //                   Format(
  //                     spec_id: (widget.formats.length + 1).toString(),
  //                     spec_name: widget.nameController.text,
  //                     price: widget.priceController.text,
  //                     old_price: widget.priceController.text,
  //                     sale_type: unlimited
  //                         ? FormatStatus.unlimited
  //                         : FormatStatus.limited,
  //                     sale_sku:
  //                         unlimited ? "9999" : widget.stockController.text,
  //                     package_price: widget.packFeeController.text,
  //                     bar_code: widget.barcodeController.text,
  //                     total_item: widget.totalItemController.text,
  //                   ),
  //                 );
  //               }

  //               if (widget.callbackList != null) {
  //                 //CHECK AGAIN
  //                 widget.callbackList!(widget.formats);
  //               }

  //               setState(() {});
  //               Navigator.pop(context);
  //               resetTextEditor();
  //             },
  //             child: const Text(
  //               "Save",
  //               style: TextStyle(fontWeight: FontWeight.bold),
  //             )),
  //       ],
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        color: theme.colorScheme.background,
        borderRadius: BorderRadius.circular(Spacing.s),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Text(
                "Item Format",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              const Spacer(),
              Text(
                widget.isFormatAdvance! ? "(Advanced)" : '(Standard)',
                style: TextStyle(
                    color: widget.isFormatAdvance! ? Colors.blue : null),
              ),
              const SizedBox(width: Spacing.xs),
              SizedBox(
                height: 35,
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Switch(
                      value: widget.isFormatAdvance!,
                      onChanged: widget.onChangedSwitch),
                ),
              )
            ],
          ),
          if (widget.isFormatAdvance!) ...[
            // ProductItemFormatList(
            //   formatList: widget.formats,
            //   onEdit: (id) {
            //     openFormFormatDialog(id: id);
            //   },
            //   onDelete: (id) {
            //     widget.formats.removeWhere((e) => e.spec_id == id);
            //     setState(() {});
            //   },
            // ),
            const SizedBox(height: Spacing.s),
            // ElevatedButton(
            //     onPressed: openFormFormatDialog, child: const Text("Add New"))
          ] else ...[
            FormatFormInputTest(
              request: widget.request,
              isUnlimited: widget.isUnlimited,
            ),
          ]
        ],
      ),
    );
  }
}
