import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_category.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_unit.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/create_product_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_product/widgets/bottom_modal_product_cate.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:video_player/video_player.dart';

class ProductInfoTest extends StatefulWidget {
  final CreateProductViewController? viewController;
  final Map<String, dynamic> request;
  final CreateProductRequestTest? requestTest;
  final Function(UploadImages)? callbackUploadImg;
  final Function(String videoFile)? callbackUploadVideo;
  final Function(ProductCategory cate)? callbackSelectCate;
  final Function(ProductUnit unit)? callbackSelectUnit;
  final List<ProductImages>? productImages;
  final String? video;
  final ProductTestViewController? productController;
  final Function(CreateProductRequestTest req)? callback;

  const ProductInfoTest({
    super.key,
    this.viewController,
    this.callbackUploadImg,
    this.callbackUploadVideo,
    this.productImages = const [],
    this.callbackSelectCate,
    this.callbackSelectUnit,
    this.video,
    required this.request,
    this.productController,
    this.requestTest,
    this.callback,
  });

  @override
  State<ProductInfoTest> createState() => _ProductInfoTestState();
}

class _ProductInfoTestState extends State<ProductInfoTest> {
  String? cateTitle;
  String? unitTitle;
  File? vidoeFile;
  late VideoPlayerController _videoController;
  bool isPause = false;

  void openBottomSheetModal({
    bool isUnitType = false,
    List<ProductCategory>? itemList,
    List<ProductUnit>? unitList,
  }) {
    showModalBottomSheet(
      context: context,
      clipBehavior: Clip.antiAlias,
      isScrollControlled: true,
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.8),
      shape: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(Spacing.normal))),
      builder: (context) {
        return BottomModalProductCate(
          itemList: itemList,
          unitList: unitList,
          onPressedCate: (selectedItem) {
            // widget.productCateController?.text = selectedItem.title.toString();
            widget.request.addAll({"cate_id": selectedItem.cate_id});
            widget.requestTest?.cateId = selectedItem.cate_id;

            if (widget.callbackSelectCate != null) {
              widget.callbackSelectCate!(selectedItem);
            }
            setState(() {});
            Navigator.pop(context);
          },
          onPressedUnit: (selectedItem) {
            // widget.unitTypeController?.text = selectedItem.title.toString();

            widget.request.addAll({"unit": selectedItem.title});
            widget.requestTest?.unit = selectedItem.title;

            if (widget.callbackSelectUnit != null) {
              widget.callbackSelectUnit!(selectedItem);
            }
            setState(() {});
            Navigator.pop(context);
          },
        );
      },
    );
  }

  Future<CroppedFile?> cropImage(String path) async {
    final croppedFile = await ImageCropper().cropImage(
      sourcePath: path,
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
          ],
        ),
        IOSUiSettings(
          title: 'Cropper',
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
            // IMPORTANT: iOS supports only one custom aspect ratio in preset list
          ],
        ),
        WebUiSettings(
          context: context,
        ),
      ],
    );

    return croppedFile;
  }

  void uploadVideo() async {
    final ImagePicker picker = ImagePicker();
    final file = await picker.pickVideo(source: ImageSource.gallery);

    if (vidoeFile != null && file != null) {
      _videoController.dispose();
    }

    if (file != null) {
      _videoController = VideoPlayerController.file(File(file.path));

      _videoController.initialize().then((value) {
        _videoController.play();
        _videoController.setLooping(true);
      });

      setState(() {
        vidoeFile = File(file.path);
      });

      widget.request.addAll({"video": vidoeFile});

      if (widget.callbackUploadVideo != null) {
        widget.callbackUploadVideo!(file.path);
      }
    }
  }

  @override
  void dispose() {
    if (vidoeFile != null || widget.video != null) {
      _videoController.dispose();
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final border = OutlineInputBorder(
        borderSide: BorderSide(width: 1, color: Colors.black.withOpacity(0.6)));

    final images = widget.productController!.images;
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Spacing.s),
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
              padding: EdgeInsets.all(8.0), child: Text("Product Image")),
          Container(
            height: 150,
            alignment: Alignment.center,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: images.length + 1,
              itemBuilder: (context, index) {
                if (index == images.length || images.isEmpty) {
                  if (images.length < 5) {
                    //when no image
                    return GestureDetector(
                      onTap: widget.productController?.uploadImage,
                      child: Container(
                          height: 150,
                          width: 150,
                          clipBehavior: Clip.antiAlias,
                          padding: const EdgeInsets.all(16),
                          decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.circular(Spacing.normal),
                          ),
                          child: images.isNotEmpty &&
                                  index == widget.productImages!.length
                              ? const Icon(Icons.add, size: 40)
                              : const Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(CupertinoIcons.camera_fill,
                                        color: Colors.grey),
                                    SizedBox(height: Spacing.s),
                                    Text(
                                      'Click here to upload',
                                      textAlign: TextAlign.center,
                                    )
                                  ],
                                )),
                    );
                  } else {
                    return const SizedBox();
                  }
                }

                final productImage = images[index];
                final isWorngAspect =
                    productImage.aspect != 1 && productImage.type == 1;

                return GestureDetector(
                  onTap: productImage.type == 0
                      ? null
                      : () {
                          final imagePath = productImage.photo;
                          if (imagePath != null) {
                            widget.productController?.cropImages(imagePath);
                          }
                        },
                  child: Stack(
                    children: [
                      Container(
                        height: 150,
                        width: 150,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Spacing.normal),
                          border: isWorngAspect
                              ? Border.all(width: 2, color: Colors.red)
                              : Border.all(width: 1, color: Colors.grey),
                          image: productImage.type == 0
                              ? DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(images[index].photo!),
                                )
                              : DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(File(images[index].photo!)),
                                ),
                        ),
                      ),
                      if (isWorngAspect)
                        const Positioned(
                            top: 4,
                            left: 4,
                            child: Icon(
                              Icons.warning_rounded,
                              color: Colors.red,
                              size: 32,
                            )),
                      Positioned(
                        top: Spacing.xs,
                        right: Spacing.xs,
                        child: InkWell(
                          onTap: () {
                            // imageListPath.remove(imageListPath[index]);
                            images.remove(images[index]);
                            setState(() {});
                          },
                          child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black.withOpacity(0.4)),
                              child: const Icon(
                                Icons.close,
                                color: Colors.white,
                              )),
                        ),
                      )
                    ],
                  ),
                );
              },
              separatorBuilder: (context, index) =>
                  const SizedBox(width: Spacing.normal),
            ),
          ),
          const SizedBox(height: Spacing.s),
          const Padding(
              padding: EdgeInsets.all(8.0), child: Text("Product Video")),
          Align(
            alignment:
                vidoeFile != null ? Alignment.center : Alignment.centerLeft,
            child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                child: Column(
                  children: [
                    Align(
                      alignment: vidoeFile != null
                          ? Alignment.center
                          : Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: vidoeFile != null || widget.video != null
                            ? () {
                                if (_videoController.value.isPlaying) {
                                  _videoController.pause();
                                  isPause = true;
                                } else {
                                  _videoController.play();
                                  isPause = false;
                                }

                                setState(() {});
                              }
                            : uploadVideo,
                        child: Container(
                          constraints: vidoeFile != null || widget.video != null
                              ? const BoxConstraints(
                                  minWidth: 200, minHeight: 200)
                              : const BoxConstraints(
                                  maxHeight: 150, minHeight: 150),
                          clipBehavior: Clip.antiAlias,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.grey.shade200,
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.circular(Spacing.normal),
                          ),
                          child: vidoeFile != null || widget.video != null
                              ? Stack(
                                  children: [
                                    AspectRatio(
                                        aspectRatio:
                                            _videoController.value.aspectRatio,
                                        child: VideoPlayer(_videoController)),
                                    if (isPause)
                                      Positioned(
                                        top: 80,
                                        bottom: 80,
                                        left: 100,
                                        right: 100,
                                        child: Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                              color:
                                                  Colors.black.withOpacity(0.4),
                                              shape: BoxShape.circle),
                                          child: const Icon(
                                            Icons.play_arrow_rounded,
                                            color: Colors.white,
                                          ),
                                        ),
                                      )
                                  ],
                                )
                              : const Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.videocam_rounded,
                                      size: 40,
                                      color: Colors.grey,
                                    ),
                                    SizedBox(height: Spacing.xs),
                                    SizedBox(
                                      width: 120,
                                      child: Text(
                                        "Click here for upload",
                                        textAlign: TextAlign.center,
                                      ),
                                    )
                                  ],
                                ),
                        ),
                      ),
                    ),
                    if (vidoeFile != null || widget.video != null) ...[
                      const SizedBox(height: Spacing.m),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              uploadVideo();
                            },
                            child: const Text(
                              "Change",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue),
                            ),
                          ),
                          InkWell(
                            onTap: () {},
                            child: const Icon(
                              Icons.fullscreen,
                              color: Colors.blue,
                            ),
                          )
                        ],
                      ),
                    ]
                  ],
                )),
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              hintText: "Product Title",
              border: border,
              focusedBorder: border,
              enabledBorder: border,
              onChange: (value) {
                widget.request.addAll({"title": value});
                widget.requestTest?.title = value;

                if (widget.callback != null) {}
              },
            ),
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              border: border,
              focusedBorder: border,
              enabledBorder: border,
              hintText: "Secondary Title",
              onChange: (text) {
                widget.request.addAll({"secondTitle": text});
                widget.requestTest?.secondaryTitle = text;
              },
            ),
          ),
          const SizedBox(height: Spacing.normal),
          SizedBox(
            height: 45,
            child: CusTextField(
              border: border,
              focusedBorder: border,
              enabledBorder: border,
              onChange: (value) {
                widget.request.addAll({"brand": value});
                widget.requestTest?.brand = value;
              },
              hintText: "Brand name",
            ),
          ),
          const SizedBox(height: Spacing.normal),
          Row(
            children: [
              Expanded(
                  flex: 2,
                  child: SizedBox(
                    height: 45,
                    child: CusTextField(
                        onTap: () {
                          openBottomSheetModal(
                            itemList: widget
                                .viewController?.productDetail!.categories,
                          );
                        },
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        hintText: "Select Category",
                        labelText: "Product Category",
                        border: border,
                        readOnly: true,
                        focusedBorder: border,
                        enabledBorder: border,
                        suffixIcon: const Icon(Icons.arrow_drop_down)),
                  )),
              const SizedBox(width: Spacing.m),
              Expanded(
                  flex: 1,
                  child: SizedBox(
                    height: 45,
                    child: CusTextField(
                        onTap: () => openBottomSheetModal(
                            isUnitType: true,
                            unitList: widget
                                .viewController?.productDetail?.unit_list),
                        // controller: widget.unitTypeController!,
                        contentPadding: const EdgeInsets.all(Spacing.s),
                        labelText: "Unit Type",
                        hintText: "Select Unit Type",
                        border: border,
                        readOnly: true,
                        focusedBorder: border,
                        enabledBorder: border,
                        suffixIcon: const Icon(Icons.arrow_drop_down)),
                  ))
            ],
          ),
        ],
      ),
    );
  }
}

class SelectedOptionType {
  final String id;
  final String title;

  SelectedOptionType({
    required this.id,
    required this.title,
  });
}

class UploadImages {
  final List<ProductImages>? images;
  final ProductImages? image;

  UploadImages({this.images, this.image});
}
