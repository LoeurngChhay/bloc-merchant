String deltaToHtml(List<Map<String, dynamic>> delta) {
  final StringBuffer htmlBuffer = StringBuffer();
  bool inOrderedList = false;
  bool inBulletList = false;

  for (var op in delta) {
    if (op.containsKey('insert')) {
      String text = op['insert'] ?? '';

      // Handle images
      if (text.startsWith('http') &&
          (text.endsWith('.png') ||
              text.endsWith('.jpg') ||
              text.endsWith('.jpeg'))) {
        htmlBuffer.write('<img src="$text" alt="Image"/>');
        continue;
      }

      // Handle newlines
      text = text.replaceAll('\n', '<br/>');

      htmlBuffer.write(text);
    }

    // Handle attributes
    if (op.containsKey('attributes')) {
      final attributes = op['attributes'];

      // Start of formatting tags
      if (attributes.containsKey('bold') && attributes['bold'] == true) {
        htmlBuffer.write('<b>');
      }
      if (attributes.containsKey('italic') && attributes['italic'] == true) {
        htmlBuffer.write('<i>');
      }
      if (attributes.containsKey('underline') &&
          attributes['underline'] == true) {
        htmlBuffer.write('<u>');
      }

      // Lists
      if (attributes.containsKey('list')) {
        if (attributes['list'] == 'ordered') {
          if (!inOrderedList) {
            htmlBuffer.write('<ol>');
            inOrderedList = true;
          }
          htmlBuffer.write('<li>');
        } else if (attributes['list'] == 'bullet') {
          if (!inBulletList) {
            htmlBuffer.write('<ul>');
            inBulletList = true;
          }
          htmlBuffer.write('<li>');
        }
      }
    }

    // End of formatting tags
    if (op.containsKey('attributes')) {
      final attributes = op['attributes'];

      // End of formatting tags
      if (attributes.containsKey('bold') && attributes['bold'] == true) {
        htmlBuffer.write('</b>');
      }
      if (attributes.containsKey('italic') && attributes['italic'] == true) {
        htmlBuffer.write('</i>');
      }
      if (attributes.containsKey('underline') &&
          attributes['underline'] == true) {
        htmlBuffer.write('</u>');
      }

      // End of lists
      if (attributes.containsKey('list')) {
        if (attributes['list'] == 'ordered') {
          htmlBuffer.write('</li>');
        } else if (attributes['list'] == 'bullet') {
          htmlBuffer.write('</li>');
        }
      }
    }
  }

  // Close any open lists
  if (inOrderedList) {
    htmlBuffer.write('</ol>');
  }
  if (inBulletList) {
    htmlBuffer.write('</ul>');
  }

  return htmlBuffer.toString();
}
