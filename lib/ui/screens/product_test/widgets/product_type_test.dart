import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_radio_tile.dart';
import 'package:flutter/material.dart';
import 'package:group_button/group_button.dart';

class ProductTypeTest extends StatefulWidget {
  final Function(int selectedIndex)? onProductCallBack;
  final Function(int selectedIndex)? onSaleCallBack;
  final GroupButtonController productTypeController;
  final GroupButtonController saleTypeController;

  const ProductTypeTest({
    super.key,
    this.onProductCallBack,
    this.onSaleCallBack,
    required this.productTypeController,
    required this.saleTypeController,
  });

  @override
  State<ProductTypeTest> createState() => _ProductTypeTestState();
}

class _ProductTypeTestState extends State<ProductTypeTest> {
  final optionButtons = ["Standard", "Topping"];
  final saleTypeButtons = ["For Sale", "Only Topping"];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(Spacing.s),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Spacing.s),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Product Type",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: Spacing.s),
          Row(
            children: [
              const Text('Item Type'),
              const SizedBox(width: Spacing.l),
              Expanded(
                child: GroupButton(
                  controller: widget.productTypeController,
                  buttons: optionButtons,
                  buttonIndexedBuilder: (selected, index, context) {
                    return CusRadioTile(
                      isSelected:
                          widget.productTypeController.selectedIndex == index,
                      title: optionButtons[index],
                      groupValue: widget.productTypeController.selectedIndex,
                      value: index,
                      onTap: () {
                        if (widget.onProductCallBack != null) {
                          widget.onProductCallBack!(index);
                          setState(() {});
                        }
                      },
                    );
                  },
                  options: const GroupButtonOptions(
                    groupingType: GroupingType.wrap,
                    direction: Axis.horizontal,
                    mainGroupAlignment: MainGroupAlignment.start,
                    crossGroupAlignment: CrossGroupAlignment.start,
                    groupRunAlignment: GroupRunAlignment.start,
                    textPadding: EdgeInsets.all(0),
                    elevation: 1,
                  ),
                  isRadio: true,
                  enableDeselect: false,
                ),
              )
            ],
          ),
          const SizedBox(height: Spacing.s),
          Visibility(
            visible: widget.productTypeController.selectedIndex == 1,
            child: Row(
              children: [
                const Text('Sale Type'),
                const SizedBox(width: Spacing.l),
                Expanded(
                  child: GroupButton(
                    controller: widget.saleTypeController,
                    buttons: saleTypeButtons,
                    buttonIndexedBuilder: (selected, index, context) {
                      return CusRadioTile(
                        isSelected:
                            widget.saleTypeController.selectedIndex == index,
                        title: saleTypeButtons[index],
                        groupValue: widget.saleTypeController.selectedIndex,
                        value: index,
                        onTap: () {
                          // unFocus();

                          if (widget.onSaleCallBack != null) {
                            widget.onSaleCallBack!(index);
                            setState(() {});
                          }
                        },
                      );
                    },
                    options: const GroupButtonOptions(
                      groupingType: GroupingType.wrap,
                      direction: Axis.horizontal,
                      mainGroupAlignment: MainGroupAlignment.start,
                      crossGroupAlignment: CrossGroupAlignment.start,
                      groupRunAlignment: GroupRunAlignment.start,
                      textPadding: EdgeInsets.all(0),
                      elevation: 1,
                    ),
                    isRadio: true,
                    enableDeselect: false,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
