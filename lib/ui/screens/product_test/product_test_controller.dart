import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/product_image.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product_test/product_test.dart';
import 'package:bloc_merchant_mobile_2/utils/image_utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProductTestViewController extends ChangeNotifier {
  List<ProductImages> images = [];
  List<File> imageFiles = [];

  Future<void> createProduct(
      Map<String, dynamic> req, CreateProductRequestTest reuestTest) async {
    print('req map :$req');
    print('--------------------');
    print("req tes  : $reuestTest");
  }

  Future<void> cropImages(String imagePath) async {
    final cropFile = await ImageUtils.cropImage(pickedFile: File(imagePath));

    final findIndex = images.indexWhere((e) => e.photo == imagePath);
    final findFileIndex = imageFiles.indexWhere((e) => e.path == imagePath);

    if (cropFile != null) {
      final aspectRatio = await ImageUtils.aspectRatio(File(cropFile.path));
      if (findIndex != -1) {
        images[findIndex] = ProductImages(
            photo: cropFile.path,
            type: 1,
            name: cropFile.path.split('/').last,
            aspect: aspectRatio);
      }

      if (findFileIndex != -1) {
        imageFiles[findFileIndex] = File(cropFile.path);
      }
      notifyListeners();
    }
  }

  Future<void> uploadImage() async {
    final ImagePicker picker = ImagePicker();
    List<XFile> getImages = [];

    if (images.length == 4) {
      //last image when select single image
      final lastImage = await picker.pickImage(source: ImageSource.gallery);

      if (lastImage != null) {
        getImages.add(lastImage);
      }
    } else {
      //remove limit 5
      getImages = await picker.pickMultiImage();
    }

    // crop image
    if (getImages.isNotEmpty && images.length <= 1) {
      await ImageUtils.cropImage(pickedFile: File(getImages[0].path))
          .then((cropImage) {
        getImages[0] = XFile(cropImage!.path);
      });
    }

    //format photo info
    for (var e in getImages) {
      final aspectRatio = await ImageUtils.aspectRatio(File(e.path));

      images.add(
        ProductImages(
            photo: e.path,
            type: 1,
            name: e.path.split('/').last,
            aspect: aspectRatio),
      );

      imageFiles.add(File(e.path));
      notifyListeners();
    }
  }
}
