import 'dart:io';

import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:bloc_merchant_mobile_2/utils/video_utils.dart';
import 'package:flutter/material.dart';
import 'package:video_editor/video_editor.dart';

class AdsPhotoThumbnailDialog extends StatefulWidget {
  final Function? onSelect;
  final File? file;

  AdsPhotoThumbnailDialog({this.onSelect, this.file, Key? key})
      : super(key: key);

  @override
  AdsPhotoThumbnailDialogState createState() => AdsPhotoThumbnailDialogState();
}

class AdsPhotoThumbnailDialogState extends State<AdsPhotoThumbnailDialog> {
  Size? screenSize;
  late final VideoEditorController videoController = VideoEditorController.file(
    widget.file!,
    minDuration: const Duration(seconds: 0),
    maxDuration: const Duration(seconds: 60),
  );
  final double height = 80;
  bool isLoading = false;
  // StatusType? statustype;

  @override
  void initState() {
    super.initState();

    videoController
        .initialize()
        .then((_) => setState(() {
              // statustype = StatusType.SUCCESS;
              setState(() {
                isLoading = false;
              });
            }))
        .catchError((error) {
      // handle minumum duration bigger than video duration error
      Navigator.pop(context);
    }, test: (e) => e is VideoMinDurationError);
  }

  @override
  void dispose() async {
    videoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text("Select Video Cover"),
          actions: [
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10, right: 10),
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue,
                    alignment: Alignment.center,
                  ),
                  onPressed: () async {
                    final config =
                        CoverFFmpegVideoEditorConfig(videoController);
                    final execute = await config.getExecuteConfig();
                    if (execute == null) {
                      DialogUtil.cusAlertDialog(
                          context: context,
                          message:
                              "Error on cover exportation initialization.");
                      return;
                    }

                    await VideoUtils.runFFmpegCommand(
                      execute,
                      onError: (e, s) => DialogUtil.cusAlertDialog(
                          context: context,
                          message: "Error on cover exportation :"),
                      onCompleted: (cover) {
                        if (!mounted) return;
                        widget.onSelect?.call(cover);
                        Navigator.pop(context);
                      },
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    child: const Text('SELECT',
                        style: TextStyle(fontSize: 14, color: Colors.white),
                        textAlign: TextAlign.center),
                  )),
            )
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: !isLoading && videoController.initialized
                  ? Container(
                      color: Colors.black,
                      child: CoverViewer(controller: videoController))
                  : const CircularProgressIndicator(),
            ),
            Container(
                padding: const EdgeInsets.all(Spacing.normal),
                height: MediaQuery.of(context).size.height * 0.2,
                color: Colors.black,
                alignment: Alignment.center,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    if (!isLoading && videoController.initialized)
                      Align(
                        alignment: Alignment.topLeft,
                        child: CoverSelection(
                          controller: videoController,
                          size: height,
                          quantity: videoController.videoDuration.inSeconds * 5,
                          selectedCoverBuilder: (cover, size) {
                            return Stack(
                              alignment: Alignment.center,
                              children: [
                                cover,
                                const Icon(Icons.check_circle,
                                    color: Colors.white)
                              ],
                            );
                          },
                        ),
                      ),
                  ],
                ))
          ],
        ));
  }
}
