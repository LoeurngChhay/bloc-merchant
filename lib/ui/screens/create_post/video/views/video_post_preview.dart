import 'package:bloc_merchant_mobile_2/ui/screens/create_post/image/create_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/create_video_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:video_editor/video_editor.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector_v2/focus_detector_v2.dart';

class VideoPostPreview extends StatefulWidget {
  static const String routeName = '/post-video-preview';

  final CreatePostArgument? args;

  const VideoPostPreview({super.key, this.args});

  @override
  State<VideoPostPreview> createState() => _VideoPostPreviewState();
}

class _VideoPostPreviewState extends State<VideoPostPreview> {
  late final VideoEditorController _videoController =
      VideoEditorController.file(
    widget.args!.video!,
    minDuration: const Duration(seconds: 0),
    maxDuration: const Duration(seconds: 60),
  );
  TextEditingController captureCtrl = TextEditingController();

  initVideo() {
    if (widget.args?.video == null) return;
    _videoController.initialize().then((value) {
      _videoController.video.play();

      setState(() {
        _videoController.maxDuration = const Duration(seconds: 60);
      });
    }).catchError((error) {
      // DialogUtil.cusAlertDialog(context: context, message: error.toString());
    });
  }

  String formatter(Duration duration) => [
        duration.inMinutes.remainder(60).toString().padLeft(2, '0'),
        duration.inSeconds.remainder(60).toString().padLeft(2, '0')
      ].join(":");

  @override
  void initState() {
    initVideo();
    super.initState();
  }

  @override
  void dispose() {
    _videoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      onVisibilityLost: () {
        if (_videoController.isPlaying) {
          _videoController.video.pause();
        }
      },
      child: Scaffold(
          extendBodyBehindAppBar: true,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            leading: const CusBackButton(),
            centerTitle: true,
            title: Container(
              padding: const EdgeInsets.all(Spacing.s),
              decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.circular(Spacing.m)),
              child: Text(
                "Video Preview",
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
              ),
            ),
            actions: [
              FilledButton(
                  onPressed: () async {
                    Navigator.pushNamed(context, CreateVideoPostView.routeName,
                        arguments: CreatePostArgument(
                            images: [], video: widget.args?.video));

                    // if (result is PopWithResults) {
                    //   PopWithResults popResult = result;

                    //   if (popResult.toPage == VideoPostPreview.routeName) {
                    //     print('result : ${popResult.toPage}');

                    //     // TODO do stuff
                    //   } else {
                    //     // pop to previous page

                    //     Navigator.pop(context, result);
                    //   }
                    // }
                  },
                  style:
                      FilledButton.styleFrom(padding: const EdgeInsets.all(0)),
                  child: const Text("Next")),
              const SizedBox(width: Spacing.s)
            ],
          ),
          body: _videoController.initialized
              ? Container(
                  color: Colors.black,
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Expanded(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Stack(
                            children: [
                              Positioned.fill(
                                child: CropGridViewer.preview(
                                    controller: _videoController),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: AnimatedBuilder(
                                  animation: _videoController.video,
                                  builder: (context, child) => AnimatedOpacity(
                                    opacity: _videoController.isPlaying ? 0 : 1,
                                    duration: const Duration(milliseconds: 100),
                                    child: Container(
                                      padding: const EdgeInsets.all(Spacing.xs),
                                      decoration: const BoxDecoration(
                                          color: Colors.white,
                                          shape: BoxShape.circle),
                                      child: const Icon(
                                        Icons.play_arrow_rounded,
                                        color: Colors.black,
                                        size: 28,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      AnimatedBuilder(
                          animation: _videoController.video,
                          builder: (context, child) {
                            final int duration =
                                _videoController.videoDuration.inSeconds;
                            final double pos =
                                _videoController.trimPosition * duration;

                            return Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.all(Spacing.m),
                                  child: Row(
                                    children: [
                                      Text(
                                        formatter(
                                            Duration(seconds: pos.toInt())),
                                        style: const TextStyle(
                                            // fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      const Spacer(),
                                      AnimatedOpacity(
                                          opacity: _videoController.isTrimming
                                              ? 1
                                              : 0,
                                          duration: kThemeAnimationDuration,
                                          child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Text(
                                                  formatter(_videoController
                                                      .startTrim),
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                                const SizedBox(width: 6),
                                                const Text('/',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                const SizedBox(width: 6),
                                                Text(
                                                  formatter(
                                                      _videoController.endTrim),
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ]))
                                    ],
                                  ),
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                        bottom: MediaQuery.of(context)
                                                    .padding
                                                    .bottom !=
                                                0
                                            ? MediaQuery.of(context)
                                                .padding
                                                .bottom
                                            : Spacing.normal),
                                    child: TrimSlider(
                                        controller: _videoController,
                                        height: 60,
                                        horizontalMargin: 60 / 4,
                                        child: TrimTimeline(
                                          textStyle: const TextStyle(
                                              fontSize: 10,
                                              color: Colors.white),
                                          controller: _videoController,
                                          padding:
                                              const EdgeInsets.only(top: 10),
                                        )))
                              ],
                            );
                          })
                    ],
                  ),
                )
              : const Center(
                  child: CircularProgressIndicator(),
                )),
    );
  }
}
