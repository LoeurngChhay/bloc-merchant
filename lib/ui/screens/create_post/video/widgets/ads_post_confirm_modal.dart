import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AdsPostComfirmModal extends StatelessWidget {
  final String? content;
  final Product? product;
  final Function()? onConfirm;

  const AdsPostComfirmModal(
      {super.key, this.content, this.onConfirm, this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      alignment: Alignment.center,
      padding: const EdgeInsets.all(Spacing.normal),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(Spacing.m)),
      child: Column(
        children: [
          Container(
            height: 40,
            alignment: Alignment.centerLeft,
            child: const Text("Confirm Post",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ),
          Expanded(
              child: Container(
                  alignment: Alignment.centerLeft,
                  child: CustomScrollView(
                    slivers: [
                      SliverList(
                          delegate: SliverChildListDelegate([
                        Container(
                          alignment: Alignment.centerLeft,
                          constraints: const BoxConstraints(
                            minHeight: 60,
                          ),
                          // ignore: prefer_is_empty
                          child: content!.trim().length <= 0
                              ? Text('! No content',
                                  style: TextStyle(
                                      color: Colors.orange.shade200,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20))
                              : Text(
                                  content!,
                                  style: const TextStyle(
                                      fontSize: 18, color: Colors.black),
                                ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          constraints: const BoxConstraints(
                            minHeight: 60,
                          ),
                          margin: const EdgeInsets.only(top: 5, bottom: 5),
                          child: product == null
                              ? const Text('! No selected product',
                                  style: TextStyle(
                                      color: Colors.orange,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20))
                              : Container(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 60,
                                          height: 60,
                                          alignment: Alignment.centerLeft,
                                          margin: const EdgeInsets.only(
                                              right: 5, top: 10),
                                          child: product != null &&
                                                  product!.thumb != ""
                                              ? CachedNetworkImage(
                                                  imageUrl: product!.thumb!,
                                                  placeholder: (context, url) =>
                                                      const CircularProgressIndicator(),
                                                  errorWidget: (context, url,
                                                          error) =>
                                                      Image.asset(
                                                          'assets/images/no_img.jpeg'),
                                                  fit: BoxFit.cover,
                                                  height: 60,
                                                  width: 60,
                                                )
                                              : Image.asset(
                                                  "assets/images/no_img.jpeg"),
                                        ),
                                        Expanded(
                                          child: Container(
                                            alignment: Alignment.topLeft,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  height: 30,
                                                  child: Text(
                                                      "ID: ${product!.product_id}",
                                                      style: const TextStyle(
                                                          fontSize: 15,
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                ),
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  margin: const EdgeInsets.only(
                                                      right: 25),
                                                  child: Text(
                                                      "${product!.title}",
                                                      style: const TextStyle(
                                                          fontSize: 15)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ])),
                        ),
                      ]))
                    ],
                  ))),
          Container(
              height: 80,
              margin: const EdgeInsets.only(bottom: Spacing.l),
              padding: const EdgeInsets.all(Spacing.normal),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                        child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        side: const BorderSide(color: Colors.red),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Cancel",
                        style: TextStyle(color: Colors.red),
                      ),
                    )),
                    const SizedBox(width: Spacing.normal),

                    Expanded(
                        child: FilledButton.icon(
                            onPressed: onConfirm,
                            icon: const Icon(Icons.check),
                            label: const Text("Confirm")))
                    // Expanded(
                    //   child: FilledButton(
                    //       style: FilledButton.styleFrom(
                    //           alignment: Alignment.center),
                    //       onPressed: () async {
                    //         Navigator.pop(context);
                    //         onConfirm?.call();
                    //       },
                    //       child: Container(
                    //           alignment: Alignment.center,
                    //           width: 100,
                    //           height: 40,
                    //           child: Container(
                    //             alignment: Alignment.center,
                    //             child: const Row(
                    //               crossAxisAlignment: CrossAxisAlignment.center,
                    //               mainAxisAlignment: MainAxisAlignment.center,
                    //               children: [
                    //                 Icon(Icons.check, size: 24),
                    //                 Text(' Confirm',
                    //                     style: TextStyle(fontSize: 18)),
                    //               ],
                    //             ),
                    //           ))),
                    // ),
                  ]))
        ],
      ),
    );
  }
}
