import 'dart:io';

import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_video.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/ads/widgets/selected_sale_product.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/image/create_post_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/create_video_post_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/views/generate_video_thumnial.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/views/video_post_preview.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/video/widgets/ads_post_confirm_modal.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class CreateVideoPostView extends StatefulWidget {
  static const String routeName = '/create-video-post';
  final CreatePostArgument args;

  const CreateVideoPostView({super.key, required this.args});

  @override
  State<CreateVideoPostView> createState() => _CreateVideoPostViewState();
}

class _CreateVideoPostViewState extends State<CreateVideoPostView> {
  Size? screenSize;
  late VideoPlayerController playerController =
      VideoPlayerController.file(widget.args.video!);
  File? fileImage;
  Size fileDimension = Size.zero;
  String fileMbSize = "";

  //form
  GlobalKey<FormState> pformKey = GlobalKey<FormState>();

  // ProductEntity? product;
  bool? isLoading = true;
  bool loadingAction = false;
  final titleCtrl = TextEditingController();
  Product? selectedProduct;

  @override
  void initState() {
    playerController.initialize().then((_) {
      fileDimension = playerController.value.size;
      setState(() {
        // fileMbSize = videoMBSize(widget.video!);
        isLoading = false;
      });
      playerController.pause();
      playerController.setLooping(true);
    });

    super.initState();
  }

  void unFocus() {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  void showLoading() {
    showDialog(
      context: context,
      builder: (context) {
        return const AlertDialog.adaptive(
          content: SizedBox(
            width: 120,
            height: 120,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 60,
                  width: 60,
                  child: CircularProgressIndicator(strokeWidth: 5),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> uploadPost(CreateVideoPostViewController viewController) async {
    //check and verify
    showModalBottomSheet(
        context: context,
        shape:
            OutlineInputBorder(borderRadius: BorderRadius.circular(Spacing.m)),
        builder: (context) => AdsPostComfirmModal(
            content: titleCtrl.text,
            product: selectedProduct,
            onConfirm: () async {
              final request = CreatePostVideoRequest(
                productId: selectedProduct == null
                    ? "0"
                    : selectedProduct!.product_id.toString(),
                title: titleCtrl.text,
                video: widget.args.video,
                cover: fileImage,
              );

              viewController.createPost(request);
              showLoading();
            }));
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CreateVideoPostViewController>(
      create: (context) => CreateVideoPostViewController(
        adsRepo: locator<AdsRepo>(),
        onSuccess: (message) {
          print('onSuccess createPost : $message');
          // Navigator.popUntil(
          //   context,
          //   ModalRoute.withName(AdsView.routeName),
          // );

          Navigator.popUntil(
            context,
            (route) {
              final isRoute = route.settings.name == VideoPostPreview.routeName;

              if (isRoute) {
                Navigator.pop(context, AsyncStatus.success);
              }

              return isRoute;
            },
          );
        },
        onError: (message) {
          DialogUtil.cusAlertDialog(context: context, message: message);
        },
      ),
      child: Consumer<CreateVideoPostViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Preview"),
          ),
          body: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: ListView(
              children: [
                Container(
                    margin: const EdgeInsets.only(left: 7, right: 7, top: 5),
                    padding: const EdgeInsets.only(
                        left: 8, right: 8, bottom: 10, top: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    height: 200,
                    constraints: const BoxConstraints(
                      maxHeight: 200,
                    ),
                    child: isLoading == false
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                  alignment: Alignment.center,
                                  color: Colors.black,
                                  width: 200,
                                  constraints: const BoxConstraints(
                                    maxHeight: 200,
                                    maxWidth: 110,
                                  ),
                                  child: AspectRatio(
                                    aspectRatio: 3 / 4,
                                    child: VideoPlayer(playerController),
                                  )),
                              const SizedBox(width: 10),
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: BeveledRectangleBorder(
                                        side: const BorderSide(width: 0.1),
                                        borderRadius:
                                            BorderRadius.circular(Spacing.xs)),
                                    backgroundColor: Colors.white,
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.zero,
                                  ),
                                  onPressed: () async {
                                    unFocus();

                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AdsPhotoThumbnailDialog(
                                                  file: widget.args.video,
                                                  onSelect: (image) {
                                                    if (mounted &&
                                                        image != null) {
                                                      setState(() {
                                                        fileImage = image;
                                                      });
                                                    }
                                                  },
                                                )));
                                  },
                                  child: Container(
                                      alignment: Alignment.center,
                                      width: 100,
                                      constraints:
                                          const BoxConstraints(maxHeight: 200),
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          fileImage != null
                                              ? Container(
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: Colors.black,
                                                    image: DecorationImage(
                                                        image: FileImage(
                                                            fileImage!),
                                                        fit: BoxFit.fitWidth),
                                                  ),
                                                )
                                              : const Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Icon(Icons.add,
                                                        color: Colors.grey),
                                                    Text('Cover',
                                                        style: TextStyle(
                                                            fontSize: 18,
                                                            color:
                                                                Colors.grey)),
                                                    Text('(Optional))',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            color:
                                                                Colors.orange)),
                                                  ],
                                                )
                                        ],
                                      ))),
                            ],
                          )
                        : Container()),
                Container(
                  margin: const EdgeInsets.all(8),
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, bottom: 5, top: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: TextFormField(
                          // decoratedStyle: const TextStyle(color: Colors.blue, fontFamily: "Roboto", fontSize: 16, height: 1.5),
                          // basicStyle: const TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16, height: 1.5),
                          textAlignVertical: TextAlignVertical.center,
                          controller: titleCtrl,
                          maxLength: 250,
                          keyboardType: TextInputType.multiline,
                          maxLines: 4,
                          decoration: const InputDecoration(
                            hintText: "Share your Caption",
                            labelText: "Share your Caption",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle(color: Colors.grey),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFFE0E0E0), width: 0.1)),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                          ),
                        ),
                      ),
                      const SizedBox(height: Spacing.normal),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Sale Product',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              if (selectedProduct == null)
                                Text('(Optional)',
                                    style: TextStyle(
                                        color: Colors.grey.withOpacity(0.6)))
                            ],
                          ),
                          FilledButton(
                              onPressed: () async {
                                final getProduct = await Navigator.pushNamed(
                                    context, ProductView.routeName,
                                    arguments: ProductViewArgument(
                                        hasSelected: true)) as Product?;

                                if (getProduct != null) {
                                  setState(() {
                                    selectedProduct = getProduct;
                                  });
                                }
                              },
                              child: const Text("Select")),
                        ],
                      ),
                      if (selectedProduct != null)
                        SelectedSaleProduct(product: selectedProduct!)
                    ],
                  ),
                ),
                Container(
                    margin: const EdgeInsets.symmetric(horizontal: 8),
                    padding: const EdgeInsets.only(
                        left: 8, bottom: 10, top: 5, right: 8),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            height: 20,
                            child: const Text('Schedule',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            height: 40,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                    width: 25,
                                    alignment: Alignment.centerLeft,
                                    child: const Icon(Icons.access_time,
                                        size: 20)),
                                Container(
                                    width: 100,
                                    alignment: Alignment.centerLeft,
                                    child: const Text("Release now",
                                        style: TextStyle(fontSize: 16))),
                                Expanded(child: Container())
                              ],
                            ),
                          )
                        ])),
              ],
            ),
          ),
          bottomNavigationBar: Container(
            color: Colors.black,
            padding: EdgeInsets.only(
                left: Spacing.m,
                right: Spacing.m,
                bottom: MediaQuery.of(context).viewPadding.bottom),
            height: 60 + MediaQuery.of(context).viewPadding.bottom,
            child: Row(
              children: [
                Expanded(
                  child: FilledButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style:
                          FilledButton.styleFrom(backgroundColor: Colors.white),
                      child: const Text(
                        "Back",
                        style: TextStyle(color: Colors.black),
                      )),
                ),
                const SizedBox(width: Spacing.normal),
                Expanded(
                  child: FilledButton.icon(
                      onPressed: () {
                        uploadPost(viewController);
                      },
                      icon: viewController.loading
                          ? const SizedBox(
                              height: 18,
                              width: 18,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            )
                          : const Icon(Icons.cloud_upload_outlined, size: 24),
                      label: const Text(
                        "Post",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
