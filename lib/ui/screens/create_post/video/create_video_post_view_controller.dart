import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_video.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class CreateVideoPostViewController extends ChangeNotifier {
  final AdsRepo adsRepo;
  final Function(String message)? onSuccess;
  final Function(String message)? onError;
  final Function()? onLoading;

  CreateVideoPostViewController(
      {required this.adsRepo, this.onSuccess, this.onError, this.onLoading});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  Future<void> createPost(CreatePostVideoRequest request) async {
    loading = true;
    if (onLoading != null) onLoading!();

    try {
      final res = await adsRepo.createPostVideo(request);
      if (onSuccess != null) {
        onSuccess!(res.message);
      }
    } catch (e) {
      print('Error Create Post : $e');
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loading = false;
    }
  }
}
