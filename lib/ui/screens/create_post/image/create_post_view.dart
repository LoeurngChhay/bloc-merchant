import 'dart:io';

import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_photo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';

import 'package:bloc_merchant_mobile_2/ui/screens/ads/widgets/selected_sale_product.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_post/image/create_post_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/product/product_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/product_list_res.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class CreatePostArgument {
  final List<String> images;
  final File? video;

  CreatePostArgument({required this.images, this.video});
}

class CreatePostView extends StatefulWidget {
  static const String routeName = '/create-image-post';
  final CreatePostArgument args;

  const CreatePostView({
    super.key,
    required this.args,
  });

  @override
  State<CreatePostView> createState() => _CreatePostViewState();
}

class _CreatePostViewState extends State<CreatePostView> {
  final ImagePicker _picker = ImagePicker();
  List<String> images = [];
  Product? selectedProduct;
  final _captionController = TextEditingController();

  initValue() {
    // print('widget.args.images : ${widget.args.images}');
    if (widget.args.images.isNotEmpty) {
      setState(() {
        images = widget.args.images;
      });
    }
  }

  @override
  void initState() {
    initValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CreatePostViewController(
          adsRepo: locator<AdsRepo>(),
          onSucces: (message) {
            Navigator.pop(context, AsyncStatus.success);
          }),
      child: Consumer<CreatePostViewController>(
          builder: (context, veiwController, child) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Post Photo"),
          ),
          body: SingleChildScrollView(
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: Padding(
                padding: const EdgeInsets.all(Spacing.m),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: (MediaQuery.of(context).size.width / 3) * 2,
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 1,
                                  color: Colors.black.withOpacity(0.2)))),
                      child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                childAspectRatio: 1,
                                mainAxisSpacing: Spacing.m,
                                crossAxisSpacing: Spacing.m,
                                crossAxisCount: 3),
                        itemCount: images.length + 1,
                        itemBuilder: (context, index) {
                          if (images.length == index) {
                            if (images.length < 5) {
                              return GestureDetector(
                                onTap: () async {
                                  List<XFile> getImgList = [];
                                  List<String> getPathList = [];

                                  if (5 - images.length == 1) {
                                    final file = await _picker.pickImage(
                                        source: ImageSource.gallery);
                                    if (file != null) {
                                      getImgList.add(file);
                                      getPathList.add(file.path);
                                    }
                                  } else {
                                    //remove limit 5
                                    getImgList = await _picker.pickMultiImage();

                                    getPathList =
                                        getImgList.map((e) => e.path).toList();
                                  }

                                  if (getPathList.isNotEmpty) {
                                    images.addAll(getPathList);
                                  }

                                  setState(() {});
                                },
                                child: Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.1),
                                      borderRadius:
                                          BorderRadius.circular(Spacing.m)),
                                  child: const Icon(Icons.add, size: 32),
                                ),
                              );
                            } else {
                              return const SizedBox();
                            }
                          }

                          return Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(Spacing.m),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage(images[index]),
                                  ),
                                ),
                              ),
                              Positioned(
                                  top: Spacing.xs,
                                  right: Spacing.xs,
                                  child: SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: IconButton(
                                      style: IconButton.styleFrom(
                                          padding: const EdgeInsets.all(0),
                                          backgroundColor:
                                              Colors.black.withOpacity(0.5)),
                                      onPressed: () {
                                        images.remove(images[index]);
                                        setState(() {});
                                      },
                                      icon: const Icon(Icons.close_rounded,
                                          color: Colors.white),
                                    ),
                                  ))
                            ],
                          );
                        },
                      ),
                    ),
                    const SizedBox(height: Spacing.m),
                    Container(
                      padding: const EdgeInsets.all(Spacing.s),
                      decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.background,
                          borderRadius: BorderRadius.circular(Spacing.m)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            controller: _captionController,
                            maxLength: 250,
                            maxLines: 4,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(16),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              labelText: 'Share your caption',
                            ),
                          ),
                          const SizedBox(height: Spacing.normal),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Sale Product',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  if (selectedProduct == null)
                                    Text('(Optional)',
                                        style: TextStyle(
                                            color:
                                                Colors.grey.withOpacity(0.6)))
                                ],
                              ),
                              FilledButton(
                                  onPressed: () async {
                                    final getProduct =
                                        await Navigator.pushNamed(
                                            context, ProductView.routeName,
                                            arguments: ProductViewArgument(
                                                hasSelected: true)) as Product?;

                                    if (getProduct != null) {
                                      setState(() {
                                        selectedProduct = getProduct;
                                      });
                                    }
                                  },
                                  child: const Text("Select")),
                            ],
                          ),
                          if (selectedProduct != null)
                            SelectedSaleProduct(product: selectedProduct!)
                        ],
                      ),
                    ),
                    const SizedBox(height: Spacing.m),
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(Spacing.s),
                      decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.background,
                          borderRadius: BorderRadius.circular(Spacing.m)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Schedule",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: Spacing.m),
                          Row(
                            children: [
                              const Icon(CupertinoIcons.time),
                              const SizedBox(width: Spacing.s),
                              Text(
                                "Release now",
                                style: Theme.of(context).textTheme.titleMedium,
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          bottomNavigationBar: Container(
            color: Colors.black,
            padding: EdgeInsets.only(
                left: Spacing.m,
                right: Spacing.m,
                bottom: MediaQuery.of(context).viewPadding.bottom),
            height: 60 + MediaQuery.of(context).viewPadding.bottom,
            child: Row(
              children: [
                Expanded(
                  child: FilledButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style:
                          FilledButton.styleFrom(backgroundColor: Colors.white),
                      child: const Text(
                        "Back",
                        style: TextStyle(color: Colors.black),
                      )),
                ),
                const SizedBox(width: Spacing.normal),
                Expanded(
                  child: FilledButton.icon(
                      onPressed: () {
                        final request = CreatePostPhotoRequest(
                            title: _captionController.text,
                            productId: selectedProduct != null
                                ? selectedProduct!.product_id.toString()
                                : '0',
                            images: images);
                        veiwController.createPost(request);
                      },
                      icon: veiwController.loading
                          ? const SizedBox(
                              height: 18,
                              width: 18,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            )
                          : const Icon(Icons.cloud_upload_outlined, size: 24),
                      label: const Text(
                        "Post",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
