import 'package:bloc_merchant_mobile_2/data/models/requests/create_post_photo.dart';
import 'package:bloc_merchant_mobile_2/data/repos/ads_repo.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class CreatePostViewController extends ChangeNotifier {
  final AdsRepo adsRepo;
  final Function(String message)? onSucces;
  final Function(String message)? onError;

  CreatePostViewController(
      {required this.adsRepo, this.onSucces, this.onError});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  Future<void> createPost(CreatePostPhotoRequest request) async {
    try {
      loading = true;

      List<MultipartFile> multipartFile = [];

      for (var e in request.images) {
        multipartFile
            .add(await MultipartFile.fromFile(e, filename: e.split('/').last));
      }

      final res = await adsRepo.createPostPhoto(request);

      if (onSucces != null) {
        onSucces!(res.message);
      }
    } catch (e) {
      print('Error createPost : $e');
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loading = false;
    }
  }
}
