import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/order_history_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/views/history_list.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderHistoryView extends StatefulWidget {
  static const String routeName = '/order-history';
  const OrderHistoryView({super.key});

  @override
  State<OrderHistoryView> createState() => _OrderHistoryViewState();
}

class _OrderHistoryViewState extends State<OrderHistoryView>
    with SingleTickerProviderStateMixin {
  TabController? _tabBarController;
  OrderHistoryViewController? _viewController;

  @override
  void initState() {
    _tabBarController = TabController(length: 3, vsync: this);
    _tabBarController?.addListener(() {
      if (_tabBarController!.indexIsChanging && _viewController != null) {
        switch (_tabBarController?.index) {
          case 0:
            _viewController?.getOrderList(HistoryOrderState.process);
            break;
          case 1:
            _viewController?.getOrderList(HistoryOrderState.completed);
            break;
          case 2:
            _viewController?.getOrderList(HistoryOrderState.cancel);
            break;
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _tabBarController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) =>
          OrderHistoryViewController(orderRepo: locator<OrderRepo>())
            ..getOrderList(HistoryOrderState.process),
      child: Consumer<OrderHistoryViewController>(
          builder: (context, viewController, child) {
        _viewController = viewController;
        return DefaultTabController(
          initialIndex: 0,
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              leading: const CusBackButton(color: Colors.white),
              title: Text(
                LocaleKeys.o_history.tr(),
                style: const TextStyle(color: Colors.white),
              ),
              bottom: PreferredSize(
                preferredSize: const Size.fromHeight(100),
                child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.black,
                    ),
                    child: TabBar(
                      labelPadding: const EdgeInsets.all(12),
                      padding: const EdgeInsets.all(Spacing.s),
                      indicatorSize: TabBarIndicatorSize.tab,
                      isScrollable: false,
                      controller: _tabBarController,
                      dividerColor: Colors.transparent,
                      indicatorWeight: 0,
                      labelColor: Colors.white,
                      indicator: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(Spacing.s),
                        border: Border.all(color: Colors.grey, width: 0),
                      ),
                      tabs: [
                        Tab(
                          icon: const Icon(Icons.settings,
                              size: 30, color: Colors.white),
                          child: Text(
                            LocaleKeys.processing.tr(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        Tab(
                          icon: const Icon(CupertinoIcons.doc_checkmark_fill,
                              size: 30, color: Colors.white),
                          child: Text(
                            LocaleKeys.completed.tr(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                        Tab(
                          icon: const Icon(Icons.cancel_rounded,
                              size: 30, color: Colors.white),
                          child: Text(
                            LocaleKeys.o_canceled.tr(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    )),
              ),
            ),
            body: TabBarView(
              controller: _tabBarController,
              physics: const NeverScrollableScrollPhysics(),
              children: const [
                HistoryList(state: HistoryOrderState.process),
                HistoryList(state: HistoryOrderState.completed),
                HistoryList(state: HistoryOrderState.cancel),
              ],
            ),
          ),
        );
      }),
    );
  }
}
