import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/order_history_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_history/views/widget/history_item.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HistoryList extends StatefulWidget {
  final int state;
  const HistoryList({super.key, required this.state});

  @override
  State<HistoryList> createState() => _HistoryListState();
}

class _HistoryListState extends State<HistoryList> {
  OrderHistoryViewController? _historyViewController;
  @override
  void initState() {
    if (_historyViewController != null) {
      // print('get list');
      _historyViewController?.getOrderList(widget.state);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderHistoryViewController>(
      builder: (context, viewController, child) {
        _historyViewController = viewController;

        if (viewController.loading) {
          return const Center(child: CircularProgressIndicator());
        }

        if (viewController.orders.isEmpty) {
          return EmptyScreen(
            title: LocaleKeys.no_orders.tr(),
            message: LocaleKeys.there_is_no_order_right_now_pleas_wait.tr(),
            onRefresh: () {
              viewController.getOrderList(widget.state);
            },
          );
        }

        return RefreshIndicator(
          onRefresh: () async {
            viewController.getOrderList(widget.state, refresh: true);
          },
          child: ListView.separated(
              padding: const EdgeInsets.all(Spacing.s),
              itemBuilder: (context, index) {
                return HistoryItem(
                  state: widget.state,
                  order: viewController.orders[index],
                );
              },
              separatorBuilder: (context, index) =>
                  const SizedBox(height: Spacing.s),
              itemCount: viewController.orders.length),
        );
      },
    );
  }
}
