import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/order_detail_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

class HistoryItem extends StatefulWidget {
  final Function()? onTap;
  final int state;
  final Order order;
  const HistoryItem(
      {super.key, required this.state, this.onTap, required this.order});

  @override
  State<HistoryItem> createState() => _HistoryItemState();
}

class _HistoryItemState extends State<HistoryItem> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return InkWell(
      onTap: () {
        //order id
        print('widget.order.order_id : ${widget.order.order_id}');

        if (widget.order.order_id != null) {
          Navigator.pushNamed(context, OrderDetailView.routeName,
              arguments: OrderArguments(
                state: widget.state,
                orderTime: widget.order.order_time,
                orderId: widget.order.order_id ?? "",
              ));
        }
      },
      child: Container(
        padding: const EdgeInsets.all(Spacing.m),
        decoration: BoxDecoration(
            color: widget.state == HistoryOrderState.cancel
                ? Colors.red[400]
                : Colors.blue[400],
            borderRadius: BorderRadius.circular(12)),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${LocaleKeys.o_time.tr()}:",
                  style: theme.textTheme.titleMedium
                      ?.copyWith(fontWeight: FontWeight.bold),
                ),
                Text(
                  widget.order.order_time.toString(),
                  style: theme.textTheme.titleMedium
                      ?.copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const SizedBox(height: Spacing.s),
            Row(
              children: [
                Container(
                  width: 80,
                  alignment: Alignment.center,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      const TextSpan(text: "ORDER ID "),
                      TextSpan(
                          text: widget.order.order_id,
                          style: theme.textTheme.titleMedium?.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                    ]),
                  ),
                ),
                const SizedBox(
                  height: 80,
                  child: VerticalDivider(color: Colors.white),
                ),
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.order.receiver_contact ?? "",
                      style: theme.textTheme.titleMedium
                          ?.copyWith(color: Colors.white),
                    ),
                    Text("TEL: ${widget.order.receiver_mobile}",
                        style: theme.textTheme.titleMedium
                            ?.copyWith(color: Colors.white)),
                    if (widget.order.reveiver_adrress != null)
                      TextButton.icon(
                        style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        onPressed: null,
                        icon: const Icon(
                          CupertinoIcons.map_pin_ellipse,
                          color: Colors.black,
                        ),
                        label: Text(
                          widget.order.reveiver_adrress ?? "",
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(color: Colors.black),
                        ),
                      ),
                  ],
                )),
                const Align(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: Colors.white,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
