import 'package:bloc_merchant_mobile_2/data/models/responses/order_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class OrderHistoryViewController extends ChangeNotifier {
  final OrderRepo orderRepo;

  OrderHistoryViewController({required this.orderRepo});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newVal) {
    _loading = newVal;
    notifyListeners();
  }

  List<Order> _orders = [];
  List<Order> get orders => _orders;
  set orders(List<Order> newValue) {
    _orders = newValue;
    notifyListeners();
  }

  CancelToken? cancelToken;

  Future<void> getOrderList(int state, {bool refresh = false}) async {
    if (!refresh) {
      loading = true;
    }
    try {
      if (cancelToken != null && !cancelToken!.isCancelled) {
        cancelToken?.cancel();
      }

      cancelToken = CancelToken();

      final res =
          await orderRepo.orderList(status: state, cancelToken: cancelToken);

      orders = res.data?.items ?? [];
      loading = false;
    } catch (e) {
      if (e is DioException) {
        if (e.type == DioExceptionType.cancel) {
          return;
        }
        loading = false;
      }
    }
  }
}
