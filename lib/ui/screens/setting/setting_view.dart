import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/language/language_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer/printer_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class SettingView extends StatelessWidget {
  static const String routeName = '/setting';
  const SettingView({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        leading: const CusBackButton(color: Colors.white),
        backgroundColor: Colors.black,
        title: Text(
          LocaleKeys.shop_setting.tr(),
          style: theme.textTheme.titleLarge
              ?.copyWith(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(Spacing.s),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              LocaleKeys.shop_setting.tr(),
              style: theme.textTheme.titleMedium?.copyWith(color: Colors.blue),
            ),
            const SizedBox(height: Spacing.s),
            Container(
              decoration: BoxDecoration(
                  color: theme.colorScheme.background,
                  borderRadius: BorderRadius.circular(12)),
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(
                      Icons.store,
                      color: Colors.black.withOpacity(0.6),
                    ),
                    title: Text(LocaleKeys.shop_info.tr()),
                    subtitle:
                        Text(LocaleKeys.name_location_and_contact_number.tr()),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 16,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                  Divider(color: Colors.black.withOpacity(0.05)),
                  ListTile(
                    onTap: () {
                      Navigator.pushNamed(context, LanguageView.routeName);
                    },
                    leading: Icon(
                      Icons.language,
                      color: Colors.black.withOpacity(0.6),
                    ),
                    title: Text(LocaleKeys.language.tr()),
                    subtitle: Text(LocaleKeys.en.tr()),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      size: 16,
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: Spacing.normal),
            //
            Text(
              LocaleKeys.other_settings.tr(),
              style: theme.textTheme.titleMedium?.copyWith(color: Colors.blue),
            ),
            const SizedBox(height: Spacing.s),
            Container(
              decoration: BoxDecoration(
                  color: theme.colorScheme.background,
                  borderRadius: BorderRadius.circular(12)),
              child: ListTile(
                onTap: () {
                  Navigator.pushNamed(context, PrinterView.routeName);
                },
                leading: Icon(
                  Icons.print,
                  color: Colors.black.withOpacity(0.6),
                ),
                title: Text(LocaleKeys.printer_config.tr()),
                subtitle: Text(LocaleKeys.no_printer_connected.tr()),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  size: 16,
                  color: Colors.black.withOpacity(0.6),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
