import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/home/home_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/login/login_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashView extends StatefulWidget {
  static const String routeName = '/';
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  Future<void> init() async {
    final authProvider = context.read<AuthProvider>();
    final printProvider = context.read<PrinterProvider>();

    await authProvider.ensureInitialization();
    await printProvider.ensureInitialization();

    Future.delayed(
      const Duration(milliseconds: 500),
      () {
        if (authProvider.isLoggedIn) {
          Navigator.of(context).pushReplacementNamed(HomeView.routeName);
        } else {
          Navigator.of(context).pushReplacementNamed(LoginView.routeName);
        }
      },
    );
  }

  @override
  void initState() {
    init();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
