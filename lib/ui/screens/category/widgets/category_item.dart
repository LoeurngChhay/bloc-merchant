import 'package:bloc_merchant_mobile_2/data/models/responses/category_list_res.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatefulWidget {
  final Function? onEdit;
  final Function? onDelete;
  final Category item;
  final bool loadingDelete;
  // ignore: prefer_const_constructors_in_immutables
  const CategoryItem(
      {super.key,
      this.onEdit,
      this.onDelete,
      required this.item,
      this.loadingDelete = false});

  @override
  State<CategoryItem> createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  String showType = "0";

  initType() {
    setState(() {
      showType = widget.item.show_type ?? "0";
    });
  }

  @override
  void initState() {
    initType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: Spacing.s),
      constraints: const BoxConstraints(
        minHeight: 40,
      ),
      child: Row(
        children: [
          Container(
              width: 35,
              alignment: Alignment.center,
              child: showType == "0"
                  ? const Icon(Icons.remove_red_eye,
                      size: 18, color: Colors.green)
                  : showType == "1"
                      ? const Icon(Icons.visibility_off,
                          size: 18, color: Colors.blue)
                      : const Icon(Icons.history,
                          size: 18, color: Colors.grey)),
          Expanded(
              child: Container(
            alignment: Alignment.centerLeft,
            child: Text(widget.item.title.toString(),
                style: TextStyle(
                    fontSize: 14,
                    color: (showType == "-1") ? Colors.grey : Colors.black)),
          )),
          Container(
            width: 45,
            alignment: Alignment.center,
            child: Text(widget.item.orderby.toString(),
                style: const TextStyle(fontSize: 14)),
          ),
          Container(
            width: 70,
            alignment: Alignment.centerRight,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  color: Colors.transparent,
                  width: 35,
                  alignment: Alignment.center,
                  child: IconButton(
                      onPressed: () {
                        // widget.onEdit?.call(item);
                        if (widget.onEdit != null) {
                          widget.onEdit!();
                        }
                      },
                      icon: const Icon(Icons.edit, color: Colors.orange),
                      padding: const EdgeInsets.all(0)),
                ),
                Container(
                    width: 35,
                    color: Colors.transparent,
                    alignment: Alignment.center,
                    child: widget.loadingDelete
                        ? const Center(
                            child: SizedBox(
                                width: 18,
                                height: 18,
                                child: CircularProgressIndicator()),
                          )
                        : IconButton(
                            onPressed: () {
                              // widget.onDelete?.call(item);
                              if (widget.onDelete != null) {
                                widget.onDelete!();
                              }
                            },
                            icon: const Icon(Icons.delete, color: Colors.red),
                            padding: const EdgeInsets.all(0)))
              ],
            ),
          ),
          const SizedBox(width: 5)
        ],
      ),
    );
  }
}
