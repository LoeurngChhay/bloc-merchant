import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/repos/category_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/category/category_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/category/widgets/category_item.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/create_category/create_category_view.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_textfield.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryView extends StatefulWidget {
  static const String routeName = '/category';
  const CategoryView({super.key});

  @override
  State<CategoryView> createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {
  final _searchController = TextEditingController();
  TextStyle headerTableStyle = const TextStyle(fontWeight: FontWeight.bold);
  double bottomAppBarHeight = 40;
  double titleWidth = 40;
  double sortWidth = 45;
  double actionWidth = 70;

  String? currentId;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CategoryViewController>(
      create: (context) => CategoryViewController(
          categoryRepo: locator<CategoryRepo>(),
          onError: (message) async {
            DialogUtil.cusAlertSnackBar(context, message);
          },
          onSuccess: (message) async {
            DialogUtil.cusAlertSnackBar(context, message);
          })
        ..getCategoryList(),
      child: Consumer<CategoryViewController>(
          builder: (context, viewController, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            leading: CusBackButton(
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context, viewController.categoryList?.items);
              },
            ),
            title: Text(
              LocaleKeys.Category.tr(),
              style: const TextStyle(color: Colors.white),
            ),
            centerTitle: true,
            actions: [
              GestureDetector(
                onTap: () async {
                  final createStatus = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const CreateCategoryView(),
                      ));

                  if (createStatus is AsyncStatus &&
                      createStatus == AsyncStatus.success) {
                    viewController.getCategoryList();
                  }
                },
                child: Container(
                  height: 30,
                  width: 30,
                  margin: const EdgeInsets.only(right: Spacing.s),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      border: Border.all(width: 1, color: Colors.white)),
                  child: const Icon(Icons.add, color: Colors.white),
                ),
              )
            ],
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(bottomAppBarHeight),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: bottomAppBarHeight,
                        child: CusTextField(
                          prefixIcon: Icon(
                            Icons.search,
                            color: Colors.black.withOpacity(0.6),
                          ),
                          hintText: LocaleKeys.search.tr(),
                          controller: _searchController,
                          backgroundColors: Colors.white,
                          contentPadding: EdgeInsets.zero,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(Spacing.s)),
                        ),
                      ),
                    ),
                    const SizedBox(width: Spacing.normal),
                    CusIconButton(
                      onPressed: () {
                        viewController.getCategoryList(
                            search: _searchController.text);
                      },
                      icon: const Icon(Icons.search),
                      height: bottomAppBarHeight,
                      width: bottomAppBarHeight,
                      style: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12)),
                    )
                  ],
                ),
              ),
            ),
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8),
                child: SizedBox(
                  height: 30,
                  child: Row(
                    children: [
                      const SizedBox(width: 35),
                      Expanded(
                          child: Text(LocaleKeys.category_title.tr(),
                              style: headerTableStyle)),
                      SizedBox(
                          width: sortWidth,
                          child: Text(LocaleKeys.sort.tr(),
                              textAlign: TextAlign.center,
                              style: headerTableStyle)),
                      SizedBox(
                          width: actionWidth,
                          child: Text(LocaleKeys.action.tr(),
                              textAlign: TextAlign.center,
                              style: headerTableStyle)),
                    ],
                  ),
                ),
              ),
              viewController.loading
                  ? const Center(
                      child: CircularProgressIndicator(),
                    )
                  : viewController.error != null
                      ? Center(
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.8,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Image.asset(
                                  'assets/images/no_internets.png',
                                ),
                                Text(
                                  LocaleKeys.error_occured.tr(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(fontSize: 28),
                                ),
                                Text(
                                  viewController.error.toString(),
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium
                                      ?.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: Spacing.normal),
                                SizedBox(
                                  height: 45,
                                  width: 150,
                                  child: FilledButton(
                                      onPressed: () {
                                        viewController.getCategoryList();
                                      },
                                      style: FilledButton.styleFrom(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      Spacing.s))),
                                      child: const Text("Refresh")),
                                )
                              ],
                            ),
                          ),
                        )
                      : Expanded(
                          child: RefreshIndicator(
                          onRefresh: () async {
                            viewController.getCategoryList(isRefresh: true);
                          },
                          child: ListView.separated(
                              itemBuilder: (context, index) {
                                return CategoryItem(
                                  item: viewController
                                      .categoryList!.items![index],
                                  onEdit: () async {
                                    final callbackMsg = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              CreateCategoryView(
                                            id: viewController.categoryList!
                                                .items![index].cate_id,
                                          ),
                                        ));

                                    if (callbackMsg is AsyncStatus &&
                                        callbackMsg == AsyncStatus.success) {
                                      viewController.getCategoryList();
                                    }
                                  },
                                  onDelete: () {
                                    final cateId = viewController
                                        .categoryList!.items![index].cate_id;

                                    setState(() {
                                      currentId = cateId;
                                    });

                                    if (cateId != null) {
                                      DialogUtil.cusConfirmDialog(
                                        context: context,
                                        title: 'Do you want to delete?',
                                        colors: Colors.red,
                                        icon: Icons.delete,
                                        onPressed: viewController.loadingAction
                                            ? null
                                            : () async {
                                                viewController
                                                    .deleteCategory(cateId)
                                                    .then((value) =>
                                                        Navigator.pop(context));
                                              },
                                      );
                                    }
                                  },
                                  loadingDelete: viewController.loadingAction &&
                                      currentId ==
                                          viewController.categoryList!
                                              .items![index].cate_id,
                                );
                              },
                              separatorBuilder: (context, index) => Divider(
                                    color: Colors.grey.shade300,
                                    height: 4,
                                  ),
                              itemCount:
                                  viewController.categoryList!.items!.length),
                        ))
            ],
          ),
        );
      }),
    );
  }
}
