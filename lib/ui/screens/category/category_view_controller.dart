import 'package:bloc_merchant_mobile_2/data/models/requests/create_category_req.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/category_list_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/create_category_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/category_repo.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_exception.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class CategoryViewController extends ChangeNotifier {
  final CategoryRepo categoryRepo;
  final Function(String message)? onError;
  final Function(String? message)? onSuccess;
  final Function(CategoryDetail detail)? setInitValue;

  CategoryViewController(
      {required this.categoryRepo,
      this.onError,
      this.onSuccess,
      this.setInitValue});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool newValue) {
    _loading = newValue;
    notifyListeners();
  }

  bool _loadingAction = false;
  bool get loadingAction => _loadingAction;
  set loadingAction(bool newValue) {
    _loadingAction = newValue;
    notifyListeners();
  }

  CategoryList? _categoryList;
  CategoryList? get categoryList => _categoryList;
  set categoryList(CategoryList? newValue) {
    _categoryList = newValue;
    notifyListeners();
  }

  CustomException? _error;
  CustomException? get error => _error;
  set error(CustomException? newValue) {
    _error = newValue;

    notifyListeners();
  }

  Future<void> getCategoryList({String? search, bool isRefresh = false}) async {
    if (!isRefresh) {
      loading = true;
    }
    try {
      final response = await categoryRepo.getCategory(search: search);
      categoryList = response.data;
    } catch (e) {
      if (e is DioException) {
        error = CustomException(e.message.toString());
      }
    } finally {
      loading = false;
    }
  }

  Future<void> getCategoryDetail(String id) async {
    loading = true;
    try {
      final response = await categoryRepo.getCategoryDetail(id);
      createCategoryResponse = response.data;
      if (setInitValue != null) {
        setInitValue!(createCategoryResponse!);
      }
    } catch (e) {
      print('error get category detail : $e');
    } finally {
      loading = false;
    }
  }

  CategoryDetail? _createCategoryResponse;
  CategoryDetail? get createCategoryResponse => _createCategoryResponse;
  set createCategoryResponse(CategoryDetail? newValue) {
    _createCategoryResponse = newValue;
    notifyListeners();
  }

  Future<void> createCategory(CreateCategoryRequest request) async {
    loadingAction = true;
    try {
      // print("createCategory : ${loadingAction}");
      final response = await categoryRepo.createCategory(request);
      createCategoryResponse = response.data;
      if (onSuccess != null) {
        onSuccess!(response.message);
      }
    } catch (e) {
      print('erorr create Category : $e');

      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingAction = false;
    }
  }

  Future<void> updateCategory(
      String cateId, CreateCategoryRequest request) async {
    loadingAction = true;
    try {
      final response =
          await categoryRepo.updateCategory(cateId: cateId, req: request);

      createCategoryResponse = response.data;
      if (onSuccess != null) {
        onSuccess!(response.message);
      }
    } catch (e) {
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingAction = false;
    }
  }

  Future<void> deleteCategory(String cateId) async {
    loadingAction = true;
    try {
      final response = await categoryRepo.deleteCategory(cateId);

      if (onSuccess != null) {
        onSuccess!(response.message);
      }

      categoryList?.items!.removeWhere((e) => e.cate_id == cateId);
    } catch (e) {
      if (onError != null) {
        if (e is DioException) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingAction = false;
    }
  }
}
