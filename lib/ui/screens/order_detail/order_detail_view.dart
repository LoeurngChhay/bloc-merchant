import 'dart:async';

import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/constants/order_state.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/locator.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order/widgets/dialog_cancel_order.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/order_detail_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/widget/order_note.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/order_detail/widget/printer_dailog.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_bluetooth_view/printer_bluetooth_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_network_view.dart/printer_network_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/theme.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_icon_button.dart';
import 'package:bloc_merchant_mobile_2/utils/dialog_util.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderArguments {
  // final OrderDetail item;
  final String orderId;
  final String? orderTime;
  final int state;

  OrderArguments({required this.orderId, this.orderTime, required this.state});
}

class OrderDetailView extends StatefulWidget {
  static const String routeName = '/order-detail';
  final OrderArguments args;

  const OrderDetailView({super.key, required this.args});

  @override
  State<OrderDetailView> createState() => _OrderDetailViewState();
}

class _OrderDetailViewState extends State<OrderDetailView> {
  bool isOpenDetail = false;
  bool loadingPrint = false;
  int? state;

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

  void showPrintChoice(
      {PrinterBluetoothViewController? bluetoothController,
      PrinterNetworkViewController? networkController,
      OrderDetail? orderDetail}) {
    showDialog(
      context: context,
      builder: (context) {
        return PrinterDialog(
          orderDetail: orderDetail,
          orderId: widget.args.orderId,
          orderTime: widget.args.orderTime,
          bluetoothViewController: bluetoothController,
          networkViewController: networkController,
        );
      },
    );
  }

  void openCancelOrder(OrderDetailViewController viewController, id) {
    showDialog(
      context: context,
      builder: (context) {
        return DialogCancelOrder(
          onSave: (reason) {
            viewController.cancelOrder(id, reason.toString());

            Navigator.pop(context);
          },
        );
      },
    );
  }

  @override
  void initState() {
    setState(() {
      state = widget.args.state;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final themeTitleMedium = theme.textTheme.titleMedium;

    // DateTime dateTime =
    //     DateFormat("dd-MM-yyyy HH:mm").parse(widget.args.orderTime.toString());

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => OrderDetailViewController(
              orderRepo: locator<OrderRepo>(),
              orderId: widget.args.orderId,
              onSuccess: (message, action) {
                if (action == OrderAction.accept) {
                  setState(() {
                    state = OrderState.waiting;
                  });
                }
              },
              onError: (message) {
                DialogUtil.cusAlertDialog(context: context, message: message);
              })
            ..getOrderDetail(),
        ),
        ChangeNotifierProvider(
          create: (context) => PrinterBluetoothViewController(
              onErrorPrint: (message) {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text("Connection Unsuccessfull"),
                      content: Text(message),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text("Ok"))
                      ],
                    );
                  },
                );
              },
              printerProvider: context.read<PrinterProvider>()),
        ),
        ChangeNotifierProvider(
          create: (context) => PrinterNetworkViewController(
              printerProvider: context.read<PrinterProvider>()),
        ),
      ],
      child: Consumer3<OrderDetailViewController,
              PrinterBluetoothViewController, PrinterNetworkViewController>(
          builder: (context, viewController, bluetoothController,
              networkController, child) {
        final item = viewController.orderDetail;
        double subtotal = double.parse(item?.product_price ?? "0");
        double discount = (item?.discount_amount ?? 0).toDouble();
        double parkageFee = double.parse(item?.package_price ?? "0");
        double vatAmount = (item?.vat_amount ?? 0).toDouble();
        // double deliveryFee = item!.delivery_fee!;
        // double bagFee = item!.bag_price!;
        double total = ((subtotal + parkageFee) - discount) + vatAmount;

        return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Text(LocaleKeys.order_details.tr()),
              actions: [
                IconButton(
                    onPressed: loadingPrint
                        ? null
                        : () {
                            showPrintChoice(
                              bluetoothController: bluetoothController,
                              networkController: networkController,
                              orderDetail: viewController.orderDetail,
                            );
                          },
                    icon: loadingPrint
                        ? const SizedBox(
                            width: 16,
                            height: 16,
                            child: CircularProgressIndicator(),
                          )
                        : const Icon(Icons.print_rounded))
              ],
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  if (widget.args.state >= OrderState.newOrder &&
                      widget.args.state <= OrderState.dilivery)
                    Padding(
                      padding: const EdgeInsets.all(Spacing.normal),
                      child: Row(
                        children: [
                          Container(
                            height: 40,
                            width: 40,
                            decoration: const BoxDecoration(
                                color: Colors.blue, shape: BoxShape.circle),
                            child: const Icon(
                              Icons.receipt_rounded,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                              child: Container(
                                  height: 5,
                                  color: state! >= OrderState.waiting
                                      ? Colors.blue
                                      : Colors.grey[300])),
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: state! >= OrderState.waiting
                                    ? Colors.blue
                                    : Colors.grey[300],
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.push_pin_rounded,
                              color: state! >= OrderState.waiting
                                  ? Colors.white
                                  : Colors.black38,
                            ),
                          ),
                          Expanded(
                              child: Container(
                                  height: 5,
                                  color: state! >= OrderState.picker
                                      ? Colors.blue
                                      : Colors.grey[300])),
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: state! >= OrderState.picker
                                    ? Colors.blue
                                    : Colors.grey[300],
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.delivery_dining,
                              color: state! >= OrderState.picker
                                  ? Colors.white
                                  : Colors.black38,
                            ),
                          ),
                          Expanded(
                              child: Container(
                                  height: 5,
                                  color: state! >= OrderState.dilivery
                                      ? Colors.blue
                                      : Colors.grey[300])),
                          Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                color: state! >= OrderState.dilivery
                                    ? Colors.blue
                                    : Colors.grey[300],
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.done_all_rounded,
                              color: state! >= OrderState.dilivery
                                  ? Colors.white
                                  : Colors.black38,
                            ),
                          ),
                        ],
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(Spacing.normal),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          LocaleKeys.o_time.tr(),
                          style: themeTitleMedium,
                        ),
                        Text(
                          widget.args.orderTime.toString(),
                          overflow: TextOverflow.ellipsis,
                          style: themeTitleMedium?.copyWith(
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(Spacing.normal),
                    decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.background,
                        border: const Border(
                            top: BorderSide(color: Colors.blue, width: 4))),
                    child: viewController.loading
                        ? const SizedBox(
                            height: 200,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : item != null
                            ? Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 1,
                                          child: Text(
                                              "${LocaleKeys.queue.tr()} #:")),
                                      Expanded(
                                          flex: 2,
                                          child: Text(
                                            item.queue_number ?? "0",
                                            style: themeTitleMedium?.copyWith(
                                                fontWeight: FontWeight.bold),
                                          )),
                                      const SizedBox(width: 40)
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 1,
                                          child: Text(
                                              "${LocaleKeys.order.tr()} #:")),
                                      Expanded(
                                          flex: 2,
                                          child: Text(
                                            widget.args.orderId,
                                            style: themeTitleMedium?.copyWith(
                                                fontWeight: FontWeight.bold),
                                          )),
                                      const SizedBox(width: 40)
                                    ],
                                  ),
                                  Divider(color: Colors.grey[400]),
                                  if (item.customer != null)
                                    Row(
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: Text(
                                                "${LocaleKeys.customer.tr()}:")),
                                        Expanded(
                                            flex: 2,
                                            child: GestureDetector(
                                              onTap: () {
                                                if (item.customer != null) {
                                                  _makePhoneCall(viewController
                                                      .orderDetail!
                                                      .customer!
                                                      .mobile
                                                      .toString());
                                                }
                                              },
                                              child: RichText(
                                                text: TextSpan(
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text:
                                                              "${item.customer!.nickname}\n",
                                                          style: const TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      TextSpan(
                                                        text:
                                                            "${item.customer!.mobile}",
                                                        style: const TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            decoration:
                                                                TextDecoration
                                                                    .underline),
                                                      )
                                                    ]),
                                              ),
                                            )),
                                        const SizedBox(width: Spacing.s),
                                        SizedBox(
                                          width: 40,
                                          height: 40,
                                          child: CusIconButton(
                                            onPressed: () {
                                              if (item.customer != null) {
                                                _makePhoneCall(item
                                                    .customer!.mobile
                                                    .toString());
                                              }
                                            },
                                            icon: const Icon(
                                              Icons.phone,
                                              color: Colors.white,
                                            ),
                                            padding: const EdgeInsets.all(8),
                                            style: BoxDecoration(
                                                color: Colors.blue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Spacing.s)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  const SizedBox(height: Spacing.m),
                                  if (item.driver != null &&
                                      item.driver_id != "0") ...[
                                    Row(
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: Text(
                                                "${LocaleKeys.driver.tr()}:")),
                                        Expanded(
                                          flex: 2,
                                          child: GestureDetector(
                                            onTap: () {
                                              _makePhoneCall(item.driver!.mobile
                                                  .toString());
                                            },
                                            child: RichText(
                                              text:
                                                  TextSpan(children: <TextSpan>[
                                                TextSpan(
                                                    text:
                                                        "${item.driver!.name}\n",
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                TextSpan(
                                                  text:
                                                      "${item.driver!.mobile}",
                                                  style: const TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      decoration: TextDecoration
                                                          .underline),
                                                )
                                              ]),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(width: Spacing.s),
                                        SizedBox(
                                          width: 40,
                                          height: 40,
                                          child: CusIconButton(
                                            onPressed: () {
                                              _makePhoneCall(item.driver!.mobile
                                                  .toString());
                                            },
                                            icon: const Icon(
                                              Icons.phone,
                                              color: Colors.white,
                                            ),
                                            padding: const EdgeInsets.all(8),
                                            style: BoxDecoration(
                                                color: Colors.orange,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Spacing.s)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                  const SizedBox(height: Spacing.m),
                                  if (item.note != "")
                                    OrderNoteWidget(item.note),
                                  Divider(color: Colors.grey[400]),
                                  ExpansionTile(
                                    initiallyExpanded: true,
                                    onExpansionChanged: (value) =>
                                        setState(() => isOpenDetail = value),
                                    collapsedShape:
                                        const RoundedRectangleBorder(
                                      side: BorderSide.none,
                                    ),
                                    shape: const RoundedRectangleBorder(
                                      side: BorderSide.none,
                                    ),
                                    tilePadding: EdgeInsets.zero,
                                    title: Text(
                                      LocaleKeys.order_details.tr(),
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    trailing: Icon(
                                      isOpenDetail
                                          ? Icons.arrow_drop_up_rounded
                                          : Icons.arrow_drop_down_rounded,
                                      size: 32,
                                    ),
                                    children: <Widget>[
                                      if (item.products!.isNotEmpty)
                                        ...item.products!.map((e) => Column(
                                              children: [
                                                Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Column(
                                                      children: [
                                                        Text(
                                                          'x${e.product_number}',
                                                          style:
                                                              themeTitleMedium
                                                                  ?.copyWith(
                                                            color: Colors.blue,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                        if (e.product_image !=
                                                            '')
                                                          Image.network(
                                                            e.product_image
                                                                .toString(),
                                                            height: 48,
                                                            width: 48,
                                                          ),
                                                      ],
                                                    ),
                                                    const SizedBox(
                                                        width: Spacing.m),
                                                    Expanded(
                                                        child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          e.product_title
                                                              .toString(),
                                                          style: themeTitleMedium
                                                              ?.copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                        ),
                                                        const SizedBox(
                                                            height: Spacing.s),
                                                        ...e.specification!.map(
                                                            (e) => Text(
                                                                '• ${e.key}: ${e.val}')),
                                                        const SizedBox(
                                                            height:
                                                                Spacing.normal),
                                                      ],
                                                    )),
                                                    const SizedBox(
                                                        width: Spacing.s),
                                                    Text(
                                                      '${e.product_price}\$',
                                                      style: themeTitleMedium
                                                          ?.copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                    )
                                                  ],
                                                ),
                                                ...e.product_addon!.map((e) {
                                                  return Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(vertical: 6),
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .symmetric(
                                                                  horizontal:
                                                                      Spacing.m,
                                                                  vertical:
                                                                      Spacing
                                                                          .xs),
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.green,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          Spacing
                                                                              .s)),
                                                          child: const Text(
                                                            "ADD",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                            width: 10),
                                                        Text(
                                                          "x${e.qty}",
                                                          style: themeTitleMedium
                                                              ?.copyWith(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .blue),
                                                        ),
                                                        const SizedBox(
                                                            width: 10),
                                                        Expanded(
                                                            child: Text(e
                                                                .product_name
                                                                .toString()))
                                                      ],
                                                    ),
                                                  );
                                                }),
                                                const SizedBox(
                                                    height: Spacing.normal),
                                                if (e.bar_code != "") ...[
                                                  SizedBox(
                                                    height: 70,
                                                    child: SfBarcodeGenerator(
                                                      value: e.bar_code,
                                                      showValue: true,
                                                    ),
                                                  ),
                                                ],
                                                Divider(
                                                  color: Colors.grey[400],
                                                  height: 48,
                                                ),
                                              ],
                                            )),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("${LocaleKeys.subtotal.tr()}:"),
                                          Text(
                                              "${subtotal.toStringAsFixed(2)}\$",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium)
                                        ],
                                      ),
                                      const SizedBox(height: Spacing.m),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("${LocaleKeys.discount.tr()}:"),
                                          Text(
                                              "-${discount.toStringAsFixed(2)}\$",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium)
                                        ],
                                      ),
                                      const SizedBox(height: Spacing.m),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("${LocaleKeys.vat.tr()}:"),
                                          Text("${item.vat_amount}\$",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium)
                                        ],
                                      ),
                                      const SizedBox(height: Spacing.m),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "${LocaleKeys.total.tr()}:",
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium
                                                ?.copyWith(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                          ),
                                          Text("${total.toStringAsFixed(2)}\$",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium
                                                  ?.copyWith(
                                                      fontWeight:
                                                          FontWeight.bold))
                                        ],
                                      ),
                                      Divider(
                                          color: Colors.grey[400], height: 32),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            LocaleKeys.shop_receive.tr(),
                                            style: const TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                              '${item.shop_receive?.toStringAsFixed(2)}\$',
                                              style: const TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold))
                                        ],
                                      )
                                    ],
                                  ),
                                ],
                              )
                            : const SizedBox(),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Visibility(
              visible: state == OrderState.newOrder,
              child: Container(
                padding: EdgeInsets.only(
                    top: Spacing.normal,
                    left: Spacing.normal,
                    right: Spacing.normal,
                    bottom: MediaQuery.of(context).padding.bottom == 0
                        ? Spacing.normal
                        : MediaQuery.of(context).padding.bottom),
                decoration: BoxDecoration(color: theme.colorScheme.background),
                child: Row(
                  children: [
                    TextButton.icon(
                        onPressed: () {
                          openCancelOrder(viewController, widget.args.orderId);
                        },
                        icon: viewController.loadingReject
                            ? const SizedBox(
                                height: 18,
                                width: 18,
                                child: CircularProgressIndicator(
                                  color: Colors.red,
                                ),
                              )
                            : const SizedBox(),
                        label: const Text(
                          "Reject",
                          style: TextStyle(color: Colors.red),
                        )),
                    const SizedBox(width: Spacing.normal),
                    Expanded(
                      child: FilledButton.icon(
                          style: FilledButton.styleFrom(
                              padding: const EdgeInsets.symmetric(
                                  vertical: Spacing.m)),
                          onPressed: () {
                            viewController.acceptOrder(widget.args.orderId);
                          },
                          icon: viewController.loadingAccept
                              ? const SizedBox(
                                  height: 18,
                                  width: 18,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                )
                              : const SizedBox(),
                          label: const Text(
                            "Accept Order",
                            style: TextStyle(color: Colors.white),
                          )),
                    )
                  ],
                ),
              ),
            ));
      }),
    );
  }

  final icons = [
    Icons.receipt_rounded,
    Icons.push_pin_rounded,
    Icons.delivery_dining,
    Icons.done_all_rounded,
  ];
}
