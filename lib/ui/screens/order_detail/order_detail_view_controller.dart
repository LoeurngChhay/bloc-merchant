import 'package:bloc_merchant_mobile_2/constants/enum.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/data/repos/order_repo.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class OrderDetailViewController extends ChangeNotifier {
  final OrderRepo orderRepo;
  final String orderId;
  final Function(String message, OrderAction action)? onSuccess;
  final Function(String message)? onError;

  OrderDetailViewController(
      {required this.orderRepo,
      required this.orderId,
      this.onSuccess,
      this.onError});

  bool _loading = false;
  bool get loading => _loading;
  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }

  bool _loadingAccept = false;
  bool get loadingAccept => _loadingAccept;
  set loadingAccept(bool value) {
    _loadingAccept = value;
    notifyListeners();
  }

  bool _loadingReject = false;
  bool get loadingReject => _loadingReject;
  set loadingReject(bool value) {
    _loadingReject = value;
    notifyListeners();
  }

  OrderDetail? _orderDetail;
  OrderDetail? get orderDetail => _orderDetail;
  set orderDetail(OrderDetail? value) {
    _orderDetail = value;
    notifyListeners();
  }

  Future<void> getOrderDetail() async {
    loading = true;
    try {
      final response = await orderRepo.orderDetail(orderId);
      orderDetail = response.data;
    } catch (e) {
      print('Error Get OrderDetail $e');
    } finally {
      loading = false;
    }
  }

  Future<void> acceptOrder(String orderId) async {
    loadingAccept = true;
    try {
      final response = await orderRepo.orderAccept(orderId);

      if (onSuccess != null) {
        onSuccess!(response.message, OrderAction.accept);
      }
    } catch (e) {
      print('Error Accept Order ; $e');
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingAccept = false;
    }
  }

  Future<void> cancelOrder(String orderId, String reason) async {
    loadingReject = true;
    try {
      final response = await orderRepo.orderCancel(orderId, reason);

      if (onSuccess != null) {
        onSuccess!(response.message, OrderAction.cancel);
      }
    } catch (e) {
      print('Error Cancel Order ; $e');
      if (e is DioException) {
        if (onError != null) {
          onError!(e.message.toString());
        }
      }
    } finally {
      loadingReject = false;
    }
  }
}
