import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

class OrderNoteWidget extends StatefulWidget {
  final String? note;

  // ignore: prefer_const_constructors_in_immutables
  OrderNoteWidget(this.note, {Key? key}) : super(key: key);

  @override
  State<OrderNoteWidget> createState() => OrderNoteWidgetState();
}

class OrderNoteWidgetState extends State<OrderNoteWidget> {
  Timer? timer;
  Random? random;
  Color noteColor = Colors.black;

  @override
  void initState() {
    super.initState();
    setState(() {
      random = Random();
      noteColor = Color.fromARGB(255, random!.nextInt(256),
          random!.nextInt(256), random!.nextInt(256));

      if (timer != null) {
        timer!.cancel();
      }
      const oneSec = Duration(milliseconds: 50);
      timer = Timer.periodic(
          oneSec,
          (Timer timer) => setState(() {
                setState(() {
                  noteColor = Color.fromARGB(255, random!.nextInt(256),
                      random!.nextInt(256), random!.nextInt(256));
                });
              }));
    });
  }

  @override
  void dispose() {
    if (timer != null) {
      timer!.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(2),
      child: Container(
          padding: const EdgeInsets.all(2),
          decoration: BoxDecoration(
            color: Colors.white, // added
            border: Border.all(color: noteColor, width: 3), // added
            borderRadius: BorderRadius.circular(8.0),
          ),
          alignment: Alignment.centerLeft,
          child: RichText(
            text: TextSpan(children: <TextSpan>[
              // ignore: prefer_interpolation_to_compose_strings
              const TextSpan(
                  text: "Note :",
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
              TextSpan(
                  text: "${widget.note}",
                  style: const TextStyle(color: Colors.black, fontSize: 18))
            ]),
          )),
    );
  }
}
