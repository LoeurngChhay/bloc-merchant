import 'dart:io';

import 'package:bloc_merchant_mobile_2/data/models/responses/login_res.dart';
import 'package:bloc_merchant_mobile_2/data/models/responses/order_detail_res.dart';
import 'package:bloc_merchant_mobile_2/providers/auth_provider.dart';
import 'package:bloc_merchant_mobile_2/providers/print_provider.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer/printer_view.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_bluetooth_view/printer_bluetooth_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/screens/printer_network_view.dart/printer_network_view_controller.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:esc_pos_bluetooth_updated/esc_pos_bluetooth_updated.dart';
import 'package:flutter/material.dart';
import 'package:print_bluetooth_thermal/print_bluetooth_thermal.dart';
import 'package:provider/provider.dart';

class PrinterDialog extends StatefulWidget {
  final OrderDetail? orderDetail;
  final PrinterBluetoothViewController? bluetoothViewController;
  final PrinterNetworkViewController? networkViewController;
  final String orderId;
  final String? orderTime;

  const PrinterDialog({
    super.key,
    this.orderDetail,
    required this.orderId,
    this.orderTime,
    this.bluetoothViewController,
    this.networkViewController,
  });

  @override
  State<PrinterDialog> createState() => _PrinterDialogState();
}

class _PrinterDialogState extends State<PrinterDialog> {
  bool loadingPrint = false;
  bool forShop = false;

  Future _runDelayed(int second) {
    return Future<dynamic>.delayed(Duration(seconds: second));
  }

  Future<void> onPrinterNetwork(
      {bool isForShop = false,
      PrinterProvider? printerProvider,
      Shop? shop}) async {
    final ipAddress = printerProvider?.printIpAddress;

    setState(() {
      loadingPrint = true;
    });

    if (ipAddress != null) {
      widget.networkViewController?.startPrint(
        widget.orderDetail!,
        ipAddress: ipAddress,
        forShop: isForShop,
        orderId: widget.orderId,
        orderTime: widget.orderTime,
        shop: shop,
      );
      Navigator.pop(context);
    } else {
      Navigator.pushNamed(context, PrinterView.routeName);
    }

    setState(() {
      loadingPrint = false;
    });
  }

  Future<void> onPrintBluetooth(
      {bool forShop = false,
      PrinterProvider? printerProvider,
      Shop? shop}) async {
    PrinterBluetooth? deviceIos;
    BluetoothInfo? deviceAndr;

    // final authProvider = context.read<AuthProvider>();

    setState(() {
      loadingPrint = true;
    });

    if (Platform.isIOS) {
      deviceIos = printerProvider?.deviceIos;
      if (deviceIos != null) {
        widget.bluetoothViewController?.connectIOSDevice(deviceIos);
      }
    } else {
      deviceAndr = printerProvider?.deviceAndr;
      if (deviceAndr != null) {
        widget.bluetoothViewController?.connectAndrDevice(deviceAndr);
      }
    }

    if (deviceIos != null || deviceAndr != null) {
      await _runDelayed(2).then((value) {
        widget.bluetoothViewController?.startPrinter(
            shop: shop,
            forShop: forShop,
            orderId: widget.orderId,
            orderDetail: widget.orderDetail,
            time: widget.orderTime);
      });

      Navigator.pop(context);
    }

    setState(() {
      loadingPrint = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = context.read<AuthProvider>();
    final printerProvider = context.read<PrinterProvider>();

    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        insetPadding: EdgeInsets.zero,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: SizedBox(
          width: MediaQuery.of(context).size.width - Spacing.l * 2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.all(Spacing.normal),
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const SizedBox(width: 40),
                    const Text(
                      "Choose for print.",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      width: 40,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.close,
                          size: 28,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: Spacing.s),
              Padding(
                padding: const EdgeInsets.all(Spacing.normal),
                child: Row(
                  children: [
                    Expanded(
                        child: InkWell(
                      onTap: loadingPrint
                          ? null
                          : () {
                              setState(() {
                                forShop = true;
                              });

                              if (printerProvider.printIpAddress == null &&
                                  printerProvider.deviceAndr == null &&
                                  printerProvider.deviceIos == null) {
                                Navigator.pushNamed(
                                    context, PrinterView.routeName);
                                return;
                              }

                              if (printerProvider.printIpAddress != null) {
                                //wifi printer
                                onPrinterNetwork(
                                  printerProvider: printerProvider,
                                  isForShop: forShop,
                                  shop: authProvider.shop,
                                );
                              } else {
                                //bluetooth printer
                                onPrintBluetooth(
                                  printerProvider: printerProvider,
                                  forShop: forShop,
                                  shop: authProvider.shop,
                                );
                              }
                            },
                      child: Container(
                        height: 120,
                        width: 120,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(8)),
                        child: loadingPrint && forShop
                            ? const SizedBox(
                                height: 22,
                                width: 22,
                                child: CircularProgressIndicator(),
                              )
                            : const Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "For Shop",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(height: Spacing.normal),
                                  Icon(
                                    Icons.print,
                                    size: 32,
                                    color: Colors.black87,
                                  )
                                ],
                              ),
                      ),
                    )),
                    const SizedBox(width: Spacing.normal),
                    Expanded(
                        child: InkWell(
                      onTap: loadingPrint
                          ? null
                          : () {
                              setState(() {
                                forShop = false;
                              });

                              if (printerProvider.printIpAddress == null &&
                                  (printerProvider.deviceAndr == null &&
                                      printerProvider.deviceIos == null)) {
                                Navigator.pushNamed(
                                    context, PrinterView.routeName);
                                return;
                              }

                              if (printerProvider.printIpAddress != null) {
                                //wifi printer
                                onPrinterNetwork(
                                  printerProvider: printerProvider,
                                  isForShop: forShop,
                                  shop: authProvider.shop,
                                );
                              } else {
                                //bluetooth printer
                                onPrintBluetooth(
                                  forShop: forShop,
                                  printerProvider: printerProvider,
                                  shop: authProvider.shop,
                                );
                              }
                            },
                      child: Container(
                        height: 120,
                        width: 120,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(8)),
                        child: loadingPrint && !forShop
                            ? const SizedBox(
                                height: 22,
                                width: 22,
                                child: CircularProgressIndicator(),
                              )
                            : const Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "For Customer",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18),
                                  ),
                                  SizedBox(height: Spacing.normal),
                                  Icon(
                                    Icons.print,
                                    size: 32,
                                    color: Colors.black87,
                                  )
                                ],
                              ),
                      ),
                    ))
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
