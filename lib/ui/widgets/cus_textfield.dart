import 'package:flutter/material.dart';

class CusTextField extends StatefulWidget {
  final TextEditingController? controller;
  final bool? showPass;
  final String? labelText;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final TextInputType? keyboardType;
  final Color? backgroundColors;
  final InputBorder? border;
  final String? hintText;
  final TextStyle? hintStyle;
  final EdgeInsets? contentPadding;
  final InputBorder? focusedBorder;
  final InputBorder? enabledBorder;
  final bool? enabled;
  final bool? readOnly;
  final Function()? onTap;
  final String? validMes;
  final bool? isValid;
  final InputDecoration? decoration;
  final String? initialValue;
  final Function(String)? onChange;

  const CusTextField({
    super.key,
    this.controller,
    this.showPass = false,
    this.labelText,
    this.suffixIcon,
    this.prefixIcon,
    this.keyboardType,
    this.backgroundColors,
    this.border,
    this.hintText,
    this.hintStyle,
    this.contentPadding,
    this.focusedBorder,
    this.enabledBorder,
    this.enabled = true,
    this.readOnly = false,
    this.onTap,
    this.validMes,
    this.isValid = false,
    this.decoration,
    this.initialValue,
    this.onChange,
  });

  @override
  State<CusTextField> createState() => _CusTextFieldState();
}

class _CusTextFieldState extends State<CusTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      obscureText: widget.showPass!,
      onTap: widget.onTap,
      keyboardType: widget.keyboardType,
      readOnly: widget.readOnly!,
      initialValue: widget.initialValue,
      onChanged: widget.onChange,
      decoration: widget.decoration ??
          InputDecoration(
            hintText: widget.hintText,
            fillColor: widget.backgroundColors,
            filled: widget.backgroundColors != null,
            contentPadding: widget.contentPadding ??
                const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
            border: widget.border ??
                OutlineInputBorder(borderRadius: BorderRadius.circular(16.0)),
            focusedBorder: widget.focusedBorder,
            enabledBorder: widget.enabledBorder,
            enabled: widget.enabled!,
            labelText: widget.labelText,
            labelStyle: TextStyle(
              color: Colors.black.withOpacity(0.8),
            ),
            hintStyle: widget.hintStyle ??
                TextStyle(color: Colors.black.withOpacity(0.6)),
            prefixIcon: widget.prefixIcon,
            suffixIcon: widget.suffixIcon,
          ),
      validator: widget.isValid!
          ? (value) {
              if (value == null || value.isEmpty || value == '') {
                return widget.validMes ?? "This field is required.";
              }
              return null;
            }
          : null,
    );
  }
}
