import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:flutter/material.dart';

class CusIconButton extends StatefulWidget {
  final Widget icon;
  final Function()? onPressed;
  final Decoration? style;
  final EdgeInsets? padding;
  final double? height;
  final double? width;
  const CusIconButton({
    super.key,
    required this.icon,
    this.onPressed,
    this.style,
    this.padding,
    this.height,
    this.width,
  });

  @override
  State<CusIconButton> createState() => _CusIconButtonState();
}

class _CusIconButtonState extends State<CusIconButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        height: widget.height,
        width: widget.width,
        padding: widget.padding,
        decoration: widget.style ??
            BoxDecoration(
                borderRadius: BorderRadius.circular(Spacing.xs),
                border: Border.all(
                    width: 1,
                    color: Theme.of(context)
                        .colorScheme
                        .onBackground
                        .withOpacity(0.6))),
        child: widget.icon,
      ),
    );
  }
}
