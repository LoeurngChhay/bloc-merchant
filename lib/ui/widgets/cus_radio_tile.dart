import 'package:flutter/material.dart';

class CusRadioTile extends StatelessWidget {
  final int value;
  final String title;
  final int? groupValue;
  final bool? isSelected;
  final Function(int? value)? onChanged;
  final Function()? onTap;

  const CusRadioTile({
    super.key,
    required this.value,
    required this.title,
    this.groupValue,
    this.isSelected = false,
    this.onTap,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        constraints: const BoxConstraints(maxWidth: 160),
        padding: const EdgeInsets.only(right: 10),
        // margin: const EdgeInsets.only(right: Spacing.s),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          border: Border.all(
              color: isSelected! ? Colors.blue : Colors.grey.shade300,
              width: 1.0),
          color: isSelected! ? Colors.blue.shade100 : Colors.grey.shade100,
        ),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Radio<int>(
                activeColor: Colors.blue,
                visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity),
                value: value,
                groupValue: groupValue,
                onChanged: onChanged),
            Text(title)
          ],
        ),
      ),
    );
  }
}
