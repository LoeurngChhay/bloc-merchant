import 'package:bloc_merchant_mobile_2/generated/locale_keys.g.dart';
import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  final Function()? onRefresh;
  final String? imagePath;
  final String? title;
  final String? message;
  const EmptyScreen(
      {super.key,
      this.onRefresh,
      this.imagePath,
      this.title = 'Empty List',
      this.message});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: [
        const SizedBox(height: 40),
        Image.asset(imagePath ?? "assets/images/bloc_driver.png", height: 250),
        const SizedBox(height: Spacing.normal),
        Text(
          title.toString(),
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Text(
          message ?? LocaleKeys.there_is_no_order_right_now_pleas_wait.tr(),
          style: const TextStyle(
            fontSize: 16.0,
            color: Colors.grey,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: Spacing.normal),
        Visibility(
          visible: onRefresh != null,
          child: SizedBox(
            width: 200,
            height: 45,
            child: ElevatedButton(
                onPressed: onRefresh, child: const Text("Refresh")),
          ),
        )
      ],
    ));
  }
}
