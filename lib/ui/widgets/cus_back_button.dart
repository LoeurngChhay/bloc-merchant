import 'package:flutter/material.dart';

class CusBackButton extends StatelessWidget {
  final Function()? onPressed;
  final Color? color;
  const CusBackButton({super.key, this.onPressed, this.color});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed ??
          () {
            Navigator.pop(context);
          },
      child: Icon(
        Icons.arrow_back_ios,
        color: color,
      ),
    );
  }
}
