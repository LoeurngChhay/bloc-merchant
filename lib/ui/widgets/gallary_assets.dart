import 'dart:io';

import 'package:bloc_merchant_mobile_2/ui/themes/spacing.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/cus_back_button.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/empty_screen.dart';
import 'package:bloc_merchant_mobile_2/ui/widgets/transparent_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:photo_manager_image_provider/photo_manager_image_provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:video_player/video_player.dart';

class AssetDialog extends StatefulWidget {
  final List<CropAspectRatioPreset>? aspects;
  final Future<void> Function(List<File> files, AssetType type)? onNextClick;
  final bool enableMultiple;
  final RequestType type;
  final bool enableCrop;
  final int? limit;

  const AssetDialog({
    super.key,
    this.onNextClick,
    this.aspects,
    this.enableMultiple = false,
    this.type = RequestType.common,
    this.enableCrop = true,
    this.limit,
  });

  @override
  State<AssetDialog> createState() => _AssetDialogState();
}

class _AssetDialogState extends State<AssetDialog> {
  bool _loadingOnNext = false;
  bool _loadingGetAsset = true;
  int _currentImg = 0;
  bool _expandPageView = true;
  String _permissionMessage = "";
  Duration curDuration = Duration.zero;
  AssetType? _selectedType;
  List<AssetPathEntity> _imagesPath = [];
  AssetPathEntity? _selectedPath;
  List<AssetEntity> _images = [];
  Map<String, File> _selectedImages = {};
  late RequestType _filterType = widget.type;

  final pageViewController =
      PageController(viewportFraction: 1, keepPage: true);

  Future<void> _onCropImageClick() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
      sourcePath: _selectedImages.values.elementAt(_currentImg).path,
      compressQuality: 75,
      aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      uiSettings: [
        AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.black,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: true,
          activeControlsWidgetColor: Colors.red,
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
          ],
        ),
        IOSUiSettings(
          title: 'Cropper',
          minimumAspectRatio: 1,
          aspectRatioLockEnabled: true,
          aspectRatioPresets: [
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.square,
          ],
        ),
      ],
    );

    File file = File(croppedFile!.path);
    final key = _selectedImages.entries.elementAt(_currentImg).key;

    setState(() {
      _selectedImages.update(key, (value) => file);
    });
  }

  void _onSelectImgOrVd(int index) async {
    final file = await _images[index].file;

    if (file == null) return;

    if (_selectedImages.containsKey(_images[index].id)) {
      setState(() {
        _selectedImages.remove(_images[index].id);
        if (_selectedImages.isEmpty) {
          _selectedType = null;
        }
      });
      return;
    }

    setState(() {
      _selectedType = _images[index].type;
    });

    if (widget.limit == _selectedImages.length) return;

    if (widget.enableMultiple && _selectedType != AssetType.video) {
      setState(() {
        _selectedImages.addAll({
          _images[index].id: file,
        });
      });
    } else {
      setState(() {
        _selectedImages = {
          _images[index].id: file,
        };
      });
    }
  }

  void _onRemoveImage() {
    setState(() {
      _selectedImages.remove(_selectedImages.keys.elementAt(_currentImg));
      if (_selectedImages.isEmpty) {
        _selectedType = null;
      }
    });
  }

  void getLocalAsset() async {
    final PermissionState ps = await PhotoManager.requestPermissionExtend();

    if (ps.isAuth) {
      try {
        setState(() {
          _loadingGetAsset = true;
        });

        final int count = await PhotoManager.getAssetCount(type: _filterType);

        if (count == 0) {
          setState(() {
            _loadingGetAsset = false;
            _images = [];
          });
        } else {
          final List<AssetEntity> entities =
              await PhotoManager.getAssetListPaged(
            page: 0,
            pageCount: count,
            type: _filterType,
          );

          setState(() {
            _loadingGetAsset = false;
            _images = entities;
          });
        }

        final List<AssetPathEntity> entitiesPath =
            await PhotoManager.getAssetPathList(type: _filterType);

        setState(() {
          _loadingGetAsset = false;
          _imagesPath = entitiesPath;
        });
      } catch (e) {
        setState(() {
          _loadingGetAsset = false;
        });
      }
    } else {
      setState(() {
        _permissionMessage = "Please turn on photo and video permission.";
      });
    }
  }

  void _pageListener() {
    setState(() {
      _currentImg = (pageViewController.page ?? 0).toInt();
    });
  }

  String formatDuration(Duration duration) {
    NumberFormat formatTime = NumberFormat("00");

    String minute = formatTime.format(duration.inMinutes.remainder(60));
    String second = formatTime.format(duration.inSeconds.remainder(60));

    return "$minute:$second";
  }

  @override
  void initState() {
    pageViewController.addListener(_pageListener);
    getLocalAsset();

    super.initState();
  }

  @override
  void dispose() {
    pageViewController.removeListener(_pageListener);
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          // LocaleKeys.gallery.tr(),
          widget.type == RequestType.image ? "Photos" : "Gallery",
        ),
        leading: CusBackButton(onPressed: _onBackClick),
        actions: [
          TextButton(
              onPressed: widget.onNextClick != null &&
                      !_loadingOnNext &&
                      _selectedImages.isNotEmpty &&
                      _selectedType != null
                  ? () async {
                      try {
                        setState(() {
                          _loadingOnNext = true;
                        });
                        await widget.onNextClick!(
                            _selectedImages.values.toList(), _selectedType!);
                      } catch (e) {
                        //
                      } finally {
                        setState(() {
                          _loadingOnNext = false;
                        });
                      }
                    }
                  : null,
              child: _loadingOnNext
                  ? const SizedBox(
                      height: 18,
                      width: 18,
                      child: CircularProgressIndicator(),
                    )
                  : Text("Add",
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: Colors.blue, fontWeight: FontWeight.w600)))

          // Container(
          //   margin: const EdgeInsets.only(right: Spacing.normal),
          //   alignment: Alignment.center,
          //   child: Stack(
          //     children: [
          //       CupertinoButton.filled(
          //           borderRadius: BorderRadius.circular(100),
          //           minSize: 0,
          //           padding: const EdgeInsets.symmetric(
          //               vertical: Spacing.xs, horizontal: Spacing.normal),
          // onPressed: widget.onNextClick != null &&
          //         !_loadingOnNext &&
          //         _selectedImages.isNotEmpty &&
          //         _selectedType != null
          //     ? () async {
          //         try {
          //           setState(() {
          //             _loadingOnNext = true;
          //           });
          //           await widget.onNextClick!(
          //               _selectedImages.values.toList(),
          //               _selectedType!);
          //         } catch (e) {
          //           //
          //         } finally {
          //           setState(() {
          //             _loadingOnNext = false;
          //           });
          //         }
          //       }
          //     : null,
          //           child: const Text(
          //             // LocaleKeys.next.tr(),
          //             "Next",
          //             style: TextStyle(
          //                 // fontFamily: Fonts.kantumruyPro,
          //                 fontWeight: FontWeight.w500),
          //           )),
          //       _loadingOnNext
          //           ? Positioned.fill(
          //               child: Container(
          //               padding: const EdgeInsets.all(Spacing.s),
          //               decoration: BoxDecoration(
          //                   color: Theme.of(context).colorScheme.primary,
          //                   borderRadius: BorderRadius.circular(100)),
          //               child: Center(
          //                   child: SizedBox(
          //                 height: 12,
          //                 width: 12,
          //                 child: CircularProgressIndicator(
          //                     strokeWidth: 2,
          //                     color: Theme.of(context).colorScheme.onPrimary),
          //               )),
          //             ))
          //           : const SizedBox()
          //     ],
          //   ),
          // )
        ],
      ),
      body: Builder(builder: (context) {
        if (_permissionMessage.isNotEmpty) {
          return Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(_permissionMessage),
              const SizedBox(height: Spacing.normal),
              const CupertinoButton(
                  minSize: 0,
                  padding: EdgeInsets.zero,
                  onPressed: PhotoManager.openSetting,
                  child: Text(
                      // LocaleKeys.setting.tr(),
                      "Setting"))
            ],
          ));
        }

        // if (_error != null) {
        //   return Center(
        //     child: FetchError(
        //       errorMessage: _error.toString(),
        //       onRetry: getLocalAsset,
        //     ),
        //   );
        // }

        return Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            _selectedImages.isNotEmpty && _expandPageView
                ? AspectRatio(
                    aspectRatio: 4 / 3,
                    child: GestureDetector(
                      onLongPress: _onContentLongPress,
                      child: Stack(
                        children: [
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: Theme.of(context)
                                  .colorScheme
                                  .onBackground
                                  .withOpacity(0.1),
                            ),
                            child: PageView.builder(
                              controller: pageViewController,
                              itemCount: _selectedImages.length,
                              itemBuilder: (context, index) => Center(
                                child: _selectedType == AssetType.image
                                    ? TransparentImage(
                                        url: _selectedImages.values
                                            .elementAt(index)
                                            .path,
                                        fit: BoxFit.cover,
                                        local: true,
                                      )
                                    : _VideoPreview(
                                        url: _selectedImages.values
                                            .elementAt(index)
                                            .path,
                                        key: Key(_selectedImages.keys
                                            .elementAt(index)),
                                      ),
                              ),
                            ),
                          ),
                          _selectedType == AssetType.image && widget.enableCrop
                              ? Positioned(
                                  left: Spacing.normal,
                                  bottom: Spacing.normal,
                                  child: CupertinoButton(
                                    onPressed: _onCropImageClick,
                                    minSize: 0,
                                    padding: EdgeInsets.zero,
                                    child: Container(
                                      padding: const EdgeInsets.all(Spacing.s),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.black.withOpacity(0.5)),
                                      child: const Icon(
                                        CupertinoIcons.crop,
                                        size: 24,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ))
                              : const SizedBox(),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: Spacing.normal),
                                child: SmoothPageIndicator(
                                    controller: pageViewController,
                                    count: _selectedImages.length,
                                    effect: const ExpandingDotsEffect(
                                      activeDotColor: Colors.blue,
                                      dotHeight: 10,
                                      dotWidth: 10,
                                    )),
                              ))
                        ],
                      ),
                    ),
                  )
                : const SizedBox(),
            if (widget.type == RequestType.common)
              Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.background),
                  child: Stack(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: CupertinoButton(
                              onPressed: _onFilterAlbumClick,
                              // minSize: 0,
                              padding: const EdgeInsets.all(4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(_selectedPath?.name ?? "Recent",
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium),
                                  const SizedBox(width: Spacing.xs),
                                  const Icon(Icons.keyboard_arrow_down_rounded,
                                      size: 18)
                                  // SvgPicture.asset(
                                  //   "assets/svgs/arrow_down.svg",
                                  //   width: 18,
                                  //   colorFilter: ColorFilter.mode(
                                  //       Theme.of(context)
                                  //           .colorScheme
                                  //           .onBackground
                                  //           .withOpacity(0.8),
                                  //       BlendMode.srcIn),
                                  // )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: CupertinoButton(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 4, vertical: 4),
                              onPressed: widget.type != RequestType.common
                                  ? null
                                  : () {
                                      if (_filterType == RequestType.common) {
                                        return;
                                      }
                                      setState(() {
                                        _filterType = RequestType.common;
                                      });
                                      getLocalAsset();
                                    },
                              child: Text("All",
                                  // LocaleKeys.all.tr(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    // fontFamily: Fonts.kantumruyPro,
                                    fontSize: 16,
                                    fontWeight:
                                        _filterType == RequestType.common
                                            ? FontWeight.w600
                                            : FontWeight.w500,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onBackground
                                        .withOpacity(
                                            _filterType == RequestType.common
                                                ? 1
                                                : 0.8),
                                  )),
                            ),
                          ),
                          Expanded(
                            child: CupertinoButton(
                              // minSize: 0,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 4, vertical: 4),
                              onPressed: widget.type == RequestType.common
                                  ? () {
                                      if (_filterType == RequestType.image) {
                                        return;
                                      }

                                      setState(() {
                                        _filterType = RequestType.image;
                                      });
                                      getLocalAsset();
                                    }
                                  : null,
                              child: Text("Photo",
                                  // LocaleKeys.photo.tr(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    // fontFamily: Fonts.kantumruyPro,
                                    fontSize: 16,
                                    fontWeight: _filterType == RequestType.image
                                        ? FontWeight.w600
                                        : FontWeight.w500,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onBackground
                                        .withOpacity(
                                            _filterType == RequestType.image
                                                ? 1
                                                : 0.8),
                                  )),
                            ),
                          ),
                          Expanded(
                            child: CupertinoButton(
                              // minSize: 0,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 4, vertical: 4),
                              onPressed: widget.type == RequestType.common
                                  ? () {
                                      if (_filterType == RequestType.video) {
                                        return;
                                      }
                                      setState(() {
                                        _filterType = RequestType.video;
                                      });
                                      getLocalAsset();
                                    }
                                  : null,
                              child: Text("Video",
                                  // LocaleKeys.video.tr(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    // fontFamily: Fonts.kantumruyPro,
                                    fontSize: 16,
                                    fontWeight: _filterType == RequestType.video
                                        ? FontWeight.w600
                                        : FontWeight.w500,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .onBackground
                                        .withOpacity(
                                            _filterType == RequestType.video
                                                ? 1
                                                : 0.8),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            Expanded(
                child: _loadingGetAsset
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : _images.isEmpty
                        ? Center(
                            child: EmptyScreen(
                                title:
                                    "There's no ${_filterType == RequestType.video ? "video" : _filterType == RequestType.image ? "photo" : "item"} to show"),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context)
                                    .colorScheme
                                    .onBackground
                                    .withOpacity(0.1)),
                            child: GridView.builder(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3,
                                      childAspectRatio: 1,
                                      mainAxisSpacing: 2,
                                      crossAxisSpacing: 2),
                              itemCount: _images.length,
                              itemBuilder: (context, index) {
                                return Stack(
                                  children: [
                                    CupertinoButton(
                                      onPressed: !widget.enableMultiple ||
                                              _selectedType == null ||
                                              _selectedType ==
                                                  _images[index].type
                                          ? () => _onSelectImgOrVd(index)
                                          : null,
                                      minSize: 0,
                                      padding: EdgeInsets.zero,
                                      child: SizedBox.expand(
                                        child: AssetEntityImage(
                                          _images[index],
                                          isOriginal: false,
                                          fit: BoxFit.cover,
                                          thumbnailSize:
                                              const ThumbnailSize.square(400),
                                          thumbnailFormat: ThumbnailFormat.png,
                                        ),
                                      ),
                                    ),
                                    if (widget.limit ==
                                            _selectedImages.length &&
                                        !_selectedImages
                                            .containsKey(_images[index].id))
                                      SizedBox.expand(
                                        child: DecoratedBox(
                                          decoration: BoxDecoration(
                                              color: Colors.white
                                                  .withOpacity(0.6)),
                                        ),
                                      ),
                                    Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Checkbox(
                                          activeColor: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          visualDensity: VisualDensity.compact,
                                          shape: const CircleBorder(),
                                          value: _selectedImages
                                              .containsKey(_images[index].id),
                                          onChanged: !widget.enableMultiple ||
                                                  _selectedType == null ||
                                                  _selectedType ==
                                                      _images[index].type
                                              ? (_) => _onSelectImgOrVd(index)
                                              : (_) {},
                                        )),
                                    _images[index].type == AssetType.video
                                        ? Positioned(
                                            right: 4,
                                            bottom: 4,
                                            child: Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 4),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  color: Colors.black
                                                      .withOpacity(0.6)),
                                              child: Transform.translate(
                                                  offset: const Offset(1, 0),
                                                  child: Text(
                                                    formatDuration(Duration(
                                                        seconds: _images[index]
                                                            .duration)),
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .titleSmall
                                                        ?.copyWith(
                                                            color:
                                                                Colors.white),
                                                  )),
                                            ))
                                        : const SizedBox(),
                                    !(!widget.enableMultiple ||
                                            _selectedType == null ||
                                            _selectedType ==
                                                _images[index].type)
                                        ? Positioned.fill(
                                            child: Container(
                                            color: Theme.of(context)
                                                .colorScheme
                                                .onBackground
                                                .withOpacity(0.5),
                                          ))
                                        : const SizedBox()
                                  ],
                                );
                              },
                            ),
                          )),
          ],
        );
      }),
    );
  }

  void _onFilterAlbumClick() {
    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(Spacing.normal))),
      isScrollControlled: true,
      builder: (context) {
        return ClipRRect(
          borderRadius:
              const BorderRadius.vertical(top: Radius.circular(Spacing.normal)),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.75,
            child: _imagesPath.isEmpty
                ? Center(
                    child: EmptyScreen(
                        title:
                            "There's no album${_filterType != RequestType.common ? " of " : ""}${_filterType == RequestType.video ? "video" : _filterType == RequestType.image ? "photo" : ""} to show"))
                : ListView.separated(
                    itemCount: _imagesPath.length,
                    separatorBuilder: (_, __) => const Divider(
                          height: 0,
                        ),
                    itemBuilder: (context, index) {
                      return CupertinoListTile(
                        onTap: () async {
                          Navigator.pop(context);

                          setState(() {
                            _loadingGetAsset = true;
                            _selectedPath = _imagesPath[index];
                          });

                          final assetCount =
                              await _selectedPath?.assetCountAsync;
                          final assets = await _selectedPath?.getAssetListPaged(
                              page: 0, size: assetCount ?? 0);

                          setState(() {
                            _images = assets ?? [];
                            _loadingGetAsset = false;
                          });
                        },
                        key: Key(_imagesPath[index].id),
                        padding: const EdgeInsets.symmetric(
                            vertical: Spacing.normal,
                            horizontal: Spacing.normal),
                        title: FutureBuilder(
                          future: _imagesPath[index].assetCountAsync,
                          builder: (context, snapshot) {
                            return Text(
                              "${_imagesPath[index].name} (${snapshot.connectionState == ConnectionState.done ? snapshot.data : 0})",
                              style: Theme.of(context).textTheme.bodyMedium,
                            );
                          },
                        ),
                        trailing: SvgPicture.asset(
                          "assets/svgs/arrow_down.svg",
                          width: 20,
                          colorFilter: ColorFilter.mode(
                              Theme.of(context)
                                  .colorScheme
                                  .onBackground
                                  .withOpacity(0.8),
                              BlendMode.srcIn),
                        ),
                      );
                    }),
          ),
        );
      },
    );
  }

  void _onBackClick() {
    if (_selectedImages.isEmpty) {
      Navigator.pop(context);
      return;
    }

    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(Spacing.normal))),
      isScrollControlled: true,
      builder: (context) {
        return SizedBox(
          height: 130 + MediaQuery.of(context).padding.bottom,
          child: Column(children: [
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  // LocaleKeys.discard.tr(),
                  "Discard",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      // fontFamily: Fonts.kantumruyPro,
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).colorScheme.error),
                ),
              ),
            ),
            const Divider(indent: Spacing.s, endIndent: Spacing.s),
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  // LocaleKeys.cancel.tr(),
                  "Cancel",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                        color: Theme.of(context).colorScheme.onBackground,
                      ),
                ),
              ),
            ),
          ]),
        );
      },
    );
  }

  void _onContentLongPress() {
    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius:
              BorderRadius.vertical(top: Radius.circular(Spacing.normal))),
      isScrollControlled: true,
      builder: (context) {
        return SizedBox(
          height: 130 + MediaQuery.of(context).padding.bottom,
          child: Column(children: [
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
                _onRemoveImage();
              },
              child: Text(
                "Remove",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).colorScheme.error),
              ),
            ),
            const Divider(),
            CupertinoButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                // LocaleKeys.cancel.tr(),

                "Cancel",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: Theme.of(context).colorScheme.onBackground,
                    ),
              ),
            ),
          ]),
        );
      },
    );
  }
}

class _VideoPreview extends StatefulWidget {
  final String url;
  const _VideoPreview({
    super.key,
    required this.url,
  });

  @override
  State<_VideoPreview> createState() => _VideoPreviewState();
}

class _VideoPreviewState extends State<_VideoPreview> {
  late VideoPlayerController _videoController;

  @override
  void initState() {
    _videoController = VideoPlayerController.file(File(widget.url),
        videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true));

    _videoController.initialize().then((_) {
      setState(() {});
    });

    super.initState();
  }

  @override
  void dispose() {
    _videoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Stack(
        children: [
          AspectRatio(
            aspectRatio: _videoController.value.aspectRatio,
            child: DecoratedBox(
              decoration: BoxDecoration(
                  color: Theme.of(context)
                      .colorScheme
                      .onBackground
                      .withOpacity(0.1)),
              child: _videoController.value.isInitialized
                  ? VideoPlayer(_videoController)
                  : const SizedBox(),
            ),
          ),
          Positioned.fill(
            child: AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: _videoController.value.isInitialized ? 1 : 0,
              child: Center(
                child: Container(
                  padding: const EdgeInsets.all(Spacing.normal),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white.withOpacity(0.3)),
                      color: Colors.black.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(100)),
                  child: Transform.translate(
                    offset: const Offset(2, 0),
                    child: const Icon(
                      CupertinoIcons.play_fill,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
