import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget {
  const AppHeader(
      {super.key,
      this.title,
      this.subtitle,
      this.showBackBtn = true,
      this.isTransparent = false,
      this.onBack,
      this.trailing,
      this.backBtnSemantics});
  final String? title;
  final String? subtitle;
  final bool showBackBtn;
  final String? backBtnSemantics;
  final bool isTransparent;
  final VoidCallback? onBack;
  final Widget Function(BuildContext context)? trailing;

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: isTransparent ? Colors.transparent : Colors.black,
      child: SafeArea(
        bottom: false,
        child: SizedBox(
          height: 64 * AppStyle(screenSize: MediaQuery.of(context).size).scale,
          child: Stack(
            children: [
              MergeSemantics(
                child: Semantics(
                  header: true,
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (title != null)
                          Text(
                            title!.toUpperCase(),
                            textHeightBehavior: const TextHeightBehavior(
                                applyHeightToFirstAscent: false),
                            style: const TextStyle(fontWeight: FontWeight.w500),
                          ),
                        if (subtitle != null)
                          Text(
                            subtitle!.toUpperCase(),
                            textHeightBehavior: const TextHeightBehavior(
                                applyHeightToFirstAscent: false),
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(color: Colors.accents[1]),
                          ),
                      ],
                    ),
                  ),
                ),
              ),
              // Positioned.fill(
              //   child: Center(
              //     child: Row(children: [
              //       Gap($styles.insets.sm),
              //       if (showBackBtn) const CusBackButton(),
              //       Spacer(),
              //       if (trailing != null) trailing!.call(context),
              //       Gap($styles.insets.sm),
              //       //if (showBackBtn) Container(width: $styles.insets.lg * 2, alignment: Alignment.centerLeft, child: child),
              //     ]),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

@immutable
class AppStyle {
  AppStyle({Size? screenSize}) {
    if (screenSize == null) {
      scale = 1;
      return;
    }
    final shortestSide = screenSize.shortestSide;
    const tabletXl = 1000;
    const tabletLg = 800;
    if (shortestSide > tabletXl) {
      scale = 1.2;
    } else if (shortestSide > tabletLg) {
      scale = 1.1;
    } else {
      scale = 1;
    }
  }

  late final double scale;
}
