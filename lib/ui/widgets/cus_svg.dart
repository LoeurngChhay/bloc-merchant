import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CusSvg extends StatelessWidget {
  final String pathName;
  final Color? color;
  final double? size;
  const CusSvg({super.key, required this.pathName, this.color, this.size = 24});

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      pathName,
      width: size,
      height: size,
      colorFilter: ColorFilter.mode(
          color ?? Theme.of(context).colorScheme.onBackground, BlendMode.srcIn),
    );
  }
}
