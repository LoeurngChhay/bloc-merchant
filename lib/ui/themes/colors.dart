import 'package:flutter/material.dart';

const Color lightPrimaryColor = Color(0xFF83CCFF);
const Color primaryColor = Color(0xFF04B0FF);
const Color darkPrimaryColor = Color(0xFF00344F);
const Color midightColor = Color.fromRGBO(26, 25, 31, 1);
// const Color primaryColor = Color.fromRGBO(54, 187, 247, 1);

const Color secondaryColor = Color(0xFFFFF0F0);
const Color redPrimary = Color(0xFFFF3A23);
const Color primaryTextColor = Color.fromRGBO(182, 186, 195, 1);
