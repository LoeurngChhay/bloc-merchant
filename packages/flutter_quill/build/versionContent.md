* Fix Multiline paste with attributes and embeds by @AtlasAutocode in https://github.com/singerdmx/flutter-quill/pull/2074


**Full Changelog**: https://github.com/singerdmx/flutter-quill/compare/v10.1.1...v10.1.2