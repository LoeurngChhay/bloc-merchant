import 'quill_localizations.dart';

/// The translations for Khmer (`km`).
class FlutterQuillLocalizationsKm extends FlutterQuillLocalizations {
  FlutterQuillLocalizationsKm([super.locale = 'km']);

  @override
  String get pasteLink => 'បិទតំណ';

  @override
  String get ok => 'អស់សំណើ';

  @override
  String get selectColor => 'ជ្រើសរើសពណ៌';

  @override
  String get gallery => 'សួនច្បារ';

  @override
  String get link => 'តំណ';

  @override
  String get open => 'បើក';

  @override
  String get copy => 'ចម្លង';

  @override
  String get remove => 'លុប';

  @override
  String get save => 'រក្សាទុក';

  @override
  String get zoom => 'ពង្រីក';

  @override
  String get saved => 'បានរក្សាទុក';

  @override
  String get text => 'អត្ថបទ';

  @override
  String get resize => 'ប្ដូរទំហំ';

  @override
  String get width => 'ទទឹង';

  @override
  String get height => 'កម្រិត';

  @override
  String get size => 'ទំហំ';

  @override
  String get small => 'តូច';

  @override
  String get large => 'ធំ';

  @override
  String get huge => 'ធំជាង';

  @override
  String get clear => 'សម្អាត';

  @override
  String get font => 'អក្សរ';

  @override
  String get search => 'ស្វែងរក';

  @override
  String get camera => 'កាមេរ៉ា';

  @override
  String get video => 'វីដេអូ';

  @override
  String get undo => 'ត្រឡប់';

  @override
  String get redo => 'ធ្វើឡើងវិញ';

  @override
  String get fontFamily => 'គ្រួសារអក្សរ';

  @override
  String get fontSize => 'ទំហំអក្សរ';

  @override
  String get bold => 'ខ្លាំង';

  @override
  String get subscript => 'អក្សរខ្ចី';

  @override
  String get superscript => 'អក្សរលើ';

  @override
  String get italic => 'បង្ហាញ';

  @override
  String get underline => 'ដាក់ស្នាមក្រោម';

  @override
  String get strikeThrough => 'គូសកាត់';

  @override
  String get inlineCode => 'កូដខាងក្នុង';

  @override
  String get fontColor => 'ពណ៌អក្សរ';

  @override
  String get backgroundColor => 'ពណ៌ផ្ទៃខាងក្រោយ';

  @override
  String get clearFormat => 'សម្អាតទ្រង់ទ្រាយ';

  @override
  String get alignLeft => 'តម្រៀបឆ្វេង';

  @override
  String get alignCenter => 'តម្រៀបកណ្តាល';

  @override
  String get alignRight => 'តម្រៀបស្តាំ';

  @override
  String get alignJustify => 'Align justify';

  @override
  String get justifyWinWidth => 'តម្រៀបជួយទទឹងបញ្ជូន';

  @override
  String get textDirection => 'ទិសដៅអត្ថបទ';

  @override
  String get headerStyle => 'ស្តាយស្បែក';

  @override
  String get normal => 'ធម្មតា';

  @override
  String get heading1 => 'ចំណងជើង 1';

  @override
  String get heading2 => 'ចំណងជើង 2';

  @override
  String get heading3 => 'ចំណងជើង 3';

  @override
  String get heading4 => 'ចំណងជើង 4';

  @override
  String get heading5 => 'ចំណងជើង 5';

  @override
  String get heading6 => 'ចំណងជើង 6';

  @override
  String get numberedList => 'បញ្ជីលេខ';

  @override
  String get bulletList => 'បញ្ជីចំណុច';

  @override
  String get checkedList => 'បញ្ជីបានពិនិត្យ';

  @override
  String get codeBlock => 'ប្លុកកូដ';

  @override
  String get quote => 'ចម្លង';

  @override
  String get increaseIndent => 'បង្កើនកន្លែង';

  @override
  String get decreaseIndent => 'កាត់បន្ថយកន្លែង';

  @override
  String get insertURL => 'បញ្ចូលURL';

  @override
  String get visitLink => 'ចូលទៅកាន់តំណ';

  @override
  String get enterLink => 'បញ្ចូលតំណ';

  @override
  String get enterMedia => 'បញ្ចូលមេឌា';

  @override
  String get edit => 'កែសម្រួល';

  @override
  String get apply => 'អនុវត្ត';

  @override
  String get hex => 'ហិច';

  @override
  String get material => 'សម្ភារៈ';

  @override
  String get color => 'ពណ៌';

  @override
  String get lineheight => 'កម្ពស់ជួរ';

  @override
  String get findText => 'ស្វែងរកអត្ថបទ';

  @override
  String get moveToPreviousOccurrence => 'ផ្លាស់ទីទៅកាន់ការកើតឡើងមុន';

  @override
  String get moveToNextOccurrence => 'ផ្លាស់ទីទៅកាន់ការកើតឡើងក្រោយ';

  @override
  String get savedUsingTheNetwork => 'បានរក្សាទុកតាមបណ្តាញ';

  @override
  String get savedUsingLocalStorage => 'បានរក្សាទុកតាមការផ្ទុកក្នុងស្រុរ';

  @override
  String theImageHasBeenSavedAt(String imagePath) {
    return 'រូបភាពបានរក្សាទុកនៅទីនេះ: $imagePath';
  }

  @override
  String get errorWhileSavingImage => 'មានកំហុសក្នុងការរក្សាទុករូបភាព';

  @override
  String get pleaseEnterTextForYourLink =>
      "សូមបញ្ចូលអត្ថបទសម្រាប់តំណរបស់អ្នក (ឧ. 'អានបន្ថែម')";

  @override
  String get pleaseEnterTheLinkURL =>
      "សូមបញ្ចូលURL តំណ (ឧ. 'https://example.com')";

  @override
  String get pleaseEnterAValidImageURL => 'សូមបញ្ចូលURL រូបភាពដែលត្រឹមត្រូវ';

  @override
  String get pleaseEnterAValidVideoURL => 'សូមបញ្ចូលURL វីដេអូដែលត្រឹមត្រូវ';

  @override
  String get photo => 'រូបថត';

  @override
  String get image => 'រូបភាព';

  @override
  String get caseSensitivityAndWholeWordSearch =>
      'ការជឿជាក់ស្តែងនិងការស្វែងរកពេញ';

  @override
  String get caseSensitive => 'Case sensitive';

  @override
  String get wholeWord => 'Whole word';

  @override
  String get insertImage => 'បញ្ចូលរូបភាព';

  @override
  String get pickAPhotoFromYourGallery => 'ជ្រើសរូបភាពពីសួនច្បារ​របស់អ្នក';

  @override
  String get takeAPhotoUsingYourCamera => 'យករូបភាពប្រើកាមេរ៉ារបស់អ្នក';

  @override
  String get pasteAPhotoUsingALink => 'បិទរូបភាពប្រើតំណ';

  @override
  String get pickAVideoFromYourGallery => 'ជ្រើសវីដេអូពីសួនច្បារ​របស់អ្នក';

  @override
  String get recordAVideoUsingYourCamera => 'កត់វីដេអូប្រើកាមេរ៉ារបស់អ្នក';

  @override
  String get pasteAVideoUsingALink => 'បិទវីដេអូប្រើតំណ';

  @override
  String get close => 'Close';

  @override
  String get searchSettings => 'Search settings';

  @override
  String get cut => 'Cut';

  @override
  String get paste => 'Paste';

  @override
  String get insertTable => 'Insert table';
}
